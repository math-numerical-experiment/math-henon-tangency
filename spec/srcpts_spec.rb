require_relative 'spec_helper'

describe InvMfd::SrcPts, "when run output" do
  before(:all) do
    @source = [0, 1, 2, 3, 4, 5]
    @obj = InvMfd::SrcPts.new(@source)
  end

  it "should output all elements" do
    string = ''
    @obj.output(0, @obj.size, string)
    string.should == (@source.map { |a| a.to_s }).join("\n") + "\n"
  end
  
  it "should output data according to index" do
    string = ''
    @obj.output(2, 3, string)
    string.should == "2\n3\n4\n"
  end

  it "should output data according to index" do
    string = ''
    @obj.output(3, 5, string)
    string.should == "3\n4\n5\n"
  end

  it "should evaluate block with each element" do
    @obj.each_with_index { |pt, i| pt.should == @source[i] }
  end

  it "shoulde be converted to array" do
    @obj.to_a.should == @source
  end

end

describe InvMfd::SrcPts, "when deleting elements" do
  before(:each) do
    @source = [0, 1, 2, 3, 4, 5]
    @size = @source.size
    @obj = InvMfd::SrcPts.new(@source)
  end

  it "should delete all elements" do
    @obj.delete_points(0, @obj.size)
    @obj.size.should == 0
  end

  it "should delete an element with index" do
    @obj.delete_points(1)
    @obj.size.should == @size - 1
    @obj[1].should == 2
    @obj.delete_points(3)
    @obj.size.should == @size - 2
    @obj[3].should == 5
  end

  it "should delete some elements with index" do
    @obj.delete_points(0, 3)
    @obj.size.should == @size - 3
    @obj[0].should == 3
    @obj.delete_points(2, 8)
    @obj[0].should == 3
    @obj[1].should == 4
  end

end

describe InvMfd::SrcPts, "when splitting" do
  before(:each) do
    @source = [0, 1, 2, 3, 4, 5]
    @obj = InvMfd::SrcPts.new(@source)
  end

  it "should return splitted objects" do
    ary = @obj.split_at(1, 3)
    ary.size.should == 3
    ary.each { |a| a.class.should == InvMfd::SrcPts }
    ary[0].data.should == [0]
    ary[1].data.should == [1, 2]
    ary[2].data.should == [3, 4, 5]
  end

  it "should return splitted objects" do
    ary = @obj.split_at(3, 5, 8)
    ary.size.should == 3
    ary.each { |a| a.class.should == InvMfd::SrcPts }
    ary[0].data.should == [0, 1, 2]
    ary[1].data.should == [3, 4]
    ary[2].data.should == [5]
  end

  it "should raise error" do
    lambda { @obj.split_at(0, 2, 3) }.should raise_error
    lambda { @obj.split_at(2, 2, 5) }.should raise_error
  end
  
end

