require_relative 'spec_helper'

module InvMfd::Calculator
  [:check_argument, :get_arithmetic, :some_variables_for_saddle_point].each { |m| public_class_method m }
end

describe InvMfd::Calculator, "When we call InvMfd::Calculator#check_argument" do
  before(:all) do
    @valids =  [[['1.4', '-0.3'], 1, 3], [['1.3', '-0.3'], 1, 0], [['1.35', '-0.3'], 1, 2]]
    @invalids = [[[1.4, '-0.3'], 1, 3], [['1.3', '-0.3'], -1, 0], [['1.35', '-0.3'], 1, 7]]
  end
  
  it "should raise error" do
    @invalids.each { |args| lambda { InvMfd::Calculator.check_argument(*args) }.should raise_error }
  end

  it "should not raise error" do
    @valids.each { |args| lambda { InvMfd::Calculator.check_argument(*args) }.should_not raise_error }
  end
  
end

describe InvMfd::Calculator, "When creating calculator" do

  before(:all) do
    MPFR.set_default_prec(256)
    csts = [['1.4', '-0.3']]
    @args = []
    csts.each do |c|
      henon_fr = MPFR::Henon::Mapping.new(c)
      pt = henon_fr.fixed_point[0]
      vals, basis = henon_fr.jacobian_matrix(pt).eigenvector
      @args << { :const => c, :period => 1, :pt => pt, :basis => basis, :mode => 0 }

      henon_fi = MPFI::Henon::MapN.new(c)
      pt = henon_fi.fixed_point[0]
      vals, basis = henon_fi.jacobian_matrix(pt).eigenvector
      @args << { :const => c, :period => 1, :pt => pt, :basis => basis, :mode => 0 }
    end
  end
  
  it "should return valid calculator" do
    @args.each do |a|
      if cal = InvMfd::Calculator.create(a[:const], a[:period], a[:pt], a[:mode])
        cal.class.to_s.should =~ /^#{a[:pt].class.to_s.sub(/::.*$/, "")}/
      end
    end
  end
  
end

describe InvMfd::Calculator, "when creating calculator by InvMfd#create_for_fp" do
  before(:all) do
    MPFR.set_default_prec(256)
    csts = [['1.4', '-0.3']]
    @args = []
    csts.each do |c|
      [MPFR, MPFI].each { |ari| (0...8).each { |mode| @args << [c, mode, ari] } }
    end
  end

  it "should return valid calculator" do
    @args.each do |a|
      if cal = InvMfd::Calculator.create_for_fp(*a)
        cal.class.to_s.should =~ /^#{a[2]}/
      end
    end
  end
  
end
