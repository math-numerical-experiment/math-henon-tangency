require_relative "spec_helper"

describe TransientTime do

  it "should return transient time." do
    MPFR.set_default_prec(256)

    period = 12
    prms = ['1.39120000000000001474e+00', '-3.00359999999999989842e-01'].map { |s| MPFR(s) }
    sink = MPFR::ColumnVector(['5.84496796076749649108e-01', '1.95952587583781711312e-01'])
    error = MPFR('1e-10')
    one_step_iterate = 10

    ttime = TransientTime.new(100000, one_step_iterate, prms, sink, period)
    p ttime.calc(MPFR::ColumnVector([0, 0]))
    p ttime.calc_old(MPFR::ColumnVector([0, 0]))

    ttime = TransientTime.new(100000, one_step_iterate, prms, sink, period)
    p ttime.calc(MPFR::ColumnVector([0.3, 0]))
    p ttime.calc_old(MPFR::ColumnVector([0.3, 0]))

    ttime = TransientTime.new(100000, one_step_iterate, prms, sink, period)
    p ttime.calc(MPFR::ColumnVector([-0.3, -0.02]))
    p ttime.calc_old(MPFR::ColumnVector([-0.3, -0.02]))
  end
end
