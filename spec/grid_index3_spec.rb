require_relative 'spec_helper'

describe Grid::IndexSet, "when changing Grid::Coordinate" do
  it "should not raise error. " do
    old_grid = Grid::Coordinate.new(MPFR::ColumnVector.new([1, 1]), MPFR.new('0.5'))
    new_grid = Grid::Coordinate.new(MPFR::ColumnVector.new([0, 1]), MPFR.new('0.05'))
    
    old_index = Grid::IndexSet.new({ :x => [-2, 1], :y => [0, 1] })
    new_index = new_grid.change_grid(old_grid, old_index)
  end
end

describe Grid::Coordinate, "when getting IndexSet including a point" do
  grid = Grid::Coordinate.new(MPFR::ColumnVector.new([1, -1]), MPFR.new('0.5'))
  [[1, -1], [2, 0], [-1, -1], [-10.2, 0.4], [8.3, 9.2]].each do |ary|
    it "should return IndexSet." do
      pt = MPFR::ColumnVector.new(ary)
      grid.index_include_pt(pt).should == grid.enclosure_index(MPFI::ColumnVector.new([pt[0], pt[1]]))
    end
  end
end

describe Grid::IndexSet do
  context "when dumping and loading" do
    before(:all) do
      @path = File.expand_path(File.dirname(__FILE__) + '/tmp_index_set.dat')
      @path_gz = @path + '.gz'
    end

    it "should get same index set" do
      index_set = Grid::IndexSet.new({ :x => [-3, 3], :y => [0, 2] },
                                     { :x => [8, 9], :y => [-10, -7] },
                                     { :x => [7, 10], :y => [2, 3] })
      index_set.dump_to_file(@path).should == @path
      index_set.dump_to_file(@path, true).should == @path_gz
      Grid::IndexSet.load_from_file(@path).should == index_set
      Grid::IndexSet.load_from_file(@path_gz).should == index_set
    end

    after(:all) do
      FileUtils.rm(@path)
      FileUtils.rm(@path_gz)
    end
  end
end
