require_relative 'spec_helper.rb'

describe Henon::BasinWidth do
  before(:all) do
    MPFR.set_default_prec(256)
    ary = ["-1.275537127677873803576197262950650941118245527924620148668280089012148211864496",
           "0.3817100261584850801456271121191026879296628940813819023081009791489895997561977"]
    @sink = MPFR::ColumnVector(ary)
    @henon = MPFR::Henon::Mapping.new(["1.39270603828125", '-0.3'], 13)
    @converge = Henon::ConvergeToBasin.new(@henon, @sink, 500, MPFR('1e-30'))
    @basin_width = Henon::BasinWidth.new(@sink, @converge)
  end

  it "should get grid size" do
    size = @basin_width.calc
    size.should be_an_instance_of MPFR
  end

  it "should sketch basin" do
    path = nil
    lambda { path = @basin_width.sketch_basin(:range => 10) }.should_not raise_error
    File.exist?(path).should be_true
  end

  after(:all) do
    FileUtils.rm_r(File.dirname(@basin_width.cache_db))
  end

end
