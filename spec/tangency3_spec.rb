require_relative 'spec_helper'

describe Intersect, 'When executing module functions' do
  before(:all) do
    MPFR.set_default_prec(256)
    ManageLog.set_log_conf(Intersect::Determine, :level => Logger::DEBUG, :io => STDOUT)
  end
  
  it "should get intersections with straight line" do
    const = ['1.39270605', '-0.3']
    henon = MPFR::Henon::Mapping.new(const)
    
    st_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 1, MPFR), henon, -15, [MPFR.new('0.9469'), MPFR.new('0.9496')])
    un_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 2, MPFR), henon, 38, [MPFR.new('0.1893'), MPFR.new('0.2213')])

    calc = Intersect::Determine.new(MPFR.new('1e-8'), MPFR.new('1e-3'))
    intersections = calc.search_intersections(st_mfd, un_mfd)

    mid = intersections[0].crd.midpoint(intersections[1].crd)
    vec = intersections[0].crd - intersections[1].crd
    nvec = MPFR::Vector2D.normal_vector(vec)
    nvec = MPFR::ColumnVector.new([-nvec[0], -nvec[1]]) if nvec[0] > 0

    tmp = mid + nvec.mul_scalar(MPFR.new('1e-5'))
    line = MappedMfd::StraightLine.new(tmp + vec, tmp - vec)

    un_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 2, MPFR), henon, 38, [MPFR.new('0.1893'), MPFR.new('0.2213')])
    res = calc.search_intersections(un_mfd, line)
    res.size.should == 2
  end

end

