require_relative 'spec_helper'

class Grid::IndexSet
  attr_accessor :index
end

describe Grid::IndexSet, "when creating union with rectangle" do
  it "should be initialized" do
    new_index = Grid::IndexSet.new({ :x => [-2, -1], :y => [2, 4] })
    new_index.valid_data?.should be_true
    new_index.index.size.should == 2
    new_index.index.assoc(-2)[1].should == [2, 4]
    new_index.index.assoc(-1)[1].should == [2, 4]

    new_index = Grid::IndexSet.new
    new_index.valid_data?.should be_true
    new_index.index.size.should == 0
    new_index.union!(:x => [-2, -1], :y => [2, 4])
    new_index.index.size.should == 2
    new_index.index.assoc(-2)[1].should == [2, 4]
    new_index.index.assoc(-1)[1].should == [2, 4]
  end

  it "should make valid union with rectangle with respect to y-coordinate" do
    new_index = Grid::IndexSet.new({ :x => 0, :y => [1, 6] })
    res = new_index.union(:x => 0, :y => [-1, 0])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [-1, 6]
    new_index.index.assoc(0)[1].should == [1, 6]
    
    res = new_index.union(:x => 0, :y => [0, 2])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [0, 6]

    res = new_index.union(:x => 0, :y => [3, 7])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 7]

    res = new_index.union(:x => 0, :y => [8, 9])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 6, 8, 9]

    res = new_index.union(:x => 0, :y => [-1, 1])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [-1, 6]

    res = new_index.union(:x => 0, :y => [6, 8])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 8]

    res = new_index.union(:x => 0, :y => [2, 4])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 6]

    res = new_index.union(:x => 0, :y => [-10, 10])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [-10, 10]

    res = new_index.union(:x => 0, :y => 0)
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [0, 6]

    res = new_index.union(:x => 0, :y => 3)
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 6]

    res = new_index.union(:x => 0, :y => 8)
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 6, 8, 8]
  end

  it "should make valid union with respect to y-coordinate" do
    new_index = Grid::IndexSet.new({ :x => 0, :y => 3 })
    res = new_index.union(:x => 0, :y => 4)
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [3, 4]

    res = new_index.union(:x => 0, :y => [3, 4])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [3, 4]

    res = new_index.union(:x => 0, :y => [0, 3])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [0, 3]

    res = new_index.union(:x => 0, :y => 3)
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [3, 3]
  end
  
  it "should make valid union with respect to x-coordinate" do
    new_index = Grid::IndexSet.new({ :x => [1, 5], :y => [2, 3] })
    res = new_index.union(:x => -1, :y => [2, 3])
    res.valid_data?.should be_true
    res.index.size.should == 6
    ([-1] + (1..5).to_a).each { |i| res.index.assoc(i)[1].should == [2, 3] }
    
    res = new_index.union(:x => [0, 1], :y => 2)
    res.valid_data?.should be_true
    res.index.size.should == 6
    res.index.assoc(0)[1].should == [2, 2]
    (1..5).each { |i| res.index.assoc(i)[1].should == [2, 3] }

    res = new_index.union(:x => [3, 8], :y => [-3, -2])
    res.valid_data?.should be_true
    res.index.size.should == 8
    (1..2).each { |i| res.index.assoc(i)[1].should == [2, 3] }
    (3..5).each { |i| res.index.assoc(i)[1].should == [-3, -2, 2, 3] }
    (6..8).each { |i| res.index.assoc(i)[1].should == [-3, -2] }
  end
  
  it "should make valid union with rectangle" do
    new_index = Grid::IndexSet.new({ :x => [0, 3], :y => [-2, -1] })
    new_index.valid_data?.should be_true
    new_index.index.size.should == 4
    (0..3).each { |i| new_index.index.assoc(i)[1].should == [-2, -1] }

    new_index.union!(:x => [-1, 1], :y => [2, 3])
    new_index.valid_data?.should be_true
    new_index.index.assoc(-1)[1].should == [2, 3]
    new_index.index.assoc(0)[1].should == [-2, -1, 2, 3]
    new_index.index.assoc(1)[1].should == [-2, -1, 2, 3]
    new_index.index.assoc(2)[1].should == [-2, -1]
    new_index.index.assoc(3)[1].should == [-2, -1]

    new_index.union!(:x => [1, 2], :y => [-1, 2])
    new_index.valid_data?.should be_true
    new_index.index.assoc(-1)[1].should == [2, 3]
    new_index.index.assoc(0)[1].should == [-2, -1, 2, 3]
    new_index.index.assoc(1)[1].should == [-2, 3]
    new_index.index.assoc(2)[1].should == [-2, 2]
    new_index.index.assoc(3)[1].should == [-2, -1]
  end

  it "should make valid union with other index set" do
    new_index = Grid::IndexSet.new({ :x => [-1, 3], :y => [2, 4] })
    new_index.valid_data?.should be_true

    new_index.union!(Grid::IndexSet.new({ :x => [-2, -1], :y => [0, 1] }))
    new_index.valid_data?.should be_true
    new_index.index.assoc(-2)[1].should == [0, 1]
    new_index.index.assoc(-1)[1].should == [0, 4]
    (0..3).each { |i| new_index.index.assoc(i)[1].should == [2, 4] }

    new_index.union!(Grid::IndexSet.new({ :x => 2, :y => [-3, 7] }))
    new_index.index.assoc(-2)[1].should == [0, 1]
    new_index.index.assoc(-1)[1].should == [0, 4]
    new_index.index.assoc(2)[1].should == [-3, 7]
    [0, 1, 3].each { |i| new_index.index.assoc(i)[1].should == [2, 4] }

    new_index.union!(Grid::IndexSet.new({ :x => [0, 4], :y => 8 }))
    new_index.index.assoc(-2)[1].should == [0, 1]
    new_index.index.assoc(-1)[1].should == [0, 4]
    new_index.index.assoc(2)[1].should == [-3, 8]
    new_index.index.assoc(4)[1].should == [8, 8]
    [0, 1, 3].each { |i| new_index.index.assoc(i)[1].should == [2, 4, 8, 8] }
  end
  
end

describe Grid::IndexSet, "when creating intersection with rectangle" do
  it "should make valid intersection with rectangle" do
    new_index = Grid::IndexSet.new({ :x => 0, :y => [1, 6] })
    res = new_index.intersection(:x => 0, :y => [-1, 0])
    res.valid_data?.should be_true
    res.empty?.should be_true
    
    res = new_index.intersection(:x => 0, :y => [0, 2])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 2]

    res = new_index.intersection(:x => 0, :y => [3, 7])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [3, 6]

    res = new_index.intersection(:x => 0, :y => [8, 9])
    res.valid_data?.should be_true
    res.empty?.should be_true

    res = new_index.intersection(:x => 0, :y => [-1, 1])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 1]

    res = new_index.intersection(:x => 0, :y => [6, 8])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [6, 6]

    res = new_index.intersection(:x => 0, :y => [2, 4])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [2, 4]

    res = new_index.intersection(:x => 0, :y => [-10, 10])
    res.valid_data?.should be_true
    res.index.assoc(0)[1].should == [1, 6]
  end
  
  it "should make valid intersection with rectangle" do
    new_index = Grid::IndexSet.new({ :x => [0, 3], :y => [-2, -1] }, { :x => [2, 5], :y => [1, 4]}, { :x => [-1, 3], :y => [3, 6] })
    new_index.valid_data?.should be_true
    new_index.index.size.should == 7
    new_index.index.assoc(-1)[1].should == [3, 6]
    (0..1).each { |i| new_index.index.assoc(i)[1].should == [-2, -1, 3, 6] }
    (2..3).each { |i| new_index.index.assoc(i)[1].should == [-2, -1, 1, 6] }
    (4..5).each { |i| new_index.index.assoc(i)[1].should == [1, 4] }

    res = new_index.intersection(:x => [-1, 4], :y => [-1, 4])
    res.valid_data?.should be_true
    res.index.size.should == 6
    res.index.assoc(-1)[1].should == [3, 4]
    (0..1).each { |i| res.index.assoc(i)[1].should == [-1, -1, 3, 4] }
    (2..3).each { |i| res.index.assoc(i)[1].should == [-1, -1, 1, 4] }
    res.index.assoc(4)[1].should == [1, 4]

    res = new_index.intersection(:x => [0, 1], :y => [0, 1])
    res.valid_data?.should be_true
    res.empty?.should be_true
  end

  it "should make valid intersection with other index set" do
    new_index = Grid::IndexSet.new({ :x => [0, 2], :y => [0, 3] }, { :x => [4, 5], :y => [3, 8] })
    new_index.valid_data?.should be_true

    new_index.intersection!(Grid::IndexSet.new({ :x => [-1, 6], :y => [2, 4] }))
    new_index.index.size.should == 5
    (0..2).each { |i| new_index.index.assoc(i)[1].should == [2, 3] }
    (4..5).each { |i| new_index.index.assoc(i)[1].should == [3, 4] }

    new_index.intersection!(Grid::IndexSet.new({ :x => 1, :y => 2 }, { :x => 5, :y => 4 }))
    new_index.index.size.should == 2
    new_index.index.assoc(1)[1].should == [2, 2]
    new_index.index.assoc(5)[1].should == [4, 4]
  end
  
end

describe Grid::IndexSet, "when subtracting" do
  it "should return subtraction" do
    new_index = Grid::IndexSet.new({ :x => 0, :y => [2, 8] })
    rectangle = { :x => 0, :y => [1, 4] }
    res = new_index.minus(rectangle)
    res.index.size.should == 1
    res.index.assoc(0)[1].should == [5, 8]
    res.intersection(rectangle).empty?.should be_true

    rectangle = { :x => 0, :y => [3, 4] }
    res = new_index.minus(rectangle)
    res.index.size.should == 1
    res.index.assoc(0)[1].should == [2, 2, 5, 8]
    res.intersection(rectangle).empty?.should be_true

    rectangle = { :x => 0, :y => [4, 9] }
    res = new_index.minus(rectangle)
    res.index.size.should == 1
    res.index.assoc(0)[1].should == [2, 3]
    res.intersection(rectangle).empty?.should be_true

    res = new_index.minus(:x => 0, :y => [1, 9])
    res.empty?.should be_true

    rectangle = { :x => [2, 3], :y => [1, 9] }
    res = new_index.minus(rectangle)
    res.index.size.should == 1
    res.index.assoc(0)[1].should == [2, 8]
    res.intersection(rectangle).empty?.should be_true
  end
end

describe Grid::IndexSet, "when wrapping" do
  it "should return wrapping" do
    new_index = Grid::IndexSet.new({ :x => 0, :y => 0 })
    res = new_index.wrap
    res.index.size.should == 3
    res.index.assoc(-1)[1].should == [-1, 1]
    res.index.assoc(0)[1].should == [-1, 1]
    res.index.assoc(1)[1].should == [-1, 1]

    new_index = Grid::IndexSet.new({ :x => 0, :y => [0, 1] }, { :x => [3, 4], :y => [-1, 0] })
    res = new_index.wrap
    res.index.size.should == 7
    (-1..1).each { |i| res.index.assoc(i)[1].should == [-1, 2] }
    (2..5).each { |i| res.index.assoc(i)[1].should == [-2, 1] }
  end
end

describe Grid::IndexSet, "when creating collar" do
  it "should return collar" do
    new_index = Grid::IndexSet.new({ :x => 0, :y => 0 })
    res = new_index.collar
    res.index.size.should == 3
    res.index.assoc(-1)[1].should == [-1, 1]
    res.index.assoc(0)[1].should == [-1, -1, 1, 1]
    res.index.assoc(1)[1].should == [-1, 1]

    new_index = Grid::IndexSet.new({ :x => 0, :y => [0, 1] }, { :x => [3, 4], :y => [-1, 0] })
    res = new_index.collar
    res.index.size.should == 7
    [-1, 1].each { |i| res.index.assoc(i)[1].should == [-1, 2] }
    res.index.assoc(0)[1].should == [-1, -1, 2, 2]
    [2, 5].each { |i| res.index.assoc(i)[1].should == [-2, 1] }
    (3..4).each { |i| res.index.assoc(i)[1].should == [-2, -2, 1, 1] }
  end
end


describe Grid::IndexSet, "when executing some methods" do
  it "should return size" do
    new_index = Grid::IndexSet.new({ :x => [1, 3], :y => [2, 4] })
    new_index.size.should == 9

    new_index = Grid::IndexSet.new({ :x => [1, 3], :y => 2 }, { :x => 0, :y => [-1, 4]}, { :x => 3, :y => [0, 6] })
    new_index.size.should == 15
  end

  it "should tell empty set from not empty set." do
    new_index = Grid::IndexSet.new({ :x => [1, 3], :y => [2, 4] })
    new_index.empty?.should_not be_true

    new_index = Grid::IndexSet.new
    new_index.empty?.should be_true

    new_index.union!({ :x => 0, :y => 0})
    new_index.empty?.should_not be_true
  end

  it "should include other set" do
    index1 = Grid::IndexSet.new({ :x => [1, 3], :y => [2, 4] })
    [[1, 2], [1, 3], [1, 4], [2, 2], [2, 3], [2, 4], [3, 2], [3, 3], [3, 4]].each do |ary|
      index1.include?(ary).should be_true
    end
    [[1, 8], [-1, 3], [5, 4], [4, 2], [0, 3], [2, 5], [3, 1], [2, 0], [0, 0]].each do |ary|
      index1.include?(ary).should be_false
    end

    index1.include?(:x => 2, :y => [0, 1]).should_not be_true
    index1.include?(:x => 2, :y => [0, 2]).should_not be_true
    index1.include?(:x => 2, :y => [0, 8]).should_not be_true
    index1.include?(:x => 2, :y => [2, 2]).should be_true
    index1.include?(:x => 2, :y => [2, 3]).should be_true
    index1.include?(:x => 2, :y => [2, 4]).should be_true
    index1.include?(:x => 2, :y => [3, 4]).should be_true
    index1.include?(:x => 2, :y => [4, 4]).should be_true
    index1.include?(:x => 2, :y => [4, 5]).should_not be_true
    index1.include?(:x => 2, :y => [5, 6]).should_not be_true

    index1.include?(:x => [0, 0], :y => 2).should_not be_true
    index1.include?(:x => [0, 1], :y => 2).should_not be_true
    index1.include?(:x => [1, 1], :y => 2).should be_true
    index1.include?(:x => [1, 3], :y => 2).should be_true
    index1.include?(:x => [1, 4], :y => 2).should_not be_true
    index1.include?(:x => [2, 3], :y => 2).should be_true
    index1.include?(:x => [2, 8], :y => 2).should_not be_true
    index1.include?(:x => [3, 8], :y => 2).should_not be_true
    index1.include?(:x => [4, 4], :y => 2).should_not be_true
    index1.include?(:x => [3, 3], :y => 2).should be_true

    other_index = Grid::IndexSet.new({ :x => [2, 3], :y => [2, 2] })
    index1.include?(other_index).should be_true
    other_index.union!(:x => 3, :y => [3, 4])
    index1.include?(other_index).should be_true
    other_index.union!(:x => [-1, 2], :y => 2)
    index1.include?(other_index).should be_false

    index2 = Grid::IndexSet.new({ :x => [0, 2], :y => [-1, 1] }, { :x => [4, 5], :y => [-1, 1] })
    index2.include?(Grid::IndexSet.new({ :x => 0, :y => [-1, 0]}, { :x => [4, 5], :y => 1})).should be_true

  end
end

