require_relative 'spec_helper'

describe Grid::Coordinate do
  before(:all) do
    MPFR.set_default_prec(256)
    @origin = MPFR::ColumnVector.new(['0.2', '-0.4'])
    @size_x = MPFR.new('0.7')
    @size_y = MPFR.new('1.2')
    @grid = Grid::Coordinate.new(@origin, @size_x, @size_y)
  end

  it "should be invalid" do
    @grid.origin.should == @origin
    @grid.size[0].should == @size_x
    @grid.size[1].should == @size_y
  end

  it "should return cube" do
    @grid.cube(0, 0).x.should == 0
    @grid.cube(0, 0).y.should == 0
    @grid.cube(0, 0).box.should be_an_instance_of MPFI::ColumnVector
    @grid.cube(-82, 9).x.should == -82
    @grid.cube(-82, 9).y.should == 9
    @grid.cube(-82, 9).box.should be_an_instance_of MPFI::ColumnVector
  end
  
  it "should equal x coordinate of other cube" do
    boxes = (-10..10).map { |x| @grid.cube(x, 20) }
    (1...boxes.size).each do |i|
      boxes[i].box[0].left.should == boxes[i - 1].box[0].right
      boxes[i].box[1].left.should == boxes[i - 1].box[1].left
      boxes[i].box[1].right.should == boxes[i - 1].box[1].right
    end
  end
  
  it "should equal y coordinate of other cube" do
    boxes = (-10..10).map { |y| @grid.cube(-17, y) }
    (1...boxes.size).each do |i|
      boxes[i].box[1].left.should == boxes[i - 1].box[1].right
      boxes[i].box[0].left.should == boxes[i - 1].box[0].left
      boxes[i].box[0].right.should == boxes[i - 1].box[0].right
    end
  end

  it "should return enclosure index of objects of mpfi" do
    ind = @grid.enclosure_index(MPFI::ColumnVector.new([MPFI.interval('0.3', '1.0'), MPFI.interval('-3.3', '-1.8')]))
    [[0, -1], [0, -2], [1, -1], [1, -2]].each { |ans| ind.include?(ans).should be_true }

    ind = @grid.enclosure_index(MPFI::ColumnVector.new([MPFI.interval('-0.75', '-0.25'), MPFI.interval('0.23', '1.3')]))
    ind.should == Grid::IndexSet.new(:x => -1, :y => 1)
  end

  it "should make wrap" do
    res_index = @grid.wrap(@grid.cube_collection([[0, 0]])).map { |c| c.index }
    [[0, 0], [0, 1], [0, -1], [1, 0], [-1, 0]].each do |ind|
      res_index.include?(ind).should be_true
    end

    res_index = @grid.wrap(@grid.cube_collection([[2, 3], [2, 4]])).map { |c| c.index }
    [[2, 3], [1, 3], [3, 3], [2, 2], [2, 4], [1, 4], [3, 4], [2, 5]].each do |ind|
      res_index.include?(ind).should be_true
    end
  end

  it "should return object for displaying" do
    obj = @grid.display_obj([0, 10], [-3, 3])
    obj.size.should == 40
  end

  it "should return a box of grid" do
    box = @grid.fi_box(-5, 3)
    (box[0].right - box[0].left - @grid.size[0]).check_error(0).should be_true
    (box[1].right - box[1].left - @grid.size[1]).check_error(0).should be_true
    box[0].left.check_error(@origin[0] + @size_x * MPFR(-5.5)).should be_true
    box[0].right.check_error(@origin[0] + @size_x * MPFR(-4.5)).should be_true
    box[1].left.check_error(@origin[1] + @size_y * MPFR(2.5)).should be_true
    box[1].right.check_error(@origin[1] + @size_y * MPFR(3.5)).should be_true
  end

  it "should return composite box of grid" do
    box = @grid.composite_fi_box(10..20, -5..5)
    (box[0].right - box[0].left - @grid.size[0] * 11).check_error(0).should be_true
    (box[1].right - box[1].left - @grid.size[1] * 11).check_error(0).should be_true
    box[0].left.check_error(@origin[0] + @size_x * MPFR(9.5)).should be_true
    box[0].right.check_error(@origin[0] + @size_x * MPFR(20.5)).should be_true
    box[1].left.check_error(@origin[1] + @size_y * MPFR(-5.5)).should be_true
    box[1].right.check_error(@origin[1] + @size_y * MPFR(5.5)).should be_true
  end
end

