require_relative 'spec_helper.rb'

describe Henon::BasinWidth do
  context "when testing convergence of a sink" do
    before(:all) do
      @prec = 300
      MPFR.set_default_prec(@prec)
      @prms = ["1.39241980783353045236959433362941513122078016169542", "-0.3"]
      @period = 36
      @threshold_close_pt = MPFR(2) ** (-140)
      @iteration = @period * 500
      @periodic_point = ['-1.27517979227424275219223754072161014775212831670417e+00', '3.81719463042498328111851898858728131595824088195972e-01']
    end

    it "should get stable periodic points" do
      ManageLog.set_log_conf(Henon::BifRange, :level => Logger::DEBUG, :io => STDOUT)
      bif_range = Henon::BifRange.new(@period, @threshold_close_pt, @iteration, 1)
      bif_range.stable_periodic_point?(@prms, MPFR::ColumnVector(@periodic_point)).should be_true
    end
  end
end
