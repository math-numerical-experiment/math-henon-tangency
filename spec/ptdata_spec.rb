require_relative 'spec_helper'

MPFR.set_default_prec(256)

describe InvMfd::PtData do
  before(:all) do
    @srcs = [[MPFR::ColumnVector.new([0, 1]), 2, MPFR.new('0.3'), 4], 
             [MPFI::ColumnVector.new([0, 1]), 2, MPFR.new('0.3'), 4]]
    @pts = @srcs.map{ |a| InvMfd::PtData.new(*a) }
  end

  it "should be able to be converted to string." do
    @pts.each do |a|
      str = a.to_s
      new = InvMfd::PtData.from_string(str)
      new.should == a
    end
  end
end

