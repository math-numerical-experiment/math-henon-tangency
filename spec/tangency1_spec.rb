require_relative 'spec_helper'

MPFR.set_default_prec(256)

describe Intersect, 'When executing module functions' do
  it "should get intersections and measure distance" do
    const = ['1.39270605', '-0.3']
    henon = MPFR::Henon::Mapping.new(const)
    
    st_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 1, MPFR), henon, -15, [MPFR.new('0.9469'), MPFR.new('0.9496')])
    un_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 2, MPFR), henon, 38, [MPFR.new('0.1893'), MPFR.new('0.2213')])
    
    st_mfd.calc_with_number(10)
    un_mfd.calc_with_number(10)
    
    res = Intersect::Function.distance_between_two_mfds(st_mfd, un_mfd)
    res.should be_an_instance_of Intersect::MfdDist

    # [Intersect::Data, Intersect::Data, ... ]
    res = Intersect::Function.get_intersections(st_mfd, un_mfd)
    res.size.should == 2
    res.each { |a| a.should be_an_instance_of Intersect::Data }

    sufficient_small = MPFR.new('1e-2')
    sufficient_large = MPFR.new('5e-3')

    # [[MPFR, Fixnum, Fixnum], Range, Range]
    res = Intersect::Function.distance_and_close_pair_index(st_mfd, un_mfd, sufficient_small)
    res[0].should be_an_instance_of Array
    res[0].size.should == 3
    res[0][0].should be_an_instance_of MPFR
    res[0][1].should be_an_instance_of Fixnum
    res[0][2].should be_an_instance_of Fixnum
    res[1].should be_an_instance_of Range
    res[2].should be_an_instance_of Range
    
    res = Intersect::Function.index_ranges_close_pts(st_mfd, un_mfd, sufficient_small)
    res.size.should == 2
    res.each { |a| a.should be_an_instance_of Range }

    Intersect::Function.distance_and_close_pair_index(st_mfd, un_mfd, sufficient_large).should be nil
    Intersect::Function.index_ranges_close_pts(st_mfd, un_mfd, sufficient_large).should be nil

  end

  it "should calculate intersection accurately" do
    const = ['1.392497593643890380859374999999999999999999999999999999999999999999999999999971e+00', '-0.3']
    henon = MPFR::Henon::Mapping.new(const)
    
    st_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 1, MPFR), henon, -15, [MPFR.new('0.9469'), MPFR.new('0.9496')])
    un_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 2, MPFR), henon, 38, [MPFR.new('0.1893'), MPFR.new('0.2213')])
    
    st_mfd.calc_with_number(10)
    un_mfd.calc_with_number(10)

    old_sizes = [st_mfd.size, un_mfd.size]
    
    res = Intersect::Function.get_intersections(st_mfd, un_mfd)
    res.size.should == 2
    res.each { |a| a.should be_an_instance_of Intersect::Data }

    err = MPFR.new('1e-5')
    res = Intersect::Function.confirm_intersect(st_mfd, un_mfd, res, err)
    res.size.should == 2
    st_mfd.size.should >= old_sizes[0]
    un_mfd.size.should >= old_sizes[1]

    res.each do |dt|
      MPFR::Vector.distance(st_mfd[dt.ind1], st_mfd[dt.ind1 + 1]).should < err
      MPFR::Vector.distance(un_mfd[dt.ind2], un_mfd[dt.ind2 + 1]).should < err
    end
    
  end
  
end

