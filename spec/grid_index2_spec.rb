require_relative 'spec_helper'

describe Grid::IndexSet, "when marshaling" do
  it "should be the same as original" do
    index1 = Grid::IndexSet.new({ :x => [-2, 1], :y => [0, 1] })
    index2 = Marshal.load(Marshal.dump(index1))
    index2.should be_an_instance_of Grid::IndexSet
    index1.should == index2
    index1.object_id.should_not == index2.object_id
  end
end

describe Grid::IndexSet, "when dumping" do
  it "should load from array" do
    index1 = Grid::IndexSet.new({ :x => [-2, 1], :y => [0, 1] }, { :x => [1, 2], :y => [-3, -2] })
    ary = index1.dump_to_array
    index2 = Grid::IndexSet.new(*ary)
    
    index1.should == index2
    (index1.object_id == index2.object_id).should_not be_true

    index2.intersection!(:x => [-1, 1], :y => [-1, 1])
    index1.should_not == index2

    index2.load_from_array(ary)
    index1.should == index2
    (index1.object_id == index2.object_id).should_not be_true
  end
end

describe Grid::IndexSet, "when converting to array" do
  it "should be same numbers as method each" do
    new_index = Grid::IndexSet.new({ :x => [-2, 1], :y => [0, 1] })
    ary = new_index.to_a
    ary.size.should == 8
    [[-2, 0], [-2, 1], [-1, 0], [-1, 1], [0, 0], [0, 1], [1, 0], [1, 1]].each do |a|
      ary.include?(a).should be_true
    end
    new_index.each do |x, y|
      ary.include?([x, y]).should be_true
      ary.delete_if { |a| a == [x, y] }
    end

    new_index = Grid::IndexSet.new({ :x => -1, :y => [-3, 1] }, { :x => [-2, 0], :y => 1 })
    ary = new_index.to_a
    ary.size.should == 7
    [[-1, -3], [-1, -2], [-1, -1], [-1, 0], [-1, 1], [-2, 1], [-2, 1]].each do |a|
      ary.include?(a).should be_true
    end
    new_index.each do |x, y|
      ary.include?([x, y]).should be_true
      ary.delete_if { |a| a == [x, y] }
    end

  end
end

describe Grid::IndexSet, "when creating related set" do
  it "should returns collar of set" do
    ind = Grid::IndexSet.new({ :x => 0, :y => 1 })
    
  end
end

