require_relative 'spec_helper'

MPFR.set_default_prec(256)

# ManageLog.set_log_conf(SrcInvMfd::CalcPtWithError, :level => Logger::DEBUG, :io => STDOUT)

describe SrcInvMfd::CalcPtWithError, "when using MPFR" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFR::Henon::Mapping.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec.neg, uvec]
    error = MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(100)
    calc = MPFR::Henon::MfdCalculator.new(cst, 1, pt, basis, :stable, nil, error)

    @calcpt = SrcInvMfd::CalcPtWithError.new(calc, 10)
    
    @error = MPFR.new('1e-4')
  end
  
  it "should calculate points on stable manifold" do
    @calcpt.set_initial_points
    file = File.expand_path('tmp1.dat')
    @calcpt.set_save_file(file)
    lambda { @calcpt.calc_mfd(@error) }.should_not raise_error
    # File.delete(file)
  end

end

describe SrcInvMfd::CalcPtWithError, "when using MPFR" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFR::Henon::Mapping.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [uvec, svec]
    error = MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(100)
    calc = MPFR::Henon::MfdCalculator.new(cst, 1, pt, basis, :unstable, true, error)

    @calcpt = SrcInvMfd::CalcPtWithError.new(calc, 25)

    @error = MPFR.new('1e-4')
  end
  
  it "should calculate points on unstable manifold" do
    @calcpt.set_initial_points
    file = File.expand_path('tmp2.dat')
    @calcpt.set_save_file(file)
    lambda { @calcpt.calc_mfd(@error) }.should_not raise_error
    # File.delete(file)
  end

end

