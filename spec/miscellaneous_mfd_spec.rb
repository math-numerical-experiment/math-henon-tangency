require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe InvMfd::SrcPts do
  before(:all) do
    data = 10.times.map do |i|
      MPFR::ColumnVector([i, i * i])
    end
    @src = InvMfd::SrcPts.new(data)
  end

  subject do
    @src
  end

  it "should get part of points." do
    srcpts = subject.portion(2..4)
    srcpts.should have(3).items
    srcpts[0].should == MPFR::ColumnVector([2, 4])
    srcpts[1].should == MPFR::ColumnVector([3, 9])
    srcpts[2].should == MPFR::ColumnVector([4, 16])
  end

  it "should get part of points by end excluding range" do
    srcpts = subject.portion(2...4)
    srcpts.should have(2).items
    srcpts[0].should == MPFR::ColumnVector([2, 4])
    srcpts[1].should == MPFR::ColumnVector([3, 9])
  end

  it "should get part of points by some ranges." do
    srcpts = subject.portion(0, 3..5, 7)
    srcpts.should have(5).items
    srcpts[0].should == MPFR::ColumnVector([0, 0])
    srcpts[1].should == MPFR::ColumnVector([3, 9])
    srcpts[2].should == MPFR::ColumnVector([4, 16])
    srcpts[3].should == MPFR::ColumnVector([5, 25])
    srcpts[4].should == MPFR::ColumnVector([7, 49])
  end
end
