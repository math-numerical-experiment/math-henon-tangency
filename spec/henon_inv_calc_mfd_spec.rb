require_relative 'spec_helper'

MPFR.set_default_prec(256)

describe SrcInvMfd::CalcPtWithNumber, "when using MPFR" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFR::Henon::Mapping.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec.neg, uvec]
    error = MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(100)
    calc = MPFR::Henon::MfdCalculator.new(cst, 1, pt, basis, :stable, nil, error)

    @calcpt = SrcInvMfd::CalcPtWithNumber.new(calc)
    
    @num = 100
  end
  
  it "should calculate #{@num} points" do
    @calcpt.set_initial_points
    ary = nil
    lambda { ary = @calcpt.calc_mfd(@num) }.should_not raise_error
    ary.size.should == @num + 2
    (1...(ary.size)).each do |i|
      ary[i - 1].initial_position.should < ary[i].initial_position
    end
  end

end

describe SrcInvMfd::CalcPtWithNumber, "when using MPFR" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFR::Henon::Mapping.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [uvec, svec]
    error = MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(100)
    calc = MPFR::Henon::MfdCalculator.new(cst, 1, pt, basis, :unstable, true, error)

    @calcpt = SrcInvMfd::CalcPtWithNumber.new(calc)
    
    @num = 100
  end
  
  it "should calculate #{@num} points" do
    @calcpt.set_initial_points
    ary = nil
    lambda { ary = @calcpt.calc_mfd(@num) }.should_not raise_error
    ary.size.should == @num + 2
    (1...(ary.size)).each do |i|
      ary[i - 1].initial_position.should < ary[i].initial_position
    end
  end

end

describe SrcInvMfd::CalcPtWithNumber, "when using MPFI" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFI::Henon::MapN.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec.neg, uvec]
    calc = MPFI::Henon::MfdCalculator.new(cst, 1, pt, basis, :stable, nil)

    @calcpt = SrcInvMfd::CalcPtWithNumber.new(calc)
    
    @num = 100
  end
  
  it "should calculate #{@num} points" do
    @calcpt.set_initial_points
    ary = nil
    lambda { ary = @calcpt.calc_mfd(@num) }.should_not raise_error
    ary.size.should == @num + 2
    (1...(ary.size)).each do |i|
      ary[i - 1].initial_position.should < ary[i].initial_position
    end
  end

end

describe SrcInvMfd::CalcPtWithNumber, "when using MPFI" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFI::Henon::MapN.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [uvec, svec]
    calc = MPFI::Henon::MfdCalculator.new(cst, 1, pt, basis, :unstable, true)

    @calcpt = SrcInvMfd::CalcPtWithNumber.new(calc)
    
    @num = 100
  end
  
  it "should calculate #{@num} points" do
    @calcpt.set_initial_points
    ary = nil
    lambda { ary = @calcpt.calc_mfd(@num) }.should_not raise_error
    ary.size.should == @num + 2
    (1...(ary.size)).each do |i|
      ary[i - 1].initial_position.should < ary[i].initial_position
    end
  end

end

