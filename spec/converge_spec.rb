require_relative 'spec_helper.rb'
require 'stringio'

class SimpleMap < MappingTemplate
  def valid_inverse_mapping?(pt, err = nil)
    pt2 = map(map(pt, 1), -1)
    pt3 = map(map(pt, -1), 1)
    pt2.check_error(pt, err) && pt3.check_error(pt, err)
  end
  
  def map_one_time(pt)
    MPFR::ColumnVector([pt[0] * 0.8, pt[1] * 0.4])
  end

  def inverse_map_one_time(pt)
    MPFR::ColumnVector([pt[0] * 1.25, pt[1] * 2.5])
  end
end

describe Henon::ConvergeToBasin do
  before(:all) do
    MPFR.set_default_prec(128)
    @map = SimpleMap.new(nil, 1)
    @converge_basin = Henon::ConvergeToBasin.new(@map, MPFR::ColumnVector([0, 0]), 1000, MPFR('1e-10'))
  end

  it "should check SimpleMap class" do
    @map.valid_inverse_mapping?(MPFR::ColumnVector([1, 2])).should be_true
  end

  it "should include some point" do
    @converge_basin.basin_include?(MPFR::ColumnVector([1, 2])).should be_true
  end

  it "should return a string" do
    sio = StringIO.new
    @converge_basin.output_basin_in_box(sio, MPFR::ColumnVector([-1, -1]),
                                        MPFR::ColumnVector([1, 1]), MPFR('5e-1'), "%.Re")
    sio.string.count("\n").should >= 25
  end
end
