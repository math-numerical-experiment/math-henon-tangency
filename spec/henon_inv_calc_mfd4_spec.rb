require_relative 'spec_helper'

MPFR.set_default_prec(256)

# ManageLog.set_log_conf(SrcInvMfd::CalcPtWithError, :level => Logger::DEBUG, :io => STDOUT)

describe SrcInvMfd::CalcPtWithError, "when using MPFI" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFI::Henon::MapN.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec.neg, uvec]
    calc = MPFI::Henon::MfdCalculator.new(cst, 1, pt, basis, :stable, nil)

    @calcpt = SrcInvMfd::CalcPtWithError.new(calc, 10)
    
    @error = MPFR.new('1e-4')
  end
  
  it "should calculate points on stable manifold" do
    @calcpt.set_initial_points
    file = File.expand_path('tmp3.dat')
    @calcpt.set_save_file(file)
    lambda { @calcpt.calc_mfd(@error) }.should_not raise_error
    # File.delete(file)
  end

end

describe SrcInvMfd::CalcPtWithError, "when using MPFI" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFI::Henon::MapN.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [uvec, svec]
    calc = MPFI::Henon::MfdCalculator.new(cst, 1, pt, basis, :unstable, true)

    @calcpt = SrcInvMfd::CalcPtWithError.new(calc, 25)

    @error = MPFR.new('1e-4')
  end
  
  it "should calculate points on unstable manifold" do
    @calcpt.set_initial_points
    file = File.expand_path('tmp4.dat')
    @calcpt.set_save_file(file)
    lambda { @calcpt.calc_mfd(@error) }.should_not raise_error
    # File.delete(file)
  end

end
