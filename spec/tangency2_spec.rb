require_relative 'spec_helper'

describe Intersect, 'When executing module functions' do
  before(:all) do
    MPFR.set_default_prec(256)
    ManageLog.set_log_conf(Intersect::Determine, :level => Logger::DEBUG, :io => STDOUT)
  end
  
  it "should not find an  intersection." do
    const = ["1.39241980792391250304093437105990190501490821181813407891869616150870569981634616851806640625", "-0.3"]
    henon = MPFR::Henon::Mapping.new(const)
    
    st_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 1, MPFR), henon, -15, [MPFR.new('0.9469'), MPFR.new('0.9496')])
    un_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 2, MPFR), henon, 38, [MPFR.new('0.1893'), MPFR.new('0.2213')])
    
    calc = Intersect::Determine.new(MPFR.new('1e-8'), MPFR.new('1e-3'))
    calc.search_intersections(st_mfd, un_mfd).should be_nil
  end

  it "should find an  intersection." do
    const = ["1.392419807923912503040934371060709698581854527906875688969545734607891063205897808074951171875", "-0.3"]
    henon = MPFR::Henon::Mapping.new(const)
    
    st_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 1, MPFR), henon, -15, [MPFR.new('0.9469'), MPFR.new('0.95')])
    un_mfd = MappedMfd::Points.new(InvMfd::Calculator.create_for_fp(const, 2, MPFR), henon, 38, [MPFR.new('0.1893'), MPFR.new('0.2213')])

    calc = Intersect::Determine.new(MPFR.new('1e-8'), MPFR.new('1e-3'))

    res = calc.search_intersections(st_mfd, un_mfd)
    res.size.should == 2

  end

end

