require_relative "spec_helper"

describe Henon::PeriodicWindow do
  def check_sink_data(sink_data, period)
    sink_data[:period] = period
    (sink_data[:true][0].position - sink_data[:true][1].position).abs.should > 0
    (sink_data[:true][0].position - sink_data[:false][0].position).abs.should > 0
    (sink_data[:true][1].position - sink_data[:false][1].position).abs.should > 0
  end

  it "should return parameter interval that a sink exists." do
    MPFR.set_default_prec(256)

    period = 12
    prms = ['1.39120000000000001474e+00', '-3.00359999999999989842e-01'].map { |s| MPFR(s) }
    sink = MPFR::ColumnVector(['5.84496796076749649108e-01', '1.95952587583781711312e-01'])
    error = MPFR('1e-10')

    pwin = Henon::PeriodicWindow.new(prms, sink, MPFR('1e-6'), [1, 0])
    sink_data = pwin.get_persistence_sink(error)
    check_sink_data(sink_data, period)

    pwin = Henon::PeriodicWindow.new(prms, sink, MPFR('1e-6'), [0, 1])
    sink_data = pwin.get_persistence_sink(error)
    check_sink_data(sink_data, period)
  end

  it "should return window" do
    MPFR.set_default_prec(256)

    error = MPFR('1e-10')
    period = 16
    prms = [MPFR('1.059'), MPFR('-0.455')]
    # p sink = MPFR::Henon::Mapping.new(prms).iterate_map(MPFR::ColumnVector([0, 0]), 1000000)
    sink = MPFR::ColumnVector(['4.28680839726321998543887872251799496540951032614837575411377310932349199094128e-01',
                               '3.521201231848544548249717301097380733241982245351044436495650800147108412973239e-01'])

    pwin = Henon::PeriodicWindow.new(prms, sink, MPFR('1e-14'), [1, 0])
    win_prm = pwin.get_window(error, MPFR('1e-20'))

    tsv_line = win_prm.to_tsv_line
    win_prm2 = Henon::WindowParameter.load_tsv_line(tsv_line)
    win_prm.primary_sink_period.should == win_prm2.primary_sink_period
    win_prm.interval_start.prms.should == win_prm2.interval_start.prms
    win_prm.interval_end.prms.should == win_prm2.interval_end.prms
  end
end
