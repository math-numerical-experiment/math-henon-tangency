require_relative 'spec_helper'

describe MappedMfd::Points, "when calculating points" do
  before(:all) do
    cst = ['1.4', '-0.3']
    @henon = MPFR::Henon::Mapping.new(cst)
    pt = @henon.fixed_point[0]
    vals, vecs = @henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec.neg, uvec]
    error = MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(100)
    @calc = MPFR::Henon::MfdCalculator.new(cst, 1, pt, basis, :stable, nil, error)
  end
  
  before(:each) do
    @pts = MappedMfd::Points.new(@calc, @henon, 12, @calc.base_two_points)
  end

  it "should calculate 10 points" do
    @pts.size.should == 2
    @pts.calc_with_number(10)
    @pts.size.should == 12
  end

  it "should caluclate with error" do
    err = MPFR.new('1e-2')
    @pts.size.should == 2
    @pts.calc_with_error(err)
    @pts.each_with_index do |pt, i|
      if i > 0
        pt.distance_from(@pts[i-1]).should < err
      end
    end
  end
  
end

describe MappedMfd::Points, "when calculating points" do
  before(:all) do
    cst = ['1.4', '-0.3']
    @henon = MPFR::Henon::Mapping.new(cst)
    @calc = MappedMfd::StraightLine.new(MPFR::ColumnVector.new([0, 0]), MPFR::ColumnVector.new([0, 1]))
  end
  
  before(:each) do
    @pts = MappedMfd::Points.new(@calc, @henon, 1, @calc.base)
  end

  it "should calculate 10 points" do
    @pts.size.should == 2
    @pts.calc_with_number(10)
    @pts.size.should == 12
  end

  it "should caluclate with error" do
    err = MPFR.new('1e-2')
    @pts.size.should == 2
    @pts.calc_with_error(err)
    @pts.each_with_index do |pt, i|
      if i > 0
        pt.distance_from(@pts[i-1]).should < err
      end
    end
  end
  
end

