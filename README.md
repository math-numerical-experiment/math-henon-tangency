# math-henon-tangency

## Installation

This program has been developed on Ubuntu (at present, version 13.04)
and composes of C programs, ruby programs, and C extended libraries of ruby.
To use this program, we need to install some programs: mainly, ruby, some gems (libraries on ruby) and some C libraries.
The installation on Ubuntu 13.04 is the following.

Firstly, we install some packages on Ubuntu;

    sudo apt-get install libgmp-dev libmpfr-dev libmpc-dev libmpfi-dev freeglut3-dev libglc-dev libargtable2-dev libtokyocabinet-dev

This program is managed by git. If we have not installed it, we need to call the following command, too;

    sudo apt-get install git

Firstly, we install ruby 2.0. The author recommends the usage of RVM.
To get further information about ruby and RVM, please refer to some pages on the internet.
After the installation of RVM, we install ruby 2.0 by the command 'rvm' and use ruby 2.0;

    rvm install ruby-2.0.0-head
    rvm use ruby-2.0.0-head

Secondly, we download the repository of this progarm and its submodules;

    git clone --recursive https://git.gitorious.org/math-numerical-experiment/math-henon-tangency.git
    cd math-henon-tangency

Thirdly, we install gems that is required to execute our ruby scripts.
To do that, we use bundler;

    gem install bundler
    bundle install

Finally, we compile some C programs and C extended libraries of ruby by using rake task;

    bundle exec rake compile

## Execution

To execute our programs, we need to use the command "bundle";

    bundle exec ruby path/to/program

## Directories

### lib

Directory of ruby library including C extended libraries.

### scripts

Directory of main executable files.
Refer to readme in each directory and help message.

### verdor

Directory to save other git repositories.
