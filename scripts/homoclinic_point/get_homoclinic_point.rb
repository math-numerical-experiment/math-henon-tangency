require_relative "../../lib/henon_tangency"

params = {
  :prec => 256,
  :log_level => Logger::FATAL,
  :format => "%.Re",
  :output => nil
}

help_message =<<HELP
Search homoclinic points.

usage: ruby #{File.basename(__FILE__)} file_stable_mfd file_unstable_mfd
HELP

begin
  OptionParser.new(help_message) do |opt|
    MPFR::Henon.command_option_define(opt, params, :only => [:prec, :log_level])
    opt.on("--format FORMAT", String, "Format of MPFR") do |v|
      params[:format] = v
    end
    opt.on("-O OUT", "--output OUT", String, "Output file") do |v|
      params[:output] = v
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

path_stable = ARGV[0]
path_unstable = ARGV[1]

unless (path_stable && path_unstable)
  $stderr.puts "Require two input files"
  exit(1)
end

ManageLog.set_log_conf(HomoclinicPoint, :level => params[:log_level], :io =>  STDOUT)

MPFR.set_default_prec(params[:prec])

error = MPFR("1e-10")
hp = HomoclinicPoint.new(path_stable, path_unstable, error)
homoclinic_points = hp.search

path_output = params[:output] || "homoclinic_points.txt"
format = params[:format]
Kernel.open(FileName.create(path_output, :position => :middle, :directory => :parent), "w") do |f|
  homoclinic_points.each do |pt, st_crd1, st_crd2, un_crd1, un_crd2|
    f.puts "#{pt[0].to_strf(format)} #{pt[1].to_strf(format)} #{st_crd1.to_strf(format)} #{st_crd2.to_strf(format)} #{un_crd1.to_strf(format)} #{un_crd2.to_strf(format)}"
  end
end
