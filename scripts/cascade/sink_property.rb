require_relative "../../lib/henon_tangency"
require_relative "search/search_sink"

RESULT_SOURCE_KEY = [:crd, :prm, :mfd_tangency]
PRM_B = "-0.3"

def eigenvalues_at_fp(prm_a)
  henon = MPFR::Henon::Mapping.new([prm_a, PRM_B])
  # fixed point of which coordinate is larger than the other
  fp = henon.fixed_point[0]
  eigen_vals = henon.jacobian_matrix(fp).eigenvalue
  # val = eigen_vals.min # Pick up an eigenvalue that is negative
end

class SinkProp
  attr_reader :result, :params

  def initialize(result, params)
    @result = result
    @params = params
  end

  # We need basin width.
  def calculate_prm_range(res, result)
    puts "Calculate parameter range of sink of period #{res.period}"
    ManageLog.set_log_conf(Henon::BifRange, :level => params[:log_level], :io => STDOUT)
    # We must take sufficient iteration number so that points converge
    test_iterate_num = res.period * 500
    threshold_close_pt = res[:basin_width] * MPFR("1e-10") # For now, we take very small value
    prm_start = res[:prm]
    pt_start = MPFR::ColumnVector(res[:crd])
    bif = Henon::BifRange.new(res.period, threshold_close_pt, test_iterate_num, 1)
    res_last = result.get(res.period - 2)
    if res_last && res_last[:prm_range]
      prm_search_size = (res_last[:prm_range][0] - res_last[:prm_range][1]).abs
    else
      prm_search_size = MPFR("1e-3")
    end
    prm_b = MPFR(PRM_B)
    prms = [prm_start, prm_b]
    if ary = bif.stable_periodic_point?(prms, pt_start)
      pt_start = ary[1]
    else
      if res_second_last = result.get(res.period - 4)
        len = (res_second_last[:prm_range][0] - res_second_last[:prm_range][1]).abs
        prm_size_estimate = prm_search_size * prm_search_size / len
        prm_split = 20
        prm_step = prm_size_estimate / prm_split
        pt_find_p = false
        prms_cur = prms.dup
        prm_split.times do |n|
          prms_cur[0] = prms_cur[0] + prm_step
          if ary = bif.stable_periodic_point?(prms_cur, pt_start)
            pt_find_p = true
            prms = prms_cur
            pt_start = ary[1]
            break
          end
        end
        unless pt_find_p
          prms_cur = prms.dup
          prm_split.times do |n|
            prms_cur[0] = prms_cur[0] - prm_step
            if ary = bif.stable_periodic_point?(prms_cur, pt_start)
              pt_find_p = true
              prms = prms_cur
              pt_start = ary[1]
              break
            end
          end
        end
      end
    end
    res[:prm_range] = bif.stable_prm_range(prms, pt_start, [0, prms[0] - prm_search_size, prms[0] + prm_search_size])
  end

  # Calculate parameter range where the same attractor exists.
  def calculate_duration(res, result)
    puts "Calculate duration of the attractor traced from the sink of period #{res.period}"
    ManageLog.set_log_conf(Henon::BifRange, :level => params[:log_level], :io => STDOUT)
    # We must take sufficient iteration number so that points converge
    test_iterate_num = res.period * 500
    threshold_close_pt = MPFR(2) ** (-MPFR.get_default_prec + 20) # This value is not used when calculating duration
    prm_start = res[:prm]
    pt_start = MPFR::ColumnVector(res[:crd])
    bif = Henon::BifRange.new(res.period, threshold_close_pt, test_iterate_num, 10)
    res_last = result.get(res.period - 2)
    if res_last && res_last[:duration]
      prm_search_size = (res_last[:duration][0] - res_last[:duration][1]).abs
    else
      prm_search_size = MPFR("1e-3")
    end
    prm_b = MPFR(PRM_B)
    prms = [prm_start, prm_b]
    res[:duration] = bif.calc_duration(prms, pt_start, [0, prms[0] - prm_search_size, prms[0] + prm_search_size])
    prm_new = res[:duration][0] * 0.75 + res[:duration][1] * 0.25
    pt_new = bif.trace_periodic_point(pt_start, [prm_start, prm_b], [prm_new, prm_b], 100, 1000)
    res[:prm] = prm_new
    res[:crd] = [pt_new[0], pt_new[1]]
  end

  BASIN_WIDTH_ITERATE = 10000

  def calculate_basin_width(res, result)
    puts "Calculate basin width of sink of period #{res.period}"
    ManageLog.set_log_conf(Henon::BasinWidth, :level => params[:log_level], :io => STDOUT)
    if res_last = result.get(res.period - 2)
      threshold_converge = res_last[:basin_width]
    else
      threshold_converge = MPFR("1e-10")
    end
    henon_fr = MPFR::Henon::Mapping.new([res[:prm], MPFR(PRM_B)], res.period)
    sink = MPFR::ColumnVector(res[:crd])
    converge = Henon::ConvergeToBasin.new(henon_fr, sink, BASIN_WIDTH_ITERATE, threshold_converge)
    file_cache = File.join(CACHE_DIRECTORY, ("boundary_%03d.tct" % res.period))
    basin_width = Henon::BasinWidth.new(sink, converge, :cache => file_cache)
    res[:basin_width_prm] = res[:prm]
    res[:basin_width] = basin_width.calc
  end

  def distance_peak(prms, iter_stable, range_stable, iter_unstable, range_unstable, err)
    distance_peak_error = err
    mfd_error = err
    henon_fr = MPFR::Henon::Mapping.new(prms)
    # We deal with branch numbers 1 and 2.
    tangent_crd_range = TangentCrdRange.new(henon_fr, InvMfd::Calculator.create_for_fp(prms, 1, MPFR), InvMfd::Calculator.create_for_fp(prms, 2, MPFR))
    st_mfd, un_mfd, range_stable, range_unstable = tangent_crd_range.coordinate_range(-iter_stable, range_stable, iter_unstable, range_unstable)

    calc = DistanceUnstablePeak.new(Intersect::Determine.new(distance_peak_error, mfd_error), distance_peak_error)
    intersection_find_p = calc.set_mfds(st_mfd, un_mfd)
    dist = calc.distance(distance_peak_error)
    [dist, range_stable, range_unstable, calc, intersection_find_p]
  end
  private :distance_peak

  def calculate_distance_peak(res, result)
    puts "Calculate distance of peak when sink of period #{res.period} exists"
    unless mfd_tangency = res[:mfd_tangency]
      if res2 = result.get(res.period - 2)
        mfd_tangency = res2[:mfd_tangency]
      end
      unless mfd_tangency
        if res3 = result.get(res.period - 1)
          mfd_tangency = res3[:mfd_tangency]
        end
      end
    end
    unless mfd_tangency
      puts "Do not know segments of manifolds that have a tangency"
      return false
    end
    range_stable = mfd_tangency[0..1]
    range_unstable = mfd_tangency[2..3]

    res[:peak_prm] = res[:prm]
    prms = [res[:prm], PRM_B]
    err = res[:basin_width] / 1000

    dist, range_stable, range_unstable, calc_peak, intersection_find_p = distance_peak(prms, result.iter_stable, range_stable, result.iter_unstable, range_unstable, err)
    res[:mfd_tangency] = range_stable + range_unstable
    res[:peak_distance] = dist
  end

  def method_period_target(target)
    case target.intern
    when :odd
      "odd?"
    when :even
      "even?"
    else
      nil
    end
  end
  private :method_period_target

  def delete_prop(target)
    keys_deleted = (params[:delete_prop] - RESULT_SOURCE_KEY)
    test_method = method_period_target(params[:sequence_period])
    puts "Delete keys:"
    p keys_deleted
    result.each do |res|
      next if test_method && !res.period.__send__(test_method)
      if target.include?(res.period)
        puts "Delete properties of period #{res.period}"
        res.data.delete_if do |key, val|
          keys_deleted.include?(key)
        end
      end
    end
    result.save
  end

  def calc_prop(target = nil)
    test_method = method_period_target(params[:sequence_period])
    result.each do |res|
      begin
        next if test_method && !res.period.__send__(test_method)
        next if target && !target.include?(res.period)
        if params[:force_calculate] && params[:force_calculate].include?(res.period)
          res.data.delete_if do |key, val|
            !RESULT_SOURCE_KEY.include?(key)
          end
        end
        unless res[:duration]
          calculate_duration(res, result)
        end
        unless res[:basin_width]
          calculate_basin_width(res, result)
        end
        unless res[:prm_range]
          calculate_prm_range(res, result)
        end
        unless res[:peak_distance]
          calculate_distance_peak(res, result)
        end
        result.save
      rescue Exception => err
        result.save
        $stderr.puts "An error when calculating properties of the sink of period #{res.period}"
        raise err
      end
    end
  end

  def get_path_estimate(res_period_current, result)
    dir_estimate = File.join(File.dirname(CascadeResult.root), "estimate", ("%03d_%03d" % [result.iter_stable, result.iter_unstable]))
    FileUtils.mkdir_p(dir_estimate)
    File.join(dir_estimate, ("%03d.yaml" % (res_period_current + 2)))
  end
  private :get_path_estimate

  # For debugging
  def puts_peak_distances_in_range(result, range_stable, range_unstable, err, prm_start, prm_end, num)
    prm_step = (prm_end - prm_start) / num
    prm = prm_start
    (num + 1).times do |i|
      prms = [prm, PRM_B]
      new_dist, range_stable_tmp, range_unstable_tmp, calc_peak_tmp, intersection_find_p = distance_peak(prms, result.iter_stable, range_stable, result.iter_unstable, range_unstable, err)
      p intersection_find_p
      p new_dist
      prm += prm_step
    end
  end
  private :puts_peak_distances_in_range

  def create_estimate_file(res, result, file_estimate)
    res_last = result.get(res.period - 2)
    if !res_last
      # 
      # Actually, to get estimation, we do not need second last sink.
      # For simplicity, we estimate only case so that second last sink exists.
      # 
      puts "Need last and second last sinks"
      return false
    end
    period_next = res.period + 2
    velocity = (res[:peak_distance] - res_last[:peak_distance]) / (res[:peak_prm] - res_last[:peak_prm])
    # The following estimation of parameter range of the sink is very rough.
    estimate_stable_prm_range = ((res[:duration][1] - res[:duration][0]) ** 2) / (res_last[:duration][1] - res_last[:duration][0]) * 0.3

    last_estimate_prm = res[:peak_prm]
    last_distance = res[:peak_distance]

    henon_fr = MPFR::Henon::Mapping.new([last_estimate_prm, PRM_B])
    two_eigenvalues = eigenvalues_at_fp(last_estimate_prm)
    eigenvalue = (two_eigenvalues[0] < 0 ? two_eigenvalues[0] : two_eigenvalues[1])
    estimate_distance = last_distance / (eigenvalue * eigenvalue)
    estimate_basin_width = res[:basin_width] * res[:basin_width] / res_last[:basin_width]
    estimation = {
      :period => period_next,
      :prm_stable_range => estimate_stable_prm_range,
      :basin_width => estimate_basin_width
    }

    err = estimate_basin_width / 10
    range_stable = res[:mfd_tangency][0..1]
    range_unstable = res[:mfd_tangency][2..3]
    maximum_error_estimate_prm = estimate_stable_prm_range / 10
    maximum_error_peak_pt = estimate_basin_width / 10
    res = { :peak_pt => nil, :last_peak_pt => nil, :count => 1, :prm_range => nil, :distance_range => nil }
    new_estimate_prm = last_estimate_prm - (last_distance - estimate_distance) / velocity
    puts "Initial peak distance: #{last_distance.to_strf("%.30Re")}"
    puts "Estimation of peak distance: #{estimate_distance.to_strf("%.30Re")}"
    len = (last_estimate_prm - new_estimate_prm).abs
    loop do
      puts "[Step #{res[:count]}]\nEstimation of parameter: #{new_estimate_prm.to_strf("%.Re")}"
      prms = [new_estimate_prm, PRM_B]
      new_dist, range_stable, range_unstable, calc_peak, intersection_find_p = distance_peak(prms, result.iter_stable, range_stable, result.iter_unstable, range_unstable, err)
      res[:last_peak_pt] = res[:peak_pt]
      res[:peak_pt] = calc_peak.peak_pt
      if (period_next.even? && intersection_find_p) || (period_next.odd? && !intersection_find_p)
        new_dist = -new_dist
      end
      if new_dist < estimate_distance
        if res[:prm_range]
          res[:prm_range][1] = new_estimate_prm
          res[:distance_range][1] = new_dist
        else
          res[:prm_range]= [last_estimate_prm, new_estimate_prm]
          res[:distance_range] = [last_distance, new_dist]
        end
      else
        if res[:prm_range]
          res[:prm_range][0] = new_estimate_prm
          res[:distance_range][0] = new_dist
        end
      end
      puts "Intersections: #{(intersection_find_p ? 'Found' : 'None')}"
      puts "New peak distance: #{new_dist.to_strf("%.30Re")}"
      prm_sub = (new_estimate_prm - last_estimate_prm).abs
      puts "Subtraction of parameters: #{prm_sub.to_strf("%.30Re")}"
      if res[:peak_pt] && res[:last_peak_pt]
        puts "Movement of peak points: #{(res[:peak_pt].distance_from(res[:last_peak_pt])).to_strf("%.30Re")}"
      end
      puts "Difference of peak distance: #{(estimate_distance - new_dist).abs.to_strf("%.30Re")}"
      puts "Next parameter: #{new_estimate_prm.to_strf("%.Re")}"
      if res[:distance_range]
        puts "Range of peak distances:"
        puts res[:distance_range][0].to_strf("%.30Re")
        puts res[:distance_range][1].to_strf("%.30Re")
      end
      print "\n"
      velocity = (new_dist - last_distance) / (new_estimate_prm - last_estimate_prm)
      prm_var = (last_estimate_prm - new_estimate_prm).abs
      last_estimate_prm = new_estimate_prm
      last_distance = new_dist
      new_estimate_prm = last_estimate_prm - (last_distance - estimate_distance) / velocity
      if res[:prm_range] && ((res[:prm_range][0] - new_estimate_prm) * (res[:prm_range][1] - new_estimate_prm) > 0)
        new_estimate_prm = (res[:prm_range][0] + res[:prm_range][1]) / 2
      end
      res[:count] += 1
      if (res[:peak_pt] && res[:last_peak_pt] && (res[:last_peak_pt].distance_from(res[:peak_pt]) < maximum_error_peak_pt)) || prm_var < maximum_error_estimate_prm
        break
      end
    end

    estimation[:prm_a_base] = last_estimate_prm
    estimation[:peak_pt] = [res[:peak_pt][0], res[:peak_pt][1]]
    estimation[:manifold] = [range_stable[0], range_stable[1], range_unstable[0], range_unstable[1]]

    CascadeResult::YAMLData.new(estimation).dump_to_file(file_estimate)
    true
  end
  private :create_estimate_file

  def get_peak_pt(prm_a, result, st_mfd_range, un_mfd_range, mfd_error)
    dist, range_stable, range_unstable, calc_peak, intersection_find_p = distance_peak([prm_a, PRM_B], result.iter_stable, st_mfd_range, result.iter_unstable, un_mfd_range, mfd_error)
    calc_peak.peak_pt
  end
  private :get_peak_pt

  def get_periodic_point_in_horseshoe(prms, iter_stable, range_stable, iter_unstable, range_unstable, period, error_periodic_point)
    henon = MPFR::Henon::Mapping.new(prms)
    tangent_crd_range = TangentCrdRange.new(henon, InvMfd::Calculator.create_for_fp(prms, 1, MPFR), InvMfd::Calculator.create_for_fp(prms, 2, MPFR))
    st_mfd, un_mfd, range_stable, range_unstable = tangent_crd_range.coordinate_range(-iter_stable, range_stable, iter_unstable, range_unstable)
    calc_determine = Intersect::Determine.new(error_periodic_point, MPFR("1e-3"))
    search_periodic_point = SearchPeriodicPoint.new(calc_determine, st_mfd, un_mfd, henon, period)
    search_periodic_point.search(error_periodic_point)
  end
  private :get_periodic_point_in_horseshoe

  def estimation_accuracy(target)
    test_method = method_period_target(params[:sequence_period])
    each_result(result, test_method) do |res|
      next if target && !target.include?(res.period)
      res_period_last = res.period - 2
      path_estimate = get_path_estimate(res_period_last, result)
      path_accuracy = path_estimate.sub(/\.yaml$/, "_accuracy.yaml")
      mfd_error = res[:basin_width] * MPFR("1e-10")
      puts "Sink of period #{res.period}:"
      res_last = result.get(res_period_last)
      estimate_exist_p = res_last && (File.exist?(path_estimate) || create_estimate_file(res_last, result, path_estimate))
      unless File.exist?(path_accuracy)
        prms = [res[:prm], PRM_B]
        args_of_mfds = [result.iter_stable, res[:mfd_tangency][0..1], result.iter_unstable, res[:mfd_tangency][2..3]]
        dist, range_stable, range_unstable, calc_peak, intersection_find_p = distance_peak(prms, *args_of_mfds, mfd_error)
        st_mfd, un_mfd = calc_peak.manifolds
        stable_manifold_points = st_mfd.to_a.map { |pt| pt.to_a.flatten }
        unstable_manifold_points = un_mfd.to_a.map { |pt| pt.to_a.flatten }
        st_dist, ind = calc_peak.calculator_with_intersections.distance(st_mfd, mfd_error, :negative => true)

        unless periodic_points = get_periodic_point_in_horseshoe(prms, *args_of_mfds, res.period, mfd_error)
          $stderr.puts "Can not find periodic points in horseshoe"
          $stderr.puts prms[0]
          $stderr.puts prms[1]
        end

        accuracy = {
          :sink_distance => calc_peak.get_projected_distance(MPFR::ColumnVector(res[:crd])),
          :stable_manifold_distance => st_dist,
          :stable_manifold => stable_manifold_points,
          :unstable_manifold => unstable_manifold_points,
          :periodic_points_in_horseshoe => periodic_points && periodic_points.map { |pt| pt.to_a.flatten }
        }
        if estimate_exist_p
          estimate = SearchSink.estimation_load(path_estimate)
          prm_start = estimate[:prm_search_start]
          prm_end = estimate[:prm_search_end]

          pt_start = get_peak_pt(prm_start, result, estimate[:manifold][0..1], estimate[:manifold][2..3], mfd_error)
          pt_end = get_peak_pt(prm_end, result, estimate[:manifold][0..1], estimate[:manifold][2..3], mfd_error)

          accuracy[:peak_point_start] = pt_start.to_a.flatten
          accuracy[:peak_point_end] = pt_end.to_a.flatten
        end
        CascadeResult::YAMLData.new(accuracy).dump_to_file(path_accuracy)
      end
      if estimate_exist_p
        estimate = SearchSink.estimation_load(path_estimate)
        prm_start = estimate[:prm_search_start]
        prm_end = estimate[:prm_search_end]

        accuracy = CascadeResult::YAMLData.new(YAML.load_file(path_accuracy))
        dist_sink = accuracy[:sink_distance]
        dist = res[:peak_distance]
        st_dist = accuracy[:stable_manifold_distance]
        pt_start = MPFR::ColumnVector(accuracy[:peak_point_start])
        pt_end = MPFR::ColumnVector(accuracy[:peak_point_end])

        puts "#{((res[:prm] - prm_start) / (prm_end - prm_start) * 100).to_strf("%.5Rf")}% of parameter search range"
        puts "Distance of unstable direction of sink: #{dist_sink.to_strf("%.20Re")}"
        puts "Subtraction of two distances of peak and sink: #{(dist - dist_sink).to_strf("%.20Re")}"
        puts "Ratio of subtraction of distances and search x range: #{((dist - dist_sink).abs / (estimate[:x_range_length] * 2)).to_strf("%.20Re")}"
        puts "Ratio of subtraction of distances and basin width: #{((dist - dist_sink).abs / res[:basin_width]).to_strf("%.20Re")}"

        puts "Peak distance of stable manifold: #{st_dist.to_strf("%.20Re")}"
        puts "Ratio of Peak distance of stable manifold and basin width: #{(st_dist / res[:basin_width]).abs.to_strf("%.20Re")}"

        sub = pt_start - pt_end
        puts "Movement of peak point: #{sub.abs.to_strf("%.20Re")}"
        puts "Vector of movement of peak point: #{sub[0].abs.to_strf("%.10Re")} #{sub[1].abs.to_strf("%.10Re")}"
        puts "Ratio of movement and basin width: #{(sub.abs / res[:basin_width]).to_strf("%.20Re")}"
        puts "Velocity: #{(sub.abs / (prm_end - prm_start)).to_strf("%.20Re")}, x: #{(sub[0].abs / (prm_end - prm_start)).to_strf("%.20Re")}"
        print "\n"
      end
    end
  end

  PATH_VERIFY = File.expand_path(File.join(File.dirname(__FILE__), "verify/execute.rb"))

  def verify_sink(res, verification_type)
    length_of_region = MPFR("1e-15") * res[:basin_width]
    dir = FileName.create("verify_period#{res.period}.tmp", :position => :middle)
    cmd = "bundle exec drbqs-execute --wait-server-finish #{PATH_VERIFY} -- #{res.period} -A '#{res[:prm]}' --sink '#{res[:crd][0]},#{res[:crd][1]}' '#{length_of_region}' --output #{dir}"
    puts "Command: #{cmd}"
    system(cmd)
    result_verify = CascadeResult::YAMLData.new(YAML.load_file(File.join(dir, "verify.yaml")))
    ret = nil
    if result_verify[:count][:true] == result_verify[:count][:total]
      ret = {
        :coordinate => result_verify[:coordinate],
        :parameter => result_verify[:parameter],
        :eigenvalue => result_verify[:eigenvalue]
      }
    end
    ret
  end
  private :verify_sink

  def verify(result, verification_type, target)
    test_method = method_period_target(params[:sequence_period])
    each_result(result, test_method) do |res|
      next if target && !target.include?(res.period)
      unless res[:verify]
        if res[:verify] = verify_sink(res, verification_type)
          result.save
        end
      end
    end
  end

  PATH_SEARCH = File.expand_path(File.join(File.dirname(__FILE__), "search/execute.rb"))

  def search_sink_by_estimate(search_type, file_estimate, file_output)
    unless File.exist?(file_output)
      cmd = "bundle exec drbqs-execute --wait-server-finish #{PATH_SEARCH} -- #{search_type} #{file_estimate} #{file_output}"
      puts "Command: #{cmd}"
      system(cmd)
    end
    if File.exist?(file_output)
      result = File.read(file_output).lines.map(&:strip)
      [MPFR(result[0]), [result[1], result[2]]]
    else
      nil
    end
  end
  private :search_sink_by_estimate

  def search_sink(search_type = :iteration)
    test_method = method_period_target(params[:sequence_period])
    period_start = nil
    result.each do |res|
      next if test_method && (!res.period.__send__(test_method) || result.get(res.period + 2))
      period_start = res.period
      break
    end
    unless period_start
      puts "Initial sink does not exist"
      return false
    end
    (period_start..params[:max_period]).each do |period|
      next if test_method && !period.__send__(test_method)
      period_next = period + 2
      puts "Search a sink of period #{period_next}"
      unless res = result.get(period)
        puts "Sink of period #{period} has not been found yet."
        return false
      end
      file_estimate = get_path_estimate(res.period, result)
      unless File.exist?(file_estimate)
        unless create_estimate_file(res, result, file_estimate)
          puts "Can not estimate sink of period #{period_next}"
          return false
        end
      end
      file_output = file_estimate.sub(/\.yaml$/, "_result.yaml")
      if ret = search_sink_by_estimate(search_type, file_estimate, file_output)
        puts "Find a sink of period #{period_next}"
        result.add(CascadeResult::SinkResult.new(period_next, { :prm => ret[0], :crd => ret[1] }))
        result.save
        calc_prop
      else
        puts "Can not find a sink of period #{period_next}"
        return false
      end
    end
  end

  def bifurcation_diagram(target)
    target.each do |period|
      unless res = result.get(period)
        puts "The sink of period #{period} has not found yet"
        return false
      end
      prm_range_args = [0]

      res_last = result.get(period - 2)
      res_second_last = result.get(period - 4)
      if res[:duration]
        prm_stable_size = (res[:duration][1] - res[:duration][0])
      elsif res_last && res_last[:duration] && res_second_last[:duration]
        prm_stable_size = ((res_last[:duration][1] - res_last[:duration][0]) ** 2) / (res_second_last[:duration][1] - res_second_last[:duration][0])
      else
        puts "Can not determine range of parameter"
        return false
      end
      unless basin_width = res[:basin_width]
        if res_last && res_second_last && res_last[:basin_width] && res_second_last[:basin_width]
          basin_width = (res_last[:basin_width] ** 2) / res_second_last[:basin_width]
        end
      end
      prm_range_args << res[:duration][0] - prm_stable_size * MPFR(0.5)
      prm_range_args << res[:duration][1] + prm_stable_size * MPFR(0.5)

      threshold_close_pt = basin_width / 1000
      bifimage = Henon::BifImage.new(period, prm_stable_size / 500, 1000, 500, threshold_close_pt, 500)
      path = FileName.create("bifurcation#{period}", :position => :middle, :directory => :self)
      puts "Output to #{path}"
      bifimage.output(path, :png, MPFR::ColumnVector(res[:crd]), [res[:prm], PRM_B], prm_range_args)
    end
  end

  def power_law_diagram
    out = FileName.create("power_law.eps", :position => :middle)
    gp = IO.popen("gnuplot", "w")
    gp.puts "set term postscript eps"
    gp.puts "set output '#{out}'"
    gp.puts "set xrange [#{result[0].period - 2}:#{result[-1].period + 2}]"
    gp.puts "set xlabel 'period of sink'"
    gp.puts "set ylabel 'logarithm'"
    gp.puts "set style line 1 linetype 1 linecolor rgb 'black' linewidth 1 pointtype 2 pointsize 1"
    gp.puts "set style line 2 linetype 1 linecolor rgb 'black' linewidth 1 pointtype 4 pointsize 1"
    gp.puts "set style line 3 linetype 1 linecolor rgb 'black' linewidth 1 pointtype 7 pointsize 1"
    gp.puts "plot '-' with linespoints title 'distance from stable manifold' linestyle 1, '-' with linespoints title 'width of main band of basin' linestyle 2, '-' with linespoints title 'size of parameter interval' linestyle 3"
    each_result(result) do |res|
      val = MPFR::Math.log(res[:peak_distance])
      gp.puts "#{res.period} #{val.to_strf("%.18Re")}"
    end
    gp.puts "e"
    each_result(result) do |res|
      val = MPFR::Math.log(res[:basin_width])
      gp.puts "#{res.period} #{val.to_strf("%.18Re")}"
    end
    gp.puts "e"
    each_result(result) do |res|
      val = MPFR::Math.log((res[:prm_range][1] - res[:prm_range][0]).abs)
      gp.puts "#{res.period} #{val.to_strf("%.18Re")}"
    end
    gp.puts "e"
    gp.close
  end

  def each_result(result, method_target = nil, &block)
    res_last = nil
    result.each do |res|
      next if method_target && !res.period.__send__(method_target)
      case block.arity
      when 1
        yield(res)
      when 2
        yield(res, res_last)
      else
        raise "Invalid arity of block"
      end
      res_last = res
    end
  end
  private :each_result

  COMMANDS_SHOW = [:sink, :sink_persist, :sink_eigenvalue, :sink_eigenvector,
                   :parameter_range, :peak_distance, :basin_width, :verify]

  def show(result, type, opts = {})
    test_method = method_period_target(params[:sequence_period])
    mpfr_format = opts[:format] || "%.30Re"
    if /^%.*R[ef]$/ !~ mpfr_format
      raise "Invalid format string for MPFR objects: #{mpfr_format}"
    end
    blank_string = " " * MPFR(0).to_strf(mpfr_format).size
    case type
    when :sink
      each_result(result, test_method) do |res|
        sink = MPFR::ColumnVector(res[:crd])
        ary = []
        ary << res.period.to_s
        ary << sink[0].to_strf(mpfr_format)
        ary << sink[1].to_strf(mpfr_format)
        ary << res[:prm].to_strf(mpfr_format)
        puts ary.join("\t")
      end
    when :sink_persist
      each_result(result, test_method) do |res|
        ary = []
        ary << res.period.to_s
        ary << res[:prm_range][0].to_strf(mpfr_format)
        ary << res[:prm_range][1].to_strf(mpfr_format)
        puts ary.join("\t")
      end
    when :sink_eigenvalue
      each_result(result, test_method) do |res|
        sink = MPFR::ColumnVector(res[:crd])
        henon_fr = MPFR::Henon::Mapping.new([res[:prm], PRM_B])
        jac = henon_fr.jacobian_matrix(sink, res.period)
        eigenvalues = jac.eigenvalue
        ary = []
        ary << res.period.to_s
        ary << eigenvalues[0].to_strf(mpfr_format)
        ary << eigenvalues[1].to_strf(mpfr_format)
        puts ary.join("\t")
      end
    when :sink_eigenvector
      each_result(result, test_method) do |res|
        sink = MPFR::ColumnVector(res[:crd])
        henon_fr = MPFR::Henon::Mapping.new([res[:prm], PRM_B])
        jac = henon_fr.jacobian_matrix(sink, res.period)
        eigenvalues, eigenvectors = jac.eigenvector
        ary = []
        ary << res.period.to_s
        eigenvectors.each do |vec|
          ary << vec[0].to_strf(mpfr_format)
          ary << vec[1].to_strf(mpfr_format)
        end
        puts ary.join("\t")
      end
    when :parameter_range
      each_result(result, test_method) do |res, res_last|
        ary = [res.period.to_s]
        if res[:prm_range]
          prm_range_size = (res[:prm_range][1] - res[:prm_range][0])
          ary << prm_range_size.abs.to_strf(mpfr_format)
        else
          ary << blank_string
          prm_range_size = nil
        end
        if prm_range_size && res_last && res_last[:prm_range]
          ary << (prm_range_size / (res_last[:prm_range][1] - res_last[:prm_range][0])).to_strf(mpfr_format)
        else
          ary << blank_string
        end
        if res[:duration]
          duration_size = (res[:duration][1] - res[:duration][0])
          ary << duration_size.abs.to_strf(mpfr_format)
        else
          ary << blank_string
          duration_size = nil
        end
        if duration_size && res_last && res_last[:duration]
          ary << (duration_size / (res_last[:duration][1] - res_last[:duration][0])).to_strf(mpfr_format)
        else
          ary << blank_string
        end
        if duration_size && prm_range_size
          ary << (prm_range_size / duration_size).to_strf(mpfr_format)
        else
          ary << blank_string
        end
        puts ary.join("\t")
      end
    when :peak_distance
      each_result(result, test_method) do |res, res_last|
        ary = [res.period.to_s]
        if res[:peak_distance]
          ary << res[:peak_distance].abs.to_strf(mpfr_format)
        else
          ary << ""
        end
        if res[:peak_distance] && res_last && res_last[:peak_distance]
          ary << (res[:peak_distance] / res_last[:peak_distance]).to_strf(mpfr_format)
        else
          ary << ""
        end
        puts ary.join("\t")
      end
    when :basin_width
      each_result(result, test_method) do |res, res_last|
        ary = [res.period.to_s]
        if res[:basin_width]
          ary << res[:basin_width].abs.to_strf(mpfr_format)
        else
          ary << ""
        end
        if res[:basin_width] && res_last && res_last[:basin_width]
          ary << (res[:basin_width] / res_last[:basin_width]).to_strf(mpfr_format)
        else
          ary << ""
        end
        puts ary.join("\t")
      end
    when :verify
      format_round_up = mpfr_format.sub(/R(.)$/, "RD\\1")
      format_round_down = mpfr_format.sub(/R(.)$/, "RU\\1")
      each_result(result, test_method) do |res, res_last|
        if data = res[:verify]
          ary = [res.period.to_s]
          if data[:coordinate]
            ary << data[:coordinate][0].to_strf(format_round_down)
            ary << data[:coordinate][1].to_strf(format_round_up)
            ary << data[:coordinate][2].to_strf(format_round_down)
            ary << data[:coordinate][3].to_strf(format_round_up)
          else
            ary.concat(Array.new(4, ""))
          end
          puts ary.join("\t")
          ary = [""]
          if data[:parameter]
            ary << data[:parameter][0].to_strf(format_round_up)
            ary << data[:parameter][1].to_strf(format_round_down)
            ary << data[:parameter][2].to_strf(format_round_up)
            ary << data[:parameter][3].to_strf(format_round_down)
          else
            ary.concat(Array.new(4, ""))
          end
          puts ary.join("\t")
          ary = [""]
          if data[:eigenvalue]
            ary << data[:eigenvalue][0].to_strf(format_round_up)
            ary << data[:eigenvalue][1].to_strf(format_round_down)
            ary << data[:eigenvalue][2].to_strf(format_round_up)
            ary << data[:eigenvalue][3].to_strf(format_round_down)
          else
            ary.concat(Array.new(2, ""))
          end
          puts ary.join("\t")
        end
      end
    end
  end
end
