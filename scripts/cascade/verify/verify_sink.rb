require_relative "../../../lib/henon_tangency"

class ContractionTest
  attr_accessor :default_size_of_split

  def initialize(mapping_mpfi, box_src, grid, transform_matrix, origin, default_size_of_split = nil)
    @mapping_mpfi = mapping_mpfi
    @box_src = box_src
    @grid = grid
    @transform_matrix = transform_matrix
    @inverse_matrix = @transform_matrix.inverse
    @origin = origin
    @default_size_of_split = default_size_of_split
  end

  def get_default_size_of_split
    (@default_size_of_split || (@box_src.max_diam_abs / 2))
  end
  private :get_default_size_of_split

  def set_default_size_of_split(*index_ary)
    if index_ary.empty?
      index_ary = [[0, 0]]
    end
    max_box_size = @box_src.max_diam_abs / 2
    size_of_split = get_default_size_of_split
    index_ary.each do |ind|
      box_arg = @grid.fi_box(*ind)
      loop do
        box_size_small_p = true
        diam = nil
        # Test only first mapped box
        box_arg.each_subdivision_by_size(size_of_split) do |b|
          box_transformed = transform_coordinate(b)
          map_box_with_jacobian_matrix(box_transformed, size_of_split) do |h|
            box_inverse = inverse_transform_coordinate(h[:box])
            if !included_in_source_box?(box_inverse) || (diam = box_inverse.max_diam_abs) > max_box_size
              box_size_small_p = false
            end
            break
          end
          break
        end
        break if box_size_small_p
        size_of_split /= 2
      end
    end
    @default_size_of_split = size_of_split
  end

  def map_box(box_arg, len, &block)
    if block_given?
      unfinished = Array.new(@mapping_mpfi.period) { Array.new }
      unfinished[0] << box_arg
      unfinished_size = unfinished.size
      until unfinished.all? { |ary| ary.empty? }
        (1..unfinished_size).each do |ind|
          n = unfinished_size - ind
          if box_unfinished = unfinished[n].pop
            if len <= box_unfinished.max_diam_abs
              boxes_split = box_unfinished.each_subdivision_by_size(len)
            else
              # boxes_split = box_unfinished.each_subdivision_by_size(box_unfinished.max_diam_abs * 0.9)
              boxes_split = [box_unfinished]
            end
            boxes_split.each do |box|
              box_new = @mapping_mpfi.map(box, 1)
              if n == (unfinished_size - 1)
                yield(box_new)
              else
                unfinished[n + 1] << box_new
              end
            end
            break
          end
        end
      end
    else
      to_enum(:map_box, box_arg, len)
    end
  end
  private :map_box

  # To get eigenvalues, this method calculates orbits with Jacobian matrices.
  def map_box_with_jacobian_matrix(box_arg, len, &block)
    if block_given?
      unfinished = Array.new(@mapping_mpfi.period) { Array.new }
      unfinished[0] << { :box => box_arg, :jacobian_matrix => MPFI::SquareMatrix.identity(2) }
      unfinished_size = unfinished.size
      until unfinished.all? { |ary| ary.empty? }
        (1..unfinished_size).each do |ind|
          n = unfinished_size - ind
          if box_unfinished = unfinished[n].pop
            if len <= box_unfinished[:box].max_diam_abs
              boxes_split = box_unfinished[:box].each_subdivision_by_size(len)
            else
              boxes_split = [box_unfinished[:box]]
            end
            boxes_split.each do |box|
              h_new = {
                :box => @mapping_mpfi.map(box, 1),
                :jacobian_matrix => @mapping_mpfi.jacobian_matrix(box) * box_unfinished[:jacobian_matrix]
              }
              if n == (unfinished_size - 1)
                yield(h_new)
              else
                unfinished[n + 1] << h_new
              end
            end
            break
          end
        end
      end
    else
      to_enum(:map_box_with_jacobian_matrix, box_arg, len)
    end
  end
  private :map_box_with_jacobian_matrix

  def transform_coordinate(box)
    (@transform_matrix * box) + @origin
  end
  private :transform_coordinate

  def inverse_transform_coordinate(box)
    @inverse_matrix * (box - @origin)
  end
  private :inverse_transform_coordinate

  def included_in_source_box?(*boxes)
    boxes.all? do |b|
      @box_src.include?(b)
    end
  end
  private :included_in_source_box?

  def map_with_coordinate_change(ind)
    box_arg = @grid.fi_box(*ind)
    included_p = true
    box_result = nil
    len = get_default_size_of_split
    box_arg.each_subdivision_by_size(len) do |box_init|
      box_transformed = transform_coordinate(box_init)
      map_box(box_transformed, len) do |box_mapped|
        box_inverse = inverse_transform_coordinate(box_mapped)
        included_p = included_p && included_in_source_box?(box_inverse)
        if box_result
          box_result = box_result.union(box_mapped)
        else
          box_result = box_mapped
        end
      end
    end
    { :box => box_result, :included => included_p }
  end

  def get_eigenvalue(index_array, len = nil)
    len ||= @box_src.max_diam_abs * 1000
    eigenvalue = nil
    index_array.each do |ind|
      box_arg = @grid.fi_box(*ind)
      box_arg.each_subdivision_by_size(len) do |b|
        box_transformed = transform_coordinate(b)
        map_box_with_jacobian_matrix(box_transformed, len) do |h|
          eigenvalue_other = h[:jacobian_matrix].eigenvalue
          if eigenvalue
            eigenvalue[0] = eigenvalue[0].union(eigenvalue_other[0])
            eigenvalue[1] = eigenvalue[1].union(eigenvalue_other[1])
          else
            eigenvalue = eigenvalue_other
          end
        end
      end
    end
    eigenvalue
  end

  def output_filename_for_debug_with_directory(ind, dir = nil)
    dir ||= "verify_mapping_data.tmp"
    FileUtils.mkdir_p(dir)
    index_str = sprintf("%d_%d", ind[0], ind[1])
    file = {
      :src => File.join(dir, "src_#{index_str}.txt"),
      :basin => File.join(dir, "basin_#{index_str}.txt"),
      :map => File.join(dir, "map_#{index_str}.txt"),
      :map2 => File.join(dir, "map2_#{index_str}.txt")
    }
  end
  private :output_filename_for_debug_with_directory

  def map_with_coordinate_change_for_debug(ind, dir = nil)
    file = output_filename_for_debug_with_directory(ind, dir)
    box_arg = @grid.fi_box(*ind)
    included_p = true
    box_result = nil
    box_transformed = transform_coordinate(box_arg)
    open(file[:src], 'a+') { |f| f.puts box_arg.to_s }
    open(file[:basin], 'a+') { |f| f.puts box_transformed.to_s }
    len = get_default_size_of_split
    box_arg.each_subdivision_by_size(len) do |box_init|
      box_transformed = transform_coordinate(box_init)
      map_box(box_transformed, len) do |box_mapped|
        box_inverse = inverse_transform_coordinate(box_mapped)
        open(file[:map], 'a+') { |f| f.puts box_mapped.to_s }
        open(file[:map2], 'a+') { |f| f.puts box_inverse.to_s }
        included_p = included_p && included_in_source_box?(box_inverse)
        if box_result
          box_result = box_result.union(box_mapped)
        else
          box_result = box_mapped
        end
      end
    end
    file.each do |key, path|
      DRbQS::Transfer.compress_enqueue(path)
    end
    { :box => box_result, :included => included_p }
  end

  def expansion_rate(ind = [0, 0])
    # Calculate expansion rate for only first subbox
    box_arg = @grid.fi_box(*ind)
    size_of_split = get_default_size_of_split
    b = box_arg.each_subdivision_by_size(size_of_split).next
    box = transform_coordinate(b)
    all_rate = []
    @mapping_mpfi.period.times do |i|
      box_new = @mapping_mpfi.map(box, 1)
      rate = box_new.max_diam_abs / box.max_diam_abs
      all_rate << rate
      box = box_new.each_subdivision_by_size(size_of_split).next
    end
    all_rate
  end
  private :expansion_rate

  def show_test_expansion(*index_ary)
    index_ary << [0, 0] if index_ary.empty?
    index_ary.each do |ind|
      all_rate = expansion_rate(ind)
      puts "Index: (#{ind[0]}, #{ind[1]})"
      all_rate.each_with_index do |rate, i|
        puts "  #{i}: #{rate.to_strf("%.10Re")}"
      end
      product = all_rate.inject(MPFR(1)) { |n, product| product * n}
      puts "  product: #{product.to_strf("%.10Re")}"
    end
  end
end
