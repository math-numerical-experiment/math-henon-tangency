require_relative "verify_sink.rb"

DRbQS.option_parser do |opt, hash_opts|
  hash_opts[:type] = "all"
  hash_opts[:prec] = 128
  hash_opts[:sink] = nil
  hash_opts[:output] = "verify.tmp"
  hash_opts[:param_a] = "1.4"
  hash_opts[:param_b] = "-0.3"
  hash_opts[:skip_eigenvalue] = false
  hash_opts[:show_expansion] = false
  opt.on("--save", "Save region data") do |v|
    hash_opts[:save] = v
  end
  opt.on("--task STR", String, 'Set the task type. The value is "x y", "border", "interior", "all", or file path') do |v|
    hash_opts[:type] = v
  end
  opt.on("--sink CRD", String, "Coordinate of the sink: 'x,y'") do |v|
    hash_opts[:sink] = v.split(",")
  end
  opt.on("--skip-eigenvalue", "Not calculate eigenvalue") do |v|
    hash_opts[:skip_eigenvalue] = true
  end
  opt.on("--output DIR", String, "Output directory") do |v|
    hash_opts[:output] = v
  end
  opt.on("--show-expansion", "Show expansion rates of iterated boxes") do |v|
    hash_opts[:show_expansion] = true
  end
  MPFR::Henon.command_option_define(opt, hash_opts, :only => [:prec, :param_a, :param_b])
end

def get_box_index(opt_type, box_range)
  box_index = []
  case opt_type.strip
  when "all"
    box_range[0].each do |x|
      box_range[1].each do |y|
        box_index << [x, y]
      end
    end
  when "interior"
    ((box_range[0].min + 1)...(box_range[0].max)).each do |x|
      ((box_range[1].min + 1)...(box_range[1].max)).each do |y|
        box_index << [x, y]
      end
    end
  when "border"
    x_min = box_range[0].min
    x_max = box_range[0].max
    box_range[1].each do |y|
      box_index << [x_min, y]
      box_index << [x_max, y]
    end
    y_min = box_range[1].min
    y_max = box_range[1].max
    ((x_min + 1)...x_max).each do |x|
      box_index << [x, y_min]
      box_index << [x, y_max]
    end
  when /^[+-]?\d+ +[+-]?\d+$/
    ary = opt_type.split.map { |s| s.to_i }
    i = 0
    while i < ary.size
      box_index << [ary[i], ary[i + 1]]
      i += 2
    end
  else
    if File.exist?(opt_type)
      ary = File.read(opt_type).split.map { |s| s.to_i }
      i = 0
      while i + 1 < ary.size
        box_index << [ary[i], ary[i + 1]]
        i += 2
      end
    else
      raise "Invalid type."
    end
  end
  box_index
end

DRbQS.define_server(:finish_exit => true) do |server, argv, opts|
  # argv = [period, parameter_a, sink_x, sink_y, length_of_region]
  if !opts[:sink]
    raise "Please specify the coordinate of the sink"
  elsif argv.size != 2
    raise "Invalid command line arguments: please specify [period, length_of_region]"
  end

  prec = opts[:prec]
  MPFR.set_default_prec(prec)

  period = argv[0].to_i
  length_of_region = MPFR(argv[1])

  split_region = 100
  n = split_region / 2
  region_range = [(-n)..n, (-n)..n]
  box_index = get_box_index(opts[:type], region_range)
  exec_method = (opts[:save] ? :map_with_coordinate_change_for_debug : :map_with_coordinate_change)

  prms = [opts[:param_a], opts[:param_b]]
  sink = MPFR::ColumnVector(opts[:sink])
  henon_fr = MPFR::Henon::Mapping.new(prms, period)

  mat = henon_fr.jacobian_matrix_iterate_map(sink, period)
  (r0, r1), (v0, v1) = mat.eigenvector

  transform_matrix = MPFI::SquareMatrix([[v0[0], v1[0]], [v0[1], v1[1]]])

  grid_size = length_of_region / (split_region + 1)
  grid = Grid::Coordinate.new(MPFR::ColumnVector([0, 0]), grid_size)

  region_box = grid.composite_fi_box(*region_range)
  mapping_mpfi = MPFI::Henon::MapN.new(prms, period)
  contraction_test = ContractionTest.new(mapping_mpfi, region_box, grid, transform_matrix, sink.to_fi_matrix)
  if opts[:show_expansion]
    contraction_test.show_test_expansion([0, 0], [n, n], [-n, -n], [n, -n], [-n, n])
    exit
  end
  contraction_test.set_default_size_of_split([n, n], [-n, -n], [n, -n], [-n, n])
  result_dir = FileName.create(opts[:output], :position => :middle, :directory => :self)
  log_file = File.join(result_dir, "log.txt")
  open(log_file, "a+") do |f|
    f.puts Time.now.to_s
    f.puts "type: #{opts[:type]}"
    f.puts "prec: #{prec}"
    f.puts "split_region: #{split_region}"
    f.puts "size_of_split: #{contraction_test.default_size_of_split}"
  end

  result = {
    :count => { :true => 0, :false => 0, :total => 0 },
    :coordinate => nil,
    :eigenvalue => nil
  }

  unless opts[:skip_eigenvalue]
    # Because calculation of eigenvalues does not need high accuracy,
    # we create a task only for eigenvalues.
    task_eigenvalue = DRbQS::Task.new(contraction_test, :get_eigenvalue, :args => [box_index]) do |serv, result_eigenvalue|
      result[:eigenvalue] = result_eigenvalue
    end
    server.queue.add(task_eigenvalue)
  end

  tgen = DRbQS::Task::Generator.new(:contraction_test => contraction_test, :period => period,
                                    :box_index => box_index, :exec_method => exec_method, :result_dir => result_dir)
  tgen.set(:generate => 500) do
    out = File.join(@result_dir, "period#{@period}.txt")
    open(out, "a+") do |f|
      f.puts "Length of initial region: #{length_of_region}"
    end
    file_result = FileName.create(@result_dir, "result.txt", :position => :middle, :type => :time,
                                  :add => :always,:directory => :parent)
    @box_index.each do |x, y|
      create_add(@contraction_test, @exec_method, :args => [[x, y]]) do |serv, result_hash|
        result[:count][:total] += 1
        if result_hash[:included]
          result[:count][:true] += 1
          result_str = "true"
        else
          result[:count][:false] += 1
          result_str = "false"
        end
        open(file_result, "a+") { |f| f.puts "#{result_str} #{result_hash[:box][0].to_str_ary.join("/")} #{result_hash[:box][1].to_str_ary.join("/")}" }
        if !result[:coordinate]
          result[:coordinate] = result_hash[:box]
        else
          result[:coordinate] = result[:coordinate].union(result_hash[:box])
        end
        open(out, "a+") do |f|
          f.puts "#{result[:count][:true]} / #{result[:count][:total]}"
        end
      end
    end
  end
  server.add_task_generator(tgen)
  server.set_file_transfer(result_dir)
  server.set_initialization_task(DRbQS::Task.new(MPFR, :set_default_prec, :args => [prec]))
  server.add_hook(:finish) do |srv|
    crd = result[:coordinate]
    consts = mapping_mpfi.constants
    res = {
      :count => result[:count],
      :parameter => [consts[0].left, consts[0].right, consts[1].left, consts[1].right],
      :coordinate => [crd[0].left, crd[0].right, crd[1].left, crd[1].right]
    }
    if ev = result[:eigenvalue]
      res[:eigenvalue] = [ev[0].left, ev[0].right, ev[1].left, ev[1].right]
    end
    path = File.join(result_dir, "verify.yaml")
    CascadeResult::YAMLData.new(res).dump_to_file(path)
  end
end
