path_server_definition = File.join(File.dirname(__FILE__), "server.rb")

usage :message => "Verify sink", :server => path_server_definition

default :server => :local, :log => "verify_log"

server :local do |server|
  server.load path_server_definition
end
