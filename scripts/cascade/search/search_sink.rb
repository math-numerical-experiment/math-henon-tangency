require_relative "../../../lib/henon_tangency"

class SearchSink
  attr_reader :estimate

  PRM_B = "-0.3"
  ITERATION_TEST_CONVERGENCE = 10000

  def initialize(estimate)
    @estimate = estimate
  end

  def check_periodic_point_convergence(prms, period, sink, threshold_confirm_sink)
    test_iterate_num = period * 500
    bif_range = Henon::BifRange.new(period, threshold_confirm_sink, test_iterate_num, 1)
    if ary = bif_range.stable_periodic_point?(prms, sink)
      [:found, prms[0], ary[1]]
    else
      nil
    end
  end
  private :check_periodic_point_convergence

  def search_by_iteration_at_parameter(prms, period, range_search, iterate_search, threshold_of_sink, threshold_confirm_sink)
    prms = prms.map { |val| MPFR(val) }
    henon_fr = MPFR::Henon::Mapping.new(prms, period)
    sink_result = henon_fr.search_sink(range_search, iterate_search, threshold_of_sink)
    res = nil
    if sink = sink_result[:found]
      res = check_periodic_point_convergence(prms, period, sink, threshold_confirm_sink)
    elsif sink_result[:not_divergent]
      res = [:not_divergent, sink_result[:not_divergent]]
    end
    res
  end

  def each_parameter(prm_a_start, prm_a_end, prm_step, &block)
    if block_given?
      if prm_a_start > prm_a_end
        prm_a_start, prm_a_end = prm_a_end, prm_a_start
      end
      prm = prm_a_start
      while prm < prm_a_end
        yield(prm)
        prm += prm_step
      end
    else
      to_enum(:each_parameter, prm_a_start, prm_a_end, prm_step, &block)
    end
  end
  private :each_parameter

  def search_by_iteration(prm_a_start, prm_a_end, iter, opts = {})
    period = estimate[:period]
    range_mfds = [-iter[0], estimate[:manifold][0..1], iter[1], estimate[:manifold][2..3]]
    prm_step = estimate[:prm_step].abs
    threshold_confirm_sink = estimate[:basin_width] * MPFR("1e-10") # For now, we take very small value
    distance_peak_error = estimate[:basin_width] / 50

    x_peak = nil
    y = nil
    x_range_length = estimate[:x_range_length]
    x_step = estimate[:x_step]

    # We test property of Cauchy sequence for a point iterated 50 times, i.e., period * 50.
    # Maximum iteration is 100.
    iterate_search = [50, 100]
    # Two threshold values that correspond to convergence and not convergence respectively.
    threshold_of_sink = [estimate[:basin_width] * 10, estimate[:basin_width] / 10]

    res = nil
    point_not_divergent = nil

    prm_range = (prm_a_start < prm_a_end ? [prm_a_start, prm_a_end] : [prm_a_end, prm_a_start])
    peak_pts = prm_range.map do |prm|
      prms = [prm, PRM_B]
      henon = MPFR::Henon::Mapping.new(prms)
      tangent_crd_range = TangentCrdRange.new(henon, InvMfd::Calculator.create_for_fp(prms, 1, MPFR), InvMfd::Calculator.create_for_fp(prms, 2, MPFR))
      st_mfd, un_mfd, range_stable_tmp, range_unstable_tmp = tangent_crd_range.coordinate_range(*range_mfds)

      calc = DistanceUnstablePeak.new(Intersect::Determine.new(distance_peak_error, MPFR("1e-3")), distance_peak_error)
      intersection_find_p = calc.set_mfds(st_mfd, un_mfd)
      new_dist = calc.distance(distance_peak_error)
      calc.peak_pt
    end
    peak_pt_step = (peak_pts[1] - peak_pts[0]).div_scalar(((prm_a_start - prm_a_end).abs / prm_step).ceil)

    peak_pt = peak_pts[0]
    each_parameter(prm_a_start, prm_a_end, prm_step).with_index do |prm, i|
      begin
        prms = [prm, PRM_B]
        peak_pt = peak_pt + peak_pt_step
        x_peak = peak_pt[0]
        y = peak_pt[1]
        x_start = x_peak - x_range_length
        x_end = x_peak + x_range_length

        if search_result = search_by_iteration_at_parameter(prms, period, [x_start, y, x_end, x_step, 0], iterate_search, threshold_of_sink, threshold_confirm_sink)
          case search_result[0]
          when :found
            res = search_result
          when :not_divergent
            point_not_divergent = search_result
          end
        end
      rescue => err
        $stderr.puts "Error raises at parameter\n#{prms[0]}\n#{prms[1]}"
        raise
      end
      break if res
    end
    res || point_not_divergent
  end

  def search_by_horseshoe_at_parameter(prms, period, range_mfds, mfd_error, threshold_confirm_sink)
    prms = prms.map { |val| MPFR(val) }
    res = nil
    henon = MPFR::Henon::Mapping.new(prms)
    tangent_crd_range = TangentCrdRange.new(henon, InvMfd::Calculator.create_for_fp(prms, 1, MPFR), InvMfd::Calculator.create_for_fp(prms, 2, MPFR))
    st_mfd, un_mfd, range_stable_tmp, range_unstable_tmp = tangent_crd_range.coordinate_range(*range_mfds)

    calc_determine = Intersect::Determine.new(mfd_error, MPFR("1e-3"))
    search_periodic_point = SearchPeriodicPoint.new(calc_determine, st_mfd, un_mfd, henon, period)
    if periodic_points = search_periodic_point.search(estimate[:basin_width] / 1000)
      periodic_points.each do |pt|
        if ary = check_periodic_point_convergence(prms, period, pt, threshold_confirm_sink)
          res = ary
          break
        end
      end
      unless res
        res = [:periodic_point_found, prms, periodic_points]
      end
    end
    res
  end

  def search_by_horseshoe(prm_a_start, prm_a_end, iter, opts = {})
    period = estimate[:period]
    range_mfds = [-iter[0], estimate[:manifold][0..1], iter[1], estimate[:manifold][2..3]]
    prm_step = estimate[:prm_step].abs
    mfd_error = estimate[:basin_width] / 50
    threshold_confirm_sink = estimate[:basin_width] * MPFR("1e-10") # For now, we take very small value

    res = nil
    periodic_point_found = nil
    each_parameter(prm_a_start, prm_a_end, prm_step) do |prm|
      begin
        prms = [prm, PRM_B]
        search_result = search_by_horseshoe_at_parameter(prms, period, range_mfds, mfd_error, threshold_confirm_sink)
        if search_result
          case search_result[0]
          when :found
            res = search_result
          when :periodic_point_found
            periodic_point_found = search_result
          end
        end
      rescue => err
        $stderr.puts "Error raises at parameter\n#{prms[0]}\n#{prms[1]}"
        raise
      end
      break if res
    end
    res || periodic_point_found
  end

  def exec(search_type, prm_a_start, prm_a_end, iter, opts = {})
    MPFR.set_default_prec(opts[:prec]) if opts[:prec]
    case search_type
    when :iteration
      search_by_iteration(prm_a_start, prm_a_end, iter, opts)
    when :horseshoe
      search_by_horseshoe(prm_a_start, prm_a_end, iter, opts)
    else
      raise "Invalid search type: #{search_type.inspect.strip}"
    end
  end

  def each_in_estimation_range(prm_step, &block)
    if prm_step < 0
      raise "Negative parameter step size"
    end
    prm = { :pos => estimate[:prm_a_base], :neg => estimate[:prm_a_base] }
    prm_next = nil
    prm_var = MPFR(0)
    while prm_var < estimate[:prm_a_range_length]
      prm_var += prm_step
      prm_next = { :pos => prm[:pos] + prm_step, :neg => prm[:neg] - prm_step }
      yield(prm[:pos], prm_next[:pos])
      # Probably, we does not need to search parameters less than estimation parameter in our situation.
      # yield(prm_next[:neg], prm[:neg])
      prm = prm_next
    end
  end

  def self.estimation_load(path_estimate)
    estimate = CascadeResult::YAMLData.new(YAML.load_file(path_estimate))
    estimate[:prm_a_range_length] = estimate[:prm_stable_range] * 200
    estimate[:prm_step] = estimate[:prm_stable_range] / 10
    estimate[:x_range_length] = estimate[:basin_width] * 2000
    estimate[:x_step] = estimate[:basin_width] / 20
    estimate[:prm_step_size] = estimate[:prm_a_range_length] / MPFR(50)
    estimate[:prm_search_start] = estimate[:prm_a_base]
    estimate[:prm_search_end] = estimate[:prm_a_base] + estimate[:prm_a_range_length]
    estimate
  end
end
