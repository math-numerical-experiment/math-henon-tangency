require_relative "search_sink"

DRbQS.option_parser("Usage of server") do |opt, hash_opts|
  hash_opts[:prec] = nil
  hash_opts[:log_level] = Logger::FATAL
  MPFR::Henon.command_option_define(opt, hash_opts, :only => [:prec, :log_level])
end

PREC_FIRST = 256

DRbQS.define_server(:finish_exit => true) do |server, argv, hash_opts|
  unless (search_type_str = argv[0]) && (file_estimate = argv[1]) && (file_output = argv[2])
    raise "Specify estimation file and output file"
  end

  MPFR.set_default_prec(PREC_FIRST)
  unless hash_opts[:prec]
    estimate = SearchSink.estimation_load(file_estimate)
    hash_opts[:prec] = (-MPFR::Math.log2(estimate[:basin_width]) + 8 * estimate[:period] + 100).ceil
  end
  prec = hash_opts[:prec]
  MPFR.set_default_prec(prec)
  estimate = SearchSink.estimation_load(file_estimate)
  puts "Precision: #{prec}"

  server.set_initialization_task(DRbQS::Task.new(MPFR, :set_default_prec, :args => [prec]))
  server.task_generator do |gen|
    obj = SearchSink.new(estimate)
    iter = File.basename(File.dirname(file_estimate)).split("_").map(&:to_i)
    search_type = search_type_str.intern
    obj.each_in_estimation_range(estimate[:prm_step_size]) do |prm, prm_next|
      gen.create_add(obj, :exec, :args => [search_type, prm, prm_next, iter, hash_opts]) do |srv, ret|
        # When a sink is found, the server exits.
        if ret
          case ret.shift
          when :found
            prm_sink, pt_sink = ret
            Kernel.open(file_output, "w") do |f|
              f.puts prm_sink.to_strf("%.Re")
              f.puts pt_sink[0].to_strf("%.Re")
              f.puts pt_sink[1].to_strf("%.Re")
            end
            srv.exit
          when :not_divergent
            puts "Not convergent completely"
            p ret
          when :periodic_point_found
            puts "Periodic points found, but not convergent"
            p ret
          end
        end
      end
    end
  end
end
