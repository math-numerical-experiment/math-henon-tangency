require_relative "sink_property"

help_message =<<HELP
Usage: ruby #{File.basename(__FILE__)}

  Examples of PERIOD_TARGET are "10..20", "10,14,18", or "15"
HELP

params = {
  :data_directory => nil,
  :prec => 256,
  :log_level => Logger::FATAL,
  :sequence_period => :all,
  :iterate => nil,
  :max_period => nil,
  :force_calculate => nil,
  :map => nil,
  :search => nil,
  :verify => nil,
  :delete_prop => nil,
  :delete_cache => false,
  :show => nil,
  :mpfr_format => nil,
  :bifurcation_diagram => nil,
  :power_law_diagram => nil,
  :estimation_accuracy => nil,
  :target => nil
}

def get_period_target(v)
  case v
  when /^(\d+)\.{2,3}(\d+)$/
    Range.new(Regexp.last_match[1].to_i, Regexp.last_match[2].to_i)
  when /^\d+$/
    [v.to_i]
  when /^\d+(,\d+)*$/
    v.split(",").map(&:to_i)
  else
    raise "Invalid option of target of period: #{v}"
  end
end

begin
  OptionParser.new(help_message) do |opt|
    MPFR::Henon.command_option_define(opt, params, :only => [:prec, :log_level])

    opt.on("-s TYPE", "--sequence-period TYPE", String, "Specify ODD or EVEN") do |v|
      case v
      when /^odd/i
        params[:sequence_period] = :odd
      when /^even/i
        params[:sequence_period] = :even
      else
        params[:sequence_period] = :all
      end
    end
    opt.on("-I ITERATES", "--iterate ITERATES", String, "Set the iterations of stable and unstable manifolds. Specify two numbers like '15 38'") do |v|
      params[:iterate] = v.split.map(&:to_i)
    end
    opt.on("-F RANGE", "--force-calc PERIOD_TARGET", String, "Force calculate of some properties") do |v|
      params[:force_calculate] = get_period_target(v)
    end
    opt.on("--map NUM", Integer, "Map tangency nearer the saddle") do |v|
      params[:map] = v
    end
    opt.on("--search-by-iteration MAX_PERIOD", Integer, "Search next sink by iteration. Specify maximum period of sink when searching") do |v|
      params[:search] = :iteration
      params[:max_period] = v
    end
    opt.on("--search-by-horseshoe MAX_PERIOD", Integer, "Search next sink by horseshoe. Specify maximum period of sink when searching") do |v|
      params[:search] = :horseshoe
      params[:max_period] = v
    end
    opt.on("--verify VERIFY_TYPE", String, "Verify specified sinks") do |v|
      params[:verify] = v
    end
    opt.on("--delete-cache", "Delete cache") do |v|
      params[:delete_cache] = true
    end
    opt.on("--delete-property KEYS", String, "Delete properties of which keys are specified comma separated string") do |v|
      params[:delete_prop] = v.split(",").map(&:intern)
    end
    opt.on("--bifurcation-diagram", "Create bifurcation diagram") do |v|
      params[:bifurcation_diagram] = true
    end
    opt.on("--power-law-diagram", "Create diagram of power law") do |v|
      params[:power_law_diagram] = true
    end
    opt.on("-t PERIOD_TARGET", "--target PERIOD_TARGET", String, "Set target periods") do |v|
      params[:target] = get_period_target(v)
    end
    opt.on("--estimation-accuracy", "Show accuracy of estimation for obtained sinks") do |v|
      params[:estimation_accuracy] = true
    end
    opt.on("--show TYPE", String, "Show properties of synks") do |v|
      params[:show] = v.downcase.intern
      unless SinkProp::COMMANDS_SHOW.include?(params[:show])
        raise "Invalid property type"
      end
    end
    opt.on("--list-iteration", "Show list of iterations of manifolds") do |v|
      params[:list_iteration] = v
    end
    opt.on("--mpfr-format STR", String, "Format string of outputing") do |v|
      params[:mpfr_format] = v
    end
    opt.on("-D DIR", "--data-directory DIR", String, "Set the data directory") do |v|
      params[:data_directory] = v
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

MPFR.set_default_prec(params[:prec])
CACHE_DIRECTORY = File.join(__dir__, "cache.tmp")
CascadeResult.root = params[:data_directory] || File.join(__dir__, "result")

if params[:list_iteration]
  dirs = Dir.glob(File.join(CascadeResult.root, "*")).map { |dir| File.basename(dir) }
  dirs.sort.each do |dir|
    puts dir
  end
  exit
elsif params[:iterate]
  iter_stable, iter_unstable = params[:iterate]
else
  dirs = Dir.glob(File.join(CascadeResult.root, "*")).map { |dir| File.basename(dir) }
  iter_stable, iter_unstable = dirs.sort.first.split("_").map(&:to_i)
end

result = CascadeResult.new(iter_stable, iter_unstable)

if params[:map]
  # Map all sinks
  # result.refine!(2)
  result.map_and_save!(params[:map], [:mfd_tangency])
elsif params[:delete_cache]
  if File.exist?(CACHE_DIRECTORY)
    puts "Delete cache: #{CACHE_DIRECTORY}"
    FileUtils.rm_r(CACHE_DIRECTORY)
  else
    puts "Cache does not exist: #{CACHE_DIRECTORY}"
  end
else
  prop = SinkProp.new(result, params)
  if params[:estimation_accuracy]
    prop.estimation_accuracy(params[:target])
  elsif params[:verify]
    raise "Periods are not specified" unless params[:target]
    prop.verify(result, params[:verify], params[:target])
  elsif params[:delete_prop]
    raise "Periods are not specified" unless params[:target]
    prop.delete_prop(params[:target])
  elsif params[:bifurcation_diagram]
    raise "Periods are not specified" unless params[:target]
    prop.bifurcation_diagram(params[:target])
  elsif params[:power_law_diagram]
    prop.power_law_diagram
  elsif params[:show]
    prop.show(result, params[:show], :format => params[:mpfr_format])
  else
    unless params[:sequence_period]
      $stderr.puts "Specify a sequence by the option '--sequence-period'"
      exit(2)
    end
    if params[:search]
      prop.search_sink(params[:search])
    else
      prop.calc_prop(params[:target])
    end
  end
end
