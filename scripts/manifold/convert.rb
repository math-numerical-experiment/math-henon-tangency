require_relative "../../lib/henon_tangency"

params = {
  :mode => :map,
  :prec => 256,
  :output => nil,
  :log_level => Logger::FATAL,
  :save_log => false,
  :distance => "1e-4",
  :max_iteration => nil,
  :compress => false
}

help_message =<<HELP
Convert manifold source file created by manifold.rb.
Mode of conversion
  map
    Map source points on manifold
  reduce
    Reduce additional information and puts only coordinates of points
  test
    Test source files of manifolds

usage:
  ruby #{File.basename(__FILE__)} --mode map -o output.dat source.dat
  ruby #{File.basename(__FILE__)} --mode reduce -o output.dat source.dat
  ruby #{File.basename(__FILE__)} --mode test source.dat [source2.dat ...]
HELP

begin
  OptionParser.new(help_message) do |opt|
    MPFR::Henon.command_option_define(opt, params, :only => [:prec, :log_level])

    opt.on("-M NUM", "--max-iteration NUM", Integer, "Set the length of the manifold by the maximum iteration number") do |v|
      params[:max_iteration] = v
    end
    opt.on("-d DIST", "--min-distance DIST", String, "Set the minimum distance between points on manifold") do |v|
      params[:distance] = v
    end
    opt.on("-o OUT", "--output OUT", String, "Set the output file") do |v|
      params[:output] = v
    end
    opt.on("--save-log", "Save log to file.") do |v|
      params[:save_log] = v
    end
    opt.on("-m MODE", "--mode MODE", String, "Set the mode of conversion: map or reduce") do |v|
      case v
      when /^map$/i
        params[:mode] = :map
      when /^reduce$/i
        params[:mode] = :reduce
      when /^test$/i
        params[:mode] = :test
      else
        raise "Invalid mode: #{v}"
      end
    end
    opt.on("--compress", "Compress output file") do |v|
      params[:compress] = true
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

if params[:mode] == :test
  file_paths = ARGV
  file_paths.each do |path|
    puts path
    check = CreateMfdPt::CheckPtFile.new(path)
    if check.valid?
      puts "Valid"
    else
      puts "Invalid file data: #{path}"
      puts "  #{check.max_angle}"
    end
  end
  exit(0)
end

unless input_file = ARGV[0]
  $stderr.puts "No input file"
  exit(1)
end

if params[:output]
  output_file = params[:output]
else
  output_file_name = "#{params[:mode]}_#{File.basename(input_file)}".sub(/\.gz$/, "")
  output_file = File.join(File.dirname(input_file), output_file_name)
end
output_file = FileName.create(output_file, :position => :middle)

MPFR.set_default_prec(params[:prec])

case params[:mode]
when :map
  dir = File.dirname(output_file)
  ManageLog.set_log_conf(CreateMfdPt::MapPtFile,
                         :level => params[:log_level],
                         :io => (params[:save_log] ? FileName.create(File.join(dir, "log_convert.txt")) : STDOUT))
  min_distance = MPFR(params[:distance])
  obj = CreateMfdPt::MapPtFile.new(input_file, MPFR.new(min_distance))
  obj.set_max_iteration(params[:max_iteration]) if params[:max_iteration]
  filenames = obj.map_file(output_file)
  if params[:compress]
    filenames.each do |path|
      if File.exist?(path)
        system("gzip", "--best", path)
      end
    end
  end
when :reduce
  InvMfd::PtDataFile.reduce_additional_info(input_file, output_file)
  if params[:compress] && File.exist?(output_file)
    system("gzip", "--best", output_file)
  end
end
