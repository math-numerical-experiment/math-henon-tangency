# Usage

To get an invariant manifold, we need to carry out two steps.
Firstly, manifold.rb gets points on an invariant manifold in the neighborhood of the fixed point.
Secondly, convert.rb maps the points calculated by manifold.rb and gets long manifold.
