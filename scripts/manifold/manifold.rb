require_relative "../../lib/henon_tangency"

params = {
  :param_a => "1.4",
  :param_b => "-0.3",
  :branch => 0,
  :max_iteration => nil,
  :distance => "1e-4",
  :prec => 256,
  :directory => nil,
  :log_level => Logger::FATAL,
  :save_log => false,
  :compress => false
}

help_message =<<HELP
Calculate an invariant manifold of fixed points of Henon map
near the constants #{params[:param_a]} and #{params[:param_b]}.
If MODE is from 0 to 3 we get an invariant manifold of the fixed point whose x-coordinate is positive.
Otherwise, we get an invariant manifold of another fixed point.

usage: ruby #{File.basename(__FILE__)} --mode MODE
HELP

begin
  OptionParser.new(help_message) do |opt|
    MPFR::Henon.command_option_define(opt, params, :only => [:prec, :param_a, :param_b, :log_level])

    opt.on("-m NUM", "--mode NUM", Integer, "Specify a branch of the invariant manifolds by an integer from 0 to 7") do |v|
      params[:branch] = v
    end
    opt.on("-M NUM", "--max-iteration NUM", Integer, "Set the length of the manifold by the maximum iteration number") do |v|
      params[:max_iteration] = v
    end
    opt.on("-d DIST", "--distance DIST", String, "Set the distance between two points on manifold") do |v|
      params[:distance] = v
    end
    opt.on("-o DIR", "--output DIR", String, "Set the output directory") do |v|
      params[:directory] = v
    end
    opt.on("--save-log", "Save log to file.") do |v|
      params[:save_log] = v
    end
    opt.on("--compress", "Compress output file") do |v|
      params[:compress] = true
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

MPFR.set_default_prec(params[:prec])

if params[:branch] < 0 || params[:branch] > 7
  $stderr.puts "Mode number is an integer from 0 to 7"
  exit(1)
elsif params[:branch] == 3 || params[:branch] == 5
  $stderr.puts "The mode number #{params[:branch]} is a missing number. Alternatively, we use the mode #{params[:branch] - 1}"
  exit(1)
end

unless params[:max_iteration]
  case params[:branch]
  when 0
    params[:max_iteration] = 18
  when 1
    params[:max_iteration] = 18
  when 2
    params[:max_iteration] = 42
  when 4
    params[:max_iteration] = 12
  when 6
    params[:max_iteration] = 22
  when 7
    params[:max_iteration] = 20
  end
end

dir = FileName.create(params[:directory] || "manifold_#{params[:branch]}", :directory => :self)
puts "Output to #{dir}"
filename = FileName.create(File.join(dir, "manifold.txt"))
error = MPFR.new(params[:distance])

ManageLog.set_log_conf(SrcInvMfd::CalcPtWithError,
                       :level => params[:log_level],
                       :io => (params[:save_log] ? FileName.create(File.join(dir, "log.txt")) : STDOUT))

const = [MPFR(params[:param_a]), MPFR(params[:param_b])]
obj = SrcInvMfd::CalcPtWithError.new(InvMfd::Calculator.create_for_fp(const, params[:branch], MPFR), params[:max_iteration])
obj.set_initial_points
obj.set_save_file(filename)
obj.calc_mfd(error)

if params[:compress] && File.exist?(obj.save_file)
  system("gzip", "--best", obj.save_file)
end
