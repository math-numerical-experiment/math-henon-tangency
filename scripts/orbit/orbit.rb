require_relative "../../lib/henon_tangency"

help_message =<<HELP
Show or save the orbit of Henon map.
HELP

params = {
  :mode => :show,
  :prec => 64,
  :transient => 0,
  :interval => 0.01,
  :param_a => "1.4",
  :param_b => "-0.3",
  :point => ["0", "0"],
  :length => 100,
  :period => 1,
  :style => MPFRPlot::STY_POINTS,
  :continue => false,
  :output => nil
}

begin
  OptionParser.new(help_message) do |opt|
    MPFR::Henon.command_option_define(opt, params, :only => [:prec, :param_a, :param_b, :period])

    opt.on("-l NUM", "--length NUM", Integer, "Set the length of orbit. Default is #{params[:length]}") do |v|
      params[:length] = v
    end
    opt.on("-i NUM", "--interval NUM", Float, "Set the interval time. Default is #{params[:interval]}") do |v|
      params[:interval] = v
    end
    opt.on("-t NUM", "--transient NUM", Integer, "Set the transient number. Default is #{params[:transient]}") do |v|
      params[:transient] = v
    end
    opt.on("-P POINT", "--point POINT", String, "Set the initial point by 'a b'. Default is '#{params[:point].join(' ')}'") do |v|
      params[:point] = v.split(" ")
    end
    opt.on("-s STYLE", "--style STYLE", String, "Set the style of points: points or dots. Default is points") do |v|
      case v
      when /^points$/i
        params[:style] = MPFRPlot::STY_POINTS
      when /^dots$/i
        params[:style] = MPFRPlot::STY_DOTS
      else
        raise "Invalid style: #{v}"
      end
    end
    opt.on("--save OUT", String, "Save orbit to file") do |v|
      params[:mode] = :save
      params[:output] = v
    end
    opt.on("--continue", "Calculate orbit endlessly") do |v|
      params[:continue] = true
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

MPFR.set_default_prec(params[:prec])
henon = MPFR::Henon::Mapping.new([params[:param_a], params[:param_b]], params[:period])
pt = henon.iterate_map(MPFR::ColumnVector.new(params[:point]), params[:transient])

case params[:mode]
when :show
  MPFRGL.init
  obj = MPFR::Obj2D.new(:style => params[:style], :type => MPFRPlot::TYPE_FR, :data => [pt])
  MPFRGL.add_object(obj)

  MPFRGL.start_thread
  sleep(params[:interval])

  params[:length].times do |i|
    pt = henon.iterate_map(pt)
    obj.add(pt)
    obj.changed_notify_display_observer
    sleep(params[:interval])
  end

  if params[:continue]
    loop do
      pt = henon.iterate_map(pt)
      obj.delete_at(0)
      obj.add(pt)
      obj.changed_notify_display_observer
      sleep(params[:interval])
    end
  else
    puts "Finish calculation of orbit"
    sleep
  end
when :save
  path = FileName.create(params[:output], :position => :middle)
  puts "Save to #{path}"
  open(path, "w") do |f|
    henon.each_pt_on_orbit(pt, params[:length]) do |orbit_pt|
      f.puts orbit_pt.to_s
    end
  end
end
