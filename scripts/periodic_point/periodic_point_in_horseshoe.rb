require_relative "../../lib/henon_tangency"

params = {
  :param_a => "1.392706035147",
  :param_b => "-0.3",
  :prec => 256,
  :log_level => Logger::FATAL,
  :period => 13,
  :range_stable => ['9.468999999999999999999999999999999999999999999999999999999999999999999999999966e-01',
                    '9.497999999999999999999999999999999999999999999999999999999999999999999999999982e-01'],
  :range_unstable => ['1.89300000000000000000000000000000000000000000000000000000000000000000000000001e-01',
                      '2.212999999999999999999999999999999999999999999999999999999999999999999999999993e-01'],
  :iter_stable => 15,
  :iter_unstable => 38,
  :error => "1e-20",
  :output => nil
}

help_message =<<HELP
Search periodic points in horseshoes near a homoclinic tangency.
HELP

begin
  OptionParser.new(help_message) do |opt|
    MPFR::Henon.command_option_define(opt, params, :only => [:prec, :param_a, :param_b, :period, :log_level])
    opt.on("-E NUM", "--error NUM", String, "Set maximum error") do |v|
      params[:error] = v
    end
    opt.on("-I NUMS", "--iterate NUMS", String, "Iteration numbers of stable and unstable manifolds that is specified by comma separated numbers") do |v|
      params[:iter_stable], params[:iter_unstable] = v.split(",").map(&:to_i)
    end
    opt.on("-S NUMS", "--stable-manifold NUMS", String, "Coordinate range on stable manifold that is specified by comma separated numbers") do |v|
      params[:range_stable] = v.split(",")
    end
    opt.on("-U NUMS", "--unstable-manifold NUMS", String, "Coordinate range on unstable manifold that is specified by comma separated numbers") do |v|
      params[:range_unstable] = v.split(",")
    end
    opt.on("--output DIR", String, "Output coordinates and manifolds to the directory") do |v|
      params[:output] = v
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

ManageLog.set_log_conf(SearchPeriodicPoint, :level => params[:log_level], :io => STDOUT)

MPFR.set_default_prec(params[:prec])

prms = [params[:param_a], params[:param_b]]
henon = MPFR::Henon::Mapping.new(prms)
period = params[:period]
iter_stable = params[:iter_stable]
iter_unstable = params[:iter_unstable]
range_stable = params[:range_stable].map { |val| MPFR(val) }
range_unstable = params[:range_unstable].map { |val| MPFR(val) }
tangent_crd_range = TangentCrdRange.new(henon, InvMfd::Calculator.create_for_fp(prms, 1, MPFR), InvMfd::Calculator.create_for_fp(prms, 2, MPFR))
st_mfd, un_mfd, range_stable, range_unstable = tangent_crd_range.coordinate_range(-iter_stable, range_stable, iter_unstable, range_unstable)

error = MPFR(params[:error])
calc_determine = Intersect::Determine.new(error, MPFR("1e-3"))
search_periodic_point = SearchPeriodicPoint.new(calc_determine, st_mfd, un_mfd, henon, period)
periodic_points = search_periodic_point.search(error)

if periodic_points
  puts "#{periodic_points.size} periodic points have been found!"
  periodic_points.each do |pt|
    puts "Coordinate:"
    puts pt[0]
    puts pt[1]
    puts "Eigenvalues:"
    henon.jacobian_matrix_iterate_map(pt, period).eigenvalue.each do |val|
      puts val
    end
    print "\n"
  end
else
  puts "Not find periodic points"
end

if params[:output]
  dir = FileName.create(params[:output], :directory => :self, :position => :middle)
  puts "Output to #{dir}"
  if periodic_points
    Kernel.open(File.join(dir, "periodic_points.txt"), "w") do |f|
      periodic_points.each do |pt|
        f.puts pt
      end
    end
  end
  st_mfd, un_mfd = search_periodic_point.manifolds
  st_mfd.output_to_file(File.join(dir, "stable_manifold.txt"))
  un_mfd.output_to_file(File.join(dir, "unstable_manifold.txt"))
end
