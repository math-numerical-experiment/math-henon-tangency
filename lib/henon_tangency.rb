require "pp"

autoload :OptionParser, "optparse"
autoload :Benchmark, "benchmark"
autoload :Zlib, "zlib"
autoload :YAML, "yaml"

gem "filename"
autoload :FileName, "filename"

require "mpfr"
require "mpfr/matrix"
require "mpc"
require "mpfi"
require "mpfi/matrix"
require "mpfi/complex"

require "math_utils"

module HenonTangency
  class LoadLibrary
    def initialize
      @lib_names = []
    end

    def find_libs
      Dir.glob(File.join(__dir__, "*")).each do |path|
        if File.directory?(path)
          dir_name = File.basename(path)
          if dir_name == "henon_tangency"
            $LOAD_PATH << path
            Dir.glob(File.join(path, "*.rb")).each do |name|
              @lib_names << File.basename(name)
            end
          else
            lib_dir_p = false
            [File.join(path, "lib"), File.join(path, "ext")].each do |dir|
              if File.exist?(dir)
                $LOAD_PATH << dir
                lib_dir_p = true
              end
            end
            if lib_dir_p
              @lib_names << File.basename(path).gsub(/-/, "_")
            end
          end
        end
      end
    end
    private :find_libs

    def load_libs
      @lib_names.each do |name|
        begin
          require name
        rescue LoadError => err
          p err
          $stderr.puts "Can not load #{name}. Maybe, we need to compile extended library."
        end
      end
    end
    private :load_libs

    def load
      find_libs
      load_libs
    end

    def self.load
      unless @load_lib
        @load_lib = HenonTangency::LoadLibrary.new
        @load_lib.load
      end
    end
  end
end

HenonTangency::LoadLibrary.load
