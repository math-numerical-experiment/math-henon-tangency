#include "ruby_mpfi_henon.h"

void Init_ruby_mpfi_henon()
{
  mpfi_henon_module = rb_define_module_under(rb_define_class("MPFI", rb_cNumeric), "Henon");

  mpfi_henon_mapping = rb_define_class_under(mpfi_henon_module, "MapN", rb_cObject);

  initialize_mpfi_henon_mapping(mpfi_henon_mapping);

  mpfi_mfd_calculator = rb_define_class_under(mpfi_henon_module, "MfdCalculator", rb_cObject);
  initialize_mpfi_mfd_calc(mpfi_mfd_calculator);
}
