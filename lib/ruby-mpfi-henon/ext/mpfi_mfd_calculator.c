#include "ruby_mpfi_henon.h"

static VALUE sym_pos, sym_neg, __mpfi_class__;
static ID eqq;
static int sym_stable_id, sym_unstable_id;

static VALUE r_mpfi_mfd_calculator_alloc (VALUE self)
{
  MPFICalcMfdData *data;
  self = Data_Make_Struct(mpfi_mfd_calculator, MPFICalcMfdData, 0, mpfi_clear_calc_mfd_data, data);
  mpfi_init_calc_mfd_data(data);
  return self;
}

/* arguments
  cst: array of String or MPFI
  period: Integer
  pt: MPFI::ColumnVector
  basis: array of MPFI::ColumnVector
  type: :stable or :unstable
  flip: true or nil
*/
static VALUE r_mpfi_mfd_calculator_initialize  (VALUE self, VALUE cst_ary, VALUE period, VALUE pt, VALUE basis_ary, VALUE type, VALUE flip)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_cst, *ptr_pt, *ptr_basis1, *ptr_basis2;
  int sym = rb_to_id(type);
  MfdType mtype;
  bool cflip;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_matrix_temp_alloc_init(ptr_cst, 2, 1);

  if (RTEST(rb_funcall(__mpfi_class__, eqq, 1, rb_ary_entry(cst_ary, 0)))) {
    MPFI *ptr_cst_ary0;
    r_mpfi_get_struct(ptr_cst_ary0, rb_ary_entry(cst_ary, 0));
    mpfi_set(mpfi_matrix_get_ptr(ptr_cst, 0), ptr_cst_ary0);
  } else {
    r_mpfi_set_robj(mpfi_matrix_get_ptr(ptr_cst, 0), rb_ary_entry(cst_ary, 0));
  }
  if (RTEST(rb_funcall(__mpfi_class__, eqq, 1, rb_ary_entry(cst_ary, 1)))) {
    MPFI *ptr_cst_ary1;
    r_mpfi_get_struct(ptr_cst_ary1, rb_ary_entry(cst_ary, 1));
    mpfi_set(mpfi_matrix_get_ptr(ptr_cst, 1), ptr_cst_ary1);
  } else {
    r_mpfi_set_robj(mpfi_matrix_get_ptr(ptr_cst, 1), rb_ary_entry(cst_ary, 1));
  }

  r_mpfi_get_matrix_struct(ptr_pt, pt);
  r_mpfi_get_matrix_struct(ptr_basis1, rb_ary_entry(basis_ary, 0));
  r_mpfi_get_matrix_struct(ptr_basis2, rb_ary_entry(basis_ary, 1));

  if (sym_stable_id == sym) {
    mtype = STABLE;
  } else if (sym_unstable_id == sym) {
    mtype = UNSTABLE;
  } else {
    rb_raise(rb_eArgError, "Invalid symbol of type.");
  }

  cflip = (RTEST(flip) ? true : false);

  mpfi_set_calc_mfd_data(data, ptr_cst, NUM2INT(period), ptr_pt, ptr_basis1, ptr_basis2, mtype, cflip);

  r_mpfi_matrix_temp_free(ptr_cst);
  return Qtrue;
}

static VALUE r_mpfi_mfd_calculator_initialize_copy (VALUE self, VALUE other)
{
  MPFICalcMfdData *ptr_self, *ptr_other;
  Data_Get_Struct(self, MPFICalcMfdData, ptr_self);
  Data_Get_Struct(other, MPFICalcMfdData, ptr_other);

  mpfi_set_calc_mfd_data(ptr_other, ptr_self->cst, ptr_self->fr_calc->period, ptr_self->pt, ptr_self->basis[0], ptr_self->basis[1],
			 ptr_self->fr_calc->type, ptr_self->fr_calc->flip);
  return Qtrue;
}

static VALUE r_mpfi_mfd_calculator_constants (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE v_cst1, v_cst2;
  MPFI *fi_cst1, *fi_cst2;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_make_struct_init(v_cst1, fi_cst1);
  r_mpfi_make_struct_init(v_cst2, fi_cst2);
  mpfi_set(fi_cst1, mpfi_matrix_get_ptr(data->cst, 0));
  mpfi_set(fi_cst2, mpfi_matrix_get_ptr(data->cst, 1));
  return rb_ary_new3(2, v_cst1, v_cst2);
}

static VALUE r_mpfi_mfd_calculator_fr_constants (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE v_cst3, v_cst4;
  MPFR *fr_cst1, *fr_cst2;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_make_struct_init(v_cst3, fr_cst1);
  r_mpfr_make_struct_init(v_cst4, fr_cst2);
  mpfr_set(fr_cst1, mpfr_matrix_get_ptr(data->fr_calc->cst, 0), GMP_RNDN);
  mpfr_set(fr_cst2, mpfr_matrix_get_ptr(data->fr_calc->cst, 1), GMP_RNDN);
  return rb_ary_new3(2, v_cst3, v_cst4);
}

static VALUE r_mpfi_mfd_calculator_period (VALUE self)
{
  MPFICalcMfdData *data;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  return INT2NUM(data->fr_calc->period);
}

static VALUE r_mpfi_mfd_calculator_type (VALUE self)
{
  MPFICalcMfdData *data;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  return (data->fr_calc->type == STABLE ? ID2SYM(sym_stable_id) : ID2SYM(sym_unstable_id));
}

static VALUE r_mpfi_mfd_calculator_pt (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE fi_pt;
  MPFIMatrix *ptr_fi_pt;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_matrix_suitable_matrix_init(&fi_pt, &ptr_fi_pt, data->pt->row, data->pt->column);
  mpfi_matrix_set(ptr_fi_pt, data->pt);
  return fi_pt;
}

static VALUE r_mpfi_mfd_calculator_fr_pt (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE fr_pt;
  MPFRMatrix *ptr_fr_pt;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&fr_pt, &ptr_fr_pt, data->pt->row, data->pt->column);
  mpfr_matrix_set(ptr_fr_pt, data->fr_calc->pt);
  return fr_pt;
}

static VALUE r_mpfi_mfd_calculator_basis (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE v_basis0, v_basis1;
  MPFIMatrix *fi_basis0, *fi_basis1;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_matrix_suitable_matrix_init(&v_basis0, &fi_basis0, data->basis[0]->row, data->basis[0]->column);
  r_mpfi_matrix_suitable_matrix_init(&v_basis1, &fi_basis1, data->basis[1]->row, data->basis[1]->column);
  mpfi_matrix_set(fi_basis0, data->basis[0]);
  mpfi_matrix_set(fi_basis1, data->basis[1]);
  return rb_ary_new3(2, v_basis0, v_basis1);
}

static VALUE r_mpfi_mfd_calculator_fr_basis (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE v_basis2, v_basis3;
  MPFRMatrix *fr_basis0, *fr_basis1;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&v_basis2, &fr_basis0, data->basis[0]->row, data->basis[0]->column);
  r_mpfr_matrix_suitable_matrix_init(&v_basis3, &fr_basis1, data->basis[1]->row, data->basis[1]->column);
  mpfr_matrix_set(fr_basis0, data->fr_calc->basis[0]);
  mpfr_matrix_set(fr_basis1, data->fr_calc->basis[1]);
  return rb_ary_new3(4, v_basis2, v_basis3);
}

static VALUE r_mpfi_mfd_calculator_flip (VALUE self)
{
  MPFICalcMfdData *data;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  return (data->fr_calc->flip ? Qtrue : Qnil);
}

static VALUE r_mpfi_mfd_calculator_init_iteration (VALUE self)
{
  MPFICalcMfdData *data;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  return INT2NUM(data->fr_calc->init_iteration);
}

static VALUE r_mpfi_mfd_calculator_normal_vec (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE r_vec;
  MPFRMatrix *c_vec;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_vec, &c_vec, data->fr_calc->normal_vec->row, data->fr_calc->normal_vec->column);
  mpfr_matrix_set(c_vec, data->fr_calc->normal_vec);
  return r_vec;
}

static VALUE r_mpfi_mfd_calculator_criterion (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE ret[4];
  MPFR *c_val[4];
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_make_struct_init(ret[0], c_val[0]);
  r_mpfr_make_struct_init(ret[1], c_val[1]);
  r_mpfr_make_struct_init(ret[2], c_val[2]);
  r_mpfr_make_struct_init(ret[3], c_val[3]);

  mpfr_set(c_val[0], data->fr_calc->criterion[0], GMP_RNDN);
  mpfr_set(c_val[1], data->fr_calc->criterion[1], GMP_RNDN);
  mpfr_set(c_val[2], data->fr_calc->criterion[2], GMP_RNDN);
  mpfr_set(c_val[3], data->fr_calc->criterion[3], GMP_RNDN);
  return rb_ary_new4(4, ret);
}

static VALUE r_mpfi_mfd_calculator_base_two_points (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE r_base_two_points0, r_base_two_points1;
  MPFRMatrix *c_base_two_points0, *c_base_two_points1;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_base_two_points0, &c_base_two_points0, data->fr_calc->base_two_points[0]->row, data->fr_calc->base_two_points[0]->column);
  r_mpfr_matrix_suitable_matrix_init(&r_base_two_points1, &c_base_two_points1, data->fr_calc->base_two_points[1]->row, data->fr_calc->base_two_points[1]->column);
  mpfr_matrix_set(c_base_two_points0, data->fr_calc->base_two_points[0]);
  mpfr_matrix_set(c_base_two_points1, data->fr_calc->base_two_points[1]);
  return rb_ary_new3(2, r_base_two_points0, r_base_two_points1);
}

static VALUE r_mpfi_mfd_calculator_base_vector (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE r_base_vector;
  MPFRMatrix *ptr_base_vector;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_base_vector, &ptr_base_vector, data->fr_calc->base_vector->row, data->fr_calc->base_vector->column);
  mpfr_matrix_set(ptr_base_vector, data->fr_calc->base_vector);
  return r_base_vector;
}

static VALUE r_mpfi_mfd_calculator_base_vector_size (VALUE self)
{
  MPFICalcMfdData *data;
  VALUE r_base_vector_size;
  MPFR *c_base_vector_size;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_make_struct_init(r_base_vector_size, c_base_vector_size);
  mpfr_set(c_base_vector_size, data->fr_calc->base_vector_size, GMP_RNDN);
  return r_base_vector_size;
}

static VALUE r_mpfi_mfd_calculator_direction_by_mapping (VALUE self, VALUE pt, VALUE initial_iteration)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_pt;
  MapDirection ret_dir;
  int ret_iteration;
  VALUE ret_dir_val = Qnil;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_get_matrix_struct(ptr_pt, pt);
  mpfi_mfd_direction_by_mapping(&ret_dir, &ret_iteration, ptr_pt, NUM2INT(initial_iteration), data);
  if (ret_dir == DIR_POS) {
    ret_dir_val = sym_pos;
  } else if (ret_dir == DIR_NEG) {
    ret_dir_val = sym_neg;
  }
  return rb_ary_new3(2, ID2SYM(ret_dir_val), INT2NUM(ret_iteration));
}

static VALUE r_mpfi_mfd_calculator_extend_line (VALUE self, VALUE near_pos, VALUE near_neg)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_near_pos, *ptr_near_neg;
  VALUE ret_pos, ret_neg;
  MPFIMatrix *ptr_ret_pos, *ptr_ret_neg;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_get_matrix_struct(ptr_near_pos, near_pos);
  r_mpfi_get_matrix_struct(ptr_near_neg, near_neg);
  r_mpfi_matrix_suitable_matrix_init(&ret_pos, &ptr_ret_pos, 2, 1);
  r_mpfi_matrix_suitable_matrix_init(&ret_neg, &ptr_ret_neg, 2, 1);
  mpfi_mfd_extend_line(ptr_ret_pos, ptr_ret_neg, ptr_near_pos, ptr_near_neg, data);
  return rb_ary_new3(2, ret_pos, ret_neg);
}

static VALUE r_mpfi_mfd_calculator_bisect_points (VALUE self, VALUE pt_pos, VALUE pt_neg)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_pt_pos, *ptr_pt_neg;
  VALUE ret_pt;
  MPFIMatrix *ptr_ret_pt;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_get_matrix_struct(ptr_pt_pos, pt_pos);
  r_mpfi_get_matrix_struct(ptr_pt_neg, pt_neg);
  r_mpfi_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  mpfi_mfd_bisect_points(ptr_ret_pt, ptr_pt_pos, ptr_pt_neg, data);
  return ret_pt;
}

static VALUE r_mpfi_mfd_calculator_box_including_mfd (VALUE self, VALUE ratio)
{
  MPFICalcMfdData *data;
  MPFR *ptr_ratio;
  VALUE ret_pt;
  MPFIMatrix *ptr_ret_pt;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_get_struct(ptr_ratio, ratio);
  r_mpfi_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  mpfi_mfd_box_including_mfd(ptr_ret_pt, ptr_ratio, data);
  return ret_pt;
}

static VALUE r_mpfi_mfd_calculator_get_mfd_point (VALUE self, VALUE pt_start)
{
  MPFICalcMfdData *data;
  MPFRMatrix *ptr_pt_start, *ptr_tmp;
  VALUE ret_pt, ret_ratio;
  MPFIMatrix *ptr_ret_pt;
  MPFR *ptr_ret_ratio;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pt_start, pt_start);
  r_mpfr_matrix_temp_alloc_init(ptr_tmp, 2, 1);
  r_mpfi_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  r_mpfr_make_struct_init(ret_ratio, ptr_ret_ratio);

  mpfi_mfd_box_including_mfd2(ptr_ret_pt, ptr_pt_start, data);
  mpfi_matrix_mid(ptr_tmp, ptr_ret_pt);
  mpfr_mfd_ratio_of_point(ptr_ret_ratio, ptr_tmp, data->fr_calc);

  r_mpfr_matrix_temp_free(ptr_tmp);
  return rb_ary_new3(2, ret_pt, ret_ratio);
}

static VALUE r_mpfi_mfd_calculator_get_mfd_point_from_ratio (VALUE self, VALUE ratio)
{
  MPFICalcMfdData *data;
  VALUE ret_pt, ret_ratio;
  MPFIMatrix *ptr_ret_pt;
  MPFR *ptr_ratio, *ptr_ret_ratio;
  MPFRMatrix *ptr_tmp;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  r_mpfr_get_struct(ptr_ratio, ratio);
  r_mpfr_make_struct_init(ret_ratio, ptr_ret_ratio);
  r_mpfr_matrix_temp_alloc_init(ptr_tmp, 2, 1);
  mpfi_mfd_box_including_mfd(ptr_ret_pt, ptr_ratio, data);
  mpfi_matrix_mid(ptr_tmp, ptr_ret_pt);
  mpfr_mfd_ratio_of_point(ptr_ret_ratio, ptr_tmp, data->fr_calc);
  r_mpfr_matrix_temp_free(ptr_tmp);
  return rb_ary_new3(2, ret_pt, ret_ratio);
}

static VALUE r_mpfi_mfd_calculator_get_pt_between_two_pts (VALUE self, VALUE pt1, VALUE pt2)
{
  MPFICalcMfdData *data;
  MPFRMatrix *ptr_tmp_fr;
  VALUE ret_pt, ret_ratio;
  MPFIMatrix *ptr_ret_pt, *ptr_pt1, *ptr_pt2, *ptr_tmp_fi;
  MPFR *ptr_ret_ratio;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfr_matrix_temp_alloc_init(ptr_tmp_fr, 2, 1);
  r_mpfi_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  r_mpfi_get_matrix_struct(ptr_pt1, pt1);
  r_mpfi_get_matrix_struct(ptr_pt2, pt2);
  r_mpfi_matrix_temp_alloc_init(ptr_tmp_fi, 2, 1);
  r_mpfr_make_struct_init(ret_ratio, ptr_ret_ratio);

  mpfi_vector_midpoint(ptr_tmp_fi, ptr_pt1, ptr_pt2);
  mpfi_matrix_mid(ptr_tmp_fr, ptr_tmp_fi);
  mpfi_mfd_box_including_mfd2(ptr_ret_pt, ptr_tmp_fr, data);
  mpfi_matrix_mid(ptr_tmp_fr, ptr_ret_pt);
  mpfr_mfd_ratio_of_point(ptr_ret_ratio, ptr_tmp_fr, data->fr_calc);

  r_mpfr_matrix_temp_free(ptr_tmp_fr);
  r_mpfi_matrix_temp_free(ptr_tmp_fi);
  return rb_ary_new3(2, ret_pt, ret_ratio);
}

static VALUE r_mpfi_mfd_calculator_get_ratio(VALUE self, VALUE pt)
{
  MPFICalcMfdData *data;
  VALUE ret_ratio;
  MPFR *ratio;
  MPFIMatrix *ptr_pt;
  MPFRMatrix *ptr_mid;
  Data_Get_Struct(self, MPFICalcMfdData, data);

  r_mpfr_make_struct_init(ret_ratio, ratio);
  r_mpfi_get_matrix_struct(ptr_pt, pt);
  r_mpfr_matrix_temp_alloc_init(ptr_mid, 2, 1);
  mpfi_matrix_mid(ptr_mid, ptr_pt);
  mpfr_mfd_ratio_of_point(ratio, ptr_mid, data->fr_calc);
  r_mpfr_matrix_temp_free(ptr_mid);
  return ret_ratio;
}

static VALUE r_mpfi_mfd_calculator_in_divergent_region_p (VALUE self, VALUE pt)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_pt;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_get_matrix_struct(ptr_pt, pt);
  if (data->fr_calc->type == STABLE) {
    MPFRMatrix *ptr_fr;
    bool res;
    r_mpfr_matrix_temp_alloc_init(ptr_fr, 2, 1);
    mpfi_matrix_mid(ptr_fr, ptr_pt);
    res = mpfr_mfd_in_divergent_region(ptr_fr, data->fr_calc);
    r_mpfr_matrix_temp_free(ptr_fr);
    if (res) {
      return Qtrue;
    } else {
      return Qnil;
    }
  } else {
    return Qnil;
  }
}

static VALUE r_mpfi_mfd_calculator_iteration_in_non_divergent_region (VALUE self, VALUE pt)
{
  MPFICalcMfdData *data;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  if (data->fr_calc->type == UNSTABLE) {
    return rb_ary_new3(2, INT2NUM(-1), self);
  } else {
    MPFIMatrix *ptr_pt;
    MPFRMatrix *ptr_fr, *ptr_mapped;
    VALUE val;
    int iteration;
    r_mpfi_get_matrix_struct(ptr_pt, pt);
    r_mpfr_matrix_temp_alloc_init(ptr_fr, 2, 1);
    mpfi_matrix_mid(ptr_fr, ptr_pt);
    r_mpfr_matrix_suitable_matrix_init(&val, &ptr_mapped, 2, 1);
    iteration = mpfr_mfd_iteration_in_non_divergent_region(ptr_mapped, ptr_fr, data->fr_calc);
    r_mpfr_matrix_temp_free(ptr_fr);
    return rb_ary_new3(2, INT2NUM(iteration), val);
  }
}

static VALUE r_mpfi_mfd_calculator_interspace (VALUE self, VALUE pt1, VALUE pt2)
{
  MPFIMatrix *ptr_pt1, *ptr_pt2;
  MPFRMatrix *ptr_fr1, *ptr_fr2;
  VALUE val_interspace;
  MPFR *interspace;
  r_mpfi_get_matrix_struct(ptr_pt1, pt1);
  r_mpfi_get_matrix_struct(ptr_pt2, pt2);
  r_mpfr_matrix_temp_alloc_init(ptr_fr1, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_fr2, 2, 1);
  mpfi_matrix_mid(ptr_fr1, ptr_pt1);
  mpfi_matrix_mid(ptr_fr2, ptr_pt2);
  r_mpfr_make_struct_init(val_interspace, interspace);
  mpfr_matrix_vector_distance(interspace, ptr_fr1, ptr_fr2);
  r_mpfr_matrix_temp_free(ptr_fr1);
  r_mpfr_matrix_temp_free(ptr_fr2);
  return val_interspace;
}

static VALUE r_mpfi_mfd_calculator_make_fr_cache (VALUE self, VALUE pt, VALUE num)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_pt;
  MPFRMatrix *ptr_pt_mid, *ptr_ret;
  VALUE val_ret;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_get_matrix_struct(ptr_pt, pt);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_mid, 2, 1);
  r_mpfr_matrix_suitable_matrix_init(&val_ret, &ptr_ret, 2, 1);
  mpfi_matrix_mid(ptr_pt_mid, ptr_pt);
  r_mpfr_henon_iteration(ptr_ret, ptr_pt_mid, data->fr_calc->cst, (data->fr_calc->type == STABLE ? -NUM2INT(num) : NUM2INT(num)));
  r_mpfr_matrix_temp_free(ptr_pt_mid);
  return val_ret;
}

static VALUE r_mpfi_mfd_calculator_make_fi_cache (VALUE self, VALUE pt, VALUE num)
{
  MPFICalcMfdData *data;
  MPFIMatrix *ptr_pt, *ptr_ret;
  VALUE val_ret;
  Data_Get_Struct(self, MPFICalcMfdData, data);
  r_mpfi_get_matrix_struct(ptr_pt, pt);
  r_mpfi_matrix_suitable_matrix_init(&val_ret, &ptr_ret, 2, 1);
  r_mpfi_henon_iteration(ptr_ret, ptr_pt, data->cst, (data->fr_calc->type == STABLE ? -NUM2INT(num) : NUM2INT(num)));
  return val_ret;
}

void initialize_mpfi_mfd_calc(VALUE mpfi_mfd_calculator)
{
  rb_define_alloc_func(mpfi_mfd_calculator, r_mpfi_mfd_calculator_alloc);
  rb_define_private_method(mpfi_mfd_calculator, "initialize", r_mpfi_mfd_calculator_initialize, 6);
  rb_define_private_method(mpfi_mfd_calculator, "initialize_copy", r_mpfi_mfd_calculator_initialize_copy, 1);
  
  rb_define_method(mpfi_mfd_calculator, "constants", r_mpfi_mfd_calculator_constants, 0);
  rb_define_method(mpfi_mfd_calculator, "fr_constants", r_mpfi_mfd_calculator_fr_constants, 0);
  rb_define_method(mpfi_mfd_calculator, "period", r_mpfi_mfd_calculator_period, 0);
  rb_define_method(mpfi_mfd_calculator, "type", r_mpfi_mfd_calculator_type, 0);
  rb_define_method(mpfi_mfd_calculator, "pt", r_mpfi_mfd_calculator_pt, 0);
  rb_define_method(mpfi_mfd_calculator, "fr_pt", r_mpfi_mfd_calculator_fr_pt, 0);
  rb_define_method(mpfi_mfd_calculator, "basis", r_mpfi_mfd_calculator_basis, 0);
  rb_define_method(mpfi_mfd_calculator, "fr_basis", r_mpfi_mfd_calculator_fr_basis, 0);
  rb_define_method(mpfi_mfd_calculator, "flip", r_mpfi_mfd_calculator_flip, 0);
  rb_define_method(mpfi_mfd_calculator, "init_iteration", r_mpfi_mfd_calculator_init_iteration, 0);
  rb_define_method(mpfi_mfd_calculator, "normal_vec", r_mpfi_mfd_calculator_normal_vec, 0);
  rb_define_method(mpfi_mfd_calculator, "criterion", r_mpfi_mfd_calculator_criterion, 0);
  rb_define_method(mpfi_mfd_calculator, "base_two_points", r_mpfi_mfd_calculator_base_two_points, 0);
  rb_define_method(mpfi_mfd_calculator, "base_vector", r_mpfi_mfd_calculator_base_vector, 0);
  rb_define_method(mpfi_mfd_calculator, "base_vector_size", r_mpfi_mfd_calculator_base_vector_size, 0);
  
  rb_define_method(mpfi_mfd_calculator, "direction_by_mapping", r_mpfi_mfd_calculator_direction_by_mapping, 2);
  rb_define_method(mpfi_mfd_calculator, "extend_line", r_mpfi_mfd_calculator_extend_line, 2);
  rb_define_method(mpfi_mfd_calculator, "bisect_points", r_mpfi_mfd_calculator_bisect_points, 2);
  rb_define_method(mpfi_mfd_calculator, "box_including_mfd", r_mpfi_mfd_calculator_box_including_mfd, 1);
  rb_define_method(mpfi_mfd_calculator, "get_mfd_point", r_mpfi_mfd_calculator_get_mfd_point, 1);
  rb_define_method(mpfi_mfd_calculator, "get_mfd_point_from_ratio", r_mpfi_mfd_calculator_get_mfd_point_from_ratio, 1);
  rb_define_method(mpfi_mfd_calculator, "get_pt_between_two_pts", r_mpfi_mfd_calculator_get_pt_between_two_pts, 2);
  rb_define_method(mpfi_mfd_calculator, "get_ratio", r_mpfi_mfd_calculator_get_ratio, 1);
  rb_define_method(mpfi_mfd_calculator, "in_divergent_region?", r_mpfi_mfd_calculator_in_divergent_region_p, 1);
  rb_define_method(mpfi_mfd_calculator, "iteration_in_non_divergent_region", r_mpfi_mfd_calculator_iteration_in_non_divergent_region, 1);
  rb_define_method(mpfi_mfd_calculator, "interspace", r_mpfi_mfd_calculator_interspace, 2);
  rb_define_method(mpfi_mfd_calculator, "make_fr_cache", r_mpfi_mfd_calculator_make_fr_cache, 2);
  rb_define_method(mpfi_mfd_calculator, "make_fi_cache", r_mpfi_mfd_calculator_make_fi_cache, 2);

  sym_stable_id = rb_intern("stable");
  sym_unstable_id = rb_intern("unstable");
  eqq = rb_intern("===");
  sym_pos = rb_intern("pos");
  sym_neg = rb_intern("neg");
  __mpfi_class__ = rb_eval_string("MPFI");
}
