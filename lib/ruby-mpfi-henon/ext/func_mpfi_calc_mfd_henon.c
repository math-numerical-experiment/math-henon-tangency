#include "func_mpfi_calc_mfd_henon.h"
#include "../../ruby-mpfr-henon/ext/func_mpfr_mfd_const.h"
#include "../../ruby-mpfi-matrix-extra/ext/ruby_mpfi_matrix_extra.h"

#define SUFFICIENT_ERROR_FACTOR 64
#define MAX_EXTEND_TRIAL 100
#define MAX_BISECTION 500

void mpfi_init_calc_mfd_data (MPFICalcMfdData *data)
{
  data->fr_calc = ALLOC_N(MPFRCalcMfdData, 1);
  mpfr_init_calc_mfd_data(data->fr_calc);
  r_mpfi_matrix_temp_alloc_init(data->cst, 2, 1);
  r_mpfi_matrix_temp_alloc_init(data->pt, 2, 1);
  r_mpfi_matrix_temp_alloc_init(data->basis[0], 2, 1);
  r_mpfi_matrix_temp_alloc_init(data->basis[1], 2, 1);
}

void mpfi_clear_calc_mfd_data (MPFICalcMfdData *data)
{
  mpfr_clear_calc_mfd_data(data->fr_calc);
  free(data->fr_calc);
  r_mpfi_matrix_temp_free(data->cst);
  r_mpfi_matrix_temp_free(data->pt);
  r_mpfi_matrix_temp_free(data->basis[0]);
  r_mpfi_matrix_temp_free(data->basis[1]);
}

static void mpfi_mfd_sufficient_small_error (MPFR *error)
{
  mpfr_set_ui(error, mpfr_get_default_prec(), GMP_RNDN);
  mpfr_exp2(error, error, GMP_RNDN);
  mpfr_ui_div(error, 1, error, GMP_RNDN);
  mpfr_mul_ui(error, error, SUFFICIENT_ERROR_FACTOR, GMP_RNDN);
}

void mpfi_set_calc_mfd_data (MPFICalcMfdData *data, MPFIMatrix *fi_cst, int period,
                             MPFIMatrix *fi_pt, MPFIMatrix *fi_basis1, MPFIMatrix *fi_basis2,
                             MfdType type, bool flip)
{
  MPFRMatrix *fr_cst, *fr_pt, *fr_basis1, *fr_basis2;
  MPFR *error;
  r_mpfr_matrix_temp_alloc_init(fr_cst, 2, 1);
  r_mpfr_matrix_temp_alloc_init(fr_pt, 2, 1);
  r_mpfr_matrix_temp_alloc_init(fr_basis1, 2, 1);
  r_mpfr_matrix_temp_alloc_init(fr_basis2, 2, 1);

  mpfi_matrix_mid(fr_cst, fi_cst);
  mpfi_matrix_mid(fr_pt, fi_pt);
  mpfi_matrix_mid(fr_basis1, fi_basis1);
  mpfi_matrix_mid(fr_basis2, fi_basis2);

  r_mpfr_temp_alloc_init(error);
  mpfi_mfd_sufficient_small_error(error);
  mpfr_set_calc_mfd_data(data->fr_calc, fr_cst, period, fr_pt, fr_basis1, fr_basis2, type, flip, error);

  r_mpfr_temp_free(error);
  r_mpfr_matrix_temp_free(fr_cst);
  r_mpfr_matrix_temp_free(fr_pt);
  r_mpfr_matrix_temp_free(fr_basis1);
  r_mpfr_matrix_temp_free(fr_basis2);

  mpfi_matrix_set(data->cst, fi_cst);
  mpfi_matrix_set(data->pt, fi_pt);
  mpfi_matrix_set(data->basis[0], fi_basis1);
  mpfi_matrix_set(data->basis[1], fi_basis2);
}

void mpfi_mfd_direction_by_mapping (MapDirection *ret_dir, int *ret_iteration, MPFIMatrix *pt_arg,
                                    int initial_iteration, MPFICalcMfdData *data)
{
  MPFIMatrix *pt, *coef, *last_coef, *vec;
  int i;
  r_mpfi_matrix_temp_alloc_init(pt, 2, 1);
  r_mpfi_matrix_temp_alloc_init(coef, 2, 1);
  r_mpfi_matrix_temp_alloc_init(last_coef, 2, 1);
  r_mpfi_matrix_temp_alloc_init(vec, 2, 1);

  r_mpfi_henon_iteration(pt, pt_arg, data->cst, (data->fr_calc->init_iteration - 1) * data->fr_calc->iteration_for_calc);
  mpfi_matrix_sub(vec, pt, data->pt);
  mpfi_vec2_project(last_coef, vec, data->basis[0], data->basis[1]);
  r_mpfi_henon_iteration(pt, pt, data->cst, data->fr_calc->iteration_for_calc);

  *ret_dir = DIR_ERROR;
  for (i = 0; i < MAX_ITERATION; i++) {
    mpfi_matrix_sub(vec, pt, data->pt);
    mpfi_vec2_project(coef, vec, data->basis[0], data->basis[1]);
    if (mpfi_has_zero(mpfi_matrix_get_ptr(coef, 1)) &&
	mpfr_cmp(r_mpfi_right_ptr(mpfi_matrix_get_ptr(last_coef, 1)), r_mpfi_right_ptr(mpfi_matrix_get_ptr(coef, 1))) <= 0 &&
	mpfr_cmp(r_mpfi_left_ptr(mpfi_matrix_get_ptr(last_coef, 1)), r_mpfi_left_ptr(mpfi_matrix_get_ptr(coef, 1))) >= 0) {
      *ret_dir = DIR_UNDECIDED;
      break;
    } else if (mpfr_cmp(r_mpfi_right_ptr(mpfi_matrix_get_ptr(coef, 1)), data->fr_calc->criterion[0]) > 0) {
      *ret_dir = DIR_POS;
      break;
    } else if (mpfr_cmp(r_mpfi_left_ptr(mpfi_matrix_get_ptr(coef, 1)), data->fr_calc->criterion[1]) < 0) {
      *ret_dir = DIR_NEG;
      break;
    }
    mpfi_matrix_set(last_coef, coef);
    r_mpfi_henon_iteration(pt, pt, data->cst, data->fr_calc->iteration_for_calc);
  }
  
  r_mpfi_matrix_temp_free(pt);
  r_mpfi_matrix_temp_free(coef);
  r_mpfi_matrix_temp_free(last_coef);
  r_mpfi_matrix_temp_free(vec);

  if (*ret_dir == DIR_ERROR) {
    printf("Can not determine direction by mpfr_mfd_direction_by_mapping\n");
    abort();
  }

}

static void mpfi_mfd_extend_end_point (MPFIMatrix *ret_pt, MPFIMatrix *pt_arg, MPFIMatrix *vec,
                                       MapDirection dir_sym, MPFICalcMfdData *data)
{
  MapDirection direction;
  int initial_iteration;
  int i;
  mpfi_mfd_direction_by_mapping(&direction, &initial_iteration, pt_arg, data->fr_calc->init_iteration, data);
  mpfi_matrix_set(ret_pt, pt_arg);
  for (i = 0; i < MAX_EXTEND_TRIAL; i++) {
    if (direction == dir_sym) {
      return;
    }
    mpfi_matrix_add(ret_pt, ret_pt, vec);
    mpfi_mfd_direction_by_mapping(&direction, &initial_iteration, ret_pt, data->fr_calc->init_iteration, data);
  }
  printf("Can not extend end point.\n");
  abort();
}

void mpfi_mfd_extend_line (MPFIMatrix *ret_pos, MPFIMatrix *ret_neg, MPFIMatrix *pt_near_pos,
                           MPFIMatrix *pt_near_neg, MPFICalcMfdData *data)
{
  MPFIMatrix *vec;
  r_mpfi_matrix_temp_alloc_init(vec, 2, 1);
  mpfi_matrix_sub(vec, pt_near_pos, pt_near_neg);
  mpfi_mfd_extend_end_point(ret_pos, pt_near_pos, vec, DIR_POS, data);
  mpfi_matrix_neg(vec, vec);
  mpfi_mfd_extend_end_point(ret_neg, pt_near_neg, vec, DIR_NEG, data);
  r_mpfi_matrix_temp_free(vec);
}

static void mpfi_mfd_box_not_going_to_pos_or_neg (MPFIMatrix *ret_undec, MPFIMatrix *ret_pos, MPFIMatrix *ret_neg,
                                                  MPFIMatrix *pt_pos, MPFIMatrix *pt_neg, MPFICalcMfdData *data)
{
  int initial_iteration[2] = { data->fr_calc->init_iteration, data->fr_calc->init_iteration };
  int tmp_iteration;
  MapDirection dir_sym;
  int i;
  mpfi_matrix_set(ret_pos, pt_pos);
  mpfi_matrix_set(ret_neg, pt_neg);
  for (i = 0; i < MAX_BISECTION; i++) {
    mpfi_vector_midpoint(ret_undec, ret_pos, ret_neg);
    mpfi_matrix_mid_interval(ret_undec, ret_undec);

    mpfi_mfd_direction_by_mapping(&dir_sym, &tmp_iteration, ret_undec,
				  (initial_iteration[0] < initial_iteration[1] ? initial_iteration[0] : initial_iteration[1]), data);
    if (dir_sym == DIR_UNDECIDED) {
      return;
    } else if (dir_sym == DIR_POS) {
      mpfi_matrix_set(ret_pos, ret_undec);
      initial_iteration[0] = tmp_iteration;
    } else if (dir_sym == DIR_NEG) {
      mpfi_matrix_set(ret_neg, ret_undec);
      initial_iteration[1] = tmp_iteration;
    }
  }
  printf("Can not get point which does not go to :pos and :neg.\n");
  mpfr_printf("%.Re\n%.Re\n%.Re\n%.Re\n",
	      r_mpfi_left_ptr(mpfi_matrix_get_ptr(pt_pos, 0)), r_mpfi_right_ptr(mpfi_matrix_get_ptr(pt_pos, 0)),
	      r_mpfi_left_ptr(mpfi_matrix_get_ptr(pt_pos, 1)), r_mpfi_right_ptr(mpfi_matrix_get_ptr(pt_pos, 1)));
  mpfr_printf("%.Re\n%.Re\n%.Re\n%.Re\n",
	      r_mpfi_left_ptr(mpfi_matrix_get_ptr(pt_neg, 0)), r_mpfi_right_ptr(mpfi_matrix_get_ptr(pt_neg, 0)),
	      r_mpfi_left_ptr(mpfi_matrix_get_ptr(pt_neg, 1)), r_mpfi_right_ptr(mpfi_matrix_get_ptr(pt_neg, 1)));
  abort();
}

static void mpfi_mfd_bisection_for_undecided_pt (MPFIMatrix *pt_ret, MPFIMatrix *pt_undec, MPFIMatrix *pt_other,
                                                 MapDirection dir_other, MPFICalcMfdData *data)
{
  MPFIMatrix *pts[2];
  int initial_iteration[2] = { data->fr_calc->init_iteration, data->fr_calc->init_iteration };
  int tmp_iteration;
  MapDirection dir_sym;
  bool end_bisection = false;
  int i;
  r_mpfi_matrix_temp_alloc_init(pts[0], 2, 1);
  r_mpfi_matrix_temp_alloc_init(pts[1], 2, 1);
  mpfi_matrix_set(pts[0], pt_undec);
  mpfi_matrix_set(pts[1], pt_other);

  for (i = 0; i < MAX_BISECTION; i++) {
    if (mpfi_matrix_intersect(pt_ret, pts[0], pts[1]) == 0) {
      mpfi_matrix_union(pt_ret, pts[0], pts[1]);
      end_bisection = true;
      break;
    }
    mpfi_vector_midpoint(pt_ret, pts[0], pts[1]);

    mpfi_mfd_direction_by_mapping(&dir_sym, &tmp_iteration, pt_ret, 
				  (initial_iteration[0] < initial_iteration[0] ? initial_iteration[0] : initial_iteration[1]), data);
    if (dir_sym == dir_other) {
      initial_iteration[1] = tmp_iteration;
      mpfi_matrix_set(pts[1], pt_ret);
    } else if (dir_sym == DIR_UNDECIDED) {
      initial_iteration[0] = tmp_iteration;
      mpfi_matrix_set(pts[0], pt_ret);
    } else {
      printf("Point is mapped to invalid direction.\n");
      abort();
    }
  }

  r_mpfi_matrix_temp_free(pts[0]);
  r_mpfi_matrix_temp_free(pts[1]);
  if (!end_bisection) {
    printf("Can not determine by bisection_for_undecided_pt.\n");
    abort();
  }
}

static void mpfi_mfd_large_x_not_going_to_pos_or_neg (MPFIMatrix *pt_ret, MPFIMatrix *pt_pos, MPFIMatrix *pt_neg,
                                                      MPFIMatrix *pt_undec, MPFICalcMfdData *data)
{
  MPFIMatrix *pts[2];
  r_mpfi_matrix_temp_alloc_init(pts[0], 2, 1);
  r_mpfi_matrix_temp_alloc_init(pts[1], 2, 1);

  mpfi_mfd_bisection_for_undecided_pt(pts[0], pt_undec, pt_pos, DIR_POS, data);
  mpfi_mfd_bisection_for_undecided_pt(pts[1], pt_undec, pt_neg, DIR_NEG, data);

  mpfi_matrix_union(pt_ret, pt_undec, pts[0]);
  mpfi_matrix_union(pt_ret, pt_ret, pts[1]);

  r_mpfi_matrix_temp_free(pts[0]);
  r_mpfi_matrix_temp_free(pts[1]);
}

void mpfi_mfd_bisect_points (MPFIMatrix *pt_ret, MPFIMatrix *pt_pos, MPFIMatrix *pt_neg, MPFICalcMfdData *data)
{
  MPFIMatrix *tmp_undec, *tmp_pos, *tmp_neg;
  r_mpfi_matrix_temp_alloc_init(tmp_undec, 2, 1);
  r_mpfi_matrix_temp_alloc_init(tmp_pos, 2, 1);
  r_mpfi_matrix_temp_alloc_init(tmp_neg, 2, 1);

  mpfi_mfd_box_not_going_to_pos_or_neg(tmp_undec, tmp_pos, tmp_neg, pt_pos, pt_neg, data);
  mpfi_mfd_large_x_not_going_to_pos_or_neg(pt_ret, tmp_pos, tmp_neg, tmp_undec, data);

  r_mpfi_matrix_temp_free(tmp_undec);
  r_mpfi_matrix_temp_free(tmp_pos);
  r_mpfi_matrix_temp_free(tmp_neg);
}

void mpfi_mfd_box_including_mfd (MPFIMatrix *pt_ret, MPFR *ratio, MPFICalcMfdData *data)
{
  MPFRMatrix *fr_pos, *fr_neg;
  MPFIMatrix *fi_pos1, *fi_pos2, *fi_neg1, *fi_neg2;

  r_mpfr_matrix_temp_alloc_init(fr_pos, 2, 1);
  r_mpfr_matrix_temp_alloc_init(fr_neg, 2, 1);

  r_mpfi_matrix_temp_alloc_init(fi_pos1, 2, 1);
  r_mpfi_matrix_temp_alloc_init(fi_pos2, 2, 1);
  r_mpfi_matrix_temp_alloc_init(fi_neg1, 2, 1);
  r_mpfi_matrix_temp_alloc_init(fi_neg2, 2, 1);

  mpfr_mfd_line_crossing_mfd(fr_pos, fr_neg, ratio, data->fr_calc);
  mpfi_matrix_from_mpfr_matrix(fi_pos1, fr_pos);
  mpfi_matrix_from_mpfr_matrix(fi_neg1, fr_neg);
  mpfi_mfd_extend_line(fi_pos2, fi_neg2, fi_pos1, fi_neg1, data);
  mpfi_mfd_bisect_points(pt_ret, fi_pos2, fi_neg2, data);
  
  r_mpfr_matrix_temp_free(fr_pos);
  r_mpfr_matrix_temp_free(fr_neg);
  r_mpfi_matrix_temp_free(fi_pos1);
  r_mpfi_matrix_temp_free(fi_pos2);
  r_mpfi_matrix_temp_free(fi_neg1);
  r_mpfi_matrix_temp_free(fi_neg2);

}

void mpfi_mfd_box_including_mfd2 (MPFIMatrix *pt_ret, MPFRMatrix *pt_start, MPFICalcMfdData *data)
{
  MPFRMatrix *fr_pos, *fr_neg;
  MPFIMatrix *fi_pos1, *fi_pos2, *fi_neg1, *fi_neg2;

  r_mpfr_matrix_temp_alloc_init(fr_pos, 2, 1);
  r_mpfr_matrix_temp_alloc_init(fr_neg, 2, 1);

  r_mpfi_matrix_temp_alloc_init(fi_pos1, 2, 1);
  r_mpfi_matrix_temp_alloc_init(fi_pos2, 2, 1);
  r_mpfi_matrix_temp_alloc_init(fi_neg1, 2, 1);
  r_mpfi_matrix_temp_alloc_init(fi_neg2, 2, 1);

  mpfr_mfd_line_crossing_mfd2(fr_pos, fr_neg, pt_start, data->fr_calc);
  mpfi_matrix_from_mpfr_matrix(fi_pos1, fr_pos);
  mpfi_matrix_from_mpfr_matrix(fi_neg1, fr_neg);
  mpfi_mfd_extend_line(fi_pos2, fi_neg2, fi_pos1, fi_neg1, data);
  mpfi_mfd_bisect_points(pt_ret, fi_pos2, fi_neg2, data);
  
  r_mpfr_matrix_temp_free(fr_pos);
  r_mpfr_matrix_temp_free(fr_neg);
  r_mpfi_matrix_temp_free(fi_pos1);
  r_mpfi_matrix_temp_free(fi_pos2);
  r_mpfi_matrix_temp_free(fi_neg1);
  r_mpfi_matrix_temp_free(fi_neg2);
}
