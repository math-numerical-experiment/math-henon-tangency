#include "ruby_mpfi_henon.h"

static ID mpfi_henon_mapping_const_id;

/* Class Henon::MapBase */
static VALUE mpfi_henon_mapping_alloc (VALUE self)
{
  HenonMPFIMap *a;
  self = Data_Make_Struct(mpfi_henon_mapping, HenonMPFIMap, 0, mpfi_henon_map_data_free, a);
  a->cst = ALLOC_N(MPFIMatrix, 1);
  mpfi_matrix_init(a->cst, 2, 1);
  return self;
}

static VALUE mpfi_henon_mapping_initialize (int argc, VALUE *argv, VALUE self)
{
  HenonMPFIMap *ptr;
  VALUE tmp;
  MPFIMatrix *ptr_tmp;
  Data_Get_Struct(self, HenonMPFIMap, ptr);
  switch(argc) {
  case 1:
    ptr->period = 1;
    break;
  case 2:
    ptr->period = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.new is one or two.");
  }
  switch(TYPE(argv[0])) {
  case T_ARRAY:
    tmp = rb_funcall(r_mpfi_col_vector, rb_intern("new"), 1, argv[0]);
    break;
  case T_STRUCT:
    tmp = rb_funcall(r_mpfi_col_vector, rb_intern("new"), 1, rb_funcall(argv[0], rb_intern("to_a"), 0));
    break;
  default:
    tmp = argv[0];
    break;
  }
  r_mpfi_get_matrix_struct(ptr_tmp, tmp);
  mpfi_matrix_set(ptr->cst, ptr_tmp);

  rb_ivar_set(self, mpfi_henon_mapping_const_id, tmp);

  return Qtrue;
}

static VALUE mpfi_henon_mapping_initialize_copy (VALUE self, VALUE other)
{
  HenonMPFIMap *ptr_self, *ptr_other;
  Data_Get_Struct(self, HenonMPFIMap, ptr_self);
  Data_Get_Struct(other, HenonMPFIMap, ptr_other);

  mpfi_matrix_set(ptr_other->cst, ptr_self->cst);
  ptr_other->period = ptr_self->period;
  
  rb_ivar_set(self, mpfi_henon_mapping_const_id, rb_ivar_get(other, mpfi_henon_mapping_const_id));
  return Qtrue;
}

static VALUE mpfi_henon_mapping_period (VALUE self)
{
  HenonMPFIMap *ptr_self;
  Data_Get_Struct(self, HenonMPFIMap, ptr_self);
  return INT2NUM(ptr_self->period);
}

static VALUE mpfi_henon_mapping_constants (VALUE self)
{
  HenonMPFIMap *ptr_self;
  VALUE v_cst1, v_cst2;
  MPFI *r_cst1, *r_cst2;
  Data_Get_Struct(self, HenonMPFIMap, ptr_self);
  r_mpfi_make_struct_init(v_cst1, r_cst1);
  r_mpfi_make_struct_init(v_cst2, r_cst2);
  mpfi_set(r_cst1, mpfi_matrix_get_ptr(ptr_self->cst, 0));
  mpfi_set(r_cst2, mpfi_matrix_get_ptr(ptr_self->cst, 1));
  return rb_ary_new3(2, v_cst1, v_cst2);
}

/* argv is array having one or two elements. First element is MPFI::ColumnVector instance. */
/* Second element is number of iteration. Default is 1. */
/* Return map MPFI::ColumnVector instance argv[1] times. */
static VALUE mpfi_henon_mapping_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFIMap *ptr_self;
  MPFIMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;
  Data_Get_Struct(self, HenonMPFIMap, ptr_self);

  r_mpfi_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfi_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfi_henon_iteration(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/* argv is array having one or two elements. First element is MPFI::ColumnVector instance. */
/* Second element is number of iteration. Default is ptr_self->period. */
/* Return map MPFI::ColumnVector instance argv[1] * ptr_self->period times. */
static VALUE mpfi_henon_mapping_iterate_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFIMap *ptr_self;
  MPFIMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;
  Data_Get_Struct(self, HenonMPFIMap, ptr_self);

  r_mpfi_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfi_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);

  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.iterate_map is one or two.");
  }
  r_mpfi_henon_iteration(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/*
 * @return [MPFI::SquareMatrix] Return the jacobian matrix of mapping.
 * @note This method does not return the jacobian matrix just one iteration, i.e.,
 *     this method ignores period of the mapping.
 */
static VALUE mpfi_henon_mapping_jacobian_matrix (int argc, VALUE *argv, VALUE self)
{
  HenonMPFIMap *ptr_self;
  MPFIMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFIMap, ptr_self);
  r_mpfi_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfi_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 2);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfi_henon_jacobian_matrix_iterate_map(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/*
 * @return [MPFI::SquareMatrix] Return the jacobian matrix of mapping iterated specified times.
 * @note This method returns jacobian matrix iterated just specified times, i.e.,
 *     this method ignores period of the mapping.
 */
static VALUE mpfi_henon_mapping_jacobian_matrix_iterate_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFIMap *ptr_self;
  MPFIMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFIMap, ptr_self);
  r_mpfi_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfi_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 2);
  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfi_henon_jacobian_matrix_iterate_map(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

void initialize_mpfi_henon_mapping (VALUE mpfi_henon_mapping)
{
  rb_funcall(mpfi_henon_mapping, rb_intern("attr_reader"), 1, rb_str_new2("const"));

  rb_define_alloc_func(mpfi_henon_mapping, mpfi_henon_mapping_alloc);
  rb_define_private_method(mpfi_henon_mapping, "initialize", mpfi_henon_mapping_initialize, -1);
  rb_define_private_method(mpfi_henon_mapping, "initialize_copy", mpfi_henon_mapping_initialize_copy, 1);

  rb_define_method(mpfi_henon_mapping, "period", mpfi_henon_mapping_period, 0);
  rb_define_method(mpfi_henon_mapping, "constants", mpfi_henon_mapping_constants, 0);
  rb_define_method(mpfi_henon_mapping, "map", mpfi_henon_mapping_map, -1);
  rb_define_method(mpfi_henon_mapping, "iterate_map", mpfi_henon_mapping_iterate_map, -1);
  /* rb_define_method(mpfi_henon_mapping, "make_orbit", mpfi_henon_mapping_make_orbit, 2); */
  /* rb_define_method(mpfi_henon_mapping, "each_pt_on_orbit", mpfi_henon_mapping_each_pt_on_orbit, 2); */
  rb_define_method(mpfi_henon_mapping, "jacobian_matrix", mpfi_henon_mapping_jacobian_matrix, -1);
  rb_define_method(mpfi_henon_mapping, "jacobian_matrix_iterate_map", mpfi_henon_mapping_jacobian_matrix_iterate_map, -1);

  mpfi_henon_mapping_const_id = rb_intern("@const");
}
