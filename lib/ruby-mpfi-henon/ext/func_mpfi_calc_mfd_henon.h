#ifndef _FUNC_MPFI_CALC_MFD_HENON_H_
#define _FUNC_MPFI_CALC_MFD_HENON_H_

#include <stdio.h>
#include <stdbool.h>
#include <mpfr.h>
#include <mpfi.h>
#include "ruby_mpfr.h"
#include "ruby_mpfr_matrix.h"
#include "../../ruby-mpfr-henon/ext/ruby_mpfr_henon.h"
#include "ruby_mpfi.h"
#include "ruby_mpfi_matrix.h"
#include "func_mpfi_henon.h"

typedef struct _MPFICalcMfdData {
  MPFRCalcMfdData *fr_calc;

  MPFIMatrix *cst;
  MPFIMatrix *pt;
  MPFIMatrix *basis[2];
} MPFICalcMfdData;

void mpfi_init_calc_mfd_data(MPFICalcMfdData *data);
void mpfi_clear_calc_mfd_data(MPFICalcMfdData *data);
void mpfi_set_calc_mfd_data(MPFICalcMfdData *data, MPFIMatrix *fi_cst, int period, MPFIMatrix *fi_pt, MPFIMatrix *fi_basis1, MPFIMatrix *fi_basis2,
			    MfdType type, bool flip);
void mpfi_mfd_direction_by_mapping(MapDirection *ret_dir, int *ret_iteration, MPFIMatrix *pt_arg, int initial_iteration, MPFICalcMfdData *data);
void mpfi_mfd_extend_line(MPFIMatrix *ret_pos, MPFIMatrix *ret_neg, MPFIMatrix *pt_near_pos, MPFIMatrix *pt_near_neg, MPFICalcMfdData *data);
void mpfi_mfd_bisect_points(MPFIMatrix *pt_ret, MPFIMatrix *pt_pos, MPFIMatrix *pt_neg, MPFICalcMfdData *data);
void mpfi_mfd_box_including_mfd(MPFIMatrix *pt_ret, MPFR *ratio, MPFICalcMfdData *data);
void mpfi_mfd_box_including_mfd2(MPFIMatrix *pt_ret, MPFRMatrix *pt_start, MPFICalcMfdData *data);

#endif /* _FUNC_MPFI_CALC_MFD_HENON_H_ */

