#ifndef _RUBY_MPFI_HENON_H_
#define _RUBY_MPFI_HENON_H_

#include "func_mpfi_henon.h"
#include "func_mpfi_calc_mfd_henon.h"

VALUE mpfi_henon_module, mpfi_henon_mapping, mpfi_mfd_calculator;

void initialize_mpfi_henon_mapping(VALUE mpfi_henon_mapping);

void initialize_mpfi_mfd_calc(VALUE mpfi_mfd_calculator);

#endif /* _RUBY_MPFI_HENON_H_ */

