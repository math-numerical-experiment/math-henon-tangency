#include "func_mpfi_henon.h"


void mpfi_henon_map_data_free(HenonMPFIMap *ptr) {
  mpfi_matrix_clear(ptr->cst);
  free(ptr->cst);
  free(ptr);
}

/* Henon map */
/* x_{n+1} = y_n + 1 - a*x_{n}^2 */
/* y_{n+1} = -b*x_{n} */
/* Set the value getted by mapping x to y. */
/* cst = {CONST1, CONST2} */
void r_mpfi_henon_map(MPFIMatrix *y, MPFIMatrix *x, MPFIMatrix *cst) {
  mpfi_mul(y->data + 1, cst->data + 1, x->data);
  mpfi_neg(y->data + 1, y->data + 1);

  mpfi_mul(y->data, x->data, x->data);
  mpfi_mul(y->data, y->data, cst->data);
  mpfi_neg(y->data, y->data);
  mpfi_add(y->data, x->data + 1, y->data);
  mpfi_add_ui(y->data, y->data, 1);
}

/* Inverse map */
/* x_n = - y_{n+1}/b */
/* y_n = (a*y_{n+1}*y_{n+1})/(b*b) + x_{n+1} - 1.0 */
/* Set the value getted by mapping x to y. */
/* cst = {CONST1, CONST2} */
void r_mpfi_henon_inverse_map(MPFIMatrix * y, MPFIMatrix * x, MPFIMatrix * cst) {
  mpfi_div(y->data, x->data + 1, cst->data + 1);
  mpfi_neg(y->data, y->data);

  mpfi_mul(y->data + 1, cst->data, x->data + 1);
  mpfi_mul(y->data + 1, y->data + 1, x->data + 1);
  mpfi_div(y->data + 1, y->data + 1, cst->data + 1);
  mpfi_div(y->data + 1, y->data + 1, cst->data + 1);
  mpfi_add(y->data + 1, y->data + 1, x->data);
  mpfi_sub_ui(y->data + 1, y->data + 1, 1);
}

void r_mpfi_henon_iteration(MPFIMatrix * b, MPFIMatrix * a, MPFIMatrix * c, int iteration) {
  if (iteration == 0) {
    mpfi_matrix_set(b, a);
  } else {
    int i;
    MPFIMatrix *tmp, *t_ret;
    r_mpfi_matrix_temp_alloc_init(tmp, 2, 1);
    r_mpfi_matrix_temp_alloc_init(t_ret, 2, 1);

    mpfi_matrix_set(tmp, a);

    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	r_mpfi_henon_map(t_ret, tmp, c);
	mpfi_matrix_set(tmp, t_ret);
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	r_mpfi_henon_inverse_map(t_ret, tmp, c);
	mpfi_matrix_set(tmp, t_ret);
      }
    }
    mpfi_matrix_set(b, t_ret);

    r_mpfi_matrix_temp_free(tmp);
    r_mpfi_matrix_temp_free(t_ret);
  }
}

void r_mpfi_henon_jacobian_matrix(MPFIMatrix *jac, MPFIMatrix *pt, MPFIMatrix *constants) {
  mpfi_set_si(jac->data, -2);
  mpfi_mul(jac->data, jac->data, constants->data);
  mpfi_mul(jac->data, jac->data, pt->data);
  mpfi_set_si(jac->data + 2, 1);
  mpfi_neg(jac->data + 1, constants->data + 1);
  mpfi_set_si(jac->data + 3, 0);
}

void r_mpfi_henon_jacobian_matrix_iterate_map (MPFIMatrix *jac, MPFIMatrix *pt, MPFIMatrix *constants, int iteration)
{
  int i;
  MPFIMatrix *pt_new, *pt_old, *jac_tmp, *jac_new;
  if (iteration < 0) {
    fprintf(stderr, "Not implemented: Jacobian matrix of inverse map");
    abort();
  }
  r_mpfi_matrix_temp_alloc_init(pt_new, 2, 1);
  r_mpfi_matrix_temp_alloc_init(pt_old, 2, 1);
  r_mpfi_matrix_temp_alloc_init(jac_tmp, 2, 2);
  r_mpfi_matrix_temp_alloc_init(jac_new, 2, 2);
  mpfi_matrix_set(pt_old, pt);
  r_mpfi_henon_jacobian_matrix(jac, pt, constants);
  for (i = 1; i < iteration; i++) {
    r_mpfi_henon_map(pt_new, pt_old, constants);
    mpfi_matrix_set(pt_old, pt_new);
    r_mpfi_henon_jacobian_matrix(jac_new, pt_old, constants);
    mpfi_matrix_mul(jac_tmp, jac_new, jac);
    mpfi_matrix_set(jac, jac_tmp);
  }
  r_mpfi_matrix_temp_free(pt_new);
  r_mpfi_matrix_temp_free(pt_old);
  r_mpfi_matrix_temp_free(jac_tmp);
  r_mpfi_matrix_temp_free(jac_new);
}

