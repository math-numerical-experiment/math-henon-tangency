#ifndef _FUNC_MPFI_HENON_H_
#define _FUNC_MPFI_HENON_H_

#include <stdio.h>
#include <ruby.h>
#include <mpfr.h>
#include <mpfi.h>
#include "ruby_mpfi_matrix.h"


typedef struct __HenonMPFIMap{
  MPFIMatrix *cst;
  int period;
} HenonMPFIMap;

void mpfi_henon_map_data_free(HenonMPFIMap *ptr);
void r_mpfi_henon_map(MPFIMatrix *y, MPFIMatrix *x, MPFIMatrix *cst);
void r_mpfi_henon_inverse_map(MPFIMatrix * y, MPFIMatrix * x, MPFIMatrix * cst);
void r_mpfi_henon_iteration(MPFIMatrix * b, MPFIMatrix * a, MPFIMatrix * c, int iteration);
void r_mpfi_henon_jacobian_matrix(MPFIMatrix *jac, MPFIMatrix *pt, MPFIMatrix *constants);
void r_mpfi_henon_jacobian_matrix_iterate_map (MPFIMatrix *jac, MPFIMatrix *pt, MPFIMatrix *constants, int iteration);

#endif /* _FUNC_MPFI_HENON_H_ */
