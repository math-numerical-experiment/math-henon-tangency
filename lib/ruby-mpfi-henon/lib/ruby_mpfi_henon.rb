require 'ruby_mpfi_henon.so'

class MPFI
  module Henon
    class MapN
      def fixed_point
        x1 = @const[1] + MPFI.new(1)
        x2 = x1 * x1
        x2 = x2 + MPFI.new(4) * @const[0]
        x2 = MPFI::Math.sqrt(x2)
        y1 = (-x1 + x2) / @const[0] / MPFI.new(2)
        y2 = (-x1 - x2) / @const[0] / MPFI.new(2)
        [MPFI::ColumnVector.new([y1, -@const[1] * y1]),
         MPFI::ColumnVector.new([y2, -@const[1] * y2])]
      end

      def _dump(limit)
        Marshal.dump([constants, period])
      end

      def self._load(s)
        self.new(*Marshal.load(s))
      end
    end
  end
end
