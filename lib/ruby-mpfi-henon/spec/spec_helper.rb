begin
  require 'rspec'
rescue LoadError
  require 'rubygems' unless ENV['NO_RUBYGEMS']
  gem 'rspec'
  require 'rspec'
end

require_relative "../../henon_tangency.rb"

require "ruby_mpfr_henon"
require "ruby_mpfi_henon"
require "mpfr/rspec"
