require_relative 'spec_helper'

describe "when calculating some values" do
  
  before(:all) do
    MPFR.set_default_prec(256)
    @csts = [['1.4', '-0.3'], ['1.25', '-0.3'], [MPFI.new('1.22'), MPFI.new('-0.3')]]
    @maps = @csts.map { |a| MPFI::Henon::MapN.new(a) }
    @maps_fr = @csts.map { |a| (MPFI === a[0] ? MPFR::Henon::Mapping.new(a.map{ |b| b.mid }) : MPFR::Henon::Mapping.new(a)) }
  end

  it "fixed points should be mapped to itself." do
    @maps.each_with_index do |m, i|
      fpts = m.fixed_point
      m.map(fpts[0]).intersect(fpts[0]).should be_true
      m.map(fpts[0], -1).intersect(fpts[0]).should be_true
      m.map(fpts[1]).intersect(fpts[1]).should be_true
      m.map(fpts[1], -1).intersect(fpts[1]).should be_true

      fpts_fr = @maps_fr[i].fixed_point
      fpts.each_with_index { |pt, j| pt.include?(fpts_fr[j]).should be_true }
    end
  end

  it "each element of jacobian matrix should equal partial derivative" do
    pt = MPFI::ColumnVector.new(['0.8', '0.14'])
    @maps.each_with_index do |m, i|
      jac = m.jacobian_matrix(pt)
      jac[0, 0].should eql -MPFI.new(2) * m.const[0] * pt[0]
      jac[0, 1].should eql MPFI.new(1)
      jac[1, 0].should eql -m.const[1]
      jac[1, 1].should eql MPFI.new(0)

      jac_fr = @maps_fr[i].jacobian_matrix(pt.mid)
      jac.include?(jac_fr).should be_true
    end
  end

  it "jacobian matrix of iterated map should be matrix multipled some jacobian matrix" do
    pt = MPFI::ColumnVector.new(['-0.28', '0.77'])
    iterate = 5
    @maps.each_with_index do |m, i|
      jac = m.jacobian_matrix_iterate_map(pt, iterate)
      jac2 = MPFI::SquareMatrix.identity(2)

      jac_fr = @maps_fr[i].jacobian_matrix_iterate_map(pt.mid, iterate)
      jac.include?(jac_fr).should be_true
    end
  end

end

