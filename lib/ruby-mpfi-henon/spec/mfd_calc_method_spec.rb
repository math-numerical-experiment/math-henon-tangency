require_relative 'spec_helper'

MPFR.set_default_prec(256)

describe MPFI::Henon::MfdCalculator, "when calculating points on invariant manifold" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFI::Henon::MapN.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec.neg, uvec]

    type = :stable
    flip = nil

    @calc = MPFI::Henon::MfdCalculator.new(cst, 1, pt, basis, type, flip)
  end

  it "should not get error" do 
    num = 100
    res = []
    lambda { (0..num).each { |i| res << @calc.box_including_mfd(MPFR.new(i.to_f / num)) } }.should_not raise_error
    ratio = []
    (1...(res.size)).each do |i|
      pt = res[i-1].midpoint(res[i]).mid
      res1, ratio1 = @calc.get_mfd_point(pt)
      res2, ratio2 = @calc.get_pt_between_two_pts(res[i-1], res[i])
      ratio1.should == ratio2
      res1.should == res2
      ratio << ratio1
    end
    ratio[0].should > 0
    ratio[0].should < 0.05
    ratio[-1].should > 0.95
    ratio[-1].should < 1.0
    (1...(ratio.size)).each { |i| ratio[i].should > ratio[i-1] }
  end

end

describe MPFI::Henon::MfdCalculator, "when calculating points on invariant manifold" do
  before(:all) do
    cst = ['1.4', '-0.3']
    henon = MPFI::Henon::MapN.new(cst)
    pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(pt).eigenvector
    svec, uvec = vecs
    basis = [svec, uvec]

    type = :stable
    flip = nil

    @calc = MPFI::Henon::MfdCalculator.new(cst, 1, pt, basis, type, flip)
  end

  it "should not get error" do
    num = 100
    (0..num).each do |i| 
      ratio = MPFR.new(i.to_f / num)
      pt1 = @calc.box_including_mfd(ratio)
      pt2, ratio2 = @calc.get_mfd_point_from_ratio(ratio)
      pt1.should == pt2
      @calc.get_ratio(pt1).should == ratio2
    end
  end

end
