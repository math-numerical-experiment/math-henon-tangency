require_relative 'spec_helper'

MPFR.set_default_prec(256)

class MPFI::MfdCalculatorTest
  SUFFICIENT_ERROR_FACTOR = 64
  MAX_EXTEND_TRIAL = 100
  MAX_BISECTION = 500

  RUBY_MAX_ITERATION = 500

  # cst is array of string meaning constant.
  # period is positive integer.
  # pt is MPFR::ColumnVector which is fixed point or periodic point.
  # basis is array of two vectors and first element corresponds to direction for calculation of manifold.
  # type is :stable or :unstable.
  # flip is true if the corresponding eigenvalue is negative, nil otherwise.
  def initialize(cst, period, pt, basis, type, flip)
    @henon = MPFI::Henon::MapN.new(cst)
    @pt = pt
    @basis = basis
    @ruby_iteration_for_calc = 2 * period * (type == :stable ? 1 : -1)
    tmp = @henon.const.mid
    @calc_r = MPFR::Henon::MfdCalculator.new([tmp[0], tmp[1]], period, @pt.mid, basis.map{ |a| a.mid }, type, flip, sufficient_small_error)
  end
  
  def sufficient_small_error
    MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(SUFFICIENT_ERROR_FACTOR)
  end

  def direction_by_mapping(pt, initial_iteration)
    pt = @henon.map(pt, (initial_iteration - 1) * @ruby_iteration_for_calc)
    last_coef = MPFI::Vector2D.project(pt - @pt, @basis[0], @basis[1])
    pt = @henon.map(pt, @ruby_iteration_for_calc)
    i = 0
    ret_dir = nil
    (0...RUBY_MAX_ITERATION).each do |i|
      coef = MPFI::Vector2D.project(pt - @pt, @basis[0], @basis[1])
      if coef[1].has_zero? && last_coef[1].right < coef[1].right && last_coef[1].left > coef[1].left
        ret_dir = :undecided
        break
      elsif coef[1].right > @calc_r.criterion[0]
        ret_dir = :pos
        break
      elsif coef[1].left < @calc_r.criterion[1]
        ret_dir = :neg
        break
      end
      last_coef = coef
      pt = @henon.map(pt, @ruby_iteration_for_calc)
      i += 1
    end
    unless ret_dir
      raise "Can not determine direction by MPFR::MfdCalculatorTest.direction_by_mapping"
    end
    [ret_dir, initial_iteration + i]
  end

  # dir_sym is :pos or :neg.
  # If direction_by_mapping(new_pt) is dir_sym, this method returns new_pt.
  def extend_end_point(pt, vec, dir_sym)
    sym, initial_iteration = direction_by_mapping(pt, @calc_r.init_iteration)
    (0...MAX_EXTEND_TRIAL).each do |i|
      if sym == dir_sym
        return pt
      end
      pt += vec
      sym, initial_iteration = direction_by_mapping(pt, @calc_r.init_iteration)
    end
    raise "Can not extend end point."
  end

  # pt1 is mapped to direction of :pos.
  # pt2 is mapped to direction of :neg.
  def extend_line(pt1, pt2)
    vec = pt2 - pt1
    [extend_end_point(pt1, vec.neg, :pos), extend_end_point(pt2, vec, :neg)]
  end

  # Retrun Hash of which keys are :pos, :neg and :undecided.
  # If return value has key of :undecided, it is invalid.
  def box_not_going_to_pos_or_neg(pt1, pt2)
    pts = { :pos => pt1, :neg => pt2 }
    initial_iteration = [@calc_r.init_iteration, @calc_r.init_iteration]
    (0...MAX_BISECTION).each do |i|
      new = (pts[:pos] + pts[:neg]).mul_scalar(MPFR.new(0.5)).mid_interval
      dir_sym, tmp = direction_by_mapping(new, initial_iteration.min)
      if dir_sym == :undecided
        pts[dir_sym] = new
        return pts
      elsif dir_sym == :pos
        initial_iteration[0] = tmp
      elsif dir_sym == :neg
        initial_iteration[1] = tmp
      end
      pts[dir_sym] = new
    end
    raise "Can not get point which does not go to :pos and :neg."
  end

  def bisection_for_undecided_pt(pt_undec, pt_other, dir_other)
    pts = { dir_other => pt_other, :undecided => pt_undec }
    initial_iteration = [@calc_r.init_iteration, @calc_r.init_iteration]
    (0...MAX_BISECTION).each do |i|
      if pts[:undecided].intersect(pts[dir_other])
        return pts[:undecided].union(pts[dir_other])
      end
      new = pts[dir_other].midpoint(pts[:undecided])
      dir_sym, tmp_iteration = direction_by_mapping(new, initial_iteration.min)
      if dir_sym == dir_other
        initial_iteration[1] = tmp_iteration
        pts[dir_other] = new
      elsif dir_sym == :undecided
        initial_iteration[0] = tmp_iteration
        pts[:undecided] = new
      else
        raise "Point is mapped to invalid direction."
      end
    end
    raise "Can not determine by bisection_for_undecided_pt."
  end

  # hash has keys: :pos, :neg and :undecided.
  def large_box_not_going_to_pos_or_neg(hash)
    pt1 = hash[:undecided]
    pt2 = bisection_for_undecided_pt(hash[:undecided], hash[:pos], :pos)
    pt3 = bisection_for_undecided_pt(hash[:undecided], hash[:neg], :neg)
    (pt1.union(pt2)).union(pt3)
  end

  # pt1 is mapped to positive direction.
  # pt2 is mapped to negative direction.
  def bisect_points(pt1, pt2)
    pts = box_not_going_to_pos_or_neg(pt1, pt2)
    if pts.has_key?(:undecided)
      large_box_not_going_to_pos_or_neg(pts)
    else
      nil
    end
  end

  def box_including_mfd(ratio)
    line = @calc_r.line_crossing_mfd(ratio)
    boxes = line.map { |a| a.to_fi_matrix }
    boxes = extend_line(*boxes)
    bisect_points(*boxes)
  end

end

describe "when calculating points on invariant manifold" do
  before(:all) do
    MPFR.set_default_prec(256)

    cst = ['1.4', '-0.3']
    @cst = cst.map { |a| MPFI.new(a) }
    henon = MPFI::Henon::MapN.new(cst)
    @pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(@pt).eigenvector
    svec, uvec = vecs
    @basis = [svec, uvec]

    @type = :stable
    @flip = nil

    @r_calc = MPFI::MfdCalculatorTest.new(cst, 1, @pt, [svec, uvec], @type, nil)
    @c_calc = MPFI::Henon::MfdCalculator.new(cst, 1, @pt, [svec, uvec], @type, @flip)
  end

  it "should have same variables" do
    @c_calc.constants.should == @cst
    @c_calc.pt.should == @pt
    @c_calc.basis.should == @basis
    @c_calc.type.should == @type
    @c_calc.flip.should == @flip
  end

  it "should get same result by line_crossing_mfd" do 
    (1...100).each do |i|
      ratio = MPFR.new(i.to_f / 100.0)
      rres = @r_calc.box_including_mfd(ratio)
      cres = @c_calc.box_including_mfd(ratio)
      rres.should == cres
    end
  end

end

describe "when initializing MPFI::Henon::MfdCalculator" do
  before(:all) do
    cst = ['1.4', '-0.3']
    @cst = cst.map { |a| MPFI.new(a) }
    henon = MPFI::Henon::MapN.new(@cst)
    @pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(@pt).eigenvector
    svec, uvec = vecs
    @basis = [svec, uvec]

    @type = :stable
    @flip = nil

    @calc = MPFI::Henon::MfdCalculator.new(cst, 1, @pt, @basis, @type, @flip)
  end

  it "should have same variables" do
    @calc.constants.should == @cst
    @calc.pt.should == @pt
    @calc.basis.should == @basis
    @calc.type.should == @type
    @calc.flip.should == @flip
  end

end
