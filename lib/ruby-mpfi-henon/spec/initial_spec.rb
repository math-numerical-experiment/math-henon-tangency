require_relative 'spec_helper'

describe "when MPFI::Henon::MapN is initialized" do
  
  before(:all) do
    MPFR.set_default_prec(128)
    @csts = [[2, 3], ['1.4', '-0.3'], [1.23, -0.3], [MPFI.new('1.5'), MPFI.new('-0.3')]]
    @maps = @csts.map { |a| MPFI::Henon::MapN.new(a) }
  end

  it "constants should return MPFI value." do
    @maps.each_with_index do |m, i|
      m.const.size.should eql 2
      m.const[0].should be_instance_of MPFI
      m.const[1].should be_instance_of MPFI
      m.const[0].should eql MPFI.new(@csts[i][0])
      m.const[1].should eql MPFI.new(@csts[i][1])
    end
  end

end

