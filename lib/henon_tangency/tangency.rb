module Intersect
  Struct.new('IntersectionData', :crd, :ind1, :ind2)
  Struct.new('IntersectionMfdDist', :dist, :ind1, :ind2)
  
  # The instance of class Intersection::Data expresses data of intersection
  # (MPFR::ColumnVector - coordinate, Integer - index of first manifold, Integer - index of second manifold).
  class Data < Struct::IntersectionData
  end

  # The instance of class Intersection::MfdDist expresses distance of two manifolds
  # (MPFR - distance, Integer - index of first manifold, Integer - index of second manifold).
  class MfdDist < Struct::IntersectionMfdDist
  end

  module Function

    # This method calculates the distance between mfd1 and mfd2 and returns Intersection::MfdDist instance.
    def distance_between_two_mfds(mfd1, mfd2)
      Intersect::MfdDist.new(*MPFR::Vector2D.distance_between_lines(mfd1, mfd2))
    end
    module_function :distance_between_two_mfds
    
    # This method calculates intersections of mfd1 and mfd2
    # and returns the array having instance of class Intersection::Data.
    # Note that this method returns nil when there is not intersection.
    # ind_args is array [[i1, i2, i3, i4], ...].
    def get_intersections(mfd1, mfd2, ind_args = [])
      raise ArgumentError, "ind_args must be Array: #{ind_args.class}" unless Array === ind_args
      if ind_args.empty?
        ind_args << [0, mfd1.size - 1, 0, mfd2.size - 1]
      else
        ind_args.each do |ind|
          unless ind[0] >= 0 && ind[1] < mfd1.size && ind[2] >= 0 && ind[3] < mfd2.size
            raise ArgumentError, "Invalid manifold index: #{ind.inspect}, Manifold size: mfd1 #{mfd1.size}, mfd2 #{mfd2.size}"
          end
        end
      end
      res_ary = []
      ind_args.each do |ind|
        res_ary += MPFR::Vector2D.intersection_of_lines2([mfd1, ind[0], ind[1]], [mfd2, ind[2], ind[3]]).map!{ |ary| Intersect::Data.new(*ary) }
      end
      res_ary.size == 0 ? nil : res_ary
    end
    module_function :get_intersections

    # Return [distance, range_of_mfd1_index, range_of_mfd2_index].
    # Points of mfd1 and mfd2 in these index ranges are not apart
    # larger than 'distance' from some points of another manifold.
    def distance_and_close_pair_index(mfd1, mfd2, distance)
      ret = MPFR::Vector2D.close_points_on_lines_base(mfd1, mfd2, distance)
      if ret[0].size > 0
        sary, uary = [], []
        ret[0].each do |ary|
          sary << ary[0]
          uary << ary[1]
        end
        sindex_range = Range.new([0, sary.min - 1].max, [mfd1.size - 1, sary.max+2].min)
        uindex_range = Range.new([0, uary.min - 1].max, [mfd2.size - 1, uary.max+2].min)
        return [ret[1..3], sindex_range, uindex_range]
      end
      return nil
    end
    module_function :distance_and_close_pair_index
    
    def index_ranges_close_pts(mfd1, mfd2, distance)
      ret = MPFR::Vector2D.close_points_on_lines_base(mfd1, mfd2, distance)
      if ret[0].size > 0
        sary, uary = [], []
        ret[0].each do |ary|
          sary << ary[0]
          uary << ary[1]
        end
        return [Range.new([0, sary.min - 1].max, [mfd1.size - 1, sary.max + 1].min), Range.new([0, uary.min - 1].max, [mfd2.size - 1, uary.max + 1].min)]
      end
      return nil
    end
    module_function :index_ranges_close_pts
    
    MAX_ITERATION_CONFIRM_INTERSECT = 500

    def calculate_mfds_for_confirmation(mfd1, mfd2, indexes, error)
      calculated = nil

      if mfd1.calculate_more?
        indexes1 = indexes[0].uniq
        indexes1.sort!
        indexes1.reverse!
        indexes1.each_with_index do |ind, i|
          if MPFR::Vector.distance(mfd1[ind], mfd1[ind + 1]) >= error
            mfd1.calc_with_number(1, :start => ind, :end => (ind + 1))
            indexes[0].each_with_index { |num, j| indexes[0][j] += 1 if num > ind }
            calculated = true
          end
        end  
      end

      if mfd2.calculate_more?
        indexes2 = indexes[1].uniq
        indexes2.sort!
        indexes2.reverse!
        indexes2.each_with_index do |ind, i|
          if MPFR::Vector.distance(mfd2[ind], mfd2[ind + 1]) >= error
            mfd2.calc_with_number(1, :start => ind, :end => (ind + 1))
            indexes[1].each_with_index { |num, j| indexes[1][j] += 1 if num > ind }
            calculated = true
          end
        end  
      end

      if calculated
        mfd1_min = mfd1.size - 1
        mfd2_min = mfd2.size - 1
        indexes[0].each_index.inject([]) do |ary, i|
          ary << [indexes[0][i], [indexes[0][i] + 2, mfd1_min].min, indexes[1][i], [indexes[1][i] + 2, mfd2_min].min]
          ary
        end
      else
        nil
      end
    end
    module_function :calculate_mfds_for_confirmation
    
    # @return [Array] An array of Intersection::Data instance of which error of coordinates is less than argument error
    def confirm_intersect(mfd1, mfd2, intersects, error, logger = nil)
      num_of_iteration = 0
      while intersects
        if num_of_iteration >= MAX_ITERATION_CONFIRM_INTERSECT
          raise "Can not confirm intersection."
        end
        indexes = intersects.inject([[], []]) do |res, dt|
          res[0] << dt.ind1
          res[1] << dt.ind2
          res
        end

        if logger
          logger.debug("[confirm_intersect #{num_of_iteration}] size: #{mfd1.size} #{mfd2.size}, index: #{indexes[0].map{ |a| a.to_s }.join(' ') }, #{indexes[1].map{ |a| a.to_s }.join(' ') }")
        end
        check_indexes = calculate_mfds_for_confirmation(mfd1, mfd2, indexes, error)
        break unless check_indexes
        intersects = get_intersections(mfd1, mfd2, check_indexes)
        if !intersects || intersects.size < check_indexes.size
          tmp_check_indexes = check_indexes.map { |ary| [[ary[0] - 5, 0].max, [ary[1] + 5, mfd1.size - 1].min,
                                                         [ary[2] - 5, 0].max, [ary[3] + 5, mfd2.size - 1].min] }
          i = 1
          while i < tmp_check_indexes.size
            if tmp_check_indexes[i - 1][1] >= tmp_check_indexes[i][0] && tmp_check_indexes[i - 1][3] >= tmp_check_indexes[i][2]
              tmp_check_indexes[i][0] = [tmp_check_indexes[i - 1][0], tmp_check_indexes[i][0]].min
              tmp_check_indexes[i][1] = [tmp_check_indexes[i - 1][1], tmp_check_indexes[i][1]].max
              tmp_check_indexes[i][2] = [tmp_check_indexes[i - 1][2], tmp_check_indexes[i][2]].min
              tmp_check_indexes[i][3] = [tmp_check_indexes[i - 1][3], tmp_check_indexes[i][3]].max
              tmp_check_indexes.delete_at(i - 1)
            else
              i += 1
            end
          end
          intersects = get_intersections(mfd1, mfd2, tmp_check_indexes)
          if !intersects || intersects.size < check_indexes.size
            intersects = get_intersections(mfd1, mfd2)
          end
        end
        num_of_iteration += 1
      end
      intersects
    end
    module_function :confirm_intersect
  end
  
  class Determine
    include Intersect::Function
    
    START_MFD_NUMBER = 10
    MAX_MFD_SIZE = 500
    PORTION_SIZE = 10

    attr_reader :manifolds, :distance

    # cutting is :number, :distance, or nil.
    def initialize(intersection_error, apart_threshold_ratio, cutting = :number, cut_actually = false)
      @intersection_error = intersection_error
      @apart_threshold_ratio = apart_threshold_ratio
      @cutting = cutting
      @logger = ManageLog.get_new_logger(self.class)
      @cut_actually = cut_actually
      unless [:number, :distance].include?(@cutting)
        raise "Invalid cutting type: #{@cutting.inspect}"
      end
      @mfd_max_length = MPFR('1e-3')
    end

    def manifold_portion(mfd, *args)
      if mfd.orbit_length > @mfd_max_length
        portion_arg = args
      else
        portion_arg = [0, *args, mfd.size - 1]
      end
      @logger.debug("manifold_portion: #{portion_arg.inspect}")
      if @cut_actually
        mfd.portion!(*portion_arg)
      else
        mfd.portion(*portion_arg)
      end
    end
    private :manifold_portion

    def initial_calculation(mfd1, mfd2)
      @logger.debug("Initial calculation for mfd1 (#{mfd1.size}) and mfd2 (#{mfd2.size})")
      if mfd1.size > MAX_MFD_SIZE || mfd2.size > MAX_MFD_SIZE
        dist = distance_between_two_mfds(mfd1, mfd2)
        res = distance_and_close_pair_index(mfd1, mfd2, dist.dist * 2)
        mfd1 = manifold_portion(mfd1, res[1]) if mfd1.calculate_more?
        mfd2 = manifold_portion(mfd2, res[2]) if mfd2.calculate_more?
        dist = distance_between_two_mfds(mfd1, mfd2)
      else
        mfd1.calc_with_number(START_MFD_NUMBER) if mfd1.calculate_more?
        mfd2.calc_with_number(START_MFD_NUMBER) if mfd2.calculate_more?
        dist = distance_between_two_mfds(mfd1, mfd2)
      end
      res = distance_and_close_pair_index(mfd1, mfd2, dist.dist * 2)
      if mfd1.calculate_more? &&
          ((ratio = MPFR::Vector.distance(mfd1[res[0][1]], mfd1[res[0][1] + 1]) / MPFR::Vector.distance(mfd2[res[0][2]], mfd2[res[0][2] + 1])) >= 2)
        calc_num = [100, ratio.ceil * (res[1].max - res[1].min)].min
        mfd1.calc_with_number(calc_num, :start => res[1].min, :end => res[1].max)
      end
      if mfd2.calculate_more? &&
          ((ratio = MPFR::Vector.distance(mfd2[res[0][2]], mfd2[res[0][2] + 1]) / MPFR::Vector.distance(mfd1[res[0][1]], mfd1[res[0][1] + 1])) >= 2)
        calc_num = [100, ratio.ceil * (res[2].max - res[2].min)].min
        mfd2.calc_with_number(calc_num, :start => res[2].min, :end => res[2].max)
      end
      [mfd1, mfd2]
    end
    private :initial_calculation
    
    def search_intersections_base(mfd1, mfd2)
      mfd1.calc_with_number(START_MFD_NUMBER) if mfd1.calculate_more?
      mfd2.calc_with_number(START_MFD_NUMBER) if mfd2.calculate_more?
      i = 0
      last_dist = Intersect::MfdDist.new(MPFR.new(100), 0, 0) # The value must be sufficient large.
      ret = nil
      loop do
        i += 1
        if intersections = get_intersections(mfd1, mfd2)
          ret = [:intersection_exist, confirm_intersect(mfd1, mfd2, intersections, @intersection_error, @logger)]
          break
        end
        mfd1.calc_with_number(mfd1.size + 1) if mfd1.calculate_more?
        mfd2.calc_with_number(mfd2.size + 1) if mfd2.calculate_more?

        new_dist = distance_between_two_mfds(mfd1, mfd2)
        @logger.debug("[#{i}]") { "#{new_dist.dist.to_strf("%.18Re")} #{mfd1.size} #{mfd2.size}" }
        decrease = (last_dist.dist - new_dist.dist) / new_dist.dist
        @logger.debug("[decrease]") { "#{decrease.to_strf("%.18Re")}" }
        if decrease < @apart_threshold_ratio
          if (!mfd1.calculate_more? || MPFR::Vector.distance(mfd1[new_dist.ind1], mfd1[new_dist.ind1 + 1]) < new_dist.dist) &&
              (!mfd2.calculate_more? || MPFR::Vector.distance(mfd2[new_dist.ind2], mfd2[new_dist.ind2 + 1]) < new_dist.dist)
            ret = [:intersection_none, new_dist]
            break
          else
            ret = [:intersection_not_found, new_dist]
            break
          end
        end
        last_dist = new_dist
        if @cutting && (mfd1.size > MAX_MFD_SIZE || mfd2.size > MAX_MFD_SIZE)
          ret = [:intersection_not_found, new_dist]
          break
        end
      end
      ret
    end
    private :search_intersections_base

    # @param [IntersectionMfdDist] mfd_dist Distance between two manifolds
    def cut_mfds(mfd1, mfd2, mfd_dist)
      @logger.debug("cut_mfds: #{@cutting.inspect}, mfd1.size = #{mfd1.size}, mfd2.size = #{mfd2.size}")
      case @cutting
      when :number
        if mfd1.calculate_more?
          portion_arg = ([0, mfd_dist.ind1 - PORTION_SIZE].max)..([mfd1.size - 1, mfd_dist.ind1 + PORTION_SIZE].min)
          mfd1 = manifold_portion(mfd1, portion_arg)
        end
        if mfd2.calculate_more?
          portion_arg = ([0, mfd_dist.ind2 - PORTION_SIZE].max)..([mfd2.size - 1, mfd_dist.ind2 + PORTION_SIZE].min)
          mfd2 = manifold_portion(mfd2, portion_arg)
        end
      when :distance
        res = distance_and_close_pair_index(mfd1, mfd2, mfd_dist.dist * 2)
        mfd1 = manifold_portion(mfd1, res[1]) if mfd1.calculate_more?
        mfd2 = manifold_portion(mfd2, res[2]) if mfd2.calculate_more?
      end
      [mfd1, mfd2]
    end
    private :cut_mfds

    # Returned index numbers are not for two manifolds of arguments mfd1 and mfd2.
    # Two manifolds that are calculated to find intersections are given by the instance variable \@manifolds.
    def search_intersections(mfd1, mfd2)
      mfd1, mfd2 = initial_calculation(mfd1, mfd2)
      i = 0
      ret = nil
      while true
        i += 1
        @logger.debug("search_intersections [#{i}] mfd1.size = #{mfd1.size}, mfd2.size = #{mfd2.size}")
        sym_result, result = search_intersections_base(mfd1, mfd2)
        @distance = (:sym_result == :intersection_exist ? nil : result)
        # MappedMfd.output_mfds_for_debug(FileName.create("out#{i}.txt"), mfd1, mfd2)
        if sym_result == :intersection_not_found
          mfd1, mfd2 = cut_mfds(mfd1, mfd2, result)
        else
          if sym_result == :intersection_exist
            ret = result
          end
          break
        end
      end
      @manifolds = [mfd1, mfd2]
      return ret
    end
  end
end

class TangentCrdRange
  INITIAL_POINTS_NUMBER = 30
  MAX_STABLE_RANGE = 10
  MAX_UNSTABLE_RANGE = 10

  def initialize(henon, stable_calculator, unstable_calculator)
    @henon = henon
    @stable_calculator = stable_calculator
    @unstable_calculator = unstable_calculator
  end

  def get_unstable_range(un_mfd, iter_unstable, range_unstable)
    un_mfd = MappedMfd::PointsObj2D.new(@unstable_calculator, @henon, iter_unstable, range_unstable)
    un_mfd.calc_with_number(INITIAL_POINTS_NUMBER)
    range_unstable_new = range_unstable.dup
    i = 0
    while un_mfd.max_variation_of_direction < Math::PI / 2.0
      len = (range_unstable_new[1] - range_unstable_new[0]).abs
      range_unstable_new[0] = range_unstable_new[0] - len
      range_unstable_new[1] = range_unstable_new[1] + len
      range_unstable_new[0] = MPFR(0) if range_unstable_new[0] < 0
      range_unstable_new[1] = MPFR(1) if range_unstable_new[0] > 1
      un_mfd = MappedMfd::PointsObj2D.new(@unstable_calculator, @henon, iter_unstable, range_unstable_new)
      un_mfd.calc_with_number(INITIAL_POINTS_NUMBER)
      i += 1
      if i > MAX_UNSTABLE_RANGE
        raise "Can not specify range of the unstable manifold"
      end
    end
    dists = [MPFR(0), MPFR(0)]
    last_dist = MPFR(0)
    pt = un_mfd[0]
    peak_ind = nil
    (1...(un_mfd.size)).each do |i|
      d = pt.distance_from(un_mfd[i])
      d2 = un_mfd[i].distance_from(un_mfd[i - 1])
      if last_dist
        dists[0] += d2
        if last_dist > d
          last_dist = nil
          peak_ind = i
        else
          last_dist = d
        end
      else
        dists[1] += d2
      end
    end
    if peak_ind
      if dists[0] < dists[1]
        range_unstable_new[0] -= (un_mfd.srcs[-1].initial_position - un_mfd.srcs[peak_ind].initial_position).abs
      else
        range_unstable_new[1] += (un_mfd.srcs[peak_ind].initial_position - un_mfd.srcs[0].initial_position).abs
      end
      range_unstable_new[0] = MPFR(0) if range_unstable_new[0] < 0
      range_unstable_new[1] = MPFR(1) if range_unstable_new[0] > 1
      un_mfd = MappedMfd::PointsObj2D.new(@unstable_calculator, @henon, iter_unstable, range_unstable_new)
      un_mfd.calc_with_number(INITIAL_POINTS_NUMBER)
    end
    [un_mfd, range_unstable_new]
  end
  private :get_unstable_range

  def get_stable_range(st_mfd, iter_stable, range_stable, un_mfd)
    st_mfd = MappedMfd::PointsObj2D.new(@stable_calculator, @henon, iter_stable, range_stable)
    st_mfd.calc_with_number(INITIAL_POINTS_NUMBER)
    range_stable_new = range_stable.dup
    i = 0
    loop do
      d = Intersect::Function.distance_between_two_mfds(st_mfd, un_mfd)
      index_min = st_mfd.size / 4.0
      index_max = st_mfd.size - index_min
      if d.ind1 >= index_min && d.ind1 <= index_max
        break
      end
      len = (range_stable_new[1] - range_stable_new[0]).abs
      if d.ind1 < index_min
        range_stable_new[0] = range_stable_new[0] - len
        range_stable_new[0] = MPFR(0) if range_stable_new[0] < 0
      else
        range_stable_new[1] = range_stable_new[1] + len
        range_stable_new[1] = MPFR(1) if range_stable_new[0] > 1
      end
      st_mfd = MappedMfd::PointsObj2D.new(@stable_calculator, @henon, iter_stable, range_stable_new)
      st_mfd.calc_with_number(INITIAL_POINTS_NUMBER)
      i += 1
      if i > MAX_STABLE_RANGE
        # st_mfd.output_to_file("st_mfd.txt")
        # un_mfd.output_to_file("un_mfd.txt")
        raise "Can not specify range of the stable manifold"
      end
    end
    [st_mfd, range_stable_new]
  end
  private :get_stable_range

  # @param [Integer] iter_stable Number of iteration to calculate the stable manifold (negative number)
  # @param [Array] range_stable Pair of numbers to specify the segment of the stable manifold
  # @param [Integer] iter_unstable Number of iteration to calculate the stable manifold (positive number)
  # @param [Array] range_unstable Pair of numbers to specify the segment of the unstable manifold
  # @note We assume that the stable manifold is almost straight and the unstable manifold is curved.
  # If the situation is reversed, this method does not work properly.
  def coordinate_range(iter_stable, range_stable, iter_unstable, range_unstable)
    un_mfd, range_unstable_new = get_unstable_range(un_mfd, iter_unstable, range_unstable)
    st_mfd, range_stable_new = get_stable_range(un_mfd, iter_stable, range_stable, un_mfd)
    [st_mfd, un_mfd, range_stable_new, range_unstable_new]
  end
end

# Calculate the distance of stable manifold and peak of unstable manifold,
# when stable and unstable manifolds intersect with each other.
class DistanceUnstablePeak
  include Intersect::Function

  class DistancePeakWithIntersection
    attr_reader :base_pt, :vector, :normal_vector

    # Calculate distance for normal_vector direction.
    def initialize(mfd_error, base_pt, vector, normal_vector)
      @mfd_error = mfd_error
      @base_pt = base_pt
      @vector = vector
      @normal_vector = normal_vector
      @logger = ManageLog.get_new_logger(self.class)
    end

    def reverse_normal_vector!
      @normal_vector = @normal_vector.mul_scalar(-1)
    end

    def calc_peak_distance(pt)
      prj = MPFR::Vector2D.project(pt - @base_pt, @vector, @normal_vector)
      prj[1]
    end

    def peak_distance_with_intersections(mfd)
      ind = 0
      dist = calc_peak_distance(mfd[0])
      (1...(mfd.size)).each do |i|
        dist_new = calc_peak_distance(mfd[i])
        if dist <= dist_new
          dist = dist_new
          ind = i
        end
      end
      [dist, ind]
    end
    private :peak_distance_with_intersections

    def get_distance(mfd, error)
      new_dist, ind = peak_distance_with_intersections(mfd)
      new_dist += (error * 2)     # you set sufficient large value to new_dist.
      begin
        dist = new_dist
        cut_range = ([0, ind - CUT_MFD_PTS_NUMBER].max)..([mfd.size, ind + CUT_MFD_PTS_NUMBER].min)
        mfd.portion!(cut_range)
        @logger.debug("Cut unstable manifold: #{cut_range.inspect.strip}")
        mfd.calc_with_number(INITIAL_MFD_PTS_NUMBER2) if mfd.size < INITIAL_MFD_PTS_NUMBER2
        new_dist, ind = peak_distance_with_intersections(mfd)
        var = (dist - new_dist).abs
        if ind + 1 < mfd.size
          dist_mfd = mfd[ind].distance_from(mfd[ind + 1])
        else
          dist_mfd = mfd[ind].distance_from(mfd[ind - 1])
        end
        @logger.debug("index: #{ind}\nvariation: #{var.to_strf("%.30Re")}\ndistance of points: #{dist_mfd.to_strf("%.30e")}\ndistance of peak: #{new_dist.to_strf("%.Re")}")
      end while var > error || dist_mfd > @mfd_error
      [new_dist, ind]
    end
    private :get_distance

    # @param [Hash] opts Option hash
    # @option opts [:negative] opts We accept negative distance and get largest absolute distance
    # @option opts [:duplicate] opts We calculate duplicated mfd and do not change mfd
    def distance(mfd, error, opts = {})
      mfd = mfd.dup if opts[:duplicate]
      mfd_neg = (opts[:negative] ? mfd.dup : nil)
      ret = get_distance(mfd, error)
      if mfd_neg
        reverse_normal_vector!
        ret_neg = get_distance(mfd_neg, error)
        if ret_neg[0].abs > ret[0].abs
          mfd.replace_srcs!(mfd_neg.srcs)
          ret = ret_neg
          ret[0] = -ret[0]
        end
        reverse_normal_vector!
      end
      ret
    end
  end

  class DistancePeakNoIntersection
    MAX_MANIFOLD_NUMBER = 100
    CALC_MANIFOLD_NUMBER = 30
    CUT_MANIFOLD_NUMBER = 3

    def initialize
      @logger = ManageLog.get_new_logger(self.class)
    end

    def distance_between_lines(st_mfd, un_mfd)
      MPFR::Vector2D.distance_between_lines(st_mfd, un_mfd)
    end
    private :distance_between_lines

    # Remove points except neighborhood of the point specified by variable "num" and two edge points.
    def change_manifold_range(mfd, num)
      num_remove_start = num + CUT_MANIFOLD_NUMBER
      if (num_removed_points = mfd.size - num_remove_start - 2) > 0
        mfd.delete_points(num_remove_start, num_removed_points)
      else
        if num == mfd.size - 1
          mfd.calc_with_number(CALC_MANIFOLD_NUMBER, :start => mfd.size - 2, :end => mfd.size - 1)
        else
          mfd.calc_with_number(CALC_MANIFOLD_NUMBER, :start => num, :end => mfd.size - 1)
        end
      end
      num_start = num - CUT_MANIFOLD_NUMBER
      num_removed_points = num_start - 1
      if num_removed_points > 0
        mfd.delete_points(1, num_removed_points)
      else
        if num == 0
          mfd.calc_with_number(CALC_MANIFOLD_NUMBER, :start => 0, :end => 1)
        else
          mfd.calc_with_number(CALC_MANIFOLD_NUMBER, :start => 0, :end => num)
        end
      end
      mfd
    end
    private :change_manifold_range

    def reduce_manifold_calculate_range(mfd, num)
      if mfd.size > MAX_MANIFOLD_NUMBER
        change_manifold_range(mfd, num)
        start_num = 0
        end_num = mfd.size - 1
      else
        start_num = [num - CUT_MANIFOLD_NUMBER, 0].max
        end_num = [num + CUT_MANIFOLD_NUMBER, mfd.size - 1].min
      end
      [start_num, end_num]
    end
    private :reduce_manifold_calculate_range

    def calculate_manifold(mfd, num)
      start_num, end_num = reduce_manifold_calculate_range(mfd, num)
      mfd.calc_with_number(CALC_MANIFOLD_NUMBER, :start => start_num, :end => end_num)
    end
    private :calculate_manifold

    def calculate_manifold_finely(mfd, num)
      start_num, end_num = reduce_manifold_calculate_range(mfd, num)
      calc_num = (end_num - start_num + 1) * 2
      if calc_num > 0
        mfd.calc_with_number(calc_num, :start => start_num, :end => end_num)
      else
        raise "Can not determine indices of manifold to calculate."
      end
    end
    private :calculate_manifold_finely

    def calculate_manifold_with_error(mfd, num, err)
      start_num, end_num = reduce_manifold_calculate_range(mfd, num)
      mfd.calc_with_error(err, :start => start_num, :end => end_num)
    end
    private :calculate_manifold_with_error

    def distance(st_mfd, un_mfd, error)
      st_mfd.calc_with_number(CALC_MANIFOLD_NUMBER)
      un_mfd.calc_with_number(CALC_MANIFOLD_NUMBER)
      st_interval = st_mfd.max_interval
      un_interval = un_mfd.max_interval
      i = 0
      loop do
        @logger.debug("distance_between_manifolds #{i += 1}") { "stable: #{st_mfd.size}, unstable: #{un_mfd.size}" }
        ary = distance_between_lines(st_mfd, un_mfd)
        distance, st_num, un_num = ary
        st_interval = st_mfd[st_num].distance_from(st_mfd[st_num + 1])
        un_interval = un_mfd[un_num].distance_from(un_mfd[un_num + 1])
        if st_interval > un_interval * 2.5
          @logger.debug("calculate stable: #{st_mfd.size}")
          calculate_manifold_finely(st_mfd, st_num)
        elsif un_interval > st_interval * 2.5
          @logger.debug("calculate stable: #{un_mfd.size}")
          calculate_manifold_finely(un_mfd, un_num)
        else
          err = [st_interval, un_interval].min / 2.0
          @logger.debug("calculate both manifolds: #{st_mfd.size} #{un_mfd.size}") { err.to_strf("%.30Re") }
          if st_mfd.orbit_length / err > 300
            calculate_manifold_finely(st_mfd, st_num)
          else
            calculate_manifold_with_error(st_mfd, st_num, err)
          end
          if un_mfd.orbit_length / err > 300
            calculate_manifold_finely(un_mfd, un_num)
          else
            calculate_manifold_with_error(un_mfd, un_num, err)
          end
          break
        end
      end

      last_distance, st_num, un_num = distance_between_lines(st_mfd, un_mfd)
      i = 0
      ret = nil
      loop do
        calculate_manifold(st_mfd, st_num)
        calculate_manifold(un_mfd, un_num)
        distance, st_num, un_num = distance_between_lines(st_mfd, un_mfd)
        @logger.debug("Distance") { distance.to_strf("%.Re") }
        if (last_distance - distance).abs < error
          ret = [distance, st_num, un_num]
          break
        end
        last_distance = distance
        i += 1
        if i > 1000
          raise "Can not determine distance between two manifolds."
        end
      end
      ret
    end
  end

  module PeakDirection
    def dump_projection_data(projection_data, output, len = 1)
      Kernel.open(output, "w") do |f|
        f.puts (projection_data[:base_pt] - projection_data[:vector].mul_scalar(len)).to_s
        f.puts projection_data[:base_pt].to_s
        f.puts (projection_data[:base_pt] + projection_data[:vector].mul_scalar(len)).to_s
        f.print "\n"
        f.puts projection_data[:base_pt].to_s
        f.puts (projection_data[:base_pt] + projection_data[:normal_vector].mul_scalar(len)).to_s
      end
    end
    module_function :dump_projection_data

    def project(pt, projection_data)
      MPFR::Vector2D.project(pt - projection_data[:base_pt], projection_data[:vector], projection_data[:normal_vector])
    end
    module_function :project

    def projected_distance(pt, projection_data)
      project(pt, projection_data)[1]
    end
    module_function :projected_distance

    def projected_distances_of_manifold(mfd, projection_data)
      mfd.map { |pt| projected_distance(pt, projection_data) }
    end
    module_function :projected_distances_of_manifold

    INTERSECTION_NEIGHBORHOOD_THRESHOLD = 0.01

    # st_mfd and un_mfd have two intersections.
    # We set normal vector from the stable manifold to the peak of unstable manifold.
    # @param [Hash] opts Option hash
    # @option opts [boolean] :portion If the value is true then manifolds are cut.
    def direction_with_two_intersections(st_mfd, un_mfd, intersections, opts = {})
      vec = (intersections[1].crd - intersections[0].crd).normalize
      projection_data = {
        :base_pt => intersections[0].crd.midpoint(intersections[1].crd),
        :vector => vec,
        :normal_vector => MPFR::Vector2D.normal_vector(vec)
      }
      first_negative_p = nil
      last_sgn = nil
      change = 0
      projected_distances_of_manifold(un_mfd, projection_data).each_with_index do |d, ind|
        if un_mfd[ind].distance_from(projection_data[:base_pt]) < INTERSECTION_NEIGHBORHOOD_THRESHOLD
          sgn = d.sgn
          if last_sgn
            if sgn != 0 && sgn != last_sgn
              change += 1
            end
          else
            first_negative_p = (d < 0)
          end
          last_sgn = sgn
        end
      end
      if change != 2
        # st_mfd.output_to_file("error_st_mfd.txt")
        # un_mfd.output_to_file("error_un_mfd.txt")
        # Kernel.open("error_intersections.txt", "w") do |f|
        #   intersections.each do |pt_intersection|
        #     f.puts pt_intersection.crd
        #   end
        # end
        raise "Not just two intersections: change=#{change}"
      end
      unless first_negative_p
        projection_data[:normal_vector] = projection_data[:normal_vector].mul_scalar(-1)
      end
      if opts[:portion]
        indices_stable = [intersections[0].ind1, intersections[1].ind1].sort
        indices_stable[0] = indices_stable[0] + 1
        indices_unstable = [intersections[0].ind2, intersections[1].ind2].sort
        indices_unstable[0] = indices_unstable[0] + 1
        st_mfd.portion!(indices_stable[0]..indices_stable[1])
        un_mfd.portion!(indices_unstable[0]..indices_unstable[1])
      end
      [st_mfd, un_mfd, projection_data]
    end
    module_function :direction_with_two_intersections

    def direction_no_intersection(st_mfd, un_mfd)
      dist = Intersect::Function.distance_between_two_mfds(st_mfd, un_mfd)
      len_un_mfd = un_mfd.orbit_length

      ind_start = dist.ind1
      len = 0
      while (len < len_un_mfd / 2 && ind_start > 0)
        len += st_mfd[ind_start].distance_from(st_mfd[ind_start - 1])
        ind_start -= 1
      end
      ind_end = dist.ind1
      len = 0
      while (len < len_un_mfd / 2 && ind_end < (st_mfd.size - 1))
        len += st_mfd[ind_start].distance_from(st_mfd[ind_start + 1])
        ind_end += 1
      end

      vec = (st_mfd[ind_end] - st_mfd[ind_start]).normalize
      projection_data = {
        :base_pt => st_mfd[dist.ind1],
        :vector => vec,
        :normal_vector => MPFR::Vector2D.normal_vector(vec)
      }
      if projected_distance(un_mfd[0], projection_data) < 0
        projection_data[:normal_vector] = projection_data[:normal_vector].mul_scalar(-1)
      end
      [st_mfd, un_mfd, projection_data]
    end
    module_function :direction_no_intersection
  end

  INITIAL_MFD_PTS_NUMBER = 300
  INITIAL_MFD_PTS_NUMBER2 = 100
  CUT_MFD_PTS_NUMBER = 5

  @@mfd_count = 0

  include PeakDirection
  attr_reader :peak_pt, :intersection_data

  # @param [Intersect::Determine] determine
  # @param [MPFR] mfd_error
  def initialize(determine, mfd_error)
    @calc_determine = determine
    @mfd_error = mfd_error
    @peak_pt = nil
    @logger = ManageLog.get_new_logger(self.class)
  end

  def initial_calculation_no_intersection(st_mfd, un_mfd)
    mfd = { :stable => st_mfd, :unstable => un_mfd }
    loop do
      mfd_dist = distance_between_two_mfds(mfd[:stable], mfd[:unstable])
      st_max_interval = mfd[:stable].max_interval
      un_max_interval = mfd[:unstable].max_interval
      if st_max_interval > un_max_interval * 3
        mfd_cut_inds = [:stable]
      elsif un_max_interval > st_max_interval * 3
        mfd_cut_inds = [:unstable]
      else
        mfd_cut_inds = [:stable, :unstable]
      end
      dist_margin = 2
      inds = nil
      loop do
        d = [mfd_dist.dist, st_max_interval, un_max_interval].max * dist_margin
        inds_ary = index_ranges_close_pts(mfd[:stable], mfd[:unstable], d)
        inds = { :stable => inds_ary[0], :unstable => inds_ary[1] }
        inds_shrink_p = false
        mfd_cut_inds.each do |key|
          if inds[key].size < mfd[key].size
            inds_shrink_p = true
            break
          end
        end
        break if inds_shrink_p
        dist_margin = (dist_margin - 1) / MPFR(2) + 1
      end
      mfd_cut_inds.each do |key|
        mfd[key].portion!(inds[key])
        mfd[key].calc_with_number(INITIAL_MFD_PTS_NUMBER2)
      end
      break if mfd_cut_inds.size == 2
    end
    mfd.each do |key, m|
      m.calc_with_number(INITIAL_MFD_PTS_NUMBER2) if m.size < INITIAL_MFD_PTS_NUMBER2
    end
    [mfd[:stable], mfd[:unstable]]
  end
  private :initial_calculation_no_intersection

  # @return [boolean] If two intersections exist then we get true. Otherwise, false.
  def initial_calculation(st_mfd, un_mfd, debug_output = false)
    @logger.debug("Start initial calculation.")
    time_str = nil
    if debug_output
      @@mfd_count += 1
      time_str = Time.now.strftime("#{@@mfd_count}_%s")
      st_mfd.output_to_file("st_mfd_start_#{time_str}.txt")
      un_mfd.output_to_file("un_mfd_start_#{time_str}.txt")
    end
    st_mfd.calc_with_number(INITIAL_MFD_PTS_NUMBER)
    un_mfd.calc_with_number(INITIAL_MFD_PTS_NUMBER)
    if (intersections = @calc_determine.search_intersections(st_mfd, un_mfd)) && intersections.size == 2
      @logger.debug("Find two intersections.")
      st_mfd, un_mfd = @calc_determine.manifolds
      st_mfd, un_mfd, intersection_data = direction_with_two_intersections(st_mfd, un_mfd, intersections, :portion => true)
    else
      @logger.debug("Number of intersections") { intersections ? "#{intersections.size} intersections" : "None" }
      intersection_data = nil
      st_mfd, un_mfd = initial_calculation_no_intersection(st_mfd, un_mfd)
    end
    if debug_output
      st_mfd.output_to_file("st_mfd_end_#{time_str}.txt")
      un_mfd.output_to_file("un_mfd_end_#{time_str}.txt")
    end
    [intersection_data, st_mfd, un_mfd]
  end
  private :initial_calculation

  # @return [MPFR] Distance from stable manifold in the direction of peak.
  def get_projected_distance(pt)
    if intersection_exist?
      @intersection_data[:calc].calc_peak_distance(pt)
    else
      MPFR::Vector2D.distance_between_point_and_line(pt, @st_mfd)[0]
    end
  end

  def calculator_with_intersections
    if intersection_exist?
      @intersection_data[:calc]
    else
      unless @calculator_with_intersections
        base_pt = @st_mfd[0].midpoint(@st_mfd[-1])
        vector = (@st_mfd[-1] - @st_mfd[0]).normalize
        normal_vector = MPFR::Vector2D.normal_vector(vector)
        @calculator_with_intersections = DistanceUnstablePeak::DistancePeakWithIntersection.new(@mfd_error, base_pt, vector, normal_vector)
        if @calculator_with_intersections.calc_peak_distance(@un_mfd[(0 + @un_mfd.size) / 2]) < 0
          @calculator_with_intersections.reverse_normal_vector!
        end
      end
      @calculator_with_intersections
    end
  end

  # @param [Hash] opts Options
  # @option opts [boolean] :debug_output Output initial manifolds to files
  # @return [boolean] If two intersections exist then we get true. Otherwise, false.
  def set_mfds(st_mfd, un_mfd, opts = {})
    # By way of caution, we reset instance variables.
    @st_mfd = nil
    @un_mfd = nil
    @peak_pt = nil
    @intersection_data = nil
    @intersection_data, @st_mfd, @un_mfd = initial_calculation(st_mfd, un_mfd, opts[:debug_output])
    if @intersection_data
      @intersection_data[:calc] = DistanceUnstablePeak::DistancePeakWithIntersection.new(@mfd_error, @intersection_data[:base_pt], @intersection_data[:vector], @intersection_data[:normal_vector])
    end
    intersection_exist?
  end

  def intersection_exist?
    !!@intersection_data
  end

  def distance(error)
    if intersection_exist?
      dist, un_ind = @intersection_data[:calc].distance(@un_mfd, error)
      if dist < 0
        puts dist.to_strf("dist=%.Re")
        @st_mfd.output_to_file(FileName.create("error_st_mfd.txt"))
        @un_mfd.output_to_file(FileName.create("error_un_mfd.txt"))
        raise "Unstable manifold moves to the opposite side. Intersections has been created or disappeared."
      end
    else
      distance_data = DistanceUnstablePeak::DistancePeakNoIntersection.new.distance(@st_mfd, @un_mfd, error)
      dist, st_ind, un_ind = distance_data
    end
    @peak_pt = (@un_mfd[un_ind] + @un_mfd[un_ind + 1]).mul_scalar(0.5)
    dist
  end

  def manifolds
    [@st_mfd, @un_mfd]
  end

  # For debugging.
  # \@un_mfd and \@st_mfd must be MappedMfd::PointsObj2D.
  # def make_image(size = MPFR.new('1e-4'))
  #   plot = MPFRPlot.new
  #   plot.set_screen(:center => @base_pt, :size => size)
  #   plot.add_object(@st_mfd, @un_mfd,
  #                   MPFR::Obj2D.new(:style => MPFRPlot::STY_LINES_POINTS, :data => [@base_pt, @base_pt + @vec]),
  #                   MPFR::Obj2D.new(:style => MPFRPlot::STY_LINES, :data => [@base_pt, @base_pt + @nvec]))
  #   plot.output(MPFRPlot.create_basename('img', :png))
  # end
end

class SearchPeriodicPoint
  include DistanceUnstablePeak::PeakDirection

  NUMBER_INITIAL_MFD = 300

  def initialize(calc_determine, st_mfd, un_mfd, mapping, iterate)
    @calc_determine = calc_determine
    @st_mfd = st_mfd
    @un_mfd = un_mfd
    @mapping = mapping
    @iterate = iterate
    @logger = ManageLog.get_new_logger(self.class)
  end

  def manifolds
    [@st_mfd, @un_mfd]
  end

  def intersection_of_line_and_curve(pt_src, projection_data, len)
    line = MappedMfd::StraightLine.new(pt_src, pt_src + projection_data[:normal_vector].mul_scalar(len))
    curve = MappedMfd::Points.new(line, @mapping, @iterate)
    curve.calc_with_number(NUMBER_INITIAL_MFD)
    res = @calc_determine.search_intersections(line, curve)
    line_new, curve_new = @calc_determine.manifolds
    [res, line_new, curve_new]
  end
  private :intersection_of_line_and_curve

  def iterate_pt(pt)
    @mapping.map(pt, @iterate)
  end
  private :iterate_pt

  def difference_mapped_pt_intersection(pt_intersection, projection_data)
    proj1 = project(pt_intersection, projection_data)
    pt_iterated = iterate_pt(pt_intersection)
    proj2 = project(pt_iterated, projection_data)
    [proj1[0] - proj2[0], pt_iterated.distance_from(pt_intersection)]
  end
  private :difference_mapped_pt_intersection

  def search_intersection_of_line_and_iteration(projection_data, search_range, num, len)
    unsuitable_normal_vector_p = false
    intersection_point_data = []
    # @st_mfd.output_to_file("st_mfd.txt")
    # @un_mfd.output_to_file("un_mfd.txt")
    # dump_projection_data(projection_data, "projection.txt", MPFR("1e-3"))
    pt_src = search_range[0]
    step = (search_range[1] - search_range[0]).div_scalar(num)
    (num + 1).times do |n|
      intersections_line_curve, line2, curve2 = intersection_of_line_and_curve(pt_src, projection_data, len)
      # line2.output_to_file("line#{'%03d' % (n += 1)}.txt")
      # curve2.output_to_file("curve#{'%03d' % n}.txt")
      if intersections_line_curve
        if intersections_line_curve.size != 1
          @logger.info("Unsuitable normal vector: #{intersections_line_curve.size} intersections found")
          p intersections_line_curve
          @st_mfd.output_to_file("error_st_mfd.txt")
          @un_mfd.output_to_file("error_un_mfd.txt")
          line2.output_to_file("error_line.txt")
          curve2.output_to_file("error_curve.txt")
          break
        end
        intersection_horseshoe = intersections_line_curve.first
        # Kernel.open("intersection#{'%03d' % n}.txt", "w") do |f|
        #   f.puts intersection_horseshoe.crd.to_s
        # end
        pt_intersection = intersection_horseshoe.crd
        diff_project, dist_mapped = difference_mapped_pt_intersection(pt_intersection, projection_data)
        ary = [diff_project, pt_src, pt_intersection, dist_mapped]
        @logger.debug("[#{n}] Intersection found: diff #{diff_project.to_strf("%.8Re")}, dist #{ary[3].to_strf("%.8Re")}")
        intersection_point_data << ary
      else
        d = @calc_determine.distance.dist
        @logger.debug("[#{n}] No intersection: #{d.to_strf("%.10Re")}")
        intersection_point_data << [nil, pt_src, d]
      end
      pt_src += step
    end
    [intersection_point_data, unsuitable_normal_vector_p]
  end
  private :search_intersection_of_line_and_iteration

  def get_periodic_point_candidate(intersection_point_data)
    periodic_point_candidate = []
    dist_between_line_and_curve = []
    dist_mapped = []
    last_ary = []
    find_intersection_p = false
    intersection_point_data.each do |ary|
      if ary[0]
        find_intersection_p = true
        dist_mapped << ary[3]
      else
        dist_between_line_and_curve << ary[2]
      end
      if last_ary[0] && ary[0] && (last_ary[0] * ary[0]) < 0
        cand = { :project_difference => [ary[0], last_ary[0]], :pt_on_line => [ary[1], last_ary[1]], :periodic_point => [ary[2], last_ary[2]] }
        if cand[:project_difference][0] < 0
          cand[:project_difference] = [cand[:project_difference][1], cand[:project_difference][0]]
          cand[:pt_on_line] = [cand[:pt_on_line][1], cand[:pt_on_line][0]]
          cand[:periodic_point] = [cand[:periodic_point][1], cand[:periodic_point][0]]
        end
        periodic_point_candidate << cand
      end
      last_ary = ary
    end
    dist_between_line_and_curve.sort!
    dist_mapped.sort!
    [periodic_point_candidate, find_intersection_p, [dist_between_line_and_curve[0], dist_between_line_and_curve[-1]], [dist_mapped[0], dist_mapped[-1]]]
  end
  private :get_periodic_point_candidate

  def distance_mapped_pt(pt)
    pt.distance_from(iterate_pt(pt))
  end
  private :distance_mapped_pt

  def bisect_candidate_periodic_point(cand, projection_data, search_line_len, error)
    pts = { :pos => cand[:periodic_point][0], :neg => cand[:periodic_point][1] }
    pts_on_line = { :pos => cand[:pt_on_line][0], :neg => cand[:pt_on_line][1] }
    dist_mapped = { :pos => distance_mapped_pt(pts[:pos]), :neg => distance_mapped_pt(pts[:neg])}
    n = 0
    while (error_new = pts[:pos].distance_from(pts[:neg])) > error
      @logger.debug("Bisect periodic point candidate #{(n += 1)}: #{error_new.to_strf("%.20Re")}")
      pt_src = pts_on_line[:pos].midpoint(pts_on_line[:neg])
      intersections_line_curve, line, curve = intersection_of_line_and_curve(pt_src, projection_data, search_line_len)
      if !intersections_line_curve
        raise "Can not bisect periodic point candidate"
      elsif intersections_line_curve.size != 1
        raise "Too many intersections: #{intersections_line_curve.size} intersections"
      end
      intersection_horseshoe = intersections_line_curve.first
      pt_intersection = intersection_horseshoe.crd
      diff_project, dist_mapped_new = difference_mapped_pt_intersection(pt_intersection, projection_data)
      key = (diff_project > 0 ? :pos : :neg)
      if dist_mapped_new > dist_mapped[key] * 0.95
        @logger.debug("Distance between pt of intersection and its iteration does not shrink!\nkey=#{key}\n#{dist_mapped_new.to_strf("%.10Re")}\n#{dist_mapped[key].to_strf("%.10Re")}")
        return nil
        # intersections_new, unsuitable_normal_vector_p_tmp = search_intersection_of_line_and_iteration(projection_data, [pts_on_line[:pos], pts_on_line[:neg]], NUM_PT_ON_LINE, search_line_len)
        # cands_new, min_dist = get_periodic_point_candidate(intersections_new)
        # if cands_new.size == 1
        #   cand = cands_new.first
        #   p cand
        #   exit
        # else
        #   @logger.debug("Too many candidates of periodic points are found: #{cands_new.size}")
        #   return nil
        # end
      end
      pts[key] = pt_intersection
      pts_on_line[key] = pt_src
      dist_mapped[key] = dist_mapped_new
    end
    pts[:pos].midpoint(pts[:neg])
  end
  private :bisect_candidate_periodic_point

  def indices_refined_by_distance_between_line_and_curve(intersection_point_data, min_dist_between_line_and_curve)
    indices = []
    d = min_dist_between_line_and_curve * 2
    intersection_point_data.each_with_index do |ary, ind|
      # ary[0] is nil, ary[1] is source point, and ary[2] is the distance between line and curve
      if indices.empty?
        if ary[2] < d
          indices << (ind - 1)
        end
      else
        if ary[2] > d
          indices << ind
          break
        end
      end
    end
    if indices.size == 0
      raise "Invalid minimum distance of lines and curves"
    end
    indices
  end
  private :indices_refined_by_distance_between_line_and_curve

  def indices_remove_no_intersection(intersection_point_data)
    indices = []
    count_no_intersection = 0
    intersection_point_data.each_with_index do |ary, ind|
      count_no_intersection += 1 unless ary[0]
      if indices.empty?
        if ary[0]
          indices << (ind - 1)
        end
      else
        if !ary[0]
          indices << ind
          break
        end
      end
    end
    if count_no_intersection == 0
      nil
    else
      indices
    end
  end
  private :indices_remove_no_intersection

  def indices_refined_by_distance_iteration(intersection_point_data, max_dist)
    indices_neighborhood = []
    intersection_point_data.each_with_index do |ary, ind|
      if ary[0] && ary[3] <= max_dist
        indices_neighborhood << ind
      end
    end
    if indices_neighborhood.empty?
      raise "There is no intersection that is mapped into the neighborhood!"
    end
    [[0, indices_neighborhood[0] - 1].max, [intersection_point_data.size - 1, indices_neighborhood[-1] + 1].min]
  end
  private :indices_refined_by_distance_iteration

  def refine_search_range(type, intersection_point_data, *args)
    @logger.debug("Refine search range by #{type}")
    case type
    when :distance_between_line_and_curve
      min_dist_between_line_and_curve = args[0]
      indices = indices_refined_by_distance_between_line_and_curve(intersection_point_data, min_dist_between_line_and_curve)
    when :remove_no_intersection
      unless indices = indices_remove_no_intersection(intersection_point_data)
        return nil
      end
    when :distance_iteration
      dists = []
      intersection_point_data.each do |ary|
        if ary[0]
          dists << ary[3]
        end
      end
      dists.sort!
      indices = nil
      d = MPFR::Math.sqrt(dists[-1] * dists[0])
      i = 0
      loop do
        i += 1
        if i == 1000
          raise "Probably this loop can not stop!!!"
        end
        indices = indices_refined_by_distance_iteration(intersection_point_data, d)
        if indices[0] > 0 || indices[-1] < (intersection_point_data.size - 1)
          break
        else
          d = MPFR::Math.sqrt(d * dists[0])
        end
      end
    else
      raise "Invalid type: #{type}"
    end
    ind_start = [0, indices[0]].max
    ind_end = (indices[1] || intersection_point_data.size - 1)
    @logger.debug("intersection_point_data: #{intersection_point_data.size} points cut by [#{ind_start}, #{ind_end}]")
    if ind_start == 0 && ind_end == (intersection_point_data.size - 1)
      nil
    else
      [intersection_point_data[ind_start][1], intersection_point_data[ind_end][1]]
    end
  end
  private :refine_search_range

  def get_range_including_periodic_points(projection_data, search_range, len)
    i = 0
    loop do
      i += 1
      if i == 1000
        raise "We can not determine the range where periodic points exist!"
      end
      intersection_point_data, unsuitable_normal_vector_p = search_intersection_of_line_and_iteration(projection_data, search_range, NUM_PT_ON_LINE, len)
      periodic_point_candidate, find_intersection_p, dists_between_line_and_curve, dists_mapped = get_periodic_point_candidate(intersection_point_data)
      @logger.debug("Maximum distance of intersection and its iteration: #{dists_mapped[1].to_strf('%.10Re')}")
      if dists_mapped[1] < len * 10
        break
      end
      search_range = refine_search_range(:distance_iteration, intersection_point_data)
    end
    search_range
  end
  private :get_range_including_periodic_points

  NUM_TRY_FIND_INTERSECTION = 30
  NUM_PT_ON_LINE = 100

  def calc_neighborhood_of_tangency
    @st_mfd.calc_with_number(NUMBER_INITIAL_MFD)
    @un_mfd.calc_with_number(NUMBER_INITIAL_MFD)
    intersections = @calc_determine.search_intersections(@st_mfd, @un_mfd)
    if intersections
      raise "Not just two intersections" unless intersections.size == 2
      @logger.debug("Find two intersections")
      @st_mfd, @un_mfd = @calc_determine.manifolds
      @st_mfd, @un_mfd, projection_data = direction_with_two_intersections(@st_mfd, @un_mfd, intersections)
    else
      @logger.debug("No intersection")
      @st_mfd, @un_mfd, projection_data = direction_no_intersection(@st_mfd, @un_mfd)
    end
    projected_coordinates = @un_mfd.map do |pt|
      project(pt, projection_data)
    end
    if intersections
      dist_between_mfds = (projected_coordinates.map { |prj| prj[1] }).max
    else
      dist_between_mfds = (projected_coordinates.map { |prj| prj[1] }).min
    end
    search_range = []
    crds = (projected_coordinates.map { |prj| prj[0] }).sort!
    search_range << projection_data[:base_pt] + projection_data[:vector].mul_scalar(crds[0])
    search_range << projection_data[:base_pt] + projection_data[:vector].mul_scalar(crds[-1])
    len = dist_between_mfds * 2
    [projection_data, search_range, len]
  end
  private :calc_neighborhood_of_tangency

  def get_search_range_near_tangency(projection_data, search_range, len)
    search_range_with_intersection_find_p = false
    NUM_TRY_FIND_INTERSECTION.times do |i|
      @logger.debug("Search for intersections [#{i}]")
      intersection_point_data, unsuitable_normal_vector_p = search_intersection_of_line_and_iteration(projection_data, search_range, NUM_PT_ON_LINE, len)
      periodic_point_candidate, find_intersection_p, dists_between_line_and_curve, dists_mapped = get_periodic_point_candidate(intersection_point_data)
      if find_intersection_p
        @logger.debug("Find one intersection at least")
        search_range = refine_search_range(:remove_no_intersection, intersection_point_data)
        search_range_with_intersection_find_p = true
        break
      else
        @logger.debug("Find no intersection")
        unless search_range_new = refine_search_range(:distance_between_line_and_curve, intersection_point_data, dists_between_line_and_curve[0])
          break
        end
        search_range = search_range_new
      end
    end
    unless search_range_with_intersection_find_p
      @logger.debug("Can not find intersections of line and mapped curve!")
      return nil
    end
    search_range
  end
  private :get_search_range_near_tangency

  def search_and_narrow_bisection_range(projection_data, search_range, len, error)
    periodic_points = nil
    dists_mapped_last = nil
    search_range_len = search_range[0].distance_from(search_range[1])
    NUM_TRY_FIND_INTERSECTION.times do |i|
      @logger.debug("Narrow range of periodic points [#{i}]")
      intersection_point_data, unsuitable_normal_vector_p = search_intersection_of_line_and_iteration(projection_data, search_range, NUM_PT_ON_LINE, len)
      periodic_point_candidate, find_intersection_p, dists_between_line_and_curve, dists_mapped = get_periodic_point_candidate(intersection_point_data)
      unless find_intersection_p
        raise "Lose track of range where there are intersections!"
      end

      shrink_range_by_distance_iteration = false
      if periodic_point_candidate.size == 2
        periodic_points = periodic_point_candidate.map do |cand|
          bisect_candidate_periodic_point(cand, projection_data, len, error)
        end
        if periodic_points[0] && periodic_points[1]
          break
        else
          shrink_range_by_distance_iteration = true
        end
      end
      if shrink_range_by_distance_iteration
        search_range = refine_search_range(:distance_iteration, intersection_point_data)
      else
        if search_range_new = refine_search_range(:remove_no_intersection, intersection_point_data)
          search_range = search_range_new
        else
          search_range = refine_search_range(:distance_iteration, intersection_point_data)
        end
      end
      if !search_range || search_range[0] == search_range[1]
        raise "Invalid search range: search_range=#{search_range.inspect.strip}"
      end
      if NUM_TRY_FIND_INTERSECTION == (i + 1)
        return nil
        # p periodic_point_candidate
        # We need to perturbe normal vector!
        # raise "Under construction: Can not find two candidates of periodic points"
      end
      search_range_len_new = search_range[0].distance_from(search_range[1])
      search_range_len = search_range_len
      if dists_mapped_last
        search_range_shrink = search_range_len_new / search_range_len
        dist_mapped_shrink = dists_mapped[0] / dists_mapped_last[0]
        @logger.debug("Search range shrink: #{search_range_shrink.to_strf('%.8Re')}, Distance shrink: #{dist_mapped_shrink.to_strf('%.8Re')}")
        if search_range_shrink * 50 < dist_mapped_shrink
          break
        end
      end
      dists_mapped_last = dists_mapped
    end
    periodic_points
  end
  private :search_and_narrow_bisection_range

  # @param [MPFR] error An error of periodic points
  # @return [Array] An array of two periodic points
  def search(error)
    projection_data, search_range, len = calc_neighborhood_of_tangency
    if search_range = get_search_range_near_tangency(projection_data, search_range, len)
      search_range = get_range_including_periodic_points(projection_data, search_range, len)
      search_and_narrow_bisection_range(projection_data, search_range, len, error)
    else
      nil
    end
  end
end

class SearchTangency
  # @param [Array] st_mfd_args [MODE, ITERATION, CRD_START, CRD_END] for stable manifold
  # @param [Array] un_mfd_args [MODE, ITERATION, CRD_START, CRD_END] for unstable manifold
  def initialize(st_mfd_args, un_mfd_args, prm_intersect, prm_apart, prm_error, mfd_error, output = nil)
    @st_mfd_args = st_mfd_args
    @un_mfd_args = un_mfd_args
    @prm_intersect = prm_intersect
    @prm_apart = prm_apart
    @prm_error = prm_error
    @mfd_error = mfd_error
    @determine = Intersect::Determine.new(@mfd_error, @mfd_error, :number, true)
    if @output = output
      @output = FileName.create(@output, :directory => :parent)
    end
  end

  def manifold_pair(prm)
    henon = MPFR::Henon::Mapping.new(prm)
    st_mfd = MappedMfd::PointsObj2D.new(InvMfd::Calculator.create_for_fp(prm, @st_mfd_args[0], MPFR),
                                        henon, @st_mfd_args[1], @st_mfd_args[2..3])
    un_mfd = MappedMfd::PointsObj2D.new(InvMfd::Calculator.create_for_fp(prm, @un_mfd_args[0], MPFR),
                                        henon, @un_mfd_args[1], @un_mfd_args[2..3])
    [st_mfd, un_mfd]
  end
  private :manifold_pair

  def calc
    bisect = MathUtils::Bisect.new
    bisect.set_objects(@prm_intersect, @prm_apart, :check => true) do |prm|
      st_mfd, un_mfd = manifold_pair(prm)
      intersections = @determine.search_intersections(st_mfd, un_mfd)
      if intersections
        res = Intersect::Function.confirm_intersect(st_mfd, un_mfd, intersections, @mfd_error / 10)
      else
        res = false
      end
      if @output
        Kernel.open(@output, 'a+') do |f|
          f.puts "#{prm[0]}\t#{prm[1]}\t#{res ? 'true' : 'false'}"
        end
      end
      res
    end
    bisect.set_creation do |prm1, prm2|
      [(prm1[0] + prm2[0]) / 2, (prm1[1] + prm2[1]) / 2]
    end
    bisect.set_end_condition do |prm1, prm2|
      (prm1[0] - prm2[0]).abs < @prm_error && (prm1[1] - prm2[1]).abs < @prm_error
    end
    bisect.execute
  end
end
