require "ruby_opengl_mpfr"
require_relative "index_set"

module Grid
  Struct.new('Cube', :box, :x, :y)
  class Cube < Struct::Cube
    def index
      [self.x, self.y]
    end

    def eql?(other)
      self.x == other.x && self.y == other.y
    end

    def hash
      (self.x < 0 ? self.x * 3 : self.x) + (self.y < 0 ? self.y * 15 : self.y * 7)
    end
    
  end

  class CubeCollection
    include Enumerable
    
    def initialize(ary)
      @data = ary
      @indexes_cache = nil
    end

    def [](i)
      @data[i]
    end
    
    def ==(other)
      if size != other.size
        false
      else
        ret = true
        each_with_index do |cube, i|
          if cube != other[i]
            return false
          end
        end
      end
    end
    
    def to_a
      @data
    end

    def to_display_obj(style)
      MPFR::Obj2D.new(:data => @data.map { |c| c.box }, :style => style)
    end

    def size
      @data.size
    end

    def +(other)
      self.class.new(@data | other.to_a)
    end
    
    def <<(cube)
      @data << cube
      @indexes_cache = nil
    end
    
    def indexes
      @indexes_cache || (@indexes_cache = @data.map { |c| [c.x, c.y] })
    end

    def clear_indexes_cache
      @indexes_cache = nil
    end
    
    def each(&block)
      if block
        @data.each(&block)
      else
        @data.each
      end
    end
    
    # 'cubes' is Array or Enumerator.
    def mapping(func, iterate, calc_size)
      res = []
      @data.each do |c|
        c.box.each_subdivision_by_size(calc_size) do |b|
          res << func.map(b, iterate)
        end
      end
      res
    end

    def restrict(restrict_indexes)
      ary = []
      @data.each { |cube| ary << cube if restrict_indexes.include?(cube.index) }
      self.class.new(ary)
    end
    
    def restrict!(restrict_indexes)
      i = 0
      while i < @data.size
        if not restrict_indexes.include?(@data[i].index)
          @data.delete_at(i)
        else
          i += 1
        end
      end
      clear_indexes_cache
    end
    
    def intersection_index(other)
      indexes & other.indexes
    end
    
    def union_index(other)
      indexes | other.indexes
    end

    def set_minus_index(other)
      indexes - other.indexes
    end
    
    def intersection(other)
      self.class.new(to_a & other.to_a)
    end

    def union(other)
      self.class.new(to_a | other.to_a)
    end

    def set_minus(other)
      self.class.new(to_a - other.to_a)
    end

    def subset?(other)
      indexes_set = indexes
      if Grid::CubeCollection === other
        other.indexes.all? { |ind| indexes_set.include?(ind) }
      elsif Array === other # Case of Array of indexes
        other.all? { |ind| indexes_set.include?(ind) }
      else
        raise ArgumentError, "Invalid argument"
      end
    end

    def output_for_chomp(io)
      each { |cube| io.puts "#{cube.x} #{cube.y}" }
    end

  end

  class Coordinate
    attr_reader :origin, :size, :left_bottom_base

    # Return [instance of Grid::Coordinate, xnum_range, ynum_range].
    def self.create(xrange, yrange, max_grid_size)
      xlen = (xrange[1] - xrange[0]).abs
      ylen = (yrange[1] - yrange[0]).abs
      xnum = (xlen / max_grid_size).ceil
      xnum += 1 if xnum % 2 == 0
      ynum = (ylen / max_grid_size).ceil
      ynum += 1 if ynum % 2 == 0
      [Grid::Coordinate.new(MPFR::ColumnVector.new([(xrange[0] + xrange[1]) / 2, (yrange[0] + yrange[1]) / 2]),
                            xlen / xnum, ylen / ynum),
       [-xnum / 2, xnum / 2], [-ynum / 2, ynum / 2]]
    end
    
    # Set origin and size of grid.
    # If the size of grid_size is one, grid is square.
    # If the size of grid_size is two, grid is rectangle.
    # The center point for region of which number is (0, 0) is origin.
    def initialize(origin, *grid_size)
      @origin = origin
      @size = grid_size
      @size[1] = grid_size[0] unless @size[1]
      @left_bottom_base = MPFR::ColumnVector.new([@origin[0] - @size[0]/MPFR.new(2), @origin[1] - @size[1]/MPFR.new(2)])
      @max_cache_size = 50000
      @cache_fi_box = []
      @cache_cube = []
    end

    def fi_box(x, y)
      if cache = @cache_fi_box.assoc([x, y])
        cache[-1]
      else
        box = MPFI::ColumnVector.new([MPFI.interval(@left_bottom_base[0] + @size[0]*x, @left_bottom_base[0] + @size[0]*(x + 1)),
                                      MPFI.interval(@left_bottom_base[1] + @size[1]*y, @left_bottom_base[1] + @size[1]*(y + 1))])
        @cache_fi_box << [[x, y], box]
        @cache_fi_box.shift if @cache_fi_box.size > @max_cache_size
        box
      end
    end

    def composite_fi_box(x_range, y_range)
      MPFI::ColumnVector.new([MPFI.interval(@left_bottom_base[0] + @size[0] * x_range.min,
                                            @left_bottom_base[0] + @size[0] * (x_range.max + 1)),
                              MPFI.interval(@left_bottom_base[1] + @size[1] * y_range.min,
                                            @left_bottom_base[1] + @size[1] * (y_range.max + 1))])
    end

    def cube(x, y)
      if cache = @cache_cube.assoc([x, y])
        cache[-1]
      else
        cube = Cube.new(fi_box(x, y), x, y)
        @cache_cube << [[x, y], cube]
        @cache_cube.shift if @cache_cube.size > @max_cache_size
        cube
      end
    end
    
    # *args is [[x1, y1], [x2, y2], ...] or [[xmin, xmax], [ymin, ymax]].
    def each_ranges(*args)
      case args.size
      when 1
        case args[0]
        when Grid::IndexSet
          args[0].each { |x, y| yield(x, y) }
        else
          args[0].each { |num| yield(*num) }
        end
      when 2
        (args[0][0]..args[0][1]).each do |x|
          (args[1][0]..args[1][1]).each { |y| yield(x, y) }
        end
      else
        raise "Invalid arguments. Number of arguments must be 1 or 2."
      end
    end
    
    def each_fi_box(*args)
      each_ranges(*args) { |x, y| yield(fi_box(x, y)) }
    end

    def each_cube(*args)
      each_ranges(*args) { |x, y| yield(cube(x, y)) }
    end

    def each_center(*args)
      each_ranges(*args) { |x, y| yield(center(x, y)) }
    end
    
    # Call block for each element of ranges.
    # The example of ranges is [[0,0], [0, 1], [0, 2]].
    def fi_box_ary(*args)
      ary = []
      each_ranges(*args) { |x, y| ary << fi_box(x, y) }
      ary
    end

    def cube_ary(*args)
      ary = []
      each_ranges(*args) { |x, y| ary << cube(x, y) }
      ary
    end

    def cube_collection(*args)
      ary = []
      each_ranges(*args) { |x, y| ary << cube(x, y) }
      Grid::CubeCollection.new(ary)
    end
    
    # Return left bottom corner of region (x, y).
    def left_bottom_corner(x, y)
      MPFR::ColumnVector.new([@left_bottom_base[0] + @size[0]*x, @left_bottom_base[1] + @size[1]*y])
    end

    # Return right bottom corner of region (x, y).
    def right_bottom_corner(x, y)
      MPFR::ColumnVector.new([@left_bottom_base[0] + @size[0]*(x + 1), @left_bottom_base[1] + @size[1]*y])
    end

    # Return left top corner of region (x, y).
    def left_top_corner(x, y)
      MPFR::ColumnVector.new([@left_bottom_base[0] + @size[0]*x, @left_bottom_base[1] + @size[1]*(y + 1)])
    end

    # Return right top corner of region (x, y).
    def right_top_corner(x, y)
      MPFR::ColumnVector.new([@left_bottom_base[0] + @size[0]*(x + 1), @left_bottom_base[1] + @size[1]*(y + 1)])
    end

    def xrange(*args)
      xmin = args[0]
      xmax = args[1] || args[0]
      [@left_bottom_base[0] + @size[0]*xmin, @left_bottom_base[0] + @size[0]*(xmax + 1)]
    end

    def yrange(*args)
      ymin = args[0]
      ymax = args[1] || args[0]
      [@left_bottom_base[1] + @size[1]*ymin, @left_bottom_base[1] + @size[1]*(ymax + 1)]
    end
    
    # Return [xmin, xmax, ymin ymax].
    def corner_coordinates(xnum, ynum)
      left_bottom = left_bottom_corner(xnum[0], ynum[0])
      right_top = right_top_corner(xnum[1], ynum[1])
      [left_bottom[0], right_top[0], left_bottom[1], right_top[1]]
    end
    
    # Return vertexies [left bottom, right bottom, right top, left top].
    def vertexies(x, y)
      [left_bottom_corner, right_bottom_corner, right_top_corner, left_top_corner]
    end

    # Return center of region (x, y).
    def center(x, y)
      MPFR::ColumnVector.new([@origin[0] + @size[0]*x, @origin[1] + @size[1]*y])
    end

    # fr is MPFR::ColumnVector.
    def index_include_pt(fr)
      x = MPFR::Math.div(MPFR::Math.sub(fr[0], @left_bottom_base[0]), @size[0]).floor
      y = MPFR::Math.div(MPFR::Math.sub(fr[1], @left_bottom_base[1]), @size[1]).floor
      Grid::IndexSet.new({ :x => x, :y => y })
    end
    
    def enclosure_index(fi)
      xfi = MPFI::Math.div(MPFI::Math.sub(fi[0], @left_bottom_base[0]), @size[0])
      yfi = MPFI::Math.div(MPFI::Math.sub(fi[1], @left_bottom_base[1]), @size[1])
      Grid::IndexSet.new({ :x => [xfi.left.floor, xfi.right.floor],
                           :y => [yfi.left.floor, yfi.right.floor] })
    end
    
    def enclosure_index_old(fi)
      xfi = MPFI::Math.div(MPFI::Math.sub(fi[0], @left_bottom_base[0]), @size[0])
      yfi = MPFI::Math.div(MPFI::Math.sub(fi[1], @left_bottom_base[1]), @size[1])
      yrange = (yfi.left.floor)..(yfi.right.floor)
      res = []
      ((xfi.left.floor)..(xfi.right.floor)).to_a.each do |x|
        yrange.each { |y| res << [x, y] }
      end
      res
    end

    def enclosure(fi)
      cube_collection(enclosure_index_old(fi))
    end

    def enclose_fi_boxes(fi_boxes)
      res = []
      fi_boxes.each { |box| res += enclosure_index_old(box) }
      cube_collection(res.uniq)
    end
    
    def enclose_cubes_indexes(cubes)
      res = []
      cubes.each { |c| res += enclosure_index_old(c.box) }
      res.uniq
    end
    
    def enclose_cubes(cubes)
      res = []
      cubes.each { |c| res += enclosure_index_old(c.box) }
      cube_collection(res.uniq)
    end
    
    def collar_index(indexes)
      new_indexes = []
      indexes.each do |ind|
        [[ind[0] - 1, ind[1] - 1], [ind[0] - 1, ind[1]], [ind[0] - 1, ind[1] + 1],
         [ind[0] + 1, ind[1] - 1], [ind[0] + 1, ind[1]], [ind[0] + 1, ind[1] + 1],
         [ind[0], ind[1] - 1], [ind[0], ind[1] + 1]].each do |new|
          new_indexes << new if not indexes.include?(new)
        end
      end
      new_indexes
    end

    # 'cubes' is instance of Grid::CubeCollection.
    def collar(cubes)
      cube_collection(collar_index(cubes.indexes))
    end

    def wrap_index(indexes)
      indexes + collar_index(indexes)
    end
    
    # 'cubes' is instance of Grid::CubeCollection.
    def wrap(cubes)
      cubes + cube_collection(collar_index(cubes.indexes))
    end

    # # Return BasicFigure::Lines instance.
    # # The keys of hash are :x and :y. If :x is Integer, :y must be Range. If :x is Range, :y must be Integer.
    # def line(hash)
    #   if Integer === hash[:x] && Range === hash[:y]
    #     BasicFigure::Lines.new([MPFR::ColumnVector.new([@origin[0] + @size[0]*hash[:x], @origin[1] + @size[1]*(hash[:y].min)]),
    #                             MPFR::ColumnVector.new([@origin[0] + @size[0]*hash[:x], @origin[1] + @size[1]*(hash[:y].max)])])
    #   elsif Integer === hash[:y] && Range === hash[:x]
    #     BasicFigure::Lines.new([MPFR::ColumnVector.new([@origin[0] + @size[0]*(hash[:x].min), @origin[1] + @size[1]*hash[:y]]),
    #                             MPFR::ColumnVector.new([@origin[0] + @size[0]*(hash[:x].max), @origin[1] + @size[1]*hash[:y]])])
    #   else
    #     raise ArgumentError, "Invalid arguments."
    #   end
    # end

    # Return region number including pt.
    def region_including(pt)
      [((pt[0] - @left_bottom_base[0]) / @size[0]).floor, ((pt[1] - @left_bottom_base[1]) / @size[1]).floor]
    end

    # Return number of region including _pt_ for x direction.
    def x_number(pt)
      ((pt[0] - @left_bottom_base[0]) / @size[0]).floor
    end

    # Return number of region including _pt_ for y direction.
    def y_number(pt)
      ((pt[1] - @left_bottom_base[1]) / @size[1]).floor
    end

    # # Evaluate &block with _direction_-coordinate of which interval is _pitch_.
    # # _direction_ is :x or :y.
    # # _num_ is integer meaning region number.
    # def each_coordinate(direction, num, pitch, &block)
    #   case direction
    #   when :x
    #     range = [left_bottom_corner(num, 0)[0], right_bottom_corner(num, 0)[0]]
    #   when :y
    #     range = [left_bottom_corner(0, num)[1], left_top_corner(0, num)[1]]
    #   end
    #   pt = range[0]
    #   while pt < range[1]
    #     yield(pt)
    #     pt += pitch
    #   end
    # end

    def mapping_region(indexes, func, iterate, calc_size)
      res = []
      each_fi_box(indexes) do |box|
        box.each_subdivision_by_size(calc_size) do |b|
          res << func.map(b, iterate)
        end
      end
      res
    end
    
    def enclosure_mapped_region(indexes, func, iterate, calc_size)
      res_index = Grid::IndexSet.new
      each_fi_box(indexes) do |box|
        box.each_subdivision_by_size(calc_size) do |b|
          res_index.union!(enclosure_index(func.map(b, iterate)))
        end
      end
      res_index
    end

    def display_obj(xrange, yrange)
      data = []
      data << left_bottom_corner(xrange[0], yrange[0])
      data << right_bottom_corner(xrange[1], yrange[0])
      (yrange[0]..yrange[1]).each do |y|
        data << left_top_corner(xrange[0], y)
        data << right_top_corner(xrange[1], y)
      end

      data << left_bottom_corner(xrange[0], yrange[0])
      data << left_top_corner(xrange[0], yrange[1])
      (xrange[0]..xrange[1]).each do |x|
        data << right_bottom_corner(x, yrange[0])
        data << right_top_corner(x, yrange[1])
      end

      obj = MPFR::Obj2D.new(:data => data, :style => MPFRPlot::STY_LINES)
      obj.add_split(*((2...(obj.size)).step(2).to_a))
      obj
    end

    def make_display_obj(index_set, edge = true)
      data = []
      if edge
        x_index, y_index = index_set.edge_coordinate_for_display_obj
        x_index.each do |ary|
          x = ary[0]
          ary[1].each { |y| data << left_bottom_corner(x, y) }
        end
        y_index.each do |ary|
          y = ary[0]
          ary[1].each { |x| data << left_bottom_corner(x, y) }
        end
        obj = MPFR::Obj2D.new(:data => data, :style => MPFRPlot::STY_LINES)
        obj.add_split(*((2...(obj.size)).step(2).to_a))
      else
        indexes = index_set.quads_coordinate_for_display_obj
        indexes.each { |a| data << left_bottom_corner(a[0], a[1]) }
        obj = MPFR::Obj2D.new(:data => data, :style => MPFRPlot::STY_QUADS)
      end
      obj
    end

    def change_grid(old_grid, index_set)
      new_index_set = Grid::IndexSet.new
      index_set.each_x_slice do |x, yary|
        xrng = old_grid.xrange(x)
        i = 0
        while i < yary.size
          yrng = old_grid.yrange(yary[i], yary[i + 1])
          new_index_set.union!(enclosure_index(MPFI::ColumnVector.interval([xrng, yrng])))
          i += 2
        end
      end
      new_index_set
    end
  end

  class MappingNoCache
    attr_reader :grid

    # 'func' must have method 'iterate_map'.
    def initialize(grid, func, calc_size)
      @grid = grid
      @func = func
      @calc_size = calc_size
    end

    def calc_enclosure_index(box, iterate)
      res_index = Grid::IndexSet.new
      box.each_subdivision_by_size(@calc_size) do |b|
        # res_index.union!(@grid.enclosure_index(@func.map(b, iterate)))
        res_index.union!(@grid.enclosure_index(@func.iterate_map(b, iterate)))
      end
      res_index
    end
    private :calc_enclosure_index

    def union_mapped_box(res_index, x, y, iterate)
      ind = calc_enclosure_index(@grid.fi_box(x, y), iterate)
      res_index.union!(ind)
    end
    private :union_mapped_box
    
    def enclosure_mapped_region(indexes, iterate)
      res_index = Grid::IndexSet.new
      indexes.each do |x, y|
        union_mapped_box(res_index, x, y, iterate)
      end
      res_index
    end

    # This method returns almost same results as enclosure_mapped_region.
    def iterate_enclosure_mapped_region(indexes, iterate)
      unless Fixnum === iterate && iterate > 0 
        raise "Invalid iterate number."
      end
      iterate.times do |i|
        indexes = enclosure_mapped_region(indexes, 1)
      end
      indexes
    end
    
  end

  class Mapping < MappingNoCache
    # 'func' must have method 'iterate_map'.
    def initialize(grid, func, calc_size, cache_size = nil)
      super(grid, func, calc_size)
      @cache = []
      @max_cache_size = cache_size || 50000
    end

    def union_mapped_box(res_index, x, y, iterate)
      if c = @cache.assoc([iterate, x, y])
        ind = c[1]
      else
        ind = calc_enclosure_index(@grid.fi_box(x, y), iterate)
        @cache << [[iterate, x, y], ind]
        @cache.pop if @cache.size > @max_cache_size
      end
      res_index.union!(ind)
    end
    private :union_mapped_box

  end

end
