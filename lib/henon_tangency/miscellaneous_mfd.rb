require "ruby_mpfr_henon"
require "ruby_mpfi_henon"

class MPFR
  def to_str_for_log
    to_strf("%.Re")
  end
end

class MPFI
  def to_str_for_log
    "#{right.to_strf("%.Re")}/#{left.to_strf("%.Re")}"
  end
end

class MPFR::ColumnVector
  alias_method :get_fr_distance, :distance_from

  def to_ptdata_string
    str_ary3("%.Re").join(" ")
  end
end

class MPFI::ColumnVector
  alias_method :get_fr_distance, :distance_center_pts

  def to_ptdata_string
    (to_str_ary.map{ |a| a.join("/") }).join(" ")
  end
end

module InvMfd

  # Create MPFR::Henon::MnfCalculator or MPFI::Henon::MnfCalculator.
  module Calculator
    DEFAULT_MPFR_MAPPING = MPFR::Henon::Mapping
    DEFAULT_MPFI_MAPPING = MPFI::Henon::MapN
    MPFR_SUFFICIENT_ERROR_FACTOR = MPFR.new(64)

    attr_accessor :const, :iterate, :evalues, :evectors, :nvectors, :map_iterate, :mode

    def self.check_argument(const, period, mode)
      if !((String === const[0] || MPFR === const[0]) && (String === const[1] || MPFR === const[1]))
        raise ArgumentError, "Invalid argument of const for #{self.class}#check_argument."
      elsif !(Integer === period && period > 0)
        raise ArgumentError, "Invalid argument of period for #{self.class}#check_argument."
      elsif !(Integer === mode && mode >= 0 && mode < 4)
        raise ArgumentError, "Invalid argument of mode for #{self.class}#check_argument."
      end
    end
    private_class_method :check_argument

    def self.get_arithmetic(const, period, pt)
      if /^MPFR/ =~ pt.class.to_s
        [DEFAULT_MPFR_MAPPING.new(const, period), MPFR]
      elsif /^MPFI/ =~ pt.class.to_s
        [DEFAULT_MPFI_MAPPING.new(const, period), MPFI]
      else
        raise ArgumentError, "Invalid class of argument pt for #{self.class}##{new}."
      end
    end
    private_class_method :get_arithmetic

    def self.information_fixed_point(mapping, pt, mode)
      vals, vecs = mapping.jacobian_matrix_iterate_map(pt).eigenvector
      if (MPFI === vals[0] ? vals[0].abs.right : vals[0].abs) < 1 &&
          (MPFI === vals[1] ? vals[1].abs.left : vals[1].abs) > 1
        sval, uval = vals
        svec, uvec = vecs
      elsif (MPFI === vals[1] ? vals[1].abs.right : vals[1].abs) < 1 &&
          (MPFI === vals[0] ? vals[0].abs.left : vals[0].abs) > 1
        uval, sval = vals
        uvec, svec = vecs
      else
        raise ArgumentError, "Not saddle point: #{pt.inspect.strip}"
      end

      if mode < 2
        if MPFI === sval && sval.has_zero?
          raise ArgumentError, "Invalid stable eigenvalue which have zero."
        end
        basis = (mode % 2 == 0 ? [svec, uvec] : [svec.neg, uvec])
        flip = ((MPFI === sval ? sval.left : sval) < 0 ? true : nil)
        [basis, :stable, flip, (flip && (mode % 2 == 1) ? true : nil), [sval, uval]]
      else
        if MPFI === uval && uval.has_zero?
          raise ArgumentError, "Invalid unstable eigenvalue which have zero."
        end
        basis = (mode % 2 == 0 ? [uvec, svec] : [uvec.neg, svec])
        flip = ((MPFI === uval ? uval.left : uval) < 0 ? true : nil)
        [basis, :unstable, flip, (flip && (mode % 2 == 1) ? true : nil), [sval, uval]]
      end
    end

    def self.some_variables_for_saddle_point(mapping, pt, mode)
      information_fixed_point(mapping, pt, mode)[0..(-2)]
    end
    private_class_method :some_variables_for_saddle_point

    def self.create_new_calculator(const, period, pt, basis, type, flip, arithmetic)
      if arithmetic == MPFI
        arithmetic::Henon::MfdCalculator.new(const, period, pt, basis, type, flip)
      else
        arithmetic::Henon::MfdCalculator.new(const, period, pt, basis, type, flip, 
                                             MPFR_SUFFICIENT_ERROR_FACTOR / MPFR::Math.exp2(MPFR.new(MPFR.get_default_prec)))
      end
    end

    # const is Array of two strings.
    # pt is MPFR or MPFI ColumnVector (fixed point, periodic point).
    # And mode is Integer 0, 1, 2 or 3 (0, 1: stable invariant manifold, 2, 3: unstable invariant manifold).
    # arithmetic is MPFR or MPFI.
    def self.create(const, period, pt, mode)
      check_argument(const, period, mode)
      mapping, arithmetic = get_arithmetic(const, period, pt)
      basis, type, flip, dup = some_variables_for_saddle_point(mapping, pt, mode)
      dup ? nil : create_new_calculator(const, period, pt, basis, type, flip, arithmetic)
    end
    
    # fp_mode is positive integer less than 8.
    def self.create_for_fp(const, fp_mode, arithmetic)
      mode = fp_mode % 4
      check_argument(const, 1, mode)
      if arithmetic == MPFR
        mapping = DEFAULT_MPFR_MAPPING.new(const, 1)
      elsif arithmetic == MPFI
        mapping = DEFAULT_MPFI_MAPPING.new(const, 1)
      else
        raise ArgumentError, "Invalid arithmetic for #{self}#create_calculator_for_fp."
      end
      pt = mapping.fixed_point[fp_mode / 4]
      basis, type, flip, dup = some_variables_for_saddle_point(mapping, pt, mode)
      dup ? nil : create_new_calculator(const, 1, pt, basis, type, flip, arithmetic)
    end
  end
  
  class DataFile
    def initialize(file_path)
      @path = file_path
      @file = nil
    end

    def open
      @file = open(@path, 'r+')
    end

    def close
      @file.close
    end

  end

  class PtData
    attr_reader :crd, :max_iteration, :past_iteration, :initial_position

    def initialize(crd, max_iteration, initial_position, past_iteration = 0)
      @crd = crd
      @max_iteration = max_iteration
      @initial_position= initial_position
      @past_iteration = past_iteration
      @cache = { }
    end

    def ==(other)
      @max_iteration == other.max_iteration && @past_iteration == other.past_iteration &&
        @initial_position == other.initial_position && @crd == other.crd
    end
    
    def position_on_mfd
      @past_iteration + @initial_position
    end

    def mpfr_crd
      MPFI::ColumnVector === @crd ? @crd.mid : @crd
    end

    def has_cache?(num)
      @cache.has_key?(num)
    end
    
    def add_cache(num, pt)
      @cache[num] = pt
    end

    def get_cache(num)
      @cache[num]
    end

    def set_max_iteration(num)
      @max_iteration = num
    end

    def distance_from(other)
      @crd.get_fr_distance(other.crd)
    end

    def to_s
      "#{@crd.to_ptdata_string} #{@max_iteration} #{@initial_position.to_strf("%.Re")} #{@past_iteration}"
    end

    def to_mid_fr
      if /^MPFI/ =~ @crd.class.to_s
        InvMfd::PtData.new(@crd.mid, @max_iteration, @initial_position, @past_iteration)
      else
        self
      end
    end
    
    def self.from_string(str)
      ary = str.split
      if /\// =~ ary[0]
        self.new(MPFI::ColumnVector.new([MPFI.interval(*ary[0].split('/')), MPFI.interval(*ary[1].split('/'))]), ary[2].to_i, MPFR.new(ary[3]), ary[4].to_i)
      else
        self.new(MPFR::ColumnVector.new([ary[0], ary[1]]), ary[2].to_i, MPFR.new(ary[3]), ary[4].to_i)
      end
    end

  end

  #
  # We assume that this class have array of coordinates of points.
  # But class of objects meaning coordinates is not specified and not restricted.
  #
  class SrcPts
    include Enumerable
    attr_reader :data

    # @param [Array] src_ary An array of source points (usually, MPFR::ColumnVector objects)
    def initialize(src_ary)
      @data = src_ary
    end

    def dup
      self.class.new(@data.dup)
    end

    def valid?
      (1...@data.size).each do |i|
        if @data[i - 1].initial_position >= @data[i].initial_position || @data[i - 1].crd == @data [i].crd
          $stderr.puts "@data[#{i - 1}]=" + @data[i-1].inspect
          $stderr.puts "@data[#{i}]=" + @data[i].inspect
          $stderr.puts "Subtraction of initial_position: " + (@data[i].initial_position - @data[i - 1].initial_position).inspect
          $stderr.puts "Subtraction of crd: " + (@data[i].crd - @data[i - 1].crd).inspect
          raise "Invalid SrcPts data."
        end
      end
      true
    end
    
    def [](num)
      @data[num]
    end

    def size
      @data.size
    end

    def each(&block)
      @data.each(&block)
    end
    
    # Add point data as num-th element.
    def add(num, pt)
      @data.insert(num, pt)
    end

    def delete_points(at, num = 1)
      num.times { |i| @data.delete_at(at) }
    end

    def output(at, num = 1, io = STDOUT)
      num_end = (at + num <= @data.size ? at + num : @data.size)
      (at...num_end).each { |i| io << @data[i].to_s + "\n" }
    end

    # @param [Array] args An array of Range objects
    def portion(*args)
      ranges = args.map do |a|
        if Fixnum === a
          Range.new(a, a)
        else
          a
        end
      end
      src_ranges = []
      ranges.sort_by(&:first).each do |r|
        if src_ranges.empty?
          src_ranges << r
        elsif src_ranges[-1].include?(r.first)
          src_ranges[-1] = Range.new(src_ranges[-1].first, [src_ranges[-1].last, r.last].max, r.exclude_end?)
        else
          src_ranges << r
        end
      end
      src_data = []
      src_ranges.each do |r|
        src_data += @data[r]
      end
      self.class.new(src_data)
    end
    
    def split_at(*args)
      if Integer === args[0] && args[0] > 0
        ret = [self.class.new(@data[0...args[0]])]
      else
        raise ArgumentError, "Invalid arguments for #{self.class}#split_at"
      end
      (1...(args.size)).each do |i|
        if Integer === args[i] && args[i-1] >= args[i]
          raise ArgumentError, "Invalid arguments for #{self.class}#split_at"
        end
        src_ary = @data[args[i-1]...args[i]]
        ret << self.class.new(src_ary) if src_ary.size > 0
      end
      ret << self.class.new(@data[args[-1]...(@data.size)]) if args[-1] < @data.size
      ret
    end

    def self.union(*srcs)
      self.new(srcs.inject([]) { |res, s| res + s.data })
    end

  end

  module PtDataFile
    def self.reduce_additional_info(input, output, mpfr_format = "%.16Re")
      open(FileName.create(output, :position => :middle), 'w') do |out|
        open(input, 'r') do |src|
          while l = src.gets
            if !(/^#/ =~ l) && !l.strip.empty?
              crd = InvMfd::PtData.from_string(l).mpfr_crd
              out.puts "#{crd[0].to_strf(mpfr_format)} #{crd[1].to_strf(mpfr_format)}"
            else
              out.puts l
            end
          end
        end
      end
    end
  end
  
  module FileMPFI
    def self.convert_to_mid(input, output)
      open(FileName.create(output, :position => :middle), 'w') do |out|
        open(input, 'r') do |src|
          while l = src.gets
            if !(/^#/ =~ l) && !l.strip.empty?
              out.puts InvMfd::PtData.from_string(l).to_mid_fr.to_s
            else
              out.puts l
            end
          end
        end
      end
    end

    def self.analyze_diameter(*args)
      count = { }
      max_diam = MPFR.new(0)
      args.each do |input|
        open(input, 'r') do |src|
          while l = src.gets
            if !(/^#/ =~ l) && !l.strip.empty?
              pt = InvMfd::PtData.from_string(l)
              [pt.crd[0].diam_abs, pt.crd[1].diam_abs].each do |d|
                exp = MPFR::Math.log10(d).ceil
                count[exp] = 0 unless count[exp]
                count[exp] += 1
                max_diam = d if max_diam < d
              end
            end
          end
        end
      end
      [max_diam, count]
    end
    
  end
end

