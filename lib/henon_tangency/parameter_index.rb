# An array having a hash as cache.
class ParameterIndex < Array
  def initialize(*args)
    super(args)
  end

  def move(hash)
    index_new = self.dup
    index_new.move!(hash)
    if @cache
      @cache.each do |k, v|
        index_new.cache[k] = v
      end
    end
    index_new
  end

  def move!(hash)
    hash.each do |key, val|
      self[key] += val
    end
  end

  def cache
    @cache || (@cache = {})
  end

  def set(hash)
    cache.merge!(hash)
  end

  def to_a
    Array.new(self)
  end

  def initialize_copy(obj)
    @cache = obj.cache.dup
    super
  end

  def inspect
    sprintf("#<%s:%#x %s, cache=%s>", self.class.to_s, self.__id__, super, @cache.inspect)
  end
end
