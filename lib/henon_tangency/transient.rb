class TransientTime
  THRESHOLD = MPFR('1e-10')

  # If +sink_orbit+ is a sink that is MPFR::ColumnVector,
  # we calculate the orbit of the sink from the value of +sink_orbit+.
  def initialize(max, iterate, prm, sink_orbit, period, threshold = nil)
    @max = max
    @iterate = iterate
    @threshold = threshold || THRESHOLD
    @prm = prm
    if MPFR::ColumnVector === sink_orbit
      sink_orbit = [sink_orbit]
      (period - 1).times do |i|
        sink_orbit << henon.map(sink_orbit[-1])
      end
    end
    @sorted_sink_orbit = sink_orbit.sort do |a, b|
      a[0] <=> b[0]
    end
    @period = period
  end

  # For drbqs via network.
  def henon
    @henon || (@henon = MPFR::Henon::Mapping.new(@prm, @iterate))
  end
  private :henon

  def calc(start_pt)
    if n = henon.transient_time(start_pt, @sorted_sink_orbit, @threshold, @max)
      if n >= 0
        [:convergent, n]
      else
        [:not_convergent]
      end
    else
      [:divergent]
    end
  end

  def calc_old(start_pt)
    pt = start_pt
    num = 0
    loop do
      pt = henon.iterate_map(pt)
      num += @iterate
      if !pt[0].number_p
        return [:divergent]
      elsif @sorted_sink_orbit.any? { |s| ((pt[0] - s[0]).abs < @threshold) && ((pt[1] - s[1]).abs < @threshold) }
        return [:convergent, num]
      elsif num > @max
        return [:not_convergent]
      end
    end
  end

end
