require "ruby_mpfr_henon"
require "ruby_mpfi_henon"
require "utils"
require "tc_grid"

module Grid
  class MappingWithCache
    include TCGrid
    attr_reader :grid

    def initialize(grid, func, calc_size, db_dir = '.')
      @grid = grid
      @func = func
      @calc_size = calc_size
      FileUtils.mkdir_p(File.expand_path(db_dir)) unless File.exist?(File.expand_path(db_dir))
      @cache = { :pos => HDB.new, :neg => HDB.new, :index => Grid::IndexSet.new }
      @db_paths = { :pos => db_dir + '/pos.tch', :neg => db_dir + '/neg.tch' }
      open_db do
        @cache[:pos].each_key do |str|
          x, y = unpack_key_string(str)
          @cache[:index].union!(:x => x, :y => y)
        end
      end
      @logger = ManageLog.get_new_logger(self.class)
    end

    def open_db(&block)
      open_database(@cache[:pos], @db_paths[:pos])
      open_database(@cache[:neg], @db_paths[:neg])
      if block_given?
        yield
        close_db
      end
    end

    def close_db
      close_database(@cache[:pos])
      close_database(@cache[:neg])
    end

    def calc_enclosure_pos_cache(box)
      res_index = Grid::IndexSet.new
      box.each_subdivision_by_size(@calc_size) do |b|
        res_index.union!(@grid.enclosure_index(@func.iterate_map(b, 1)))
      end
      res_index
    end

    def include_index?(str, x, y)
      ary = str.unpack("w*")
      (0...(ary.size)).step(3) do |i|
        x1, y1 = change_sgn(ary[i], ary[i + 1], ary[i + 2])
        if x1 == x && y1 == y
          return true
        end
      end
      return nil
    end
    
    def create_cache(index)
      index.each do |x1, y1|
        key1 = pack_coordinate_key(x1, y1)
        unless @cache[:pos][key1]
          pos_index = calc_enclosure_pos_cache(@grid.fi_box(x1, y1))
          @cache[:pos][key1] = pack_index(pos_index)
          pos_index.each do |x2, y2|
            key2 = pack_coordinate_key(x2, y2)
            if s = @cache[:neg][key2]
              unless include_index?(s, x1, y1)
                @cache[:neg][key2] = s + key1
              end
            else
              @cache[:neg][key2] = key1
            end
          end
        end
      end
    end

    def union_restrict_index(index)
      increment = index.minus(@cache[:index])
      @cache[:index].union!(index)
      if increment.size > 0
        @logger.debug("Computation of creating cache") { "#{increment.size} / #{@cache[:index].size} boxes." }
        open_db { create_cache(increment) }
      end
    end
    
    def enclosure_mapped_region(indexes, iterate)
      unless @cache[:index].include?(indexes)
        raise "Invalid argument"
      end
      res_index = Grid::IndexSet.new
      open_db do
        if iterate == :pos or iterate == :neg
          indexes.each do |x, y|
            if s = @cache[iterate][pack_coordinate_key(x, y)]
              res_index.union!(unpack_index_string(s))
            end
          end  
        else
          raise "Invalid argument."
        end
      end
      res_index
    end
    
  end
  
end

class CombiInvSet
  def initialize(grid_map)
    @grid_map = grid_map
    @logger = ManageLog.get_new_logger(self.class)
  end

  def target_region(indexes)
    @grid_map.grid.make_display_obj(indexes)
  end

  def comb_inv_set_indexes(initial_indexes)
    @grid_map.union_restrict_index(initial_indexes)
    new_index = initial_indexes
    i = 0
    begin
      @logger.debug("Step #{i}") do
        "Number of boxes: #{new_index.size}"        
      end
      i += 1
      old_index = new_index
      index_pos = @grid_map.enclosure_mapped_region(old_index, :pos)
      index_neg = @grid_map.enclosure_mapped_region(old_index, :neg)
      index_neg.intersection!(old_index)
      new_index = index_pos.intersection(index_neg)
    end while old_index != new_index
    new_index
  end

  def expand_initial_indexes(initial_indexes, save_prefix = nil)
    old_inv_nbd = comb_inv_set_indexes(initial_indexes).wrap
    new_inv_nbd = comb_inv_set_indexes(old_inv_nbd).wrap
    i = 0
    while i < 1000
      @logger.debug("Step #{i} starts.")
      old_inv_nbd.union!(new_inv_nbd)
      new_inv_nbd = comb_inv_set_indexes(old_inv_nbd).wrap
      if save_prefix
        open(FileName.create("#{save_prefix}_#{i}.dat", :position => :middle), 'w') { |f| f.puts new_inv_nbd.dump_to_array.inspect }
      end
      @logger.debug("old: #{old_inv_nbd.size} boxes, new: #{new_inv_nbd.size}")
      if old_inv_nbd.include?(new_inv_nbd)
        @logger.debug("Get isolating neighborhood.")
        ret = old_inv_nbd
        break
      end
      i += 1
    end
    if save_prefix
      open(FileName.create("#{save_prefix}_result.dat", :position => :middle), 'w') { |f| f.puts ret.dump_to_array.inspect }
    end
    ret
  end

  def suitable_comb_inv_set_indexes?(indexes, initial_indexes)
    initial_indexes.include?(indexes.wrap)
  end
  
end

class IndexPair
  def initialize(grid_map)
    @grid_map = grid_map
    @logger = ManageLog.get_new_logger(self.class)
  end

  # You need to check 'inv_set_indexes' by CombiInvSet#suitable_comb_inv_set_indexes?
  # before executing this method.
  def calc_index_pair(inv_part_indexes)
    comb_inv_indexes = inv_part_indexes
    col = comb_inv_indexes.collar
    p0 = @grid_map.enclosure_mapped_region(comb_inv_indexes, :pos)
    p0.intersection!(col)
    begin
      lastp0 = p0
      p0 = @grid_map.enclosure_mapped_region(p0, :pos)
      p0.intersection!(col)
      p0.union!(lastp0)
    end while p0 != lastp0
    p1 = p0.union(comb_inv_indexes)
    pbar1 = @grid_map.enclosure_mapped_region(p1, :pos)
    pbar0 = pbar1.minus(comb_inv_indexes)
    [p1, p0, pbar1, pbar0]
  end

  def check_result(p1, p0, pbar1, pbar0)
    if not pbar1.include?(p1)
      @logger.info("pbar1 does not include p1.")
    elsif not pbar0.include?(p0)
      @logger.info("pbar0 does not include p0")
    elsif not pbar1.include?(@grid_map.enclosure_mapped_region(p1, :pos))
      @logger.info("pbar1 does not include F(p1).")
    elsif not pbar0.include?(@grid_map.enclosure_mapped_region(p0, :pos))
      @logger.info("pbar0 does not include F(p0).")
    else
      return true
    end
    return false
  end

  def output_cubical_representation(io, domain_index)
    domain_index.each do |x, y|
      image_index = @grid_map.enclosure_mapped_region(Grid::IndexSet.new({ :x => x, :y => y }), :pos)
      ary = image_index.to_a.map! { |a| "(#{a[0]}, #{a[1]})" }
      io.puts "(#{x}, #{y}) -> {#{ary.join(', ')}}"
    end
  end
  
end

# ['~/Work/MathCalc/henon/lib/ruby', '~/Work/MathCalc/henon/lib/ext'].each { |d| $: << File.expand_path(d) }

# require 'mpfr_matrix'
# require 'mpfi_matrix'
# require 'ruby_mpfr_henon'
# require 'ruby_mpfi_henon'

# require 'utils'

# class CombiInvSet
#   def initialize(grid_map, iterate)
#     @grid_map = grid_map
#     @iterate = iterate
#     @logger = ManageLog.get_new_logger(self.class)
#   end

#   def target_region(indexes)
#     @grid_map.grid.make_display_obj(indexes)
#   end

#   def comb_inv_set_indexes(initial_indexes)
#     new_index = initial_indexes
#     i = 0
#     begin
#       @logger.debug("Step #{i}") do
#         "Number of boxes: #{new_index.size}"        
#       end
#       i += 1
#       old_index = new_index
#       index_pos = @grid_map.enclosure_mapped_region(old_index, @iterate)
#       index_neg = @grid_map.enclosure_mapped_region(old_index, -@iterate)
#       index_neg.intersection!(initial_indexes)
#       new_index = index_pos.intersection(index_neg)
#     end while old_index != new_index
#     new_index
#   end

#   def expand_initial_indexes(initial_indexes, save_prefix = nil)
#     old_inv_nbd = comb_inv_set_indexes(initial_indexes).wrap
#     new_inv_nbd = comb_inv_set_indexes(old_inv_nbd).wrap
#     i = 0
#     while i < 1000
#       @logger.debug("Step #{i} starts.")
#       old_inv_nbd.union!(new_inv_nbd)
#       new_inv_nbd = comb_inv_set_indexes(old_inv_nbd).wrap
#       if save_prefix
#         open("#{save_prefix}_#{i}.dat", 'w') { |f| f.puts new_inv_nbd.dump_to_array.inspect }
#       end
#       @logger.debug("old: #{old_inv_nbd.size} boxes, new: #{new_inv_nbd.size}")
#       if old_inv_nbd.include?(new_inv_nbd)
#         @logger.debug("Get isolating neighborhood.")
#         ret = old_inv_nbd
#         break
#       end
#       i += 1
#     end
#     if save_prefix
#       open("#{save_prefix}_result.dat", 'w') { |f| f.puts ret.dump_to_array.inspect }
#     end
#     ret
#   end

#   def suitable_comb_inv_set_indexes?(indexes, initial_indexes)
#     initial_indexes.include?(indexes.wrap)
#   end
  
# end

# class IndexPair
#   def initialize(grid_map, iterate)
#     @grid_map = grid_map
#     @iterate = iterate
#     @logger = ManageLog.get_new_logger(self.class)
#   end

#   # You need to check 'inv_set_indexes' by CombiInvSet#suitable_comb_inv_set_indexes?
#   # before executing this method.
#   def calc_index_pair(inv_part_indexes)
#     comb_inv_indexes = inv_part_indexes
#     col = comb_inv_indexes.collar
#     p0 = @grid_map.enclosure_mapped_region(comb_inv_indexes, @iterate)
#     p0.intersection!(col)
#     begin
#       lastp0 = p0
#       p0 = @grid_map.enclosure_mapped_region(p0, @iterate)
#       p0.intersection!(col)
#       p0.union!(lastp0)
#     end while p0 != lastp0
#     p1 = p0.union(comb_inv_indexes)
#     pbar1 = @grid_map.enclosure_mapped_region(p1, @iterate)
#     pbar0 = pbar1.minus(comb_inv_indexes)
#     [p1, p0, pbar1, pbar0]
#   end

#   def check_result(p1, p0, pbar1, pbar0)
#     if not pbar1.include?(p1)
#       @logger.info("pbar1 does not include p1.")
#     elsif not pbar0.include?(p0)
#       @logger.info("pbar0 does not include p0")
#     elsif not pbar1.include?(@grid_map.enclosure_mapped_region(p1, @iterate))
#       @logger.info("pbar1 does not include F(p1).")
#     elsif not pbar0.include?(@grid_map.enclosure_mapped_region(p0, @iterate))
#       @logger.info("pbar0 does not include F(p0).")
#     else
#       return true
#     end
#     return false
#   end

#   def output_cubical_representation(io, domain_index)
#     domain_index.each do |x, y|
#       image_index = @grid_map.enclosure_mapped_region(Grid::IndexSet.new({ :x => x, :y => y }), @iterate)
#       ary = image_index.to_a.map! { |a| "(#{a[0]}, #{a[1]})" }
#       io.puts "(#{x}, #{y}) -> {#{ary.join(', ')}}"
#     end
#   end
  
# end

