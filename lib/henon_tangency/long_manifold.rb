require "ruby_mpfr_henon"
require "ruby_mpfi_henon"

class MPFR::Henon::MfdCalculator
  alias_method :initial_two_points, :base_two_points
end

class MPFI::Henon::MfdCalculator
  def initial_two_points
    [box_including_mfd(MPFR.new(0)), box_including_mfd(MPFR.new(1))]
  end
end

module SrcInvMfd

  # @example
  #   obj = SrcInvMfd::CalcPtWithError.new(calc, max_iteration)
  #   obj.set_initial_points
  #   obj.set_save_file(FileName.create(path, :position => :middle))
  #   obj.calc_mfd(error)
  class CalcPtWithError
    MAX_CALC_AT_ONE_STEP = 5000
    MAX_DATA_SIZE = 5000
    LOG_OUTPUT = 1000

    attr_reader :save_file

    def initialize(calc, max_iteration)
      @calc = calc
      @max_iteration = max_iteration
      # @calc.set_max_iteration_for_unstable_manifold(@max_iteration) if @max_iteration
      @save_file = nil

      @num_calc_pts = 0
      @log_data = { :start_time => StartTime.new }
      @logger = ManageLog.get_new_logger(self.class)
    end

    # +pt+ is an InvMfd::PtData instance.
    def add_cache(pt, iterate)
      pt.add_cache(iterate, @calc.make_fr_cache(pt.crd, iterate)) unless pt.has_cache?(iterate)
    end

    def new_ptdata(pt, ratio)
      iteration, mapped = @calc.iteration_in_non_divergent_region(pt)
      data = InvMfd::PtData.new(pt, iteration, ratio)
      data.add_cache(iteration, mapped)
      data
    end
    private :new_ptdata

    def get_ptdata_from_pt(pt)
      ratio = @calc.get_ratio(pt)
      new_ptdata(pt, ratio)
    end
    private :get_ptdata_from_pt

    def get_ptdata_from_ratio(ratio)
      new_ptdata(*@calc.get_mfd_point_from_ratio(ratio))
    end
    private :get_ptdata_from_ratio

    def set_initial_points(*args)
      case args.size
      when 0
        @srcs = InvMfd::SrcPts.new(@calc.initial_two_points.map { |pt| get_ptdata_from_pt(pt) })
      when 1
        @srcs = InvMfd::SrcPts.new(args[0].map { |pt| get_ptdata_from_pt(pt) })
      when 2
        @srcs = InvMfd::SrcPts.new(args.map { |ratio| get_ptdata_from_ratio(MPFR(ratio)) })
      end
      if not (@srcs && InvMfd::SrcPts === @srcs)
        raise ArgumentError, "Invalid argument"
      end
      @log_data[:start_pt] = @srcs[0]
      @log_data[:total] = (@srcs[0].initial_position - @srcs[-1].initial_position).abs
    end

    def start_comment
      cst = @calc.constants
      pt = @calc.pt
      basis = @calc.basis
      <<COMMENT
# #{LogMessage.time_now}, #{pt.class.to_s.sub(/::.*$/, '')}, precision #{MPFR.get_default_prec}
# const : #{cst[0].to_str_for_log}
#         #{cst[1].to_str_for_log}
# target: #{pt[0].to_str_for_log}
#         #{pt[1].to_str_for_log}
# basis1: #{basis[0][0].to_str_for_log}
#         #{basis[0][1].to_str_for_log}
# basis2: #{basis[1][0].to_str_for_log}
#         #{basis[1][1].to_str_for_log}
# period: #{@calc.period}, type: #{@calc.type.to_s}, flip: #{(@calc.flip ? 'true' : 'nil')}, max iteration: #{@max_iteration}
COMMENT
    end
    private :start_comment

    def set_save_file(path)
      @save_file = path
      Kernel.open(@save_file, 'a+') { |f| f.print start_comment }
    end

    def distance_two_pts(pt1, pt2)
      iterate = (pt1.max_iteration <= @max_iteration && pt1.max_iteration >= 0 ? pt1.max_iteration : @max_iteration)
      add_cache(pt1, iterate)
      add_cache(pt2, iterate)
      pt1.get_cache(iterate).distance_from(pt2.get_cache(iterate))
    end
    private :distance_two_pts

    # Calculate point between @srcs[num] and @srcs[num + 1] and add the point to @srcs.
    def add_new_mfd_pt(num)
      @srcs.add(num + 1, new_ptdata(*@calc.get_pt_between_two_pts(@srcs[num].crd, @srcs[num + 1].crd)))
      @num_calc_pts += 1
    end
    private :add_new_mfd_pt
    
    def calc_fwd(ind, error)
      i = 0
      while (dist = distance_two_pts(@srcs[ind], @srcs[ind + 1])) > error
        @logger.debug("[calc_fwd distance]"){ "#{dist.to_strf("%.30Re")}" }
        add_new_mfd_pt(ind)
        if i > MAX_CALC_AT_ONE_STEP
          @logger.warn("[calc_fwd]"){ "Too many calculation #{i}." }
          break
        end
        i += 1
      end
      if dist == 0
        raise "Distance is #{dist}. Calculation does not make progress."
      end
      ind
    end
    private :calc_fwd

    def calc_bck(ind, error)
      i = 0
      while (dist = distance_two_pts(@srcs[ind], @srcs[ind - 1])) > error
        @logger.debug("[calc_bck distance]"){ "#{dist.to_strf("%.30Re")}" }
        add_new_mfd_pt(ind-1)
        if i > MAX_CALC_AT_ONE_STEP
          @logger.warn("[calc_bck]"){ "Too many calculation #{i}." }
          break
        end
        i += 1
      end
      if dist == 0
        raise "Distance is #{dist}. Calculation does not make progress."
      end
      ind
    end
    private :calc_bck

    def save_and_delete(ind)
      finish = ((@srcs[ind - 1].initial_position - @log_data[:start_pt].initial_position).abs / @log_data[:total]).to_f
      @logger.info("[estimation]"){ "#{sprintf("%.12f", finish * 100.0)} % finished. Remaining time is #{@log_data[:start_time].remaining_time(finish)}." }
      if @save_file
        @logger.info("[save]"){ "save #{ind-1} points." }
        Kernel.open(@save_file, 'a+') { |f| @srcs.output(0, ind, f) }
        begin
          @srcs.valid?
        rescue
          @logger.error("Invalid order of points on source manifold.")
          raise
        end
        @srcs.delete_points(0, ind)
        0
      else
        ind
      end
    end
    private :save_and_delete

    def calc_mfd(error)
      if MPFR === error
        ind = 0
        begin
          while ind < @srcs.size - 1
            @logger.debug("[calculation]"){ "index: #{ind}, data size: #{@srcs.size}" }
            ind = calc_fwd(ind, error) + 1
            ind = calc_bck(ind, error)
            ind = save_and_delete(ind) if @srcs.size > MAX_DATA_SIZE
          end
        ensure
          save_and_delete(@srcs.size)
        end
        @srcs.data
      else
        raise ArgumentError, "Invalid argument. error must be MPFR."
      end
    end
  end

  # 
  # Usage
  # obj = SrcInvMfd::CalcPtWithNumber.new(calc)
  # obj.set_initial_points
  # obj.set_save_file(FileName.create(path, :position => :middle))
  # obj.calc_mfd(number)
  # 
  class CalcPtWithNumber < CalcPtWithError
    def initialize(calc)
      super(calc, nil)
    end
    
    def add_new_mfd_pt(ind, ratio)
      if ratio > 0 && ratio < 1
        @srcs.add(ind + 1, new_ptdata(*@calc.get_mfd_point_from_ratio(ratio)))
        @num_calc_pts += 1
      else
        raise ArgumentError, "Invalid argument of #{self.class}#add_new_mfd_pt."
      end
    end
    private :add_new_mfd_pt

    def start_comment
      cst = @calc.constants
      pt = @calc.pt
      basis = @calc.basis
      <<COMMENT
# #{LogMessage.time_now}, #{pt.class.to_s.sub(/::.*$/, '')}, precision #{MPFR.get_default_prec}
# const : #{cst[0].to_str_for_log}
#         #{cst[1].to_str_for_log}
# target: #{pt[0].to_str_for_log}
#         #{pt[1].to_str_for_log}
# basis1: #{basis[0][0].to_str_for_log}
#         #{basis[0][1].to_str_for_log}
# basis2: #{basis[1][0].to_str_for_log}
#         #{basis[1][1].to_str_for_log}
# period: #{@calc.period}, type: #{@calc.type.to_s}, flip: #{(@calc.flip ? 'true' : 'nil')}
COMMENT
    end
    private :start_comment

    def calc_mfd(number)
      fraction = @srcs[0].initial_position
      pitch = (@srcs[-1].initial_position - @srcs[0].initial_position) / MPFR.new(number + 1)
      ind = 0
      for i in 0...number
        fraction += pitch
        add_new_mfd_pt(ind, fraction)
        ind += 1
        ind = save_and_delete(ind) if @srcs.size > MAX_DATA_SIZE
      end
      save_and_delete(@srcs.size)
      @srcs.data
    end

  end

end

module CreateMfdPt
  class TmpFilePtData
    TMP_FILE_NAME = 'tmp_ptdata.dat'

    def initialize(dir)
      @dir = FileName.create(dir.sub(/\/$/, '') + "/#{Time.now.to_i}_#{rand(10000)}", :directory => :self)
      @files = [FileName.create("#{@dir}/#{TMP_FILE_NAME}", :position => :middle)]
      @remove = true
    end

    def not_remove_file
      @remove = nil
    end

    def add_new_tmpfile
      @files << FileName.create("#{@dir}/#{TMP_FILE_NAME}", :position => :middle)
    end

    def [](num)
      if num >= -@files.size && num < @files.size
        @files[num]
      else
        raise ArgumentError, "Invalid number of temporary file"
      end
    end

    def exist?(num)
      FileTest.exist?(@files[num])
    end

    def last_tmpfile
      ret = nil
      (1..@files.size).each do |i|
        if exist?(-i)
          ret = @files[-i]
          break
        end
      end
      ret
    end

    def new_tmpfile
      add_new_tmpfile if exist?(-1)
      @files[-1]
    end

    def delete_tmpfile(num)
      if FileTest.exist?(@files[num])
        File.delete(@files[num])
        @files.delete_at(num)
      end
    end

    def delete_all_tmpfiles
      (0...@files.size).each { |i| delete_tmpfile(0) }
    end

    # Delete @files[1...-2]
    def delete_old_tmpfiles
      if @files.size > 3
        (1..(@files.size - 2)).each { |i| delete_tmpfile(1) }
      end
    end

    def clear
      delete_all_tmpfiles
      Dir.rmdir(@dir)
    end
    
  end

  class MapPtData

    MAX_DATA_SIZE = 1000

    attr_accessor :map_twice, :current_iteration

    # @param [MPFR::Henon::Mapping,MPFI::Henon::Mapping] henon Mapping
    # @param [boolean] inverse If the iteration is backward then the value is true
    def initialize(henon, inverse)
      @logger = ManageLog.get_logger(self.class)
      @henon = henon
      @sign = (inverse ? -1 : 1)
    end

    # @param [InvMfd::PtData] ptdata Source point data
    # @param [Fixnum] iteration Iteration number that is positive
    def map(ptdata, iteration)
      if ptdata && (ptdata.max_iteration - iteration) >= 0
        InvMfd::PtData.new(@henon.iterate_map(ptdata.crd, @sign * iteration), ptdata.max_iteration - iteration,
                           ptdata.initial_position, ptdata.past_iteration + iteration)
      else
        nil
      end
    end
  end

  class SavePtData

    MAX_DATA_SIZE = 1000

    attr_reader :error, :last
    
    def initialize(error)
      @error = error
      @last = nil
    end

    def reset_last_data
      @last = nil
    end

    def apart_sufficiently?(pt1)
      pt1.distance_from(@last) > @error
    end
    private :apart_sufficiently?

    def output(io, ary)
      ary.each do |pt|
        if pt && (!@last || (@last && apart_sufficiently?(pt)))
          io.puts pt.to_s
          @last = pt
        elsif @last && !pt
          io.puts "\n"
          @last = nil
        end
      end
    end

    def output_simply(io, ary)
      last = nil
      ary.each do |pt|
        if pt
          io.puts pt.to_s
          last = pt
        elsif last && !pt
          io.puts "\n"
          last = nil
        end
      end
    end

  end

  class MapPtFile
    module ParserComment
      # @return [Array] An array of strings of first comment lines and first point data
      def get_data_lines(path)
        lines = []
        MathUtils::DataFile.each_line(path) do |l|
          lines << l
          unless /^#/ =~ l
            break
          end
        end
        lines
      end

      # Return [MPFR or MPFI, constants, period, :stable or :unstable, flip (true or nil)].
      def analyze_file_comment(lines)
        case lines[0]
        when /MPFR/
          arithmetic = MPFR
        when /MPFI/
          arithmetic = MPFI
        else
          raise ArgumentError, "Invalid file."
        end
        data = []
        (1..2).each do |i|
          if /[0-9e\.\+-\/]+$/ =~ lines[i]
            data << (arithmetic == MPFR ? MPFR.new(Regexp.last_match[0]) : MPFI.interval(*Regexp.last_match[0].split('/')))
          else
            raise ArgumentError, "Invalid file."
          end
        end
        if arithmetic == MPFR
          base_pt = MPFR::ColumnVector(lines[3..4].map { |str| str.scan(/[^ ]+$/).first })
          basis1 = MPFR::ColumnVector(lines[5..6].map { |str| str.scan(/[^ ]+$/).first })
          basis2 = MPFR::ColumnVector(lines[7..8].map { |str| str.scan(/[^ ]+$/).first })
        else
          $stderr.puts "Under construction"
          base_pt = nil
          basis1 = nil
          basis2 = nil
        end
        data_str = lines[9]
        first_pt_str = lines[10]
        if /period: (\d+), type: (.+), flip: (true|nil)/ =~ data_str && (first_pt = InvMfd::PtData.from_string(first_pt_str))
          period = Regexp.last_match[1].to_i
          type = Regexp.last_match[2].to_sym
          flip = eval(Regexp.last_match[3])
          first_pt.set_max_iteration(MAX_ITERATION_FIRST_PT) if first_pt.max_iteration < 0
          if /max iteration: (.+)$/ =~ data_str
            max_iteration = Regexp.last_match[1].to_i
          end
          [arithmetic, data, period, type, flip, first_pt, max_iteration, base_pt, [basis1, basis2]]
        else
          raise ArgumentError, "Invalid file."
        end
      end
      private :analyze_file_comment

      def parse_comment(arg_file)
        analyze_file_comment(get_data_lines(arg_file))
      end
    end

    include CreateMfdPt::MapPtFile::ParserComment

    MAX_DATA_SIZE = 5000
    MAX_ITERATION_FIRST_PT = 100

    def initialize(file, error, tmp_dir = '/tmp')
      @logger = ManageLog.get_logger(self.class)
      @save_ptdata = CreateMfdPt::SavePtData.new(error)
      @tmpfile = CreateMfdPt::TmpFilePtData.new(tmp_dir)
      @file = file

      arithmetic, constants, period, @type, @flip, first_pt, @max_iteration, base_pt, basis_ary = parse_comment((Array === @file ? @file[0] : @file))
      henon = henon_mapping(arithmetic, constants, period)
      @mapping = CreateMfdPt::MapPtData.new(henon, @type == :stable)
      @initial_iteration = get_initial_iteration(first_pt, error)
    end

    def datafile_open(path, &block)
      if /\.gz$/ =~ path
        Zlib::GzipReader.open(path, &block)
      else
        Kernel.open(path, 'r', &block)
      end
    end
    private :datafile_open

    def henon_mapping(arithmetic, constants, period)
      arithmetic == MPFI ? MPFI::Henon::MapN.new(constants, period) : MPFR::Henon::Mapping.new(constants, period)
    end
    private :henon_mapping

    def set_max_iteration(num)
      @max_iteration = num
    end

    def get_initial_iteration(first_pt, error)
      i = 0
      new = @mapping.map(first_pt, 1)
      while first_pt.distance_from(new) < error
        new = @mapping.map(new, 1)
        i += 1
      end
      @logger.info('[initial iteration]'){ "#{i}" }
      i
    end
    private :get_initial_iteration

    def read_splited_data(io, &block)
      ary = []
      while l = io.gets
        unless /^#/ =~ l
          l.strip!
          if not l.empty?
            ary << InvMfd::PtData.from_string(l)
          else
            ary << nil
          end
          if ary.size >= MAX_DATA_SIZE
            yield(ary)
            ary = []
          end
        end
      end
      yield(ary) if ary.size > 0
    end
    private :read_splited_data

    def map_ptdata_one_step(io_src, io_out, io_tmp, iteration)
      read_splited_data(io_src) do |ary|
        new = ary.map { |pt| @mapping.map(pt, iteration) }
        @save_ptdata.output_simply(io_tmp, new) if io_tmp
        @save_ptdata.output(io_out, new) if io_out
      end
    end
    private :map_ptdata_one_step

    def map_ptdata_first_step(io_src, io_out, io_tmp)
      save_first_pt = true
      read_splited_data(io_src) do |ary|
        if save_first_pt
          @save_ptdata.output(io_out, [ary[0]]) if !@flip
          save_first_pt = nil
        end
        new = ary.map { |pt| @mapping.map(pt, @initial_iteration) }
        @save_ptdata.output_simply(io_tmp, new) if io_tmp
        @save_ptdata.output(io_out, new) if io_out
      end
    end
    private :map_ptdata_first_step

    def create_filenames(output)
      if @flip
        filename = FileName.new(output, :position => :middle, :add => :always)
        [filename.create, filename.create]
      else
        [FileName.create(output, :position => :middle)]
      end
    end
    private :create_filenames

    def iterate_tmpfile_ptdata(io_out, step, start_iteration)
      iteration = start_iteration + 1
      while iteration <= @max_iteration
        @logger.info("[iteration]") { iteration }
        tmp_new = @tmpfile.new_tmpfile
        tmp_last = @tmpfile.last_tmpfile
        Kernel.open(tmp_new, 'w') do |tmp|
          Kernel.open(tmp_last, 'r') { |src| map_ptdata_one_step(src, io_out, tmp, step) }
        end
        iteration += step
        @tmpfile.delete_old_tmpfiles
      end
    end
    private :iterate_tmpfile_ptdata

    def map_first_ptdata_file(out, tmp)
      if Array === @file
        @file.each do |path|
          datafile_open(path) { |src| map_ptdata_first_step(src, out, tmp) }
        end
      else
        datafile_open(@file) { |src| map_ptdata_first_step(src, out, tmp) }
      end
    end
    private :map_first_ptdata_file

    def output_file_open_write_comment(path)
      out = Kernel.open(path, "w")
      lines = get_data_lines(@file)
      lines.pop
      out.puts lines.join
      out
    end
    private :output_file_open_write_comment

    # This method is incomplete implementation for first point on invariant manifold.
    def map_file(output)
      @tmpfile.delete_all_tmpfiles
      filenames = create_filenames(output)
      @logger.info("[output]"){ filenames.join(" ") }
      out = output_file_open_write_comment(filenames[0])
      @initial_iteration -= 1 if @flip && @initial_iteration % 2 == 1
      Kernel.open(@tmpfile.new_tmpfile, 'w') do |tmp|
        map_first_ptdata_file(out, tmp)
      end
      if @flip
        iterate_tmpfile_ptdata(out, 2, @initial_iteration)
        @logger.info("[finish]"){ "#{filenames[0]}" }
        out.close
        @save_ptdata.reset_last_data
        out = output_file_open_write_comment(filenames[1])
        Kernel.open(@tmpfile.new_tmpfile, 'w') do |tmp|
          Kernel.open(@tmpfile[0], 'r') { |src| map_ptdata_one_step(src, out, tmp, 1) }
        end
        iterate_tmpfile_ptdata(out, 2, @initial_iteration + 1)
        @logger.info("[finish]"){ "#{filenames[1]}" }
      else
        iterate_tmpfile_ptdata(out, 1, @initial_iteration)
        @logger.info("[finish]"){ "#{filenames[0]}" }
      end
      out.close
      @tmpfile.clear
      filenames
    end
  end

  class CheckPtFile
    attr_reader :max_angle

    def initialize(path)
      @path = path
    end

    def angle(pt1, pt2, pt3)
      v1 = (pt2 - pt1).normalize
      v2 = (pt3 - pt2).normalize
      cos = (1 - (v1 - v2).abs / 2)
      MPFR::Math.acos(cos)
    end

    # Return false if points on manifold is curved more than the specified value.
    # Source data of manifold points must be almost straight.
    def valid?(limit_angle = 0.01)
      last_pts = []
      angle_max = 0.0
      DataFile.each_line(@path) do |line|
        unless /^#/ =~ line
          current_pt = MPFR::ColumnVector.new(line.split[0..1])
          if last_pts.size == 2
            ang = angle(*last_pts, current_pt)
            angle_max = ang if ang.abs > angle_max.abs
            last_pts.shift
          end
          last_pts << current_pt
        end
      end
      @max_angle = angle_max
      limit_angle > angle_max
    end
  end
end
