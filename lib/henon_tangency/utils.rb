require 'fileutils'
require 'logger'
require 'singleton'

module LogMessage
  def self.program_name
    $PROGRAM_NAME + (ARGV.size > 0 ? ' ' + ARGV.join(" ") : "")
  end

  def self.time_now
    Time.now.strftime("%c")
  end

end

# 
# Create logger for log level of Class.
#
# Usage
# class Test
#   def initialize
#     @logger = ManageLog.get_new_logger(self.class)
#   end
# end
# ManageLog.set_log_conf(Test, :level => Logger::WARN, :io => STDOUT)
#
module ManageLog

  # The instance of this class has data for logging.
  # This class include Singleton, so there is unique instance.
  class Manager
    include Singleton
    
    def initialize
      @data = { }
      @data[:default] = { :level => Logger::INFO, :io => STDOUT }
    end

    # Check whether hash is valid for configuration.
    def check(hash)
      if !hash.has_key?(:io)
        raise ArgumentError, "Invalid argument hash, which must include the key :io"
      elsif !hash.has_key?(:level) ||
          !(hash[:level] == Logger::DEBUG || hash[:level] == Logger::INFO || hash[:level] == Logger::WARN ||
            hash[:level] == Logger::ERROR || hash[:level] == Logger::FATAL)
        raise ArgumentError, "Invalid argument hash, which must include the key :level"
      end
      hash
    end
    private :check
    
    # Set default configuration. See set_log_conf for detail of hash.
    def set_default_conf(hash)
      @data[:default] = check(hash)
    end

    # Set configuration for class_name by hash.
    # The example of hash is { :level => Logger::INFO, :io => STDOUT }.
    # hash[:level] is Logger::DEBUG, Logger::INFO, Logger::WARN, Logger::ERROR or Logger::FATAL.
    def set_log_conf(class_name, hash)
      @data[class_name] = check(hash)
    end

    # Initialize logger for class_name.
    def init_logger(class_name)
      logger = Logger.new(@data[class_name][:io])
      logger.level = @data[class_name][:level]
      logger
    end
    private :init_logger

    # Return logger for class_name.
    def get_new_logger(class_name = :default)
      init_logger((@data.has_key?(class_name) ? class_name : :default))
    end
    
    # If transfered_logger is not Logger, return new logger.
    def get_logger(transfered_logger, class_name = :default)
      Logger === transfered_logger ? transfered_logger : get_new_logger(class_name)
    end

  end

  # See ManageLog::Manager.set_default_conf.
  def self.set_default_conf(hash)
    ManageLog::Manager.instance.set_default_conf(hash)
  end

  # See ManageLog::Manager.set_log_conf.
  def self.set_log_conf(class_name, hash)
    ManageLog::Manager.instance.set_log_conf(class_name, hash)
  end

  # See ManageLog::Manager.get_new_logger.
  def self.get_new_logger(class_name = :default)
    ManageLog::Manager.instance.get_new_logger(class_name)
  end
  
  # See ManageLog::Manager.get_logger.
  def self.get_logger(transfered_logger, class_name = :default)
    ManageLog::Manager.instance.get_logger(transfered_logger, class_name)
  end

  def self.log_level_args
    ["FATAL", "ERROR", "WARN", "INFO", "DEBUG"]
  end

  def self.log_level_from_arg(arg_str)
    if /^f/i =~ arg_str
      Logger::FATAL
    elsif /^e/i =~ arg_str
      Logger::ERROR
    elsif /^w/i =~ arg_str
      Logger::WARN
    elsif /^i/i =~ arg_str
      Logger::INFO
    elsif /^d/i =~ arg_str
      Logger::DEBUG
    else
      raise ArgumentError, "Invalid level of logging."
    end
  end
  
end

module OutputIO
  
  def self.open(out, &block)
    case out
    when IO
      io = out
      need_to_close = nil
    when String
      io = File.open(out, "w")
      need_to_close = true
    else
      raise ArgumentError, "Invalid object for outputting."
    end
    yield(io)
    io.close if need_to_close
  end
  
end

class StartTime
  
  # Record time which self initialize.
  def initialize
    @time = Time.now
  end

  # Return [hour, min, sec] from second.
  def hour_min_sec(arg_sec)
    min = arg_sec.to_i / 60
    sec = arg_sec.to_i % 60
    hour = min / 60
    min = min & 60
    [hour, min, sec]
  end
  private :hour_min_sec

  # Return elapsed time from time self initialize.
  def elapsed_time
    Time.now - @time
  end

  def remaining_time(finish)
    (hour_min_sec(elapsed_time / finish).map { |a| a.to_s }).join(":")
  end

end

