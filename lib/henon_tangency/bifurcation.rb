require "ruby_mpfr_henon"
require "ruby_mpfi_henon"
require "utils"

class Integer
  def divisor(include_self = true)
    if self > 0
      divisors = (1..((self / 2.0).floor)).select { |i| (self % i) == 0 }
      divisors << self if include_self
      divisors
    else
      raise "Divisor is not defined!"
    end
  end
end

module Henon

  class Bifurcation
    attr_accessor :initial_point

    DEFAULT_OUTPUT_FORMAT = "%.10Re"

    def initialize(prm_space, transient, num, check = 100)
      @prm_space = prm_space
      @transient = transient
      @num = num
      @check = check
      @threshold = MPFR('1e-10')
      @initial_point = MPFR::ColumnVector([0, 0])
    end

    def output_to_io(io, format = nil)
      initial_pt = nil
      format ||= DEFAULT_OUTPUT_FORMAT
      @prm_space.each do |prm|
        points = []
        henon = MPFR::Henon::Mapping.new(prm)
        initial_pt = henon.iterate_map(@initial_point, @transient)
        henon.each_pt_on_orbit(initial_pt, @num) do |pt|
          if pt[0].number_p
            out = nil
            if points.size >= @check
              out = true
            elsif points.all? { |pt2| (pt2[0] - pt[0]).abs > @threshold || (pt2[1] - pt[1]).abs > @threshold }
              out = true
              points << pt
            end
            if out
              io.puts "#{prm.map { |a| a.to_strf(format)}.join(' ') } #{pt.to_s(format)}"
            end
          else
            break
          end
        end
        io.puts ""
      end
    end

    def output(path, format = nil)
      open(path, 'w') do |f|
        output_to_io(f, format)
      end
    end
  end

  class BifRange
    def initialize(period, th_close_pt, map_num, max_period, threshold_apart = MPFR.new('1e-3'))
      @logger = ManageLog.get_new_logger(self.class)
      @period = period
      @mapping_number = map_num
      @max_period = max_period
      @threshold_close_pt = th_close_pt
      @threshold_apart = threshold_apart
    end

    MAX_CONVERGENCE_ITERATE = 100_000
    NUM_TEST_CONVERGENCE = 10

    def distances_between_pts_on_orbit(henon_fr, orbit_size, pt_start, period)
      pt_last = pt_start
      orbit_size.times.map do |n|
        pt_new = henon_fr.iterate_map(pt_last, period)
        dist = pt_new.distance_from(pt_last)
        pt_last = pt_new
        dist
      end
    end
    private :distances_between_pts_on_orbit

    # @note We must pay attention to the default precision.
    #   If the default precision is not sufficient large, this method can not determine convergence.
    def test_convergence(henon_fr, pt_base, candidate_period)
      periodic_pt = nil
      pt1 = henon_fr.iterate_map(pt_base, candidate_period)
      pt2 = henon_fr.iterate_map(pt1, candidate_period)
      dist1 = pt_base.distance_from(pt1)
      if dist1 < @threshold_close_pt
        return pt2
      end
      jac = henon_fr.jacobian_matrix_iterate_map(pt2)
      eigenvalues = jac.eigenvalue
      eigenvalues_less_than_one_p = eigenvalues.all? { |val| val.abs < 1 }
      @logger.debug("Eigenvalues") { "#{eigenvalues[0].to_strf("%.20Re")} #{eigenvalues[1].to_strf("%.20Re")}" }
      distances = distances_between_pts_on_orbit(henon_fr, NUM_TEST_CONVERGENCE, pt2, candidate_period)
      if !eigenvalues_less_than_one_p
        @logger.debug("Not sink")
        return nil
      end
      convergence_rates = (distances.size - 1).times.map do |n|
        distances[n + 1] / distances[n]
      end
      @logger.debug("Convergence rate") { (convergence_rates.map { |rate| rate.to_strf("%.20Re") }).join("\n") }
      rate_max_contraction = nil
      convergence_rates.sort.each do |rate|
        rate_max_contraction = rate if rate < 1
      end
      unless rate_max_contraction
        @logger.debug("Not Cauchy sequence?")
        return nil
      end
      iterate_sufficient_large = (MPFR::Math.log(@threshold_close_pt / distances.max) / MPFR::Math.log(rate_max_contraction)).ceil * 2
      num = (iterate_sufficient_large * candidate_period).to_i
      if MAX_CONVERGENCE_ITERATE < num
        @logger.debug("Too slow convergence") { "Need to iterate #{num} times" }
        num = MAX_CONVERGENCE_ITERATE
      end
      pt_new = henon_fr.iterate_map(pt2, num)
      distances = distances_between_pts_on_orbit(henon_fr, NUM_TEST_CONVERGENCE, pt_new, candidate_period)

      if distances.max < @threshold_close_pt
        @logger.debug("Cauchy sequence") { dist.to_strf("%.30Re") }
        periodic_pt = pt_new
      else
        convergence_rates = (distances.size - 1).times.map do |n|
          distances[n + 1] / distances[n]
        end
        if convergence_rates.all? { |rate| rate < 1 }
          periodic_pt = pt_new
          @logger.debug("Cauchy sequence (Not complete convergence)") { distances.max.to_strf("%.30Re") }
        else
          @logger.debug("Not Cauchy sequence?") { (convergence_rates.map { |rate| rate.to_strf("%.20Re") }).join("\n") }
        end
      end
      periodic_pt
    end

    THRESHOLD_MIN_CANDIDATE = "1e-4"

    # @return [Array,false] Return an array of period and coordinate
    #     if a periodic point related to pt_start exists. Otherwise, false.
    # @param [Array] prms An array of two constants of Henon map.
    # @param [MPFR::ColumnVector] pt_start Initial point
    # @note Constant iteration number can not always determine convergences of orbits,
    #     because convergence rate are not constant.
    #     For example, at parameters in a neighborhood of period doubling bifurcation,
    #     the convergence of an orbit is very slow.
    #     Therefore, if orbits do not converge, this method assume that a stable periodic orbit exists
    #     and seek a candidate of period of the stable periodic orbit.
    #     Then, it estimates convergence rates and investigate the orbit after sufficient large iteration
    #     (which is maybe sufficient large, but is not completely).
    def stable_periodic_point?(prms, pt_start)
      @logger.debug("Start stable_periodic_point?")
      henon_fr = MPFR::Henon::Mapping.new(prms, @period)
      pt_base = henon_fr.iterate_map(pt_start, @mapping_number)
      dist = pt_start.distance_from(pt_base).abs
      @logger.debug("Distance (initial)") { dist.to_strf("%.30Re") }
      if dist > @threshold_apart
        @logger.debug("Divergence")
        return false
      elsif dist < @threshold_close_pt
        number_test_period = @mapping_number
      else
        number_test_period = @max_period
      end
      pt_new = pt_base
      ind_almost_min = nil
      distances = []
      number_test_period.times do |i|
        pt_new = henon_fr.iterate_map(pt_new)
        dist = pt_new.distance_from(pt_base).abs
        distances << dist
        @logger.debug("Distance (#{i})") { dist.to_strf("%.30Re") }
        if dist < @threshold_close_pt
          @logger.debug("Convergence") { @threshold_close_pt.to_strf("%.30Re") }
          ind_almost_min = i
          break
        end
      end

      unless ind_almost_min
        threshold = MPFR(THRESHOLD_MIN_CANDIDATE)
        dist_almost_min = distances[0]
        ind_almost_min = 0
        (1...(distances.size)).each do |i|
          dist = distances[i]
          if (dist / dist_almost_min) < threshold
            dist_almost_min = dist
            ind_almost_min = i
          end
        end
      end

      candidate_period = ind_almost_min + 1
      @logger.debug("Candidate period #{candidate_period}") { distances[ind_almost_min].to_strf("%.30Re") }

      ret = false
      if periodic_pt = test_convergence(henon_fr, pt_base, candidate_period)
        ret = [candidate_period, periodic_pt]
        candidate_period.divisor(false).reverse.each do |period_tmp|
          @logger.debug("Test small period #{period_tmp}")
          if periodic_pt_new = test_convergence(henon_fr, periodic_pt, period_tmp)
            @logger.debug("Period #{period_tmp} is convergent")
            ret = [period_tmp, periodic_pt_new]
          end
        end
      end
      ret
    end

    # @return [Array,false] If a stable periodic point of period exists in a neighborhood of pt_start,
    #     we get an array [period, coordinate]. Otherwise, false.
    def periodic_point_exist?(prms, pt_start, period)
      if ret = stable_periodic_point?(prms, pt_start)
        @logger.debug("Period: target #{period}, get #{ret[0]}")
        if ret[0] == period
          return ret
        end
      end
      false
    end

    # @return [Array,false] If an attractor same as pt_start exists, this method returns an array,
    #   which is used in Henon::BifRange#shrink_range.
    #   Otherwise false. In special cale that pt_start diverges, false.
    def same_attractor_exist?(prms, pt_start)
      @logger.info("Search same attractor for #{prms[0].to_strf("%.30Re")} #{prms[1].to_strf("%.30Re")}")
      henon_fr = MPFR::Henon::Mapping.new(prms, @period)
      pt_base = henon_fr.iterate_map(pt_start, @mapping_number)
      if pt_base.bounded?
        if pt_base.distance_from(pt_start) > @threshold_apart
          @logger.info("Attractor does not exist.") { pt_base.to_s("%.30Re") }
          return false
        end
        pt_new = pt_base
        @max_period.times do |i|
          pt_new = henon_fr.iterate_map(pt_new)
          dist = pt_new.distance_from(pt_base)
          @logger.debug("Distance") { dist.to_strf("%.30Re") }
          if pt_base.distance_from(pt_start) > @threshold_apart
            @logger.info("Attractor does not exist.") { pt_new.to_s("%.30Re") }
            return false
          end
        end
        @logger.info("Attractor exists.") { pt_new.to_s("%.30Re") }
        [0, pt_new]
      else
        false
      end
    end

    SHRINK_ITERATE_STEP = 5
    SPLIT_RANGE = 20

    # Test whether returned values of specified method changes many times at each parameter in the range
    # If the returned values changes many times, the parameter range is not suitable for bisection method.
    def shrink_range(pt_start, prm_ary, ind, prm_true, prm_false, method_name, method_opt_args = [])
      ret = nil
      pt_start_tmp = pt_start
      step_rest = SHRINK_ITERATE_STEP
      while step_rest > 0
        pt_true = pt_start_tmp
        prm_var = (prm_false - prm_true) / MPFR.new(SPLIT_RANGE)
        if prm_var == 0
          @logger.debug("prm_false coincides with prm_true")
          break
        end
        @logger.debug("prm_var = #{prm_var.to_strf("%.30Re")}")
        prm_cur = prm_true
        prm_tmp = prm_ary.dup
        ary = SPLIT_RANGE.times.map do |j|
          prm_cur += prm_var
          prm_tmp[ind] = prm_cur
          if res = __send__(method_name, prm_tmp, pt_start_tmp, *method_opt_args)
            pt_start_tmp = res[1]
            [true, prm_cur, pt_start_tmp]
          else
            [false, prm_cur, pt_start_tmp]
          end
        end

        num_change = 0
        find_first_false = -1
        last_res = true
        ary.each_with_index do |a, j|
          if a[0] != last_res
            num_change += 1
            if find_first_false < 0
              find_first_false = j
              if j > 0
                prm_true = ary[j - 1][1]
                pt_true = ary[j - 1][2]
              end
            end
            prm_false = a[1]
          end
          last_res = a[0]
        end
        if num_change != 1
          @logger.debug("Not suitable parameters for bisection method: num_change=#{num_change}")
          # ary.each_with_index do |a, i|
          #   p a
          # end
        end
        ret = [pt_true, prm_true, prm_false]
        if find_first_false > 0
          step_rest -= 1
        else
          prm_false += prm_var
        end
      end
      ret
    end
    private :shrink_range
    
    RATIO_PRM_RANGE_AND_ERR = "1e-4"
    
    # @return [Array] An array of the interval of periodic window
    # @param [Array] prms_start An array of two parmeters of Henon map
    # @param [MPFR::ColumnVector] pt_start Initial point of iteration
    # @param [Array] prm_range An array [index_number_to_be_changed, min_parameter, max_parameter].
    #     min_parameter and max_parameter must be parameters where the attractor does not exist.
    # @param [MPFR,nil] max_prm_diff A maximum error of the returned interval.
    #     If max_prm_diff is nil, we refine parameter range until errors are sufficient small
    #     compared with the size of parameter range.
    def stable_prm_range(prms_start, pt_start, prm_range, max_prm_diff = nil)
      prms_tmp1 = prms_start.dup
      prms_tmp2 = prms_start.dup
      ind = prm_range[0]
      prms_tmp1[ind] = prm_range[1]
      prms_tmp2[ind] = prm_range[2]

      if ret = stable_periodic_point?(prms_start, pt_start)
        period = ret[0]
        pt_start = ret[1]
      else
        raise ArgumentError, "Invalid prms_start and pt_start."
      end
      if stable_periodic_point?(prms_tmp1, pt_start)
        raise ArgumentError, "Invalid prms_tmp1 and pt_start."
      end
      if stable_periodic_point?(prms_tmp2, pt_start)
        raise ArgumentError, "Invalid prms_tmp2 and pt_start."
      end

      pt_min, prm_min_true, prm_min_false = shrink_range(pt_start, prms_start, ind, prms_start[ind], prm_range[1], :periodic_point_exist?, [period])
      prms_tmp1[ind] = prm_min_true

      pt_max, prm_max_true, prm_max_false = shrink_range(pt_start, prms_start, ind, prms_start[ind], prm_range[2], :periodic_point_exist?, [period])
      prms_tmp2[ind] = prm_max_true

      max_prm_diff ||= (prm_max_false - prm_min_false).abs * MPFR(RATIO_PRM_RANGE_AND_ERR)

      min_err_size = (prm_min_true - prm_min_false).abs
      while max_prm_diff < min_err_size
        pt_min, prm_min_true, prm_min_false = shrink_range(pt_min, prms_tmp1, ind, prm_min_true, prm_min_false, :periodic_point_exist?, [period])
        min_err_size_new = (prm_min_true - prm_min_false).abs
        if (min_err_size_new / min_err_size) > 0.5
          @logger.debug("Can not refine the minimum value of parameter range") { min_err_size_new.to_strf("%.30Re") }
          break
        end
        min_err_size = min_err_size_new
      end

      min_err_size = (prm_max_true - prm_max_false).abs
      while max_prm_diff < min_err_size
        pt_max, prm_max_true, prm_max_false = shrink_range(pt_max, prms_tmp1, ind, prm_max_true, prm_max_false, :periodic_point_exist?, [period])
        min_err_size_new = (prm_max_true - prm_max_false).abs
        if (min_err_size_new / min_err_size) > 0.5
          @logger.debug("Can not refine the maximum value of parameter range")
          break
        end
        min_err_size = min_err_size_new
      end

      if prm_min_true == prm_max_true
        raise "Stable parameter range has 0 length."
      end
      [prm_min_true, prm_max_true]
    end

    # @param [Array] prms_start An array of two MPFR objects.
    # @param [MPFR::ColumnVector] pt_start
    # @param [Array] prm_range [prm_index, prm1, prm2], where prm1 and prm2 do not have same attractor for prm_start.
    # @param [MPFR,nil] max_prm_diff A maximum error of the returned interval.
    #     If max_prm_diff is nil, we refine parameter range until errors are sufficient small
    #     compared with the size of parameter range.
    # @return [Array] An array that means interval that the sink persists.
    def calc_duration(prms_start, pt_start, prm_range, max_prm_diff = nil)
      # We must improve the following algorithm!
      prms_tmp1 = prms_start.dup
      prms_tmp2 = prms_start.dup
      prms_tmp1[prm_range[0]] = prm_range[1]
      prms_tmp2[prm_range[0]] = prm_range[2]
      ind = prm_range[0]
      if prms_tmp1 == prms_tmp2
        raise ArgumentError, "Invalid parameter range: length of range is 0"
      end
      unless same_attractor_exist?(prms_start, pt_start)
        raise ArgumentError, "Invalid prms_start and pt_start."
      end
      if same_attractor_exist?(prms_tmp1, pt_start)
        raise ArgumentError, "Invalid prms_tmp1 and pt_start."
      end
      if same_attractor_exist?(prms_tmp2, pt_start)
        raise ArgumentError, "Invalid prms_tmp2 and pt_start."
      end

      pt_min, prm_min_true, prm_min_false = shrink_range(pt_start, prms_start, ind, prms_start[ind], prm_range[1], :same_attractor_exist?)
      prms_tmp1[ind] = prm_min_true

      pt_max, prm_max_true, prm_max_false = shrink_range(pt_start, prms_start, ind, prms_start[ind], prm_range[2], :same_attractor_exist?)
      prms_tmp2[ind] = prm_max_true

      max_prm_diff ||= (prm_max_false - prm_min_false).abs * MPFR(RATIO_PRM_RANGE_AND_ERR)

      min_err_size = (prm_min_true - prm_min_false).abs
      while max_prm_diff < min_err_size
        pt_min, prm_min_true, prm_min_false = shrink_range(pt_min, prms_tmp1, ind, prm_min_true, prm_min_false, :same_attractor_exist?)
        min_err_size_new = (prm_min_true - prm_min_false).abs
        if (min_err_size_new / min_err_size) > 0.5
          @logger.debug("Can not refine the minimum value of parameter range") { min_err_size_new.to_strf("%.30Re") }
          break
        end
        min_err_size = min_err_size_new
      end

      min_err_size = (prm_max_true - prm_max_false).abs
      while max_prm_diff < min_err_size
        pt_max, prm_max_true, prm_max_false = shrink_range(pt_max, prms_tmp1, ind, prm_max_true, prm_max_false, :same_attractor_exist?)
        min_err_size_new = (prm_max_true - prm_max_false).abs
        if (min_err_size_new / min_err_size) > 0.5
          @logger.debug("Can not refine the maximum value of parameter range")
          break
        end
        min_err_size = min_err_size_new
      end

      if prm_min_true == prm_max_true
        raise "Stable parameter range has 0 length."
      end
      [prm_min_true, prm_max_true]
    end

    def trace_periodic_point(pt_start, prm_start, prm_end, split_num, iterate_num)
      prm_var = [(prm_end[0] - prm_start[0]) / split_num, (prm_end[1] - prm_start[1]) / split_num]
      prms = prm_start.dup
      pt = pt_start
      split_num.times do |i|
        prms[0] += prm_var[0]
        prms[1] += prm_var[1]
        henon_fr = MPFR::Henon::Mapping.new(prms, @period)
        pt = henon_fr.iterate_map(pt, iterate_num)
      end
      henon_fr = MPFR::Henon::Mapping.new(prm_end, @period)
      henon_fr.iterate_map(pt, iterate_num)
    end
  end

  class BifImage
    def initialize(period, step_size, transient, map_num, threshold_close_pt, max_check_num)
      @period = period
      @step_size = step_size
      @transient = transient
      @map_num = map_num
      @threshold_close_pt = threshold_close_pt
      @max_check_num = max_check_num
    end

    def make_orbit(pt_start, henon_fr, &block)
      pt = henon_fr.iterate_map(pt_start, @transient)
      orbits = []
      henon_fr.each_pt_on_orbit(pt, @map_num) do |a|
        exist_near_pt = false
        orbits.each do |b|
          if (a[0] - b[0]).abs < @threshold_close_pt && (a[1] - b[1]).abs < @threshold_close_pt
            exist_near_pt = true
            break
          end
        end
        yield(a) unless exist_near_pt
        orbits << a if orbits.size < @max_check_num
      end
      pt
    end

    # @param [String] path Output path
    # @param [MPFR::ColumnVector] pt_start Initial point of tracing
    # @param [Array] prm_range An array that is [index, minimum parameter, maximum parameter]
    # @param [Hash] opts Option hash
    # @option opts [String] :format Format string of the method MPFR#to_strf.
    #   If the value is nil then sufficient precise format is set automatically.
    def output_text(path, pt_start, prms_start, prm_range, opts = {})
      unless format = opts[:format]
        format = "%.#{(-MPFR::Math.log10(@step_size * MPFR("1e-10"))).to_i}Re"
      end
      ind = prm_range[0]
      prm_min, prm_max = prm_range[1..2].sort
      prms_tmp = prms_start.dup
      
      Kernel.open(path, 'w') do |f|
        pt_start_tmp = pt_start
        prm_cur = prms_start[ind]
        while prm_min < prm_cur
          prms_tmp[ind] = prm_cur
          pt_start_tmp = make_orbit(pt_start_tmp, MPFR::Henon::Mapping.new(prms_tmp, @period)) do |pt|
            f.puts "#{prms_tmp[ind].to_strf(format)} #{pt[0].to_strf(format)} #{pt[1].to_strf(format)}"
          end
          prm_cur -= @step_size
        end

        pt_start_tmp = pt_start
        prm_cur = prms_start[ind] + @step_size
        while prm_max > prm_cur
          prms_tmp[ind] = prm_cur
          pt_start_tmp = make_orbit(pt_start_tmp, MPFR::Henon::Mapping.new(prms_tmp, @period)) do |pt|
            f.puts "#{prms_tmp[ind].to_strf(format)} #{pt[0].to_strf(format)} #{pt[1].to_strf(format)}"
          end
          prm_cur += @step_size
        end
      end
      path
    end

    def get_image_range(path, pt_start, threshold_apart)
      prms = []
      xrange = []
      prm_last = nil
      skip_prm = false
      orbit = []
      Kernel.open(path, "r") do |f|
        while l = f.gets
          ary = l.split
          prm = MPFR(ary[0])
          pt = MPFR::ColumnVector(ary[1..2])
          if prm_last == prm
            if !skip_prm && pt.distance_from(pt_start) > threshold_apart
              skip_prm = true
              orbit.clear
            end
            if !skip_prm
              orbit << pt
            end
          else
            skip_prm = false
            if orbit.size > 0
              prms << prm_last if prm_last
              x_coordinate = (xrange.concat(orbit.map { |pt| pt[0] })).sort!
              xrange = [x_coordinate[0], x_coordinate[-1]]
              orbit.clear
            end
            skip_prm = pt.distance_from(pt_start) > threshold_apart
            if !skip_prm
              orbit << pt
            end
          end
          prm_last = prm
        end
        if orbit.size > 0
          prms << prm_last if prm_last
          x_coordinate = (xrange.concat(orbit.map { |pt| pt[0] })).sort!
          xrange = [x_coordinate[0], x_coordinate[1]]
        end
      end
      prms.sort!
      prm_range = [prms[0], prms[-1]]
      if xrange.size != 2 || prm_range.size != 2
        p prm_range
        p xrange
        raise "Invalid source file of bifurcation diagram"
      end
      prm_len = prm_range[1] - prm_range[0]
      prm_range = [prm_range[0] - prm_len * 0.2, prm_range[1] + prm_len * 0.2]
      xlen = xrange[1] - xrange[0]
      xrange = [xrange[0] - xlen * 0.2, xrange[1] + xlen * 0.2]
      [prm_range, xrange]
    end
    private :get_image_range

    # @param [String] dir Output directory
    # @param [Symbol] image_format  Format of output image
    # @param [MPFR::ColumnVector] pt_start Initial point of tracing
    # @param [Array] prm_range An array that is [index, minimum parameter, maximum parameter]
    # @param [Hash] opts Option hash
    # @option opts [String] :text File name of text data
    # @option opts [String] :image File name of output image
    def output(dir, image_format, pt_start, prms_start, prm_range, opts = {})
      path_text = FileName.create(dir, opts[:text] || "orbit.txt", :directory => :parent)
      path_image = FileName.create(dir, opts[:image] || "bifurcation.png", :directory => :parent)
      output_text(path_text, pt_start, prms_start, prm_range)
      xrange, yrange = get_image_range(path_text, pt_start, MPFR("1e-1"))

      plot = MPFRPlot.new
      plot.set_screen(:xrange => xrange, :yrange => yrange)
      plot.add_object(MPFR::DataFile.new(path_text, MPFRPlot::STY_DOTS, "1:2"))
      plot.output(path_image, image_format)
      # plot.save_gnuplot_cmds("gnuplot_command.txt", "gnuplot_data.txt", path_image, image_format)
    end
  end

  class ParameterValue
    attr_accessor :prms, :sink, :period, :position

    def initialize(hash)
      @prms = hash[:prms]
      @sink = hash[:sink]
      @period = hash[:period]
      @position = hash[:position]
    end

    def to_a
      @prms
    end

    def [](i)
      @prms[i]
    end

    def shift(prm_vary, position)
      prms_new = @prms.map.with_index do |el, i|
        el + prm_vary[i] * position
      end
      self.class.new(:prms => prms_new, :position => position)
    end

    def dividing_point(other, pos)
      prms_new = @prms.map.with_index do |v, i|
        v * (1 - pos) + other[i] * pos
      end
      self.class.new(:prms => prms_new)
    end

    def midpoint(other)
      dividing_point(other, 0.5)
    end

    def distance_from(other)
      sum = 0
      @prms.each_with_index do |v, i|
        sum += (v - other[i]) ** 2
      end
      MPFR::Math.sqrt(sum)
    end
  end

  class WindowParameter
    def initialize(primary_sink, prm_start, prm_end)
      @primary_sink = primary_sink
      @prm_start = prm_start
      @prm_end = prm_end
    end

    def to_tsv_line
      str = (@prm_start[:true].prms + @prm_start[:false].prms + @prm_end[:true].prms +
             @prm_end[:false].prms + @primary_sink.prms + [@primary_sink.sink[0], @primary_sink.sink[1]]).join("\t") do |prm|
        prm.to_strf("%.Re")
      end
      "#{@primary_sink.period}\t#{str}"
    end

    def self.load_tsv_line(l)
      ary = l.strip.split("\t")
      prms = ary[9..10].map { |s| MPFR(s) }
      sink = MPFR::ColumnVector(ary[11..12])
      prm_primary_sink = ParameterValue.new(:period => ary[0].to_i, :sink => sink, :prms => prms)
      prm_start = {
        :true => ParameterValue.new(:prms => ary[1..2].map { |s| MPFR(s) }),
        :false => ParameterValue.new(:prms => ary[3..4].map { |s| MPFR(s) })
      }
      prm_end = {
        :true => ParameterValue.new(:prms => ary[5..6].map { |s| MPFR(s) }),
        :false => ParameterValue.new(:prms => ary[7..8].map { |s| MPFR(s) })
      }
      self.new(prm_primary_sink, prm_start, prm_end)
    end

    def primary_sink_parameter(pos = 0.5)
      prm1 = @prm_start[:true].to_a
      prm2 = @prm_end[:true].to_a
      prm1.map.with_index do |val, i|
        val * (1 - pos) + prm2[i] * pos
      end
    end

    def primary_sink
      @primary_sink.sink
    end

    def primary_sink_period
      @primary_sink.period
    end

    def primary_sink_parameter
      @primary_sink.prms
    end

    def interval_start
      @prm_start[:true].midpoint(@prm_start[:false])
    end

    def interval_end
      @prm_end[:true].midpoint(@prm_end[:false])
    end

    def interval_size
      interval_start.distance_from(interval_end)
    end

    def bifurcation_to_io(io, format, pspace, sink, transient, num)
      bif = Henon::Bifurcation.new(pspace, transient, num)
      bif.initial_point = sink
      bif.output_to_io(io, format)
    end
    private :bifurcation_to_io

    def output_file(out, vary_ind, transient)
      int_size = interval_size
      step_size = int_size / 1000
      int_margin = interval_size / 10

      prm_middle_sink = primary_sink_parameter
      henon = MPFR::Henon::Mapping.new(prm_middle_sink.to_a)
      sink = henon.map(@primary_sink.sink, transient)

      prm_start = interval_start
      prm_end = interval_end

      pspaces = []
      pspaces << ParameterRegion.new(prm_middle_sink, vary_ind => [prm_start - int_margin, step_size])
      pspaces << ParameterRegion.new(prm_middle_sink, vary_ind => [prm_end + int_margin, step_size])
      open(out, 'w') do |f|
        pspaces.each do |pspace|
          bifurcation_to_io(f, "%.14Re", pspace, sink, transient, 1000)
        end
      end
    end
  end

  class PeriodicWindow
    class ErrorTooLongWindow < StandardError
    end

    MAX_PERIOD = 100
    DEFAULT_ITERATE_NUMBER = 1000
    MAX_ITERATE_NUMBER = 100000
    MAX_SEARCH_PERIOD = 1000
    DEFAULT_INITIAL_POS = MPFR('1e-4')

    attr_accessor :iterate_number

    def initialize(start_prms, start_sink, threshold, prm_vary)
      @start_prms = ParameterValue.new(:prms => start_prms, :position => 0)
      @threshold = threshold
      @prm_vary = normalize_prm_vary(prm_vary)
      @iterate_number = DEFAULT_ITERATE_NUMBER
      @max_iterate_number = MAX_ITERATE_NUMBER
      unless get_period(@start_prms, start_sink)
        raise ArgumentError, "Invalid initial argument."
      end
    end

    def normalize_prm_vary(prm_vary)
      square_sum = prm_vary.inject(0) do |sum, el|
        sum + el * el
      end
      length = MPFR::Math.sqrt(square_sum)
      prm_vary.map do |el|
        el / length
      end
    end
    private :normalize_prm_vary

    def get_prm(pos)
      @start_prms.shift(@prm_vary, pos)
    end
    private :get_prm

    def get_period(prms, initial_point = nil, precise = false)
      henon = MPFR::Henon::Mapping.new(prms.to_a)
      sink = henon.iterate_map(initial_point || prms.sink || @start_prms.sink, precise ? @iterate_number : @max_iterate_number)
      if period = henon.sink_period(sink, @threshold, MAX_PERIOD)
        prms.period = period
        prms.sink = sink
        return period
      end
      nil
    end
    private :get_period

    def get_period_from_pos(pos, initial_point = nil, precise = false)
      prms = get_prm(pos)
      period = get_period(prms, initial_point, precise)
      [prms, period]
    end
    private :get_period_from_pos

    def check_period_at_pos(period, pos, initial_point = nil)
      prms, period_new = get_period_from_pos(pos, initial_point)
      if period == period_new
        return prms
      end
      false
    end
    private :check_period_at_pos

    def window_end_prm(prm_sink, pos_step)
      initial_point = prm_sink.sink
      pos = prm_sink.position + pos_step
      (1..MAX_SEARCH_PERIOD).each do |i|
        prms_new = get_prm(pos)
        unless result = window_exist?(prms_new, initial_point)
          return [prm_sink, prms_new, initial_point]
        end
        initial_point = result
        unless period_new = get_period(prms_new, initial_point)
          period_new = get_period(prms_new, initial_point, true)
        end
        if period_new
          prm_sink = prms_new
          initial_point = prms_new.sink
        end
        pos += pos_step
        pos_step *= 2
        if i == MAX_SEARCH_PERIOD
          raise "Can not determine parameter that window ends."
        end
      end
    end
    private :window_end_prm

    REFINE_ONE_STEP = 10

    def bisect_sink_parameter(period, prm_sink, prm_different_period, error)
      if prm_sink.period == prm_different_period.period
        raise "Invalid parameters for bisection."
      end
      initial_point = prm_sink.sink
      bisect = MathUtils::Bisect.new
      bisect.set_objects(prm_sink, prm_different_period) do |prm_new|
        if period_new = get_period(prm_new, initial_point, true)
          initial_point = prm_new.sink
        end
        period == period_new
      end
      bisect.set_creation do |prm1, prm2|
        get_prm((prm1.position + prm2.position) / 2)
      end
      bisect.set_end_condition do |prm1, prm2|
        (prm1.position - prm2.position).abs < error
      end
      bisect.execute
    end
    private :bisect_sink_parameter

    # It is difficult to obtain sinks near bifurcation parameter.
    # So, this method is not accurate.
    def period_boundary_parameter(period, prm_true, pos_step, error)
      prm_sink, prm_not_window, initial_pt = window_end_prm(prm_true, pos_step)
      if prm_sink.period != prm_true.period
        bisect_sink_parameter(prm_true.period, prm_true, prm_sink, error)
      else
        result = bisect_crisis_parameter(error, prm_sink, prm_not_window, initial_pt, true)
        prm_window_edge, prm_not_window_new = result[:true], result[:false]
        if prm_window_edge.period
          if prm_window_edge.period == prm_true.period
            [prm_window_edge, prm_not_window_new]
          else
            bisect_sink_parameter(prm_window_edge.period, prm_true, prm_window_edge, error)
          end
        elsif prm_latest = result[:sink][-1]
          sgn_start = (prm_not_window_new.position - prm_latest.position).sgn
          pos_step = error * sgn_start / 100
          pos = prm_latest.position + pos_step
          i = 0
          while sgn_start * (pos - prm_not_window_new.position) < 0
            prms_new = get_prm(pos)
            if period = get_period(prms_new, prm_latest.sink, true)
              prm_latest = prms_new
              pos += pos_step
            else
              break
            end
          end
          [prm_latest, prm_not_window_new]
        end
      end
    end
    private :period_boundary_parameter

    def extend_sink(error, prm_sink, initial_pos_step = nil)
      unless period = prm_sink.period
        raise "Invalid sink parameter."
      end
      initial_pos_step ||= error
      prm_start_true, prm_start_false = period_boundary_parameter(period, prm_sink, -initial_pos_step, error)
      prm_end_true, prm_end_false = period_boundary_parameter(period, prm_sink, initial_pos_step, error)

      # unless prm_start_false.period
      #   henon = MPFR::Henon::Mapping.new(prm_start_false.to_a)
      #   open(FileName.create("false.txt", :position => :middle), 'w') do |f|
      #     henon.each_pt_on_orbit(henon.map(prm_start_true.sink, @iterate_number), 1000) do |pt|
      #       f.puts pt.to_s
      #     end
      #   end
      # end

      { :period => period,
        :true => [prm_start_true, prm_end_true],
        :false => [prm_start_false, prm_end_false] }
    end
    private :extend_sink

    def persistence_sink_data(error, initial_pos, initial_pos_step)
      prm_init, period = get_period_from_pos(initial_pos)
      unless period
        raise "Invalid sink."
      end
      extend_sink(error, prm_init, initial_pos_step)
    end
    private :persistence_sink_data

    def get_persistence_sink(error, initial_pos_step = DEFAULT_INITIAL_POS)
      persistence_sink_data(error, 0, initial_pos_step)
    end

    CHECK_SINK_NUMBER = 500

    def check_persistence_sink(period, prm_start, prm_end)
      initial_point = prm_start.sink
      step_size = (prm_end.position - prm_start.position).abs / CHECK_SINK_NUMBER
      prm_start.position.step(prm_end.position, step_size) do |pos|
        # puts "#{pos.to_strf("%.10Re")} #{check_period_at_pos(period, pos, initial_point) ? 'true' : 'false'}"
        if prm_new = check_period_at_pos(period, pos, initial_point)
          initial_point = prm_new.sink
        else
          return false
        end
      end
      true
    end

    def get_lesser_period_sink(sink_data, error)
      result_sink = nil
      sink_data[:false].each_with_index do |prm_false, i|
        if prm_false.period and prm_false.period < sink_data[:period]
          sink_data_new = extend_sink(error, prm_false)
          if sink_data_new2 = get_lesser_period_sink(sink_data_new, error)
            sink_data_new = sink_data_new2
          end
          if !result_sink or sink_data_new[:period] < result_sink[:period]
            result_sink = sink_data_new
          end
        end
      end
      result_sink
    end
    private :get_lesser_period_sink

    WINDOW_ORBIT_SIZE = 10000
    RATIO_PHASE_SPACE = 0.5

    def ratio_in_phase_space(orbit_crds, threshold)
      phase_space_size = 0
      crd_block = [orbit_crds[0]]
      (1...(orbit_crds.size)).each do |i|
        if not orbit_crds[i].number_p
          return false
        elsif orbit_crds[i] - crd_block[-1] > threshold
          phase_space_size += (crd_block[-1] - crd_block[0] + threshold)
          crd_block.clear
        end
        crd_block << orbit_crds[i]
      end
      if crd_block.size > 0
        phase_space_size += (crd_block[-1] - crd_block[0] + threshold)
        crd_block.clear
      end
      phase_space_size / (orbit_crds[-1] - orbit_crds[0] + threshold)
    end
    private :ratio_in_phase_space

    def window_exist?(prms, start_pt, transient = 10000, threshold = MPFR('1e-3'))
      henon = MPFR::Henon::Mapping.new(prms.to_a)
      start_orbit = henon.iterate_map(start_pt, transient)
      orbit = henon.make_orbit(start_orbit, WINDOW_ORBIT_SIZE)
      orbit_x = orbit.map { |pt| pt[0] }.sort!
      orbit_y = orbit.map { |pt| pt[1] }.sort!
      ratio_x = ratio_in_phase_space(orbit_x, threshold)
      ratio_y = ratio_in_phase_space(orbit_y, threshold)
      if ratio_x and ratio_y
        # puts average_ratio = (ratio_x + ratio_y) / 2
        [ratio_x, ratio_y].max < RATIO_PHASE_SPACE ? orbit[-1] : false
      else
        false
      end
    end
    private :window_exist?

    def bisect_crisis_parameter(error, prm_in_window, prm_after_crisis, initial_point, get_period = false)
      period_determined = []
      bisect = MathUtils::Bisect.new
      bisect.set_objects(prm_in_window, prm_after_crisis) do |prm_new|
        if result = window_exist?(prm_new, initial_point)
          initial_point = result
          if get_period && get_period(prm_new, initial_point, true)
            period_determined << prm_new
            initial_point = prm_new.sink
          end
          true
        else
          false
        end
      end
      bisect.set_creation do |prm1, prm2|
        get_prm((prm1.position + prm2.position) / 2)
      end
      bisect.set_end_condition do |prm1, prm2|
        (prm1.position - prm2.position).abs < error
      end
      bisect_result = bisect.execute
      { :true => bisect_result[0], :false => bisect_result[1], :sink => period_determined }
    end
    private :bisect_crisis_parameter

    def get_window(error, initial_pos_step = DEFAULT_INITIAL_POS)
      prm_sink, prm_not_window, initial_pt = window_end_prm(@start_prms, -initial_pos_step)
      prm_start = bisect_crisis_parameter(error, prm_sink, prm_not_window, initial_pt)
      prm_sink, prm_not_window, initial_pt = window_end_prm(@start_prms, initial_pos_step)
      prm_end = bisect_crisis_parameter(error, prm_sink, prm_not_window, initial_pt)

      window_pos = [prm_start[:true].position, prm_end[:true].position]
      prm_periods = window_pos.map do |pos_end|
        initial_pt = @start_prms.sink
        size_from_edge = pos_end - @start_prms.position
        win_step = [size_from_edge.sgn * (window_pos[0] - window_pos[1]).abs / 30, size_from_edge / 10].min
        @start_prms.position.step(pos_end, win_step).map do |pos|
          result = get_period_from_pos(pos, initial_pt, true)
          if result[1]
            initial_pt = result[0].sink
          end
          result
        end
      end

      min_period = @start_prms.period
      prm_periods[0].reverse!
      prm_period_data = prm_periods[0] + prm_periods[1]
      prm_period_data.each do |prm, period|
        if period and period < min_period
          min_period = period
        end
      end

      data_include_primary = prm_period_data.select do |prm, period|
        period == min_period
      end
      primary_sink = data_include_primary[data_include_primary.size / 2][0]

      WindowParameter.new(primary_sink, prm_start, prm_end)
    end
  end

end
