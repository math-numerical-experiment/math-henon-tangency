require 'grid'

require 'tokyocabinet'
include TokyoCabinet

module TCGrid
  

  def open_database(db, path)
    case db
    when TDB
      unless db.open(path, TDB::OWRITER | TDB::OCREAT)
        raise "TDB Open Error: #{db.errmsg(db.ecode)}"
      end
    when HDB
      unless db.open(path, HDB::OWRITER | HDB::OCREAT)
        raise "HDB Open Error: #{db.errmsg(db.ecode)}"
      end
    else
      raise "Invalid database class."
    end
  end
  module_function :open_database

  def close_database(db)
    unless db.close
      raise "DB Close Error: #{db.errmsg(db.ecode)}"
    end
  end
  module_function :close_database

  def pack_coordinate_key(x, y)
    # x >= 0 && y >= 0    sgn = 0
    # x >= 0 && y < 0     sgn = 1
    # x < 0  && y >= 0    sgn = 2
    # x < 0  && y < 0     sgn = 3
    if x < 0
      x = -x
      sgn = 2
    else
      sgn = 0
    end
    if y < 0
      y = -y
      sgn += 1
    end
    [x, y, sgn].pack("w3")
  end
  module_function :pack_coordinate_key

  def change_sgn(x, y, sgn)
    case sgn
    when 1
      y = -y
    when 2
      x = -x
    when 3
      x = -x
      y = -y
    end
    [x, y]
  end
  module_function :change_sgn
  
  def unpack_key_string(str)
    change_sgn(*str.unpack("w3"))
  end
  module_function :unpack_key_string

  def pack_index(indexes)
    str = ''
    indexes.each { |x, y| str += pack_coordinate_key(x, y) }
    str
  end
  module_function :pack_index

  def unpack_index_string(str)
    res_index = Grid::IndexSet.new
    ary = str.unpack("w*")
    (0...(ary.size)).step(3) do |i|
      x, y = change_sgn(ary[i], ary[i + 1], ary[i + 2])
      res_index.union!(:x => x, :y => y)
    end
    res_index
  end
  module_function :unpack_index_string

  def pack_mpfr(fr)
    a, b = fr.get_str
    pack_coordinate_key(a.to_i, b)
  end
  module_function :pack_mpfr

  def unpack_mpfr_str(str)
    a, b, c = str.unpack('w3')
    case c.to_i
    when 0
      src = '0.' + a.to_s + 'e' + b.to_s
    when 1
      src = '0.' + a.to_s + 'e-' + b.to_s
    when 2
      src = '-0.' + a.to_s + 'e' + b.to_s
    when 3
      src = '-0.' + a.to_s + 'e-' + b.to_s
    end
    MPFR(src)
  end
  module_function :unpack_mpfr_str

end
