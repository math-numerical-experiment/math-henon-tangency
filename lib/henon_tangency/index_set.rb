module Grid
  class IndexSet
    def initialize(*args)
      # The format of @index is [[x1, [y1_00, y1_01, y1_10, y1_11, ... ]],
      #                          [x2, [y2_00, y2_01, y2_10, y2_11, ... ]], ... ]
      # xn < x(n+1)
      # yn_00 <= yn_01, yn_01 < y(n+1)_00
      @index = []
      args.each { |h| union_with_rectangle!(h) }
    end

    def dump_to_array
      @index.inject([]) do |res, ary|
        (0...(ary[1].size)).step(2) { |i| res << { :x => ary[0], :y => ary[1][i..(i+1)] } }
        res
      end
    end

    def load_from_array(ary)
      @index.clear
      ary.each { |h| union_with_rectangle!(h) }
      valid_data?
    end

    def dump_to_file(path, compress = false)
      if compress && /\.gz$/ !~ path
        path = path + '.gz'
      end
      raise "#{path} has already existed." if File.exist?(path)
      s = Marshal.dump(self)
      if compress
        Zlib::GzipWriter.open(path) { |gz| gz.write s }
      else
        open(path, 'w') { |f| f.print s }
      end
      path
    end

    def self.load_from_file(path)
      if /\.gz$/ =~ path
        s = Zlib::GzipReader.open(path) { |gz| gz.read }
      else
        s = File.read(path)
      end
      Marshal.load(s)
    end

    # For debugging
    def valid_data?
      if @index.size > 0
        last_x = @index[0][0]
        (1...(@index.size)).each do |i|
          if last_x >= @index[i][0]
            raise "@index instance of Grid::IndexSet does not satisfy condition 'xn < x(n+1)'."
          end
          last_x = @index[i][0]
        end
        @index.each do |ary|
          if ary[1].size % 2 == 1
            raise "@index instance of Grid::IndexSet does not satisfy condition 'ary_y.size % 2 == 0'."
          end
          (1...(ary[1].size)).each do |i|
            if i % 2 == 0
              if ary[1][i - 1] >= ary[1][i]
                raise "@index instance of Grid::IndexSet does not satisfy condition 'yn_00 < yn_01'."
              end
            else
              if ary[1][i - 1] > ary[1][i]
                raise "@index instance of Grid::IndexSet does not satisfy condition 'yn_00 <= yn_01'."
              end
            end
          end
        end
        ids = (@index.map { |a| a.object_id }).uniq
        if ids.size != @index.size
          raise "An array in @index have more than one indexes."
        end
        ids2 = (@index.map { |a| a[1].object_id }).uniq
        if ids2.size != @index.size
          raise "An array which is a second element of an array in @index have more than one indexes."
        end
      end
      true
    end

    def set_index(index)
      @index = Marshal.load(Marshal.dump(index))
    end
    protected :set_index

    def get_index
      @index
    end
    protected :get_index

    def each_x_slice(&block)
      @index.each { |ary| yield(*ary) }
    end
    
    def edge_coordinate_for_display_obj
      x_index = Marshal.load(Marshal.dump(@index))
      i = 0
      while i < x_index.size
        x = x_index[i][0] - 1
        inc = (x_index.assoc(x) ? 1 : 2)
        tmp = x_index[i][1].dup
        (0...(tmp.size)).step(2) { |j| union_at_x_coordinate(x_index, x, tmp[j..(j+1)]) }
        i += inc
      end
      x_index.each do |ary|
        ary[0] += 1
        (1...(ary[1].size)).step(2) { |j| ary[1][j] += 1 }
        k = 1
        while k < ary[1].size - 1
          delete_continuous_number(ary[1], k, 0)
          k += 2
        end
      end
      y_index = []
      @index.each do |ary|
        new_ary_x = [ary[0], ary[0] + 1]
        (0...(ary[1].size)).step(2) do |i|
          (ary[1][i]..(ary[1][i+1] + 1)).each_with_index do |y, j|
            if old_ary = y_index.assoc(y)
              if old_ary[1][1] == new_ary_x[0]
                old_ary[1][1] += 1
              else
                old_ary[1] += new_ary_x
              end
            else
              insert_new_index_data(y_index, y, new_ary_x.dup)
            end
          end
        end
      end
      [x_index, y_index]
    end

    def quads_coordinate_for_display_obj
      data = []
      @index.each do |ary|
        ary_x = [ary[0], ary[0] + 1]
        (0...(ary[1].size)).step(2) do |i|
          data << [ary_x[0], ary[1][i]]
          data << [ary_x[1], ary[1][i]]
          data << [ary_x[1], ary[1][i + 1] + 1]
          data << [ary_x[0], ary[1][i + 1] + 1]
        end
      end
      data
    end
    
    def get_array(arg)
      case arg
      when Integer
        return [arg, arg]
      when Array
        if arg.size == 2 && Integer === arg[0] && Integer === arg[1]
          if arg[0] < arg[1]
            return arg
          else
            return [arg[1], arg[0]]
          end
        end
      end
      raise ArgumentError, "Invalid keys of hash."
    end
    private :get_array

    def position_in_index(ary_y, cur_y_index)
      min_num = cur_y_index.size
      max_num = cur_y_index.size
      determined_x = nil
      determined_y = nil
      cur_y_index.each_with_index do |sep, i|
        if !determined_x && ary_y[0] <= sep
          min_num = i
          determined_x = true
        end
        if determined_x && !determined_y && ary_y[1] <= sep
          max_num = i
          determined_y = true
        end
        break if determined_x && determined_y
      end
      [min_num, max_num]
    end
    private :position_in_index
    
    def delete_continuous_number(cur_y_index, pos, diff = 1)
      if pos % 2 == 1 && pos > 0
        continue = true
        while continue && (pos + 1) < cur_y_index.size
          if cur_y_index[pos + 1] - cur_y_index[pos] <= diff
            cur_y_index.delete_at(pos)
            cur_y_index.delete_at(pos)
          else
            continue = nil
          end
        end
      else
        raise ArgumentError, "Invalid index of array. Index must be odd."
      end
    end
    private :delete_continuous_number

    def insert_new_index_data(index_ary, new_x, new_ary_y)
      pos = index_ary.size
      index_ary.each_with_index do |ary, i|
        if new_x < ary[0]
          pos = i
          break
        end
      end
      index_ary.insert(pos, [new_x, new_ary_y])
    end
    private :insert_new_index_data
    
    # 'ary_y' is array having two elements.
    def union_at_x_coordinate(index_ary, x, ary_y)
      if cur_y = index_ary.assoc(x) # cur_y = [x, [min0, max0, min1, max1, ... ]]
        min_num, max_num = position_in_index(ary_y, cur_y[1])
        if min_num % 2 == 0
          if min_num == max_num
            cur_y[1].insert(min_num, ary_y[0], ary_y[1])
          elsif max_num % 2 == 0
            (0...(max_num - min_num)).each { |i| cur_y[1].delete_at(min_num) }
            cur_y[1].insert(min_num, ary_y[0], ary_y[1])
          else
            (0...(max_num - min_num)).each { |i| cur_y[1].delete_at(min_num) }
            cur_y[1].insert(min_num, ary_y[0])
          end
          delete_continuous_number(cur_y[1], min_num - 1) if min_num - 1 > 0
          delete_continuous_number(cur_y[1], min_num + 1)
        elsif min_num != max_num
          (0...(max_num - min_num)).each { |i| cur_y[1].delete_at(min_num) }
          if max_num % 2 == 0
            cur_y[1].insert(min_num, ary_y[1])
          end
          delete_continuous_number(cur_y[1], min_num - 2) if min_num - 2 > 0
          delete_continuous_number(cur_y[1], min_num)
        end
      else
        insert_new_index_data(index_ary, x, ary_y.dup)
      end
    end
    private :union_at_x_coordinate
    
    # 'hash' has the keys :x and :y.
    # hash[:x] and hash[:y] is array [min, max] or Integer.
    def union_with_rectangle!(hash)
      ary_x = get_array(hash[:x])
      ary_y = get_array(hash[:y])
      (ary_x[0]..ary_x[1]).each { |x| union_at_x_coordinate(@index, x, ary_y) }
    end
    protected :union_with_rectangle!

    def union_with_other_set!(other)
      other.get_index.each do |data|
        x = data[0]
        (0...(data[1].size)).step(2).each { |i| union_at_x_coordinate(@index, x, data[1][i..(i+1)]) }
      end
    end
    protected :union_with_other_set!

    def union!(arg)
      case arg
      when Grid::IndexSet
        union_with_other_set!(arg)
      when Hash
        union_with_rectangle!(arg)
      else
        raise ArgumentError, "Argument must be Hash or Grid::IndexSet."
      end
    end

    def union(arg)
      new_obj = self.class.new
      new_obj.set_index(@index)
      new_obj.union!(arg)
      new_obj
    end

    def intersection_at_number_of_index(index_ary, num, ary_y)
      min_num, max_num = position_in_index(ary_y, index_ary[num][1])
      if min_num == max_num
        if min_num % 2 == 0
          index_ary[num][1][min_num] != ary_y[1] ? nil : [ary_y[1], ary_y[1]]
        else
          ary_y
        end
      else
        ret = index_ary[num][1][min_num..max_num]
        if max_num % 2 == 1
          ret[-1] = ary_y[1]
        elsif ary_y[1] == ret[-1]
          ret << ret[-1] 
        elsif max_num != index_ary[num][1].size
          ret.delete_at(-1)
        end
        ret.insert(0, ary_y[0]) if min_num % 2 == 1
        ret
      end
    end
    private :intersection_at_number_of_index
    
    def intersection_with_rectangle!(hash)
      ary_x = get_array(hash[:x])
      ary_y = get_array(hash[:y])
      xrange = ary_x[0]..ary_x[1]
      num = 0
      while num < @index.size
        if xrange.include?(@index[num][0]) && (ary = intersection_at_number_of_index(@index, num, ary_y))
          @index[num][1] = ary
          num += 1
        else
          @index.delete_at(num)
        end
      end
    end
    protected :intersection_with_rectangle!

    def intersection_with_other_set!(other)
      executed = []
      oind = other.get_index
      num = 0
      while num < @index.size
        if ary = oind.assoc(@index[num][0])
          other_ary_y = ary[1]
          intersact_ary = []
          (0...(other_ary_y.size)).step(2) do |i|
            if intermediate = intersection_at_number_of_index(@index, num, other_ary_y[i..(i+1)])
              intersact_ary += intermediate
            end
          end
          if intersact_ary.size > 0
            @index[num][1] = intersact_ary
            num += 1
          else
            @index.delete_at(num)
          end
        else
          @index.delete_at(num)
        end
      end
    end
    protected :intersection_with_other_set!
    
    def intersection!(arg)
      case arg
      when Grid::IndexSet
        intersection_with_other_set!(arg)
      when Hash
        intersection_with_rectangle!(arg)
      else
        raise ArgumentError, "Argument must be Hash or Grid::IndexSet."
      end
    end

    def intersection(arg)
      new_obj = self.class.new
      new_obj.set_index(@index)
      new_obj.intersection!(arg)
      new_obj
    end

    def minus_at_x_coordinate(index_ary, x, ary_y)
      if cur_y = index_ary.assoc(x)
        min_num, max_num = position_in_index(ary_y, cur_y[1])
        del_start = min_num
        del_num = max_num - min_num
        if max_num % 2 == 1
          if ary_y[1] == cur_y[1][max_num]
            del_num += 1
          else
            cur_y[1].insert(max_num, ary_y[1] + 1)
          end
        elsif ary_y[1] == cur_y[1][max_num]
          if cur_y[1][max_num] == cur_y[1][max_num + 1]
            del_num += 2
          else
            cur_y[1].insert(max_num + 1, ary_y[1] + 1)
            del_num += 1
          end
        end
        if min_num % 2 == 1
          cur_y[1].insert(min_num, ary_y[0] - 1)
          del_start += 1
        end
        del_num.times { |i| cur_y[1].delete_at(del_start) }
        index_ary.delete(cur_y) if cur_y[1].size == 0
      end
    end
    private :minus_at_x_coordinate
    
    def minus_rectangle!(hash)
      ary_x = get_array(hash[:x])
      ary_y = get_array(hash[:y])
      (ary_x[0]..ary_x[1]).each { |x| minus_at_x_coordinate(@index, x, ary_y) }
    end
    private :minus_rectangle!

    def minus_set!(other)
      other.get_index.each do |data|
        x = data[0]
        (0...(data[1].size)).step(2).each { |i| minus_at_x_coordinate(@index, x, data[1][i..(i+1)]) }
      end
    end
    private :minus_set!

    def minus!(arg)
      case arg
      when Grid::IndexSet
        minus_set!(arg)
      when Hash
        minus_rectangle!(arg)
      else
        raise ArgumentError, "Argument must be Hash or Grid::IndexSet."
      end
    end

    def minus(arg)
      new_obj = self.class.new
      new_obj.set_index(@index)
      new_obj.minus!(arg)
      new_obj
    end

    def wrap
      tmp_index = []
      @index.each do |src|
        ary_y = []
        (0...(src[1].size)).step(2).each { |i| ary_y += [src[1][i] - 1, src[1][i + 1] + 1] }
        i = 1
        while i < ary_y.size - 1
          delete_continuous_number(ary_y, i)
          i += 2
        end
        tmp_index << [src[0], ary_y]
      end
      new_index = Marshal.load(Marshal.dump(tmp_index))
      tmp_index.each do |ary|
        ary_y = ary[1]
        (0...(ary_y.size)).step(2).each { |i| union_at_x_coordinate(new_index, ary[0] - 1, ary_y[i..(i+1)]) }
        (0...(ary_y.size)).step(2).each { |i| union_at_x_coordinate(new_index, ary[0] + 1, ary_y[i..(i+1)]) }
      end
      new_obj = self.class.new
      new_obj.set_index(new_index)
      new_obj
    end
    
    def collar
      new_index = wrap
      new_index.minus!(self)
      new_index
    end

    def empty?
      @index.empty?
    end

    # 'ary' must be array of two integer [x, y].
    def include_one_box?(index_ary)
      if ary = @index.assoc(index_ary[0])
        num = ary[1].size
        ary[1].each_with_index do |y, i|
          if index_ary[1] <= y
            num = i
            break
          end
        end
        num % 2 == 1 or ary[1][num] == index_ary[1] ? true : false
      else
        false
      end
    end
    private :include_one_box?

    def include_y_intervals?(y_other_ary, y_self_ary)
      ret = true
      (0...(y_other_ary.size)).step(2) do |i|
        min_num, max_num = position_in_index(y_other_ary[i..(i+1)], y_self_ary)
        if not ((min_num % 2 == 1 and min_num == max_num) or
                (min_num % 2 == 0 and y_self_ary.size > max_num and y_other_ary[i] >= y_self_ary[min_num] and max_num - min_num <= 1))
          ret = false
          break
        end
      end
      ret
    end
    
    def include_rectangle?(hash)
      ary_x = get_array(hash[:x])
      ary_y = get_array(hash[:y])
      (ary_x[0]..ary_x[1]).all? do |x|
        if cur_ary = @index.assoc(x)
          include_y_intervals?(ary_y, cur_ary[1])          
        else
          false
        end
      end
    end
    private :include_rectangle?
    
    def include_index_set?(other)
      other.get_index.all? do |ary|
        if cur_ary = @index.assoc(ary[0])
          include_y_intervals?(ary[1], cur_ary[1])
        else
          false
        end
      end
    end
    private :include_index_set?
    
    def include?(*args)
      case args.size
      when 1
        case args[0]
        when Grid::IndexSet
          return include_index_set?(args[0])
        when Array
          return include_one_box?(args[0])
        when Hash
          return include_rectangle?(args[0])
        end
      when 2
        return include_one_box?(args)
      end
      raise ArgumentError, "Need one or two arguments."
    end

    def size
      @index.inject(0) do |sum, ary|
        (0...(ary[1].size)).step(2) do |i|
          sum += (ary[1][i+1] - ary[1][i] + 1)
        end
        sum
      end
    end

    def ==(other)
      other_index = other.get_index
      if @index.size == other_index.size
        @index.all? do |ary_self|
          if ary_other = other_index.assoc(ary_self[0])
            ary_other[1] == ary_self[1]
          else
            false
          end
        end
      else
        false
      end
    end
    
    def to_a
      ary = []
      @index.each do |a|
        (0...(a[1].size)).step(2) do |i|
          (a[1][i]..a[1][i+1]).each { |y| ary << [a[0], y] }
        end
      end
      ary
    end

    def each(&block)
      @index.each do |a|
        (0...(a[1].size)).step(2) do |i|
          (a[1][i]..a[1][i+1]).each { |y| block.call(a[0], y) }
        end
      end
    end

    # def compress_x!
    #   if @index[0][0] >= 0
    #     ind_max_neg = nil
    #     ind_min_non_neg = 0
    #   else
    #     ind_max_neg = 0
    #     ind_min_non_neg = nil
    #     @index.each_with_index do |ary, i|
    #       if ary[0] => 0
    #         ind_max_neg = i - 1
    #         ind_min_non_neg = i
    #         break
    #       end
    #     end  
    #   end

    #   if ind_max_neg
    #     @index[ind_max_neg][0] = -2 if @index[ind_max_neg][0] < -2
    #     last_val = @index[ind_max_neg]
    #     i = ind_max_neg - 1
    #     while i >= 0
    #       if last_val == (@index[i][0] + 1)

    #       else
    #         @index[i][0] = @index[i + 1][0] - 2  
    #       end
          
    #       i -= 1
    #     end
    #   end
    #   # if ind_min_non_neg
    #   #   @index[ind_min_non_neg][0] = 0 if @index[ind_min_non_neg][0] > 0
    #   #   i = ind_min_non_neg + 1
    #   #   while i < @index.size
    #   #     @index[i][0] = @index[i]
    #   #   end
    #   # end
      
    # end
    # private :compress_x!
    
    # def compress_for_chomp!
    #   compress_x!
    # end
    
    def output_for_chomp(io)
      each { |x, y| io.puts "#{x} #{y}" }
    end
    
  end
  
end
