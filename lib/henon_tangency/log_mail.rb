require 'net/smtp'

class LogByMail
  def initialize(to, from, smtp_serv)
    @to = to
    @from = from
    @smtp_serv = smtp_serv
    @port = 25
  end

  def send(title, message)
    Net::SMTP.start(@smtp_serv, @port) do |smtp|
      smtp.send_mail <<EndOfMail, @from, @to
From: <#{@from}>
To: <#{@to}>
Subject: #{title}
Date: #{Time.now}
Message-Id: <#{Time.now.to_i.to_s}#{@from}>

#{message}
EndOfMail
    end
  end
end

