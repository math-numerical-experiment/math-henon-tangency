# The classes inheriting MappingTemplate must have the following methods:
#  map(pt, iterate = 1)
#  iterate_map(pt, iterate = nil)
# 
# Instead of above methods, we can define the following methods:
#  map_one_time(pt)
#  inverse_map_one_time(pt)
class MappingTemplate
  attr_reader :period, :cst

  def initialize(cst, period = 1)
    @cst = cst
    @period = period
  end

  def map(pt, iterate = 1)
    if iterate > 0
      iterate.times do |i|
        pt = map_one_time(pt)
      end
    elsif iterate < 0
      (-iterate).times do |i|
        pt = inverse_map_one_time(pt)
      end
    end
    pt
  end

  def iterate_map(pt, iterate = nil)
    map(pt, (iterate || 1) * period)
  end

  def make_orbit(pt, num)
    ret = [pt]
    (num - 1).times { ret << iterate_map(ret[-1]) }
    ret
  end

  def each_pt_on_orbit(pt, num, &block)
    if block_given?
      last = pt
      num.times do
        yield(last)
        last = iterate_map(last)
      end
    else
      to_enum(:each_pt_on_orbit, pt, num)
    end
  end

end
