require "ruby_mpfr_henon"
require "ruby_mpfi_henon"
require_relative "tc_grid"

module Henon
  class ConvergeToBasin
    attr_reader :mapping

    def initialize(mapping, st_ppt, map_num, dist_threshold)
      @logger = ManageLog.get_new_logger(self.class)
      @mapping = mapping
      @map_num = map_num
      @st_ppt = @mapping.iterate_map(st_ppt, @map_num)
      @dist_threshold = dist_threshold
      @logger.debug('sink') { @st_ppt.to_s }
      unless basin_include?(@st_ppt)
        raise ArgumentError, "Invalid stable periodic point."
      end
    end

    def basin_include?(pt)
      mapped = @mapping.iterate_map(pt, @map_num)
      dist = mapped.distance_from(@st_ppt)
      @logger.debug('distance') { dist.to_strf("%.30Re") }
      dist < @dist_threshold
    end

    def iteration_number_to_basin(pt)
      @map_num.times do |i|
        pt = @mapping.iterate_map(pt)
        if pt.distance_from(@st_ppt) < @dist_threshold
          return i * @mapping.period
        end
      end
      nil
    end

    def output(out, size_x, size_y, pitch, format = "%.Re")
      top_right = MPFR::ColumnVector.new([@st_ppt[0] + size_x / 2, @st_ppt[1] + size_y / 2])
      bottom_left = MPFR::ColumnVector.new([@st_ppt[0] - size_x / 2, @st_ppt[1] - size_y / 2])
      i = 0
      OutputIO.open(out) do |f|
        MPFR::Vector2D.rectangle_each_pt(bottom_left, top_right, pitch, pitch) do |pt|
          if basin_include?(pt)
            f.puts "#{pt[0].to_strf(format)} #{pt[1].to_strf(format)}"
          end
          i += 1
          @logger.info("Finish calculating #{i} points.") if i % 1000 == 0
        end
      end
    end
    
    def output2(out, center, size_x, size_y, pitch, format = "%.Re")
      top_right = MPFR::ColumnVector.new([center[0] + size_x / 2, center[1] + size_y / 2])
      bottom_left = MPFR::ColumnVector.new([center[0] - size_x / 2, center[1] - size_y / 2])
      i = 0
      num = [0, 0]
      OutputIO.open(out) do |f|
        MPFR::Vector2D.rectangle_each_pt(bottom_left, top_right, pitch, pitch) do |pt|
          if basin_include?(pt)
            f.puts "#{pt[0].to_strf(format)} #{pt[1].to_strf(format)}"
            num[0] += 1
          else
            num[1] += 1
          end
          i += 1
          @logger.info("Finish calculating #{i} points.") if i % 1000 == 0
        end
      end
      num
    end

    RATE_OF_BASIN = 0.3

    def output_basin_in_box(out_io, left_bottom_corner, right_top_corner, pitch, format)
      num = [0, 0]
      MPFR::Vector2D.rectangle_each_pt(left_bottom_corner, right_top_corner, pitch, pitch) do |pt|
        if basin_include?(pt)
          out_io.puts "#{pt[0].to_strf(format)} #{pt[1].to_strf(format)}"
          num[1] += 1
        end
        num[0] += 1
      end
      num
    end
    
    def output_basin_y_line(out_io, grid, x, y_last_line, yrange, pitch, format)
      res_y = []
      y = y_last_line[0] - 1
      while y >= yrange[0]
        num = output_basin_in_box(out_io, grid.left_bottom_corner(x, y), grid.right_top_corner(x, y), pitch, format)
        if num[1].to_f / num[0].to_f < RATE_OF_BASIN
          break
        else
          res_y << y
        end
        y -= 1
      end

      y = y_last_line[0]
      while y <= y_last_line[1]
        num = output_basin_in_box(out_io, grid.left_bottom_corner(x, y), grid.right_top_corner(x, y), pitch, format)
        unless num[1].to_f / num[0].to_f < RATE_OF_BASIN
          res_y << y
        end
        y += 1
      end

      y = y_last_line[1] + 1
      while y <= yrange[1]
        num = output_basin_in_box(out_io, grid.left_bottom_corner(x, y), grid.right_top_corner(x, y), pitch, format)
        if num[1].to_f / num[0].to_f < RATE_OF_BASIN
          break
        else
          res_y << y
        end
        y += 1
      end
      result = [res_y.min, res_y.max]
      @logger.debug("x=#{x}: #{result.inspect.strip}")
      result
    end

    def output_primary_basin(out, size_x, size_y, box_pitch, format = "%.Re")
      top_right = MPFR::ColumnVector.new([@st_ppt[0] + size_x / 2, @st_ppt[1] + size_y / 2])
      bottom_left = MPFR::ColumnVector.new([@st_ppt[0] - size_x / 2, @st_ppt[1] - size_y / 2])

      grid = Grid::Coordinate.new(@st_ppt, box_pitch)
      ary1 = grid.index_include_pt(top_right).to_a[0]
      ary2 = grid.index_include_pt(bottom_left).to_a[0]
      x_index = [ary2[0], ary1[0]]
      y_index = [ary2[1], ary1[1]]
      pitch = box_pitch / 4

      OutputIO.open(out) do |f|
        @logger.info("Range for calculation: #{x_index.inspect.strip} #{y_index.inspect.strip}")
        first_line = output_basin_y_line(f, grid, 0, [0, 0], y_index, pitch, format)

        last_line = first_line
        x = 1
        while x <= x_index[1]
          last_line = output_basin_y_line(f, grid, x, last_line, y_index, pitch, format)
          @logger.info("Finish calculating #{x} line.") { last_line.inspect.strip } if x % 100 == 0
          if last_line[0] && last_line[1]
            x += 1
          else
            break
          end
        end

        last_line = first_line
        x = -1
        while x >= x_index[0]
          last_line = output_basin_y_line(f, grid, x, last_line, y_index, pitch, format)
          @logger.info("Finish calculating #{x} line.") { last_line.inspect.strip } if x % 100 == 0
          if last_line[0] && last_line[1]
            x -= 1
          else
            break
          end
        end
      end
    end
    
  end

  class ConvergeToBasin2 < ConvergeToBasin
    def initialize(mapping, st_ppt, map_num, dist_threshold)
      @logger = ManageLog.get_new_logger(self.class)
      @mapping = mapping
      @map_num = map_num
      @st_ppt = @mapping.iterate_map(st_ppt, @map_num)
      @dist_threshold = dist_threshold
      @logger.debug('sink') { @st_ppt.to_s }
      @st_ppt_orbit = [@st_ppt]
      (@mapping.period - 1).times do
        new_pt = @mapping.map(@st_ppt_orbit[-1], 1)
        @st_ppt_orbit << new_pt
      end
      unless basin_include?(@st_ppt)
        raise ArgumentError, "Invalid stable periodic point."
      end
    end

    def basin_include?(pt)
      mapped = @mapping.iterate_map(pt, @map_num)
      @st_ppt_orbit.any? do |orbit_pt|
        dist = mapped.distance_from(orbit_pt)
        @logger.debug('distance') { dist.to_strf("%.30Re") }
        dist < @dist_threshold
      end
    end
  end

  IN_BASIN = 0
  NOT_IN_BASIN = 1

  class BasinData
    attr_accessor :box_size

    include TCGrid
    
    # conv is ConvergeToBasin.
    def initialize(conv, grid, db_path = 'basin.tct')
      @logger = ManageLog.get_new_logger(self.class)
      @conv = conv
      @grid = grid
      dir = File.dirname(File.expand_path(db_path))
      FileUtils.mkdir_p(dir) unless File.exist?(dir)
      @tdb = TDB.new
      @tdb_path = db_path
      @box_size = 10
    end

    def open_db(&block)
      open_database(@tdb, @tdb_path)
      if block_given?
        res = nil
        begin
          res = yield
        ensure
          close_db
        end
        return res
      end
    end

    def close_db
      close_database(@tdb)
    end

    def each_number_pt(xnum, ynum, size, &block)
      x_crds = []
      y_crds = []
      (0...size).each do |i|
        num = [i + xnum, i + ynum]
        pt = @grid.center(*num)
        x_crds << [num[0], pt[0]]
        y_crds << [num[1], pt[1]]
      end
      count = 0
      x_crds.each do |x|
        y_crds.each do |y|
          if yield([x[0], y[0]], MPFR::ColumnVector([x[1], y[1]]))
            count += 1
          end
        end
      end
      count.to_f / (size * size).to_f
    end

    def get_basin_pt(xnum, ynum, pt = nil, overwrite = nil)
      pt ||= @grid.center(xnum, ynum)
      key = pack_coordinate_key(xnum, ynum)
      if !@tdb[key] || overwrite
        if @conv.basin_include?(pt)
          @tdb[key] = { 'basin' => IN_BASIN, 'x' => pack_mpfr(pt[0]), 'y' => pack_mpfr(pt[1]) }
          true
        else
          @tdb[key] = { 'basin' => NOT_IN_BASIN }
          nil
        end  
      else
        if @tdb[key]['basin'].to_i == IN_BASIN
          true
        else
          nil
        end
      end
    end

    def get_surrounding(xnum, ynum)
      [[xnum, ynum], [xnum - 1, ynum - 1], [xnum, ynum - 1],
       [xnum + 1, ynum - 1], [xnum - 1, ynum], [xnum + 1, ynum],
       [xnum - 1, ynum + 1], [xnum, ynum + 1], [xnum + 1, ynum + 1]].map do |ary|
        [ary, get_basin_pt(*ary)]
      end
    end

    def search_basin_pts(xnum, ynum, size, overwrite)
      each_number_pt(xnum, ynum, size) do |num, pt|
        get_basin_pt(num[0], num[1], pt, overwrite)
      end
    end

    RATE_OF_BASIN = 0.05

    def output_basin_y_line(x, y_last_line, yrange, box_size, overwrite)
      res_y = []
      y_last_line = [[y_last_line[0], yrange[0]].max, [y_last_line[1], yrange[1]].min]
      if y_last_line[0] > y_last_line[1]
        return nil
      end
      y = y_last_line[0] - 1
      while y >= yrange[0]
        if search_basin_pts(x, y, box_size, overwrite) < RATE_OF_BASIN
          break
        else
          res_y << y
        end
        y -= box_size
      end

      y = y_last_line[0]
      while y <= y_last_line[1]
        if search_basin_pts(x, y, box_size, overwrite) > RATE_OF_BASIN
          res_y << y
        end
        y += box_size
      end

      y = y_last_line[1] + 1
      while y <= yrange[1]
        if search_basin_pts(x, y, box_size, overwrite) < RATE_OF_BASIN
          break
        else
          res_y << y
        end
        y += box_size
      end
      result = (res_y.size == 0 ? nil : [res_y.min, res_y.max])
      @logger.debug("x=#{x}: #{result.inspect.strip}")
      result
    end

    def output_simple(xrange, yrange, overwrite = nil)
      @logger.info("Range for calculation: x #{xrange.inspect.strip}, y #{yrange.inspect.strip}")
      open_db do
        i = 0
        (xrange[0]..xrange[1]).each do |xnum|
          (yrange[0]..yrange[1]).each do |ynum|
            i += 1
            get_basin_pt(xnum, ynum, nil, overwrite)
            @logger.info("Finish calculating #{i} points.") if i % 100 == 0
          end
        end
      end
    end

    def output_basin(start, xrange, yrange, overwrite = nil)
      @logger.info("Range for calculation: x #{xrange.inspect.strip}, y #{yrange.inspect.strip}")
      open_db do
        if first_y_line = output_basin_y_line(start[0], [start[1], start[1]], yrange, @box_size, overwrite)
          last_y_line = first_y_line
          x = @box_size
          while x < xrange[1]
            if last_y_line = output_basin_y_line(x, last_y_line, yrange, @box_size, overwrite)
              @logger.info("Finish calculating #{x} line.") { last_y_line.inspect.strip } #if x % 10 == 0
              x += @box_size
            else
              break
            end
          end
          last_y_line = first_y_line
          x = -@box_size
          while x > xrange[0]
            if last_y_line = output_basin_y_line(x, last_y_line, yrange, @box_size, overwrite)
              @logger.info("Finish calculating #{x} line.") { last_y_line.inspect.strip } #if x % 10 == 0
              x -= @box_size
            else
              break
            end
          end
        end
      end
    end

  end

  class BoundaryCoordinate
    attr_reader :count, :coordinate, :direction
    
    # direction - :up, :down, :left, :right
    def initialize(xnum, ynum, direction)
      @start_coordinate = [xnum, ynum]
      @start_direction = direction
      @coordinate = [xnum, ynum]
      @direction = direction
      @count = 0
    end

    # Come to (x, y) by going in direction.
    # ret[:num][0] not basin -> ret[:next][0]
    # ret[:num][0] basin, ret[:num][1] not basin -> ret[:next][1]
    # ret[:num][0] basin, ret[:num][1] basin -> ret[:next][2]
    def condition(direction, x, y)
      ret = { }
      case direction
      when :up
        ret[:num] = [[x, y], [x - 1, y]]
        ret[:next] = [:right, :up, :left]
      when :down
        ret[:num] = [[x - 1, y - 1], [x, y - 1]]
        ret[:next] = [:left, :down, :right]
      when :left
        ret[:num] = [[x - 1, y], [x - 1, y - 1]]
        ret[:next] = [:up, :left, :down]
      when :right
        ret[:num] = [[x, y - 1], [x, y]]
        ret[:next] = [:down, :right, :up]
      end
      ret[:next].map! do |a|
        case a
        when :up
          [:up, x, y + 1]
        when :down
          [:down, x, y - 1]
        when :left
          [:left, x - 1, y]
        when :right
          [:right, x + 1, y]
        end
      end
      ret
    end
    
    def next(&block)
      cond = condition(@direction, *@coordinate)
      if yield(*cond[:num][0])
        if yield(*cond[:num][1])
          res = cond[:next][2]
        else
          res = cond[:next][1]
        end
      else
        res = cond[:next][0]
      end
      @direction = res[0]
      @coordinate = res[1..2]
      @count += 1
    end

    def finish?
      @start_coordinate[0] == @coordinate[0] && @start_coordinate[1] == @coordinate[1] && @direction == @start_direction
    end
  end

  class BasinBoundary < BasinData
    
    # conv is ConvergeToBasin.
    def initialize(conv, grid, db_path = 'basin.tct')
      super(conv, grid, db_path)
    end

    def set_start_point(direction, xnum, ynum)
      @boundary = BoundaryCoordinate.new(xnum, ynum, direction)
    end

    def valid_position?(boundary)
      xnum, ynum = boundary.coordinate
      case boundary.direction
      when :up
        get_basin_pt(xnum, ynum - 1) && !get_basin_pt(xnum - 1, ynum - 1)
      when :down
        get_basin_pt(xnum - 1, ynum) && !get_basin_pt(xnum, ynum)
      when :left
        get_basin_pt(xnum, ynum) && !get_basin_pt(xnum, ynum - 1)
      when :right
        get_basin_pt(xnum - 1, ynum - 1) && !get_basin_pt(xnum - 1, ynum)
      end
    end
    
    # Output to file.
    def get_boundary(hash = {})
      max = hash[:max]
      io = hash[:io]
      format = hash[:format] || "%.Re"
      open_db do
        io.puts(@grid.left_bottom_corner(*@boundary.coordinate).to_s(format)) if io
        begin
          exit_loop = nil
          @boundary.next do |xnum, ynum, direction|
            res = get_basin_pt(xnum, ynum)
            exit_loop = max && @boundary.count > max
            res
          end
          io.puts(@grid.left_bottom_corner(*@boundary.coordinate).to_s(format)) if io
          break if exit_loop
        end while @boundary
      end
    end
    
  end
  
  class OutputBasinData
    include TCGrid

    def initialize(tdb)
      @tdb = TDB.new
      @tdb_path = tdb
    end

    def open_db(&block)
      open_database(@tdb, @tdb_path)
      if block_given?
        res = yield
        close_db
        return res
      end
    end

    def close_db
      close_database(@tdb)
    end

    def isolate?(xcen, ycen)
      [[1, 1], [1, 0], [0, 1], [1, -1], [-1, 1], [-1, 0], [0, -1], [-1, -1]].all? do |x, y|
        around = pack_coordinate_key(x + xcen, y + ycen)
        val = @tdb[around]
        !val || (val['basin'].to_i != IN_BASIN)
      end
    end

    def output_to_file(path, format, &block)
      open_db do
        open(path, 'a+') do |f|
          @tdb.each do |key, val|
            if yield(key, val)
              ary = [unpack_mpfr_str(val['x']), unpack_mpfr_str(val['y'])].map { |fr| fr.to_strf(format)}
              f.puts ary.join(' ')
            end
          end
        end
      end
    end

    def output_data_to_file(path, format = "%.Re")
      output_to_file(path, format) { |key, val| val['basin'].to_i == IN_BASIN }
    end
    
    def output_isolated_points(path, format = "%.Re")
      output_to_file(path, format) { |key, val| val['basin'].to_i == IN_BASIN && isolate?(*unpack_key_string(key)) }
    end

    def output_main_points(path, format = "%.Re")
      output_to_file(path, format) { |key, val| val['basin'].to_i == IN_BASIN && !isolate?(*unpack_key_string(key)) }
    end
    
  end

end
