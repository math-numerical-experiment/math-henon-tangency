require "ruby_mpfr_henon"
require "ruby_mpfi_henon"

module MappedMfd
  def self.output_mfds_for_debug(path, *mfds)
    mfds[0].output_to_file(path, 'a+')
    if mfds.size > 1
      mfds[1..-1].each do |m|
        Kernel.open(path, 'a+') do |f|
          f.puts ""
        end
        m.output_to_file(path, 'a+')
      end
    end
  end

  module Utils
    # @param [Range,nil] range Index range If the value is nil, yield for all indices
    def each_range_index(range, &block)
      if block_given?
        range.each do |ind|
          if ind >= 0 && ind < size
            yield(ind)
          end
        end
      else
        to_enum(:each_range_index, range)
      end
    end
    private :each_range_index

    # @param [Range,Fixnum,nil] arg Specify range of indices of manifold
    def orbit_length(arg = nil)
      arg ||= (0...size)
      case arg
      when Range
        sum = nil
        each_range_index(arg) do |i|
          if sum
            sum += self[i - 1].distance_from(self[i])
          else
            sum = MPFR(0)
          end
        end
      when Fixnum
        if arg >= 0 && arg < size - 1
          sum = self[arg].distance_from(self[arg + 1])
        else
          raise ArgumentError, "Invalid index number of segment of manifold: #{arg}"
        end
      else
        raise ArgumentError, "Invalid object of method orbit_length: #{arg.class}"
      end
      sum
    end

    def max_interval(range = nil)
      max_interval = nil
      each_range_index(range || (0...size)) do |i|
        if max_interval
          d = self[i].distance_from(self[i - 1])
          max_interval = d if d > max_interval
        else
          max_interval = MPFR(0)
        end
      end
      max_interval
    end

    def output_to_file(path, mode = 'w')
      open(path, mode) do |f|
        each do |pt|
          f.puts pt
        end
      end
    end

    def calc_direction(pt_from, pt_to)
      x = pt_to[0] - pt_from[0]
      y = pt_to[1] - pt_from[1]
      if x == 0
        if y > 0
          MPFR(Math::PI / 2.0)
        else
          MPFR(-Math::PI / 2.0)
        end
      else
        tan = y / x
        arg = MPFR::Math.atan(tan)
        if x < 0
          if y < 0
            arg -= Math::PI
          else
            arg += Math::PI
          end
        end
        arg
      end
    end
    private :calc_direction

    def segment_direction(ind)
      calc_direction(self[ind], self[ind + 1])
    end

    def all_segment_directions
      (size - 1).times.map do |i|
        segment_direction(i)
      end
    end

    def max_variation_of_direction(directions_cache = nil)
      directions = (directions_cache || all_segment_directions).sort
      vars = (directions.size - 1).times.map { |i| directions[i + 1] - directions[i] }
      vars << MPFR(2 * Math::PI + directions[0] - directions[-1])
      2 * Math::PI - vars.max
    end
  end

  class StraightLine
    include Enumerable
    include MappedMfd::Utils

    attr_reader :base

    def initialize(pt_start, pt_end)
      @base = [pt_start, pt_end]
      @base_vector = pt_end - pt_start
      @base_length = pt_end.distance_from(pt_start)
    end

    def get_mfd_point(pt)
      [pt, get_ratio(pt)]
    end

    def get_ratio(pt)
      (pt - @base[0]).abs / @base_length
    end

    def get_mfd_point_from_ratio(ratio)
      [@base[0] + @base_vector.mul_scalar(ratio), ratio]
    end

    def get_pt_between_two_pts(pt1, pt2)
      midpt = pt1.midpoint(pt2)
      [midpt, get_ratio(midpt)]
    end

    def [](i)
      @base[i]
    end

    alias_method :at, :[]

    def each(&block)
      @base.each(&block)
    end

    def size
      @base.size
    end

    def calculate_more?
      false
    end
  end

  class Points
    include Enumerable
    include MappedMfd::Utils

    attr_reader :srcs, :iteration

    # @param [MPFR::Henon::MfdCalculator,MPFI::Henon::MfdCalculator,MappedMfd::StraightLine] calc
    #   The following methods are required:
    #   - get_mfd_point(pt)    Return an array of two elements: coordinate and ratio   
    #   - get_ratio(pt)    Return MPFR object between 0 and 1
    #   - get_mfd_point_from_ratio(ratio)    Return the same array as that of get_mfd_point
    #   - get_pt_between_two_pts(pt1, pt2)    Return the same array as that of get_mfd_point
    # @param [MPFR::Henon::Mapping,MPFI::Henon::MapN] mapping
    # @param [Integer] iteration An integer which may be negative
    # @param [InvMfd::SrcPts,Enumerable] src
    #   If src is not InvMfd::SrcPts, src is an object that is a list of ratios of points
    #   or a list of points and we convert it to InvMfd::SrcPts by using the method "map"
    def initialize(calc, mapping, iteration, src = [0, 1])
      @calc = calc
      @mapping = mapping
      @iteration = iteration
      @srcs = initial_src_points(src)

      @num_calc_pts = 0
      @logger = ManageLog.get_new_logger(self.class)
    end

    def dup
      self.class.new(@calc, @mapping, @iteration, @srcs.dup)
    end

    def replace_srcs!(srcs_new)
      @srcs = srcs_new
    end

    def [](i)
      @srcs[i].get_cache(:mfd)
    rescue => err
      $stderr.puts "ERROR: @srcs.size=#{@srcs.size}, i=#{i.inspect}"
      raise err
    end

    alias_method :at, :[]

    def each(&block)
      if block_given?
        @srcs.each { |pt| yield(pt.get_cache(:mfd)) }
      else
        Enumerator.new { |y| @srcs.each { |pt| y << pt.get_cache(:mfd) } }
      end
    end

    # For debugging.
    def each_srcs(&block)
      if block_given?
        @srcs.each { |pt| yield(pt) }
      else
        Enumerator.new { |y| @srcs.each { |pt| y << pt } }
      end
    end
    
    def size
      @srcs.size
    end

    def delete_points(at, num = 1)
      @srcs.delete_points(at, num)
    end

    # @param [InvMfd::SrcPts,Enumerable] arg
    def initial_src_points(arg)
      if InvMfd::SrcPts === arg
        arg
      else
        pt_ary = arg.map do |a|
          if Numeric === a
            get_ptdata_from_ratio((MPFR === a ? a : MPFR.new(a)))
          else
            get_ptdata_from_pt(a)
          end
        end
        InvMfd::SrcPts.new(pt_ary)
      end
    end
    private :initial_src_points

    def new_ptdata(pt, ratio)
      data = InvMfd::PtData.new(pt, @iteration, ratio)
      data.add_cache(:mfd, @mapping.iterate_map(pt, @iteration))
      data
    end
    private :new_ptdata

    def get_ptdata_from_pt(pt)
      ratio = @calc.get_ratio(pt)
      new_ptdata(pt, ratio)
    end
    private :get_ptdata_from_pt

    def get_ptdata_from_ratio(ratio)
      new_ptdata(*@calc.get_mfd_point_from_ratio(ratio))
    end
    private :get_ptdata_from_ratio

    def distance_two_pts(pt1, pt2)
      pt1.get_cache(:mfd).distance_from(pt2.get_cache(:mfd))
    end
    private :distance_two_pts

    # Calculate point between @srcs[num] and @srcs[num + 1] and add the point to @srcs.
    def add_new_mfd_pt(num)
      @srcs.add(num + 1, new_ptdata(*@calc.get_pt_between_two_pts(@srcs[num].crd, @srcs[num + 1].crd)))
      @num_calc_pts += 1
    end
    private :add_new_mfd_pt

    # This method is obsolete.
    # If the distance between two points on manifold is not sufficient large, 
    # this method add an invalid pointt on manifold.
    def add_new_mfd_pt_by_ratio(ind, ratio)
      if ratio > 0 && ratio < 1
        @srcs.add(ind + 1, new_ptdata(*@calc.get_mfd_point_from_ratio(ratio)))
        @num_calc_pts += 1
      else
        raise ArgumentError, "Invalid argument of #{self.class}#add_new_mfd_pt_by_ratio."
      end
    end
    private :add_new_mfd_pt_by_ratio

    def add_new_mfd_pt_at_start_pt(ind, start_pt)
      @srcs.add(ind + 1, new_ptdata(*@calc.get_mfd_point(start_pt)))
      @num_calc_pts += 1
    end
    private :add_new_mfd_pt_at_start_pt

    def calc_fwd(ind, error)
      i = 0
      while (dist = distance_two_pts(@srcs[ind], @srcs[ind + 1])) > error
        add_new_mfd_pt(ind)
      end
      ind
    end
    private :calc_fwd

    def calc_bck(ind, error)
      i = 0
      while (dist = distance_two_pts(@srcs[ind], @srcs[ind - 1])) > error
        add_new_mfd_pt(ind-1)
        i += 1
      end
      ind
    end
    private :calc_bck

    # The key of opt is :start or :end and its value is Integer.
    def calc_with_error(error, opt = { })
      ind = opt[:start] || 0
      ind_gap = (opt[:end] ? @srcs.size - opt[:end] : 1)
      unless MPFR === error
        raise ArgumentError, "Invalid argument. error must be MPFR, but it is #{error.inspect}"
      end
      if (ind <= @srcs.size - ind_gap) && ind_gap > 0
        while ind < @srcs.size - ind_gap
          ind = calc_fwd(ind, error) + 1
          ind = calc_bck(ind, error)
        end
      else
        raise ArgumentError, "Invalid arguments: ind = #{ind.inspect}, ind_gap = #{ind_gap.inspect}"
      end
    end

    # The key of hash is :start or :end and its value is Integer.
    def calc_with_number(number, opt = { })
      ind = opt[:start] || 0
      ind_end = opt[:end] || (@srcs.size - 1)
      ind = 0 if ind < 0
      ind_end = (@srcs.size - 1) if ind_end >= @srcs.size
      if ind < ind_end
        iterate = (ind_end - ind)
        num_one_step = number / iterate
        iterate.times do
          start_pt = @srcs[ind].crd
          pitch = (@srcs[ind + 1].crd - @srcs[ind].crd).div_scalar(num_one_step + 1)
          num_one_step.times do
            start_pt += pitch
            add_new_mfd_pt_at_start_pt(ind, start_pt)
            ind += 1
          end
          ind += 1
        end
      else
        raise ArgumentError, "Invalid argument for calc_mfd_with_number."
      end
    end

    # @param [Array] ranges An array of ranges or integers
    def portion(*ranges)
      self.class.new(@calc, @mapping, @iteration, @srcs.portion(*ranges))
    end

    # @param [Array] ranges An array of ranges or integers
    def portion!(*ranges)
      @srcs = initial_src_points(@srcs.portion(*ranges))
      self
    end
    
    def calculate_more?
      true
    end
    
    # For debugging.
    def valid_data?
      (1...(size)).each do |i|
        if @srcs[i-1].initial_position >= @srcs[i].initial_position
          raise "Invalid @srcs."
        end
      end
    end
  end
end

class MPFR::Obj2D
  def calculate_more?
    false
  end

  def output_to_file(path, mode = 'w', format = "%.Re")
    open(path, mode) do |f|
      each do |pt|
        f.puts pt.to_s(format)
      end
    end
  end
end
