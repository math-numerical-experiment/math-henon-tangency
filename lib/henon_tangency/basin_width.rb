require_relative "basin"

module Henon
  class FindBoundary
    attr_reader :count

    def initialize(threshold)
      @threshold = threshold
      @data = []
      @count = { :true => 0, :false => 0 }
    end

    def add(result)
      if result
        @count[:true] += 1
      else
        @count[:false] += 1
      end
      @data << result
      @data.shift if @data.size > @threshold
    end

    def clear
      @data.clear
      @count[:true] = 0
      @count[:false] = 0
    end

    def all_in_basin?
      @data.all?
    end

    def size
      @data.size
    end

    def enum_each_number(range)
      case range
      when Range
        return range.each
      when Array
        if range.size == 2
          if range[0] <= range[1]
            return range[0].step(range[1], 1)
          else
            return range[0].step(range[1], -1)
          end
        end
      end
      raise "Invalid object for index range."
    end
    private :enum_each_number

    def each_index(range_x, range_y)
      i = 0
      if Fixnum === range_x
        enum_each_number(range_y).each do |y|
          yield(range_x, y, i)
          i+= 1
        end
      elsif Fixnum === range_y
        enum_each_number(range_x).each do |x|
          yield(x, range_y, i)
          i+= 1
        end
      else
        raise "Invalid object for index range."
      end
    end
    private :each_index

    # Return index of first basin point
    def search(calc, range_x, range_y)
      # If sequential points whose number is @threshold are in basin,
      # there is a boundary.
      # Bisection method does not work, because most points in phase space is actually basin.
      history = []
      each_index(range_x, range_y) do |x, y, i|
        history << [x, y]
        add(calc.get_basin_pt(x, y))
        if i < @threshold
          if i == (@threshold - 1) && all_in_basin?
            # Points of first @threshold are all in basin
            raise "Too thick basin."
          end
        else
          history.shift
          if all_in_basin?
            return history[0]
          end
        end
      end
      nil
    end
  end

  class BasinWidth
    DEFAULT_GRID_SIZE = "1e-5"

    attr_reader :cache_db

    def initialize(origin, converge, opts = {})
      @logger = ManageLog.get_new_logger(self.class)
      @origin = origin
      @converge = converge
      @type = opts[:type] || :vertical
      @initial_grid_size = MPFR(DEFAULT_GRID_SIZE)
      filename = FileName.new(opts[:cache] || 'cache.tmp/boundary.tct', :type => :time, :position => :middle)
      @cache_db = filename.create(:add => :always, :directory => :parent)
      check_instance_variable
    end

    def check_instance_variable
      unless MPFR::ColumnVector === @origin
        raise "Invalid origin: #{@origin.inspect}"
      end
      unless @converge.basin_include?(@origin)
        raise "Origin is not in basin."
      end
      unless (@type == :vertical || @type == :horizontal)
        raise "Invalid type: #{@type.inspect}"
      end
      unless (MPFR === @initial_grid_size && @initial_grid_size > 0)
        raise "Invalid initial grid size: #{@initial_grid_size.inspect}"
      end
      true
    end
    private :check_instance_variable

    def include_points_around_origin?(grid_coord, depth = 1)
      (-depth..depth).each do |x|
        (-depth..depth).each do |y|
          if x != 0 || y != 0
            unless @converge.basin_include?(grid_coord.center(x, y))
              return false
            end
          end
        end
      end
      true
    end
    private :include_points_around_origin?

    def bisect_grid_size(size_true, size_false)
      bisect = MathUtils::Bisect.new
      try = 0
      while true
        results = bisect.set_objects(size_true, size_false, :check => true, :noerror => true) do |grid_size|
          grid_coord = Grid::Coordinate.new(@origin, grid_size)
          include_points_around_origin?(grid_coord)
        end
        break if results[0]
        @logger.debug("Find initial grid_size: #{size_true.to_strf("%.8Re")} #{size_false.to_strf("%.8Re")} #{results.inspect}")
        raise "Not suitable point." if try > 10
        if results[1] && results[2]
          size_false *= MPFR(10)
          size_true = size_true
        else
          size_false = size_true
          size_true *= size_true
        end
        try += 1
        @logger.debug("Retry bisect_grid_size") { [size_true.to_strf("%.14Re"), size_false.to_strf("%.14Re")] }
      end
      bisect.set_creation do |size1, size2|
        MPFR::Math.sqrt(size1 * size2)
      end
      bisect.set_end_condition do |x, y|
        y / x < 4
      end
      bisect.execute
      bisect.result
    end
    private :bisect_grid_size

    def get_grid_size
      grid_sizes = bisect_grid_size(MPFR(2) ** (-MPFR.get_default_prec + 20), @initial_grid_size)
      s = MPFR("1e#{MPFR::Math.log10(grid_sizes[1]).floor.to_i}") / 50
      @logger.debug("Grid Size") { s.to_strf("%.8Re") }
      s
    end
    private :get_grid_size

    def get_boundary_calculator(grid_size)
      Henon::BasinBoundary.new(@converge, Grid::Coordinate.new(@origin, grid_size), @cache_db)
    end
    private :get_boundary_calculator

    LENGTH_BOUNDARY = 200
    SEARCH_RANGE = LENGTH_BOUNDARY * 2
    MIN_BASIN_NUMBER = 5
    SEARCH_TRY_NUMBER = 5

    def search_boundary(find_boundary, calc, range_x, range_y)
      try = 0
      search_range = SEARCH_RANGE
      find = nil
      begin
        find = find_boundary.search(calc, range_x, range_y)
      rescue
        try += 1
        if Array === range_x
          range_x = range_x.map { |val| val * 2 }
        elsif Array === range_y
          range_y = range_y.map { |val| val * 2 }
        else
          raise "Invalid range argument: #{range_x.inspect} #{range_y.inspect}"
        end
        @logger.debug("Retry search boundary: search_range=#{search_range}")
        raise "Can not find boundary." if try > SEARCH_TRY_NUMBER
        retry
      end
      @logger.debug("Search boundary") { "in basin: #{find_boundary.count[:true]}, not in basin: #{find_boundary.count[:false]}"}
      find_boundary.clear
      find
    end

    def boundary_start_pt
      jac = @converge.mapping.jacobian_matrix_iterate_map(@origin, @converge.mapping.period)
      eigenvalues, (vec1, vec2) = jac.eigenvector

      dir_x = MPFR::ColumnVector([1, 0])
      dir_y = MPFR::ColumnVector([0, 1])
      product_x = vec1.inner_product(dir_x).abs + vec2.inner_product(dir_x).abs
      product_y = vec1.inner_product(dir_y).abs + vec2.inner_product(dir_y).abs

      calc = get_boundary_calculator(get_grid_size)
      ret = [calc]
      find_boundary = FindBoundary.new(MIN_BASIN_NUMBER)
      if product_x < product_y
        # Search x direction
        find = search_boundary(find_boundary, calc, [-SEARCH_RANGE, SEARCH_RANGE], -LENGTH_BOUNDARY)
        ret << [:up, find[0], find[1]]
        find = search_boundary(find_boundary, calc, [SEARCH_RANGE, -SEARCH_RANGE], LENGTH_BOUNDARY)
        ret << [:down, find[0], find[1] - 1]
      else
        # Search y direction
        find = search_boundary(find_boundary, calc, LENGTH_BOUNDARY, [-SEARCH_RANGE, SEARCH_RANGE])
        ret << [:left, find[0], find[1] - 1]
        find = search_boundary(find_boundary, calc, -LENGTH_BOUNDARY, [SEARCH_RANGE, -SEARCH_RANGE])
        ret << [:right, find[0], find[1]]
      end
      ret
    end
    private :boundary_start_pt

    def calc
      calc, boundary1, boundary2 = boundary_start_pt
      if boundary1 && boundary2
        out = FileName.new(File.dirname(@cache_db) + '/boundary.dat').create
        open(out, 'w') do |f|
          calc.set_start_point(*boundary1)
          calc.get_boundary(max: LENGTH_BOUNDARY * 2, io: f)
          f.puts "\n"
          
          calc.set_start_point(*boundary2)
          calc.get_boundary(max: LENGTH_BOUNDARY * 2, io: f)
        end

        lines = [[]]
        open(out, 'r') do |f|
          while l = f.gets
            l.strip!
            if l.size > 0
              lines[-1] << MPFR::ColumnVector(l.split)
            elsif lines[-1].size > 0
              lines << []
            end
          end
        end
        lines.delete_if { |ary| ary.size == 0 }
        distance = MPFR::Vector2D.distance_between_lines(*lines)[0]
      else
        raise "Can not get the starting poirts of boundary: #{boundary1.inspect} #{boundary2.inspect}"
      end
    end

    def sketch_basin(opts = {})
      range_size = opts[:range] || LENGTH_BOUNDARY
      path = FileName.new(File.dirname(@cache_db) + '/sketch_basin.dat', :position => :middle, :type => :time).create
      grid_coord = Grid::Coordinate.new(@origin, get_grid_size)
      open(path, 'w') do |f|
        (-range_size..range_size).each do |x|
          (-range_size..range_size).each do |y|
            crd = grid_coord.center(x, y)
            if @converge.basin_include?(crd)
              f.puts crd.to_s("%.20Re")
            end
          end
        end
      end
      path
    end
  end

end
