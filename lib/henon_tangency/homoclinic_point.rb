require_relative "long_manifold"

class HomoclinicPoint
  class Lines
    class Segment
      def initialize
        @data = []
      end

      def add_point(crd, mfd_crd)
        @data << [crd, mfd_crd]
      end

      def [](i)
        @data[i][0]
      end

      alias_method :at, :[]

      def method_missing(m, *args, &block)
        @data.__send__(m, *args, &block)
      end

      def mfd_coordinate(i)
        @data[i][1]
      end
    end

    attr_reader :segment

    def initialize(*segments)
      @segment = segments
    end

    def add_new_segment(num = nil)
      seg = HomoclinicPoint::Lines::Segment.new
      if num
        if (num >= 0 && num <= @segment.size) || (num < 0 && num >= -(@segment.size + 1))
          @segment.insert(num, seg)
        else
          raise ArgumentError, "Invalid index number: #{num}"
        end
      else
        @segment << seg
      end
    end

    def delete_empty_segment!
      @segment.delete_if { |seg| seg.empty? }
    end

    def add_point(segment_num, crd, mfd_crd)
      @segment[segment_num].add_point(crd, mfd_crd)
    end

    def [](i)
      @segment[i]
    end
  end

  include CreateMfdPt::MapPtFile::ParserComment
  GRID_SIZE = "1e-2"

  def initialize(st_mfd_file, un_mfd_file, error)
    @grid = Grid::Coordinate.new(MPFR::ColumnVector([0, 0]), MPFR(GRID_SIZE))
    @file = { :stable => st_mfd_file, :unstable => un_mfd_file }
    @intersection_determine = Intersect::Determine.new(error, MPFR("1e-1"))
    @logger = ManageLog.get_new_logger(self.class)
  end

  def load_file(path)
    lines_split = Hash.new { |h, k| h[k] = HomoclinicPoint::Lines.new }
    pt_args_last = nil
    index_last = nil
    MathUtils::DataFile.each_line(path) do |l|
      unless /^#/ =~ l
        l.strip!
        if l.size == 0
          pt_args_last = nil
          index_last = nil
        else
          a = l.split
          pt_args_new = [MPFR::ColumnVector(a[0..1]), MPFR(a[3]) + a[4].to_i]
          index_new = @grid.region_including(pt_args_new[0])
          lines = lines_split[index_new]
          if index_last != index_new
            lines.add_new_segment
            if pt_args_last
              lines.add_point(-1, *pt_args_last)
              lines_split[index_last].add_point(-1, *pt_args_new)
            end
          end
          lines.add_point(-1, *pt_args_new)
          pt_args_last = pt_args_new
          index_last = index_new
        end
      end
    end
    lines_split
  end
  private :load_file

  def search_intersections(st_mfd, un_mfd)
    intersections = []
    st_mfd.segment.each_with_index do |segment_stable, num_stable|
      un_mfd.segment.each_with_index do |segment_unstable, num_unstable|
        if ary = Intersect::Function.get_intersections(segment_stable, segment_unstable)
          ary.each do |intr|
            intersections << [intr.crd, segment_stable.mfd_coordinate(intr.ind1), segment_stable.mfd_coordinate(intr.ind1 + 1), segment_unstable.mfd_coordinate(intr.ind2), segment_unstable.mfd_coordinate(intr.ind2 + 1)]
          end
        end
      end
    end
    intersections
  end

  def calculator_arguments(path)
    arithmetic, constants, period, type, flip, first_pt, max_iteration, base_pt, basis_ary = parse_comment(path)
    mapping = MPFR::Henon::Mapping.new(constants, period)
    { :mapping => mapping, :calculator => InvMfd::Calculator.create_new_calculator(constants, period, base_pt, basis_ary, type, flip, arithmetic) }
  end
  private :calculator_arguments

  def get_manifold_ary(type, calc, mapping, mfd_crds)
    iteration = mfd_crds[0].floor
    iteration2 = mfd_crds[1].floor
    if iteration == iteration2
      crds = mfd_crds.map { |crd| crd - iteration }
      [MappedMfd::Points.new(calc, mapping, (type == :stable ? -iteration : iteration), crds)]
    else
      manifolds = []
      manifolds << MappedMfd::Points.new(calc, mapping, (type == :stable ? -iteration : iteration), [mfd_crds[0] - iteration, MPFR(1)])
      ((iteration + 1)...iteration2).each do |iter|
        manifolds << MappedMfd::Points.new(calc, mapping, (type == :stable ? -iter : iter), [MPFR(0), MPFR(1)])
      end
      manifolds << MappedMfd::Points.new(calc, mapping, (type == :stable ? -iteration2 : iteration2), [MPFR(0), mfd_crds[1] - iteration2])
      manifolds
    end
  end
  private :get_manifold_ary

  def narrow_homoclinic_point(ary, calc_args)
    st_mfd_ary = get_manifold_ary(:stable, calc_args[:stable][:calculator], calc_args[:stable][:mapping], ary[1..2])
    un_mfd_ary = get_manifold_ary(:unstable, calc_args[:unstable][:calculator], calc_args[:unstable][:mapping], ary[3..4])
    intersections = []
    st_mfd_ary.each do |st_mfd|
      un_mfd_ary.each do |un_mfd|
        unless MPFR::Vector2D.intersection_of_lines2([st_mfd, 0, st_mfd.size - 1], [un_mfd, 0, un_mfd.size - 1]).empty?
          if intersections_new = @intersection_determine.search_intersections(st_mfd, un_mfd)
            st_mfd, un_mfd = @intersection_determine.manifolds
            intersections_new.map! do |intersection|
              [intersection.crd,
               st_mfd.iteration.abs + st_mfd.srcs[intersection.ind1].initial_position,
               st_mfd.iteration.abs + st_mfd.srcs[intersection.ind1 + 1].initial_position,
               un_mfd.iteration.abs + un_mfd.srcs[intersection.ind2].initial_position,
               un_mfd.iteration.abs + un_mfd.srcs[intersection.ind2 + 1].initial_position]
            end
            intersections.concat(intersections_new)
          end
        end
      end
    end
    if intersections.empty?
      raise "The intersection disappears!"
    elsif intersections.size != 1
      $stderr.puts "Found more than one intersections. Not sufficient precision of manifolds! Only first intersection is saved"
    end
    intersections.first
  end
  private :narrow_homoclinic_point

  def intersections_sort_uniq(intersections)
    intersections2 = intersections.sort do |a, b|
      cmp = a[1] <=> b[1]
      if cmp == 0
        a[3] <=> b[3]
      else
        cmp
      end
    end
    intersections_uniq = []
    intersections2.each_with_index do |ary, i|
      if !(intersections_uniq.size > 0 && intersections_uniq[-1][0] == ary[0])
        intersections_uniq << ary
      end
    end
    intersections_uniq
  end
  private :intersections_sort_uniq

  def search
    intersections = []
    calc_args = { :stable => calculator_arguments(@file[:stable]), :unstable => calculator_arguments(@file[:unstable]) }
    st_mfd_split = load_file(@file[:stable])
    un_mfd_split = load_file(@file[:unstable])
    st_mfd_split.each do |index, st_mfd_lines|
      if un_mfd_split.has_key?(index)
        @logger.debug("index (#{index[0]}, #{index[1]})")
        intersections.concat(search_intersections(st_mfd_lines, un_mfd_split[index]))
      end
    end
    intersections.map! do |ary|
      narrow_homoclinic_point(ary, calc_args)
    end
    intersections_sort_uniq(intersections)
  end
end
