require "ruby_opengl_mpfr"
require_relative "manifold"

module MappedMfd
  
  # This class is for displaying.
  # When we execute methods which change point data, we must update @obj2d.
  class PointsObj2D < Points

    attr_reader :obj2d
    
    def initialize(calc, mapping, iteration, src, style = MPFRPlot::STY_LINES)
      super(calc, mapping, iteration, src)
      @obj2d = MPFR::Obj2D.new(:style => style, :data => to_a)
    end

    def add_new_mfd_pt(num)
      super(num)
      @obj2d.add(self[num + 1], num + 1)
    end
    private :add_new_mfd_pt

    # This method is obsolete.
    def add_new_mfd_pt_by_ratio(ind, ratio)
      super(ind, ratio)
      @obj2d.add(self[ind + 1], ind + 1)
    end
    private :add_new_mfd_pt_by_ratio

    def add_new_mfd_pt_at_start_pt(ind, start_pt)
      super(ind, start_pt)
      @obj2d.add(self[ind + 1], ind + 1)
    end
    private :add_new_mfd_pt_at_start_pt

    def portion!(*ranges)
      super(*ranges)
      @obj2d.set_data_ary(to_a)
      self
    end
    
  end

end

