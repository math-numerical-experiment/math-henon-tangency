class CascadeResult
  @root = nil

  def self.root=(path)
    @root = path
  end

  def self.root
    @root
  end

  def self.load_yaml_file(path)
    YAML.load_file(path).map do |ary|
      CascadeResult::SinkResult.new(*ary)
    end
  end

  attr_reader :data, :iter_stable, :iter_unstable

  def initialize(*args)
    case args.size
    when 1
      @dir = nil
      load_result_file(args[0])
      data_sort!
    when 2
      iter_stable, iter_unstable = args
      @iter_stable = iter_stable
      @iter_unstable = iter_unstable
      set_directory
      load_result
    else
      raise "Invalid arguments of #{self.class.to_s}"
    end
  end

  def set_directory
    @dir = File.join(self.class.root, ("%03d_%03d" % [@iter_stable, @iter_unstable]))
    FileUtils.mkdir_p(@dir)
    @create_backup = nil
  end
  private :set_directory

  def path_result
    File.join(@dir, "result.yaml")
  end
  private :path_result

  def path_result_source
    File.join(@dir, "result_source.yaml")
  end
  private :path_result_source

  def load_yaml_file(path)
    self.class.load_yaml_file(path)
  end
  private :load_yaml_file

  def data_sort!
    @data.sort! { |a, b| a.period <=> b.period }
  end
  private :data_sort!

  def load_result_file(path)
    if File.exist?(path)
      @data = load_yaml_file(path)
    else
      @data = []
    end
  end
  private :load_result_file

  def load_result
    load_result_file(path_result)
    if @data.empty? && File.exist?(path_result_source)
      @data = load_yaml_file(path_result_source)
    end
    data_sort!
  end
  private :load_result

  def [](key)
    @data[key]
  end

  def []=(key, val)
    @data[key] = val
  end

  def save
    if File.exist?(path_result) && !@create_backup
      FileUtils.mv(path_result, "#{path_result}.old")
      @create_backup = true
    end
    vals = @data.map do |res|
      res.to_array
    end
    Kernel.open(path_result, 'w') do |f|
      f.puts YAML.dump(vals)
    end
  end

  def refine!(num_data)
    data_odd = @data.select { |res| res.period.odd? }
    data_even = @data.select { |res| res.period.even? }
    if data_odd.size > num_data
      data_odd = data_odd[(-num_data)..-1]
    end
    if data_even.size > num_data
      data_even = data_even[(-num_data)..-1]
    end
    @data = data_odd + data_even
    data_sort!
  end

  def get(period)
    @data.find { |res| res.period == period }
  end

  # @param [CascadeResult::SinkResult] data_new New data of sink
  def add(data_new)
    if get(data_new.period)
      raise "Data of period #{data_new.period} already exists"
    end
    @data << data_new
    data_sort!
  end

  def each(&block)
    @data.each(&block)
  end

  PRM_B = "-0.3"

  def map_and_save!(num, inherit_keys = nil)
    @iter_stable -= num
    @iter_unstable += num
    set_directory

    @data.map! do |res|
      pt = MPFR::ColumnVector(res[:crd])
      henon = MPFR::Henon::Mapping.new([res[:prm], PRM_B])
      pt_new = henon.map(pt, num)
      data = { :prm => res[:prm], :crd => [pt_new[0], pt_new[1]] }
      if inherit_keys
        inherit_keys.each do |key|
          data[key] = res[key]
        end
      end
      CascadeResult::SinkResult.new(res.period, data)
    end
    save
  end

  class YAMLData
    attr_reader :data

    def initialize(vals_hash)
      @data = hash_convert_to_mpfr(vals_hash)
    end

    def convert_to_mpfr_by_class(obj)
      case obj
      when String
        string_convert_to_mpfr(obj)
      when Array
        array_convert_to_mpfr(obj)
      when Hash
        hash_convert_to_mpfr(obj)
      else
        obj
      end
    end
    private :convert_to_mpfr_by_class

    def string_convert_to_mpfr(val)
      MPFR(val)
    end
    private :string_convert_to_mpfr

    def array_convert_to_mpfr(ary)
      ary.map do |val|
        convert_to_mpfr_by_class(val)
      end
    end
    private :array_convert_to_mpfr

    def hash_convert_to_mpfr(hash)
      h = {}
      hash.each do |key, val|
        h[key] = convert_to_mpfr_by_class(val)
      end
      h
    end
    private :hash_convert_to_mpfr

    def convert_to_string_by_class(obj)
      case obj
      when MPFR
        mpfr_convert_to_string(obj)
      when Array
        array_convert_to_string(obj)
      when Hash
        hash_convert_to_string(obj)
      else
        obj
      end
    end
    private :convert_to_string_by_class

    def mpfr_convert_to_string(obj)
      obj.to_strf("%.Re")
    end
    private :mpfr_convert_to_string

    def array_convert_to_string(ary)
      ary.map do |obj|
        convert_to_string_by_class(obj)
      end
    end
    private :array_convert_to_string

    def hash_convert_to_string(hash)
      h = {}
      hash.each do |key, val|
        h[key] = convert_to_string_by_class(val)
      end
      h
    end
    private :hash_convert_to_string

    def [](key)
      @data[key]
    end

    def []=(key, val)
      @data[key] = val
    end

    def dump_to_file(path)
      Kernel.open(path, "w") do |f|
        f.print YAML.dump(hash_convert_to_string(@data))
      end
    end

    def self.load_file(path)
      self.new(YAML.load_file(path))
    end
  end

  class SinkResult < YAMLData
    attr_reader :period

    def initialize(period, vals_hash)
      @period = period
      @data = hash_convert_to_mpfr(vals_hash)
    end

    def to_array
      [@period, hash_convert_to_string(@data)]
    end

    def <=>(other)
      self.period <=> other.period
    end
  end
end
