#include "func_mpfr_ikeda.h"

/*
  Ikeda map
  x_{n+1} = p + B (x_n \cos t - y_n \sin t)
  y_{n+1} = B(x_n \sin t + y_n \cos t)
  t = k - a / (1 + x_n^2 + y_n^2)
  cst = {a, B, k, p}
*/
void r_mpfr_ikeda_map (MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst)
{
  MPFR t, cos_t, sin_t, tmp;
  mpfr_init(&t);
  mpfr_init(&cos_t);
  mpfr_init(&sin_t);
  mpfr_init(&tmp);

  mpfr_mul(&tmp, x->data, x->data, GMP_RNDN);
  mpfr_mul(&t, x->data + 1, x->data + 1, GMP_RNDN);
  mpfr_add(&t, &t, &tmp, GMP_RNDN);
  mpfr_add_ui(&t, &t, 1, GMP_RNDN);
  mpfr_div(&t, cst->data, &t, GMP_RNDN);
  mpfr_sub(&t, cst->data + 2, &t, GMP_RNDN);

  mpfr_sin_cos(&sin_t, &cos_t, &t, GMP_RNDN);

  mpfr_mul(&tmp, x->data, &cos_t, GMP_RNDN);
  mpfr_mul(y->data, x->data + 1, &sin_t, GMP_RNDN);
  mpfr_sub(&tmp, &tmp, y->data, GMP_RNDN);
  mpfr_mul(&tmp, cst->data + 1, &tmp, GMP_RNDN);
  mpfr_add(y->data, cst->data + 3, &tmp, GMP_RNDN);

  mpfr_mul(&tmp, x->data, &sin_t, GMP_RNDN);
  mpfr_mul(y->data + 1, x->data + 1, &cos_t, GMP_RNDN);
  mpfr_add(&tmp, &tmp, y->data + 1, GMP_RNDN);
  mpfr_mul(y->data + 1, cst->data + 1, &tmp, GMP_RNDN);

  mpfr_clear(&t);
  mpfr_clear(&cos_t);
  mpfr_clear(&sin_t);
  mpfr_clear(&tmp);
}

/*
  Inverse map
  x_n = (y_{n+1} \cos t_n + (p - x_{n+1}) \sin t_n) / B
  y_n = (y_{n+1} \sin t_n + (p - x_{n+1}) \cos t_n) / B
  t = k - a / (1 + ((x_{n+1} - p)^2 + y_{n+1}^2) / B^2)
  cst = {a, B, k, p}
*/
void r_mpfr_ikeda_inverse_map (MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst)
{
  MPFR t, cos_t, sin_t, p_x, tmp;
  mpfr_init(&t);
  mpfr_init(&cos_t);
  mpfr_init(&sin_t);
  mpfr_init(&p_x);
  mpfr_init(&tmp);

  mpfr_sub(&p_x, cst->data + 3, x->data, GMP_RNDN);
  mpfr_mul(&tmp, &p_x, &p_x, GMP_RNDN);
  mpfr_mul(&t, x->data + 1, x->data + 1, GMP_RNDN);
  mpfr_add(&tmp, &t, &tmp, GMP_RNDN);
  mpfr_add_ui(&tmp, &tmp, 1, GMP_RNDN);
  mpfr_div(&tmp, cst->data, &tmp, GMP_RNDN);
  mpfr_mul(&t, cst->data + 1, cst->data + 1, GMP_RNDN);
  mpfr_div(&tmp, &tmp, &t, GMP_RNDN);
  mpfr_sub(&t, cst->data + 2, &tmp, GMP_RNDN);

  mpfr_sin_cos(&sin_t, &cos_t, &t, GMP_RNDN);

  mpfr_mul(y->data, x->data + 1, &cos_t, GMP_RNDN);
  mpfr_mul(&tmp, &p_x, &sin_t, GMP_RNDN);
  mpfr_add(y->data, y->data, &tmp, GMP_RNDN);
  mpfr_div(y->data, y->data, cst->data + 1, GMP_RNDN);

  mpfr_mul(y->data + 1, x->data + 1, &sin_t, GMP_RNDN);
  mpfr_mul(&tmp, &p_x, &cos_t, GMP_RNDN);
  mpfr_add(y->data + 1, y->data + 1, &tmp, GMP_RNDN);
  mpfr_div(y->data + 1, y->data + 1, cst->data + 1, GMP_RNDN);

  mpfr_clear(&t);
  mpfr_clear(&cos_t);
  mpfr_clear(&sin_t);
  mpfr_clear(&p_x);
  mpfr_clear(&tmp);
}

void r_mpfr_ikeda_iteration (MPFRMatrix * b, MPFRMatrix * a, MPFRMatrix * c, int iteration) {
  if (iteration == 0) {
    mpfr_matrix_set(b, a);
  } else {
    int i;
    MPFRMatrix *tmp, *t_ret;
    r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);
    r_mpfr_matrix_temp_alloc_init(t_ret, 2, 1);

    mpfr_matrix_set(tmp, a);

    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	r_mpfr_ikeda_map(t_ret, tmp, c);
	mpfr_matrix_set(tmp, t_ret);
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	r_mpfr_ikeda_inverse_map(t_ret, tmp, c);
	mpfr_matrix_set(tmp, t_ret);
      }
    }
    mpfr_matrix_set(b, t_ret);

    r_mpfr_matrix_temp_free(tmp);
    r_mpfr_matrix_temp_free(t_ret);
  }
}

/* void r_mpfr_ikeda_jacobian_matrix (MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants) { */
/*   mpfr_set_si(jac->data, -2, GMP_RNDN); */
/*   mpfr_mul(jac->data, jac->data, constants->data, GMP_RNDN); */
/*   mpfr_mul(jac->data, jac->data, pt->data, GMP_RNDN); */
/*   mpfr_set_si(jac->data + 2, 1, GMP_RNDN); */
/*   mpfr_neg(jac->data + 1, constants->data + 1, GMP_RNDN); */
/*   mpfr_set_si(jac->data + 3, 0, GMP_RNDN); */
/* } */

/* void r_mpfr_ikeda_fixed_point (MPFRMatrix *fp1, MPFRMatrix *fp2, MPFRMatrix *constants) { */
/*   MPFRMatrix x, y; */
/*   mpfr_matrix_init(&x, 2, 1); */
/*   mpfr_matrix_init(&y, 2, 1); */

/*   /\* start: calculate fixed points *\/ */
/*   mpfr_add_ui(x.data, constants->data + 1, 1, GMP_RNDN); */
/*   mpfr_mul(x.data + 1, x.data, x.data, GMP_RNDN); */
/*   mpfr_mul_ui(y.data, constants->data, 4, GMP_RNDN); */
/*   mpfr_add(x.data + 1, x.data + 1, y.data, GMP_RNDN); */
/*   mpfr_sqrt(x.data + 1, x.data + 1, GMP_RNDN); */
/*   mpfr_neg(x.data, x.data, GMP_RNDN); */

/*   mpfr_add(fp1->data, x.data, x.data + 1, GMP_RNDN); */
/*   mpfr_sub(fp2->data, x.data, x.data + 1, GMP_RNDN); */

/*   mpfr_div_ui(fp1->data, fp1->data, 2, GMP_RNDN); */
/*   mpfr_div(fp1->data, fp1->data, constants->data, GMP_RNDN); */
/*   mpfr_mul(fp1->data + 1, fp1->data, constants->data + 1, GMP_RNDN); */
/*   mpfr_neg(fp1->data + 1, fp1->data + 1, GMP_RNDN); */

/*   mpfr_div_ui(fp2->data, fp2->data, 2, GMP_RNDN); */
/*   mpfr_div(fp2->data, fp2->data, constants->data, GMP_RNDN); */
/*   mpfr_mul(fp2->data + 1, fp2->data, constants->data + 1, GMP_RNDN); */
/*   mpfr_neg(fp2->data + 1, fp2->data + 1, GMP_RNDN); */

/*   mpfr_matrix_clear(&x); */
/*   mpfr_matrix_clear(&y); */
/* } */

void r_mpfr_ikeda_cumulative_distance (MPFR *distance, MPFRMatrix *pt, MPFRMatrix *cst, int period) {
  int i;
  MPFR tmp;
  MPFRMatrix *orbits;
  mpfr_init(&tmp);
  orbits = (MPFRMatrix *) malloc(sizeof(MPFRMatrix) * (period + 1));
  for (i = 0; i <= period; i++) {
    mpfr_matrix_init(&orbits[i], 2, 1);
  }
  r_mpfr_ikeda_map(&orbits[0], pt, cst);
  for (i = 1; i < period; i++) {
    r_mpfr_ikeda_map(&orbits[i], &orbits[i-1], cst);
  }

  mpfr_matrix_vector_distance(distance, &orbits[period - 1], pt);
  for (i = 0; i < period - 1; i++) {
    r_mpfr_ikeda_map(&orbits[period], &orbits[period - 1], cst);
    mpfr_matrix_set(&orbits[period - 1], &orbits[period]);
    mpfr_matrix_vector_distance(&tmp, &orbits[period], &orbits[i]);
    mpfr_add(distance, distance, &tmp, GMP_RNDN);
  }

  mpfr_clear(&tmp);
  for (i = 0; i <= period; i++) {
    mpfr_matrix_clear(&orbits[i]);
  }
  free(orbits);
}
