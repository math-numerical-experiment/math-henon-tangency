#ifndef _RUBY_MPFR_IKEDA_H_
#define _RUBY_MPFR_IKEDA_H_

#include "func_mpfr_ikeda.h"

VALUE r_mpfr_vector_2d_module, mpfr_mfd_calculator, mpfr_ikeda_mapping, mpfr_ikeda_module, float_ikeda;

typedef struct __IkedaMPFRMap{
  MPFRMatrix *cst;
  int period;
} IkedaMPFRMap;

#endif /* _RUBY_MPFR_IKEDA_H_ */

