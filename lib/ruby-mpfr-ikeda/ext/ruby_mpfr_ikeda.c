#include "ruby_mpfr_ikeda.h"

static VALUE __mpfr_class__;
static ID mpfr_ikeda_mapping_const_id;

static void mpfr_ikeda_mapping_free(IkedaMPFRMap *ptr) {
  mpfr_matrix_clear(ptr->cst);
  free(ptr->cst);
  free(ptr);
}

static VALUE mpfr_ikeda_mapping_alloc(VALUE self) {
  IkedaMPFRMap *a;
  self = Data_Make_Struct(mpfr_ikeda_mapping, IkedaMPFRMap, 0, mpfr_ikeda_mapping_free, a);
  a->cst = ALLOC_N(MPFRMatrix, 1);
  mpfr_matrix_init(a->cst, 4, 1);
  return self;
}

static VALUE mpfr_ikeda_mapping_initialize (int argc, VALUE *argv, VALUE self) {
  IkedaMPFRMap *ptr;
  VALUE tmp;
  MPFRMatrix *ptr_tmp;
  Data_Get_Struct(self, IkedaMPFRMap, ptr);
  switch(argc) {
  case 1:
    ptr->period = 1;
    break;
  case 2:
    ptr->period = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Ikeda::Mapping.new is one or two.");
  }
  switch(TYPE(argv[0])) {
  case T_ARRAY:
    tmp = rb_funcall(r_mpfr_col_vector, rb_intern("new"), 1, argv[0]);
    break;
  case T_STRUCT:
    tmp = rb_funcall(r_mpfr_col_vector, rb_intern("new"), 1, rb_funcall(argv[0], rb_intern("to_a"), 0));
    break;
  default:
    tmp = argv[0];
    break;
  }
  r_mpfr_get_matrix_struct(ptr_tmp, tmp);
  if (ptr_tmp->size != 4) {
    rb_raise(rb_eArgError, "Invalid number of parameters.");
  }
  mpfr_matrix_set(ptr->cst, ptr_tmp);

  rb_ivar_set(self, mpfr_ikeda_mapping_const_id, tmp);
  return Qtrue;
}

static VALUE mpfr_ikeda_mapping_initialize_copy(VALUE self, VALUE other) {
  IkedaMPFRMap *ptr_self, *ptr_other;
  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  Data_Get_Struct(other, IkedaMPFRMap, ptr_other);

  mpfr_matrix_set(ptr_other->cst, ptr_self->cst);
  ptr_other->period = ptr_self->period;
  
  rb_ivar_set(self, mpfr_ikeda_mapping_const_id, rb_ivar_get(other, mpfr_ikeda_mapping_const_id));
  return Qtrue;
}

static VALUE mpfr_ikeda_mapping_period(VALUE self) {
  IkedaMPFRMap *ptr_self;
  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  return INT2NUM(ptr_self->period);
}

static VALUE mpfr_ikeda_mapping_constants(VALUE self) {
  IkedaMPFRMap *ptr_self;
  VALUE v_cst[4];
  MPFR *r_cst[4];
  int i;
  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  for (i = 0; i < 4; i++) {
    r_mpfr_make_struct_init(v_cst[i], r_cst[i]);
    mpfr_set(r_cst[i], mpfr_matrix_get_ptr(ptr_self->cst, i), GMP_RNDN);
  }
  return rb_ary_new3(4, v_cst[0], v_cst[1], v_cst[2], v_cst[3]);
}

/* argv is array having one or two elements. First element is MPFR::ColumnVector instance. */
/* Second element is number of iteration. Default is 1. */
/* Return map MPFR::ColumnVector instance argv[1] times. */
static VALUE mpfr_ikeda_mapping_map (int argc, VALUE *argv, VALUE self) {
  IkedaMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Ikeda::Mapping.map is one or two.");
  }
  r_mpfr_ikeda_iteration(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/* argv is array having one or two elements. First element is MPFR::ColumnVector instance. */
/* Second element is number of iteration. Default is ptr_self->period. */
/* Return map MPFR::ColumnVector instance argv[1] * ptr_self->period times. */
static VALUE mpfr_ikeda_mapping_iterate_map (int argc, VALUE *argv, VALUE self) {
  IkedaMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);

  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Ikeda::Mapping.iterate_map is one or two.");
  }
  r_mpfr_ikeda_iteration(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

static VALUE mpfr_ikeda_mapping_make_orbit(VALUE self, VALUE start, VALUE num) {
  IkedaMPFRMap *ptr_self;
  MPFRMatrix *ptr_start, *ptr_tmp;
  int n, i;
  VALUE ret, tmp;

  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_start, start);
  n = NUM2INT(num);
  ret = rb_ary_new3(1, start);
  r_mpfr_matrix_suitable_matrix_init (&tmp, &ptr_tmp, 2, 1);
  mpfr_matrix_set(ptr_tmp, ptr_start);
  for (i = 1; i < n; i++) {
    r_mpfr_ikeda_iteration(ptr_tmp, ptr_tmp, ptr_self->cst, ptr_self->period);
    rb_ary_push(ret, r_mpfr_matrix_robj(ptr_tmp));
  }
  return ret;
}

static VALUE mpfr_ikeda_mapping_each_pt_on_orbit(VALUE self, VALUE start, VALUE num) {
  IkedaMPFRMap *ptr_self;
  MPFRMatrix *ptr_start, *ptr_tmp;
  int n, i;
  VALUE ret, tmp;

  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_start, start);
  n = NUM2INT(num);
  r_mpfr_matrix_suitable_matrix_init (&tmp, &ptr_tmp, 2, 1);
  mpfr_matrix_set(ptr_tmp, ptr_start);
  ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp));
  for (i = 1; i < n; i++) {
    r_mpfr_ikeda_iteration(ptr_tmp, ptr_tmp, ptr_self->cst, ptr_self->period);
    ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp));
  }
  return ret;
}

/* static VALUE mpfr_ikeda_mapping_jacobian_matrix(VALUE self, VALUE arg) { */
/*   IkedaMPFRMap *ptr_self; */
/*   MPFRMatrix *ptr_arg, *ptr_ret; */
/*   VALUE ret; */

/*   Data_Get_Struct(self, IkedaMPFRMap, ptr_self); */
/*   r_mpfr_get_matrix_struct(ptr_arg, arg); */
/*   r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 2); */
/*   r_mpfr_ikeda_jacobian_matrix(ptr_ret, ptr_arg, ptr_self->cst); */
/*   return ret; */
/* } */

/* /\* Return jacobian matrix of _iterate_ iteration of Ikeda mapping at _arg_. *\/ */
/* /\* _iterate_ must be positive by Ikeda::Mapping#jacobian_matrix_iterate_map_base. *\/ */
/* static VALUE mpfr_ikeda_mapping_jacobian_matrix_iterate_map(VALUE self, VALUE arg, VALUE iterate) { */
/*   IkedaMPFRMap *ptr_self; */
/*   VALUE ret; */
/*   MPFRMatrix *ptr_arg, *ptr_jac_ret, *ptr_jac_tmp, *ptr_jac_new, *ptr_pt_old, *ptr_pt_new; */
/*   int times, i; */

/*   Data_Get_Struct(self, IkedaMPFRMap, ptr_self); */
/*   r_mpfr_get_matrix_struct(ptr_arg, arg); */
/*   r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_jac_ret, 2, 2); */
/*   r_mpfr_matrix_temp_alloc_init(ptr_jac_tmp, 2, 2); */
/*   r_mpfr_matrix_temp_alloc_init(ptr_jac_new, 2, 2); */
/*   r_mpfr_matrix_temp_alloc_init(ptr_pt_old, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(ptr_pt_new, 2, 1); */

/*   times = FIX2INT(iterate); */
/*   mpfr_matrix_set(ptr_pt_old, ptr_arg); */
/*   r_mpfr_ikeda_jacobian_matrix(ptr_jac_ret, ptr_arg, ptr_self->cst); */
/*   for (i = 1; i < times; i++) { */
/*     r_mpfr_ikeda_map(ptr_pt_new, ptr_pt_old, ptr_self->cst); */
/*     mpfr_matrix_set(ptr_pt_old, ptr_pt_new); */
/*     r_mpfr_ikeda_jacobian_matrix(ptr_jac_new, ptr_pt_old, ptr_self->cst); */
/*     mpfr_matrix_mul(ptr_jac_tmp, ptr_jac_new, ptr_jac_ret); */
/*     mpfr_matrix_set(ptr_jac_ret, ptr_jac_tmp); */
/*   } */
/*   r_mpfr_matrix_temp_free(ptr_jac_tmp); */
/*   r_mpfr_matrix_temp_free(ptr_jac_new); */
/*   r_mpfr_matrix_temp_free(ptr_pt_new); */
/*   r_mpfr_matrix_temp_free(ptr_pt_old); */
/*   return ret; */
/* } */

/* static VALUE mpfr_ikeda_mapping_fixed_point(VALUE self) { */
/*   IkedaMPFRMap *ptr_self; */
/*   MPFRMatrix *ptr_ret1, *ptr_ret2; */
/*   VALUE ret1, ret2; */

/*   Data_Get_Struct(self, IkedaMPFRMap, ptr_self); */
/*   r_mpfr_matrix_suitable_matrix_init (&ret1, &ptr_ret1, 2, 1); */
/*   r_mpfr_matrix_suitable_matrix_init (&ret2, &ptr_ret2, 2, 1); */

/*   r_mpfr_ikeda_fixed_point(ptr_ret1, ptr_ret2, ptr_self->cst); */
/*   if (mpfr_cmp(ptr_ret1->data, ptr_ret2->data) > 0) { */
/*     return rb_ary_new3(2, ret1, ret2); */
/*   } else { */
/*     return rb_ary_new3(2, ret2, ret1); */
/*   } */
/* } */

static VALUE mpfr_ikeda_mapping_search_sink(VALUE self, VALUE prm_args, VALUE iterate_args, VALUE threshold_args)
{
  IkedaMPFRMap *ptr_self;
  VALUE ret_val = Qnil;
  MPFRMatrix *ptr_pt_start, *ptr_old, *ptr_new;
  MPFR *threshold_in_basin, *threshold_apart_basin, *sub_x, *sub_y, *x_start, *y_start, *prm_end, *step;
  int ind, sgn, init_iteration, max_iteration, i;
  bool find_pt = false;

  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_start, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_old, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_new, 2, 1);

  r_mpfr_get_struct(x_start, RARRAY_PTR(prm_args)[0]);
  r_mpfr_get_struct(y_start, RARRAY_PTR(prm_args)[1]);
  mpfr_set(mpfr_matrix_get_ptr(ptr_pt_start, 0), x_start, GMP_RNDN);
  mpfr_set(mpfr_matrix_get_ptr(ptr_pt_start, 1), y_start, GMP_RNDN);
  r_mpfr_get_struct(prm_end, RARRAY_PTR(prm_args)[2]);
  r_mpfr_get_struct(step, RARRAY_PTR(prm_args)[3]);
  ind = NUM2INT(RARRAY_PTR(prm_args)[4]);
  if (ind < 0 || ind > 1) {
    rb_raise(rb_eArgError, "Invalid index argument.");
  }
  sgn = mpfr_sgn(step);
  if (sgn == 0) {
    rb_raise(rb_eArgError, "Invalid step argument.");
  } else if (sgn < 0) {
    mpfr_neg(step, step, GMP_RNDN);
  }

  init_iteration = NUM2INT(RARRAY_PTR(iterate_args)[0]) * ptr_self->period;
  max_iteration = NUM2INT(RARRAY_PTR(iterate_args)[1]);

  r_mpfr_get_struct(threshold_in_basin, RARRAY_PTR(threshold_args)[0]);
  r_mpfr_get_struct(threshold_apart_basin, RARRAY_PTR(threshold_args)[1]);

  if (mpfr_cmp(mpfr_matrix_get_ptr(ptr_pt_start, ind), prm_end) > 0) {
    mpfr_swap(mpfr_matrix_get_ptr(ptr_pt_start, ind), prm_end);
  }

  r_mpfr_temp_alloc_init(sub_x);
  r_mpfr_temp_alloc_init(sub_y);
  while (mpfr_cmp(mpfr_matrix_get_ptr(ptr_pt_start, ind), prm_end) <= 0) {
    r_mpfr_ikeda_iteration(ptr_old, ptr_pt_start, ptr_self->cst, init_iteration);
    for (i = 0; i < max_iteration; i++) {
      r_mpfr_ikeda_iteration(ptr_new, ptr_old, ptr_self->cst, ptr_self->period);
      mpfr_sub(sub_x, mpfr_matrix_get_ptr(ptr_old, 0), mpfr_matrix_get_ptr(ptr_new, 0), GMP_RNDN);
      mpfr_sub(sub_y, mpfr_matrix_get_ptr(ptr_old, 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
      if (mpfr_sgn(sub_x) < 0) {
	mpfr_neg(sub_x, sub_x, GMP_RNDN);
      }
      if (mpfr_sgn(sub_y) < 0) {
	mpfr_neg(sub_y, sub_y, GMP_RNDN);
      }
      if (mpfr_cmp(sub_x, threshold_apart_basin) > 0 && mpfr_cmp(sub_y, threshold_apart_basin) > 0) {
	break;
      } else if (mpfr_cmp(sub_x, threshold_in_basin) < 0 && mpfr_cmp(sub_x, threshold_apart_basin) < 0) {
	MPFRMatrix *ptr_ret_val;
	r_mpfr_matrix_suitable_matrix_init (&ret_val, &ptr_ret_val, 2, 1);
	mpfr_matrix_set(ptr_ret_val, ptr_new);
	find_pt = true;
	break;
      }
      mpfr_matrix_set(ptr_old, ptr_new);
    }
    if (find_pt) {
      break;
    }
    mpfr_add(mpfr_matrix_get_ptr(ptr_pt_start, ind), mpfr_matrix_get_ptr(ptr_pt_start, ind), step, GMP_RNDN);
  }

  r_mpfr_matrix_temp_free(ptr_pt_start);
  r_mpfr_matrix_temp_free(ptr_old);
  r_mpfr_matrix_temp_free(ptr_new);
  r_mpfr_temp_free(sub_x);
  r_mpfr_temp_free(sub_y);
  return ret_val;
}

static VALUE mpfr_ikeda_mapping_sink_period(VALUE self, VALUE sink, VALUE threshold, VALUE max_iterate)
{
  IkedaMPFRMap *ptr_self;
  MPFRMatrix *ptr_sink, *ptr_pt_old, *ptr_pt_new;
  MPFR *threshold_distance, *tmp_distance;
  int max_iterate_num, i;
  VALUE ret_val;

  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_sink, sink);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_old, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_new, 2, 1);
  r_mpfr_get_struct(threshold_distance, threshold);
  r_mpfr_temp_alloc_init(tmp_distance);
  max_iterate_num = NUM2INT(max_iterate);
  ret_val = Qnil;
  mpfr_matrix_set(ptr_pt_old, ptr_sink);
  for (i = 1; i <= max_iterate_num; i++) {
    r_mpfr_ikeda_map(ptr_pt_new, ptr_pt_old, ptr_self->cst);
    mpfr_matrix_vector_distance(tmp_distance, ptr_sink, ptr_pt_new);
    if (mpfr_cmp(tmp_distance, threshold_distance) < 0) {
      ret_val = INT2NUM(i);
      break;
    }
    mpfr_matrix_set(ptr_pt_old, ptr_pt_new);
  }
  r_mpfr_matrix_temp_free(ptr_pt_old);
  r_mpfr_matrix_temp_free(ptr_pt_new);
  r_mpfr_temp_free(tmp_distance);
  return ret_val;
}

static VALUE mpfr_ikeda_mapping_cumulative_distance(VALUE self, VALUE pt, VALUE iterate) {
  IkedaMPFRMap *ptr_self;
  MPFRMatrix *ptr_pt;
  MPFR *distance;
  VALUE v_distance;
  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  r_mpfr_make_struct_init(v_distance, distance);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_ikeda_cumulative_distance(distance, ptr_pt, ptr_self->cst, NUM2INT(iterate));
  return v_distance;
}

static int mpfr_ikeda_mapping_nearest_point(MPFRMatrix **ptr_sorted_orbit, int num_end, MPFRMatrix *pt) {
  int num_start, i;
  num_start = 0;

  if (mpfr_cmp(mpfr_matrix_get_ptr(pt, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[0], 0)) < 0) {
    return -1;
  } else if (mpfr_cmp(mpfr_matrix_get_ptr(pt, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[num_end], 0)) > 0) {
    return num_end;
  }

  while (num_end - num_start > 1) {
    i = (num_end + num_start) / 2;
    if (mpfr_cmp(mpfr_matrix_get_ptr(pt, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[i], 0)) >= 0) {
      num_start = i;
    } else {
      num_end = i;
    }
  }

  return num_start;
}

static VALUE mpfr_ikeda_mapping_transient_time(VALUE self, VALUE pt, VALUE sorted_orbit, VALUE threshold, VALUE max_iterate) {
  IkedaMPFRMap *ptr_self;
  MPFRMatrix **ptr_sorted_orbit, *ptr_new, *ptr_old, *ptr_pt;
  MPFR *ptr_sub, *ptr_threshold;
  VALUE ret_val;
  int sink_period, iterate, ind, max_ind, i, max_iterate_num;
  sink_period = RARRAY_LEN(sorted_orbit);
  Data_Get_Struct(self, IkedaMPFRMap, ptr_self);
  ptr_sorted_orbit = (MPFRMatrix **) malloc(sizeof(MPFRMatrix *) * sink_period);
  for (i = 0; i < sink_period; i++) {
    r_mpfr_get_matrix_struct(ptr_sorted_orbit[i], RARRAY_PTR(sorted_orbit)[i]);
  }
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_matrix_temp_alloc_init(ptr_new, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_old, 2, 1);
  mpfr_matrix_set(ptr_old, ptr_pt);
  r_mpfr_temp_alloc_init(ptr_sub);
  r_mpfr_get_struct(ptr_threshold, threshold);
  max_ind = sink_period - 1;
  max_iterate_num = NUM2INT(max_iterate);

  iterate = 0;
  while (true) {
    r_mpfr_ikeda_iteration(ptr_new, ptr_old, ptr_self->cst, ptr_self->period);
    iterate += ptr_self->period;
    if (!mpfr_number_p(mpfr_matrix_get_ptr(ptr_new, 0))) {
      ret_val = Qnil;
      break;
    }
    ind = mpfr_ikeda_mapping_nearest_point(ptr_sorted_orbit, max_ind, ptr_new);
    if (ind == -1) {
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[0], 0), mpfr_matrix_get_ptr(ptr_new, 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[0], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
    } else if (ind == max_ind) {
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_new, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[max_ind], 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[max_ind], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
    } else {
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[ind], 0), mpfr_matrix_get_ptr(ptr_new, 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[ind], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_new, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[ind + 1], 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[ind + 1], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
    }

    if (iterate > max_iterate_num) {
      ret_val = INT2NUM(-1);
      break;
    }
    mpfr_matrix_set(ptr_old, ptr_new);
  }

  r_mpfr_matrix_temp_free(ptr_new);
  r_mpfr_matrix_temp_free(ptr_old);
  r_mpfr_temp_free(ptr_sub);
  free(ptr_sorted_orbit);
  return ret_val;
}

/* START: Ikeda mapping with float calculation */

/* #include "func_float_ikeda.h" */

/* /\* Arguments are [x, y], iteration and [a, b]. iteration is optional. *\/ */
/* static VALUE float_ikeda_map_base (int argc, VALUE *argv, VALUE self) { */
/*   double retx, rety; */

/*   int iterate; */
/*   switch(argc) { */
/*   case 2: */
/*     iterate = 1; */
/*     break; */
/*   case 3: */
/*     iterate = NUM2INT(argv[1]); */
/*     break; */
/*   default: */
/*     rb_raise(rb_eArgError, "Number of arguments for Ikeda::Mapping.map is one or two."); */
/*   } */

/*   rfloat_ikeda_iteration(&retx, &rety, NUM2DBL(rb_ary_entry(argv[0], 0)), NUM2DBL(rb_ary_entry(argv[0], 1)), */
/* 			 NUM2DBL(rb_ary_entry(argv[2], 0)), NUM2DBL(rb_ary_entry(argv[2], 1)), iterate); */
/*   return rb_ary_new3(2, rb_float_new(retx), rb_float_new(rety)); */
/* } */

/* END: Ikeda mapping with float calculation */

void Init_ruby_mpfr_ikeda() {

  __mpfr_class__ = rb_define_class("MPFR", rb_cNumeric);
  /* __mpfr_class__ = rb_eval_string("MPFR"); */

  /* START: Basic methods of Ikeda mapping */

  mpfr_ikeda_module = rb_define_module_under(__mpfr_class__, "Ikeda");
  mpfr_ikeda_mapping = rb_define_class_under(mpfr_ikeda_module, "Mapping", rb_cObject);
  rb_define_alloc_func(mpfr_ikeda_mapping, mpfr_ikeda_mapping_alloc);
  rb_define_private_method(mpfr_ikeda_mapping, "initialize", mpfr_ikeda_mapping_initialize, -1);
  rb_define_private_method(mpfr_ikeda_mapping, "initialize_copy", mpfr_ikeda_mapping_initialize_copy, 1);

  rb_define_method(mpfr_ikeda_mapping, "period", mpfr_ikeda_mapping_period, 0);
  rb_define_method(mpfr_ikeda_mapping, "constants", mpfr_ikeda_mapping_constants, 0);
  rb_define_method(mpfr_ikeda_mapping, "map", mpfr_ikeda_mapping_map, -1);
  rb_define_method(mpfr_ikeda_mapping, "iterate_map", mpfr_ikeda_mapping_iterate_map, -1);
  rb_define_method(mpfr_ikeda_mapping, "make_orbit", mpfr_ikeda_mapping_make_orbit, 2);
  rb_define_method(mpfr_ikeda_mapping, "each_pt_on_orbit", mpfr_ikeda_mapping_each_pt_on_orbit, 2);
  /* rb_define_method(mpfr_ikeda_mapping, "jacobian_matrix", mpfr_ikeda_mapping_jacobian_matrix, 1); */
  /* rb_define_method(mpfr_ikeda_mapping, "jacobian_matrix_iterate_map", mpfr_ikeda_mapping_jacobian_matrix_iterate_map, 2); */
  /* rb_define_method(mpfr_ikeda_mapping, "fixed_point", mpfr_ikeda_mapping_fixed_point, 0); */

  /* END: Basic methods of Ikeda mapping */

  rb_define_method(mpfr_ikeda_mapping, "search_sink", mpfr_ikeda_mapping_search_sink, 3);
  rb_define_method(mpfr_ikeda_mapping, "sink_period", mpfr_ikeda_mapping_sink_period, 3);
  rb_define_method(mpfr_ikeda_mapping, "cumulative_distance", mpfr_ikeda_mapping_cumulative_distance, 2);
  rb_define_method(mpfr_ikeda_mapping, "transient_time", mpfr_ikeda_mapping_transient_time, 4);

  mpfr_ikeda_mapping_const_id = rb_intern("@const");


  /* START: Ikeda mapping with float calculation */

  /* float_ikeda = rb_define_module("FloatIkeda"); */
  /* rb_define_singleton_method(float_ikeda, "iterate_map_base", float_ikeda_map_base, -1); */

  /* END: Ikeda mapping with float calculation */  
}
