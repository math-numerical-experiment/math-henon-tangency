#ifndef _FUNC_MPFR_IKEDA_H_
#define _FUNC_MPFR_IKEDA_H_

#include <stdio.h>
#include <stdbool.h>
#include <mpfr.h>
#include "ruby_mpfr_matrix.h"
#include "../../ruby-mpfr-matrix-extra/ext/func_mpfr_matrix_extra.h"

void r_mpfr_ikeda_map(MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst);
void r_mpfr_ikeda_inverse_map(MPFRMatrix * y, MPFRMatrix * x, MPFRMatrix * cst);
void r_mpfr_ikeda_iteration(MPFRMatrix * b, MPFRMatrix * a, MPFRMatrix * c, int iteration);
/* void r_mpfr_ikeda_jacobian_matrix(MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants); */
/* void r_mpfr_ikeda_fixed_point(MPFRMatrix *fp1, MPFRMatrix *fp2, MPFRMatrix *constants); */
void r_mpfr_ikeda_cumulative_distance(MPFR *distance, MPFRMatrix *pt, MPFRMatrix *cst, int period);

#endif /* _FUNC_MPFR_IKEDA_H_ */
