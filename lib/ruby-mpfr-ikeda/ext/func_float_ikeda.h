#ifndef _FUNC_FLOAT_IKEDA_H_
#define _FUNC_FLOAT_IKEDA_H_

#include <math.h>

typedef struct {
  double a;
  double b;
  double k;
  double p;
} FloatIkeda;

void rfloat_ikeda_map(double *retx, double *rety, double x, double y, FloatIkeda *ikeda);
void rfloat_ikeda_inverse_map(double *retx, double *rety, double x, double y, FloatIkeda *ikeda);
void rfloat_ikeda_iteration(double *retx, double *rety, double x, double y, FloatIkeda *ikeda, int iteration);

#endif /* _FUNC_FLOAT_IKEDA_H_ */
