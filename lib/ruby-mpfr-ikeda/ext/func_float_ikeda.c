#include "func_float_ikeda.h"

/*
  Ikeda map
  x_{n+1} = p + B (x_n \cos t - y_n \sin t)
  y_{n+1} = B(x_n \sin t + y_n \cos t)
  t = k - a / (1 + x_n^2 + y_n^2)
*/
void rfloat_ikeda_map(double *retx, double *rety, double x, double y, FloatIkeda *ikeda)
{
  double t, cos_t, sin_t;
  t = ikeda->k - ikeda->a / (1.0 + x * x + y * y);
  cos_t = cos(t);
  sin_t = sin(t);
  *retx = ikeda->p + ikeda->b * (x * cos_t - y * sin_t);
  *rety = ikeda->b * (x * sin_t + y * cos_t);
}

/*
  Inverse map
  x_n = (y_{n+1} \cos t_n + (p - x_{n+1}) \sin t_n) / B
  y_n = (y_{n+1} \sin t_n + (p - x_{n+1}) \cos t_n) / B
  t = k - a / (1 + ((x_{n+1} - p)^2 + y_{n+1}^2) / B^2)
*/
void rfloat_ikeda_inverse_map(double *retx, double *rety, double x, double y, FloatIkeda *ikeda)
{
  double t, cos_t, sin_t, p_x;
  p_x = ikeda->p - x;
  t = ikeda->k - ikeda->a / (1.0 + p_x * p_x + y * y) / (ikeda->b * ikeda->b);
  cos_t = cos(t);
  sin_t = sin(t);
  *retx = (y * cos_t + p_x * sin_t) / ikeda->b;
  *rety = (y * sin_t + p_x * cos_t) / ikeda->b;
}

void rfloat_ikeda_iteration(double *retx, double *rety, double x, double y, FloatIkeda *ikeda, int iteration)
{
  int i;
  if (iteration == 0) {
    *retx = x;
    *rety = y;
  } else {
    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	rfloat_ikeda_map(retx, rety, x, y, ikeda);
	x = *retx;
	y = *rety;
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	rfloat_ikeda_inverse_map(retx, rety, x, y, ikeda);
	x = *retx;
	y = *rety;
      }
    }
  }
}
