require "fileutils"

cmd = ARGV[0] || "import"

files_to_be_imported = %w(
datafile.c
datafile.h
dataobj.c
dataobj.h
dataobj_fi.c
dataobj_fi.h
dataobj_fr.c
dataobj_fr.h
display_func.c
display_func.h
display_func_fr.c
display_func_fr.h
draw_ptdata.c
draw_ptdata.h
ptdata.c
ptdata.h
ptdata_fi.c
ptdata_fi.h
ptdata_fr.c
ptdata_fr.h
screen.c
screen.h
screen_fr.c
screen_fr.h
)

case cmd
when "import"
  dir = File.expand_path(File.join(__dir__, "../../../vendor/math-display-pts/src/plane/"))
  files = files_to_be_imported.map { |file| File.join(dir, file) }
  FileUtils.cp(files, ".")
when "delete"
  files = files_to_be_imported.map { |file| File.join(__dir__, file) }
  files.delete_if { |path| !File.exist?(path) }
  FileUtils.rm(files)
else
  raise "Invalid command: #{cmd}"
end
