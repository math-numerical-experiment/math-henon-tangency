#include "ruby_opengl_mpfr.h"
#include <unistd.h>

#define INITIAL_WINDOW_TITLE "Plot Points"
#define INITIAL_WINDOW_POSITION_X 0
#define INITIAL_WINDOW_POSITION_Y 0
#define INITIAL_WINDOW_WIDTH 800
#define INITIAL_WINDOW_HEIGHT 800

#define GNUPLOT_PREFIX "img"
#define GNUPLOT_MAX_NAME 100

#define RB_SLEEP_TIME_VAL "@sleep"

/* #define NO_ANTIALIAS */

static VALUE rb_mpfrgl_instance, rb_mpfrgl_instance_objects;
static ID __data_ary__, __plot_style__, __sleep__;

static MPFRScreen2D screen[1];
static Color col[1] = { { 0, NOT_USE_ALPHA }};

static DisplayState disp[1] = {{ NULL, 0, false, false, 0, 4 }};

static VALUE rb_mpfrgl_set_fr_ptdata(VALUE self, VALUE obj, VALUE ary, VALUE change_start, VALUE style, VALUE split) {
  DataObject *dtobj;
  Data_Get_Struct(obj, DataObject, dtobj);
  rb_mpfr_screen_set_dbl_from_fr(dtobj, ary, NUM2INT(change_start), screen);
  dataobj_set_default_style(dtobj, NUM2INT(style));
  if (RARRAY_LEN(split) > 0) {
    int i, size = RARRAY_LEN(split);
    for (i = 0; i < size; i++) {
      dataobj_set_split(dtobj, NUM2INT(*(RARRAY_PTR(split) + i)) * 2);
    }
  }
  return Qnil;
}

static VALUE rb_mpfrgl_set_fi_dbl_ptdata(VALUE self, VALUE obj, VALUE ary, VALUE change_start, VALUE style, VALUE split) {
  DataObject *dtobj;
  Data_Get_Struct(obj, DataObject, dtobj);
  rb_mpfr_screen_set_dbl_from_fi(dtobj, ary, NUM2INT(change_start), screen);
  dataobj_set_default_style(dtobj, NUM2INT(style));
  if (RARRAY_LEN(split) > 0) {
    int i, size = RARRAY_LEN(split);
    for (i = 0; i < size; i++) {
      dataobj_set_split(dtobj, NUM2INT(*(RARRAY_PTR(split) + i)) * 2);
    }
  }
  return Qnil;
}

static VALUE rb_mpfrgl_set_fi_quads_ptdata(VALUE self, VALUE obj, VALUE ary, VALUE change_start, VALUE style) {
  DataObject *dtobj;
  Data_Get_Struct(obj, DataObject, dtobj);
  rb_mpfr_screen_set_quads_from_fi(dtobj, ary, NUM2INT(change_start), screen);
  dataobj_set_default_style(dtobj, NUM2INT(style));
  return Qnil;
}

static void rb_mpfrgl_draw(VALUE obj) {
  DataObject *dtobj;
  Data_Get_Struct(obj, DataObject, dtobj);
  switch(dtobj->default_style) {
  case STY_DOTS:
    dataobj_draw_dots(dtobj);
    break;
  case STY_LINES:
    dataobj_draw_lines(dtobj);
    break;
  case STY_POINTS:
    dataobj_draw_points_cross(dtobj, screen->dbl_scrn->cross_size / screen->dbl_scrn->zoom);
    break;
  case STY_LINES_POINTS:
    dataobj_draw_lines(dtobj);
    dataobj_draw_points_cross(dtobj, screen->dbl_scrn->cross_size / screen->dbl_scrn->zoom);
    break;
  case STY_QUADS:
    dataobj_draw_quads(dtobj);
    break;
  case STY_QUADS_EDGE:
    dataobj_draw_quads_edge(dtobj);
    break;
  }
}

static void change_scale_of_objects() {
  int i, len_objs;
  VALUE *objs, *ary;
  DataObject *dtobj;
  
  len_objs = RARRAY_LEN(rb_mpfrgl_instance_objects);
  objs = RARRAY_PTR(rb_mpfrgl_instance_objects);

  for (i = 0; i < len_objs; i++) {
    ary = RARRAY_PTR(*(objs + i));
    switch(NUM2INT(*(ary + 1))) {
    case MPFR_PLOT_TYPE_FR:
      Data_Get_Struct(*(ary + 4), DataObject, dtobj);
      rb_mpfr_screen_set_dbl_from_fr(dtobj, rb_funcall(*(ary + 2), __data_ary__, 0), 0, screen);
      break;
    case MPFR_PLOT_TYPE_FI:
      switch(NUM2INT(rb_funcall(*(ary + 2), __plot_style__, 0))) {
      case STY_DOTS:
      case STY_LINES:
      case STY_POINTS:
      case STY_LINES_POINTS:
	Data_Get_Struct(*(ary + 4), DataObject, dtobj);
	rb_mpfr_screen_set_dbl_from_fi(dtobj, rb_funcall(*(ary + 2), __data_ary__, 0), 0, screen);
	Data_Get_Struct(*(ary + 5), DataObject, dtobj);
	dataobj_free(dtobj);
	break;
      case STY_QUADS:
      case STY_QUADS_EDGE:
	Data_Get_Struct(*(ary + 4), DataObject, dtobj);
	dataobj_free(dtobj);
	Data_Get_Struct(*(ary + 5), DataObject, dtobj);
	rb_mpfr_screen_set_quads_from_fi(dtobj, rb_funcall(*(ary + 2), __data_ary__, 0), 0, screen);
	break;
      }
      break;
    }
  }
  
}

static void rb_mpfrgl_gnuplot_objects(MPFRScreen2D *screen) {
  if (NUM2INT(rb_funcall(rb_mpfrgl_instance, rb_intern("count_disp"), 0)) > 0) {
    char gp_file[GNUPLOT_MAX_NAME];
    time_t now;
    struct tm *t_st_now;
    FILE *gp;
    bool first = true;
    int i, j, len_objs, len_ary;
    VALUE *objs, *ary;
    DataObject *dtobj;

    time(&now);
    t_st_now = localtime(&now);
    sprintf(gp_file, "%s%d%02d%02d%02d%02d%02d", GNUPLOT_PREFIX,
	    t_st_now->tm_year+1900, t_st_now->tm_mon+1, t_st_now->tm_mday, t_st_now->tm_hour, t_st_now->tm_min, t_st_now->tm_sec);
    gp = popen("gnuplot","w");
    printf("Making %s.eps\n", gp_file);
    fprintf(gp, "set term postscript eps\nset output '%s.eps'\n", gp_file);
    screen2d_gnuplot_set_range(screen->dbl_scrn, gp);
    mpfr_screen2d_gnuplot_set_divisions(screen, gp);

    fprintf(gp, "plot ");
  
    len_objs = RARRAY_LEN(rb_mpfrgl_instance_objects);
    objs = RARRAY_PTR(rb_mpfrgl_instance_objects);
  
    for (i = 0; i < len_objs; i++) {
      ary = RARRAY_PTR(*(objs + i));
      len_ary = RARRAY_LEN(*(objs + i));
      if (RTEST(*(ary + 3))) {
	for (j = 4; j < len_ary; j++) {
	  Data_Get_Struct(*(ary + j), DataObject, dtobj);
	  if (dtobj->data_size > 0) {
	    if (!first) {
	      fprintf(gp, ", ");
	    } else {
	      first = false;
	    }
	    
	    draw_ptdata_puts_gnuplot_style(gp, dtobj->default_style);
	  }
	}
      }
    }
    fprintf(gp, "\n");

    for (i = 0; i < len_objs; i++) {
      ary = RARRAY_PTR(*(objs + i));
      len_ary = RARRAY_LEN(*(objs + i));
      if (RTEST(*(ary + 3))) {
	for (j = 4; j < len_ary; j++) {
	  Data_Get_Struct(*(ary + j), DataObject, dtobj);
	  if (dtobj->data_size > 0) {
	    dataobj_output_to_gnuplot_default_style(dtobj, gp);
	  }
	}
      }
    }
    
    pclose(gp);
    printf("Finish making %s.eps\n", gp_file);
  } else {
    printf("There is no object to plot.\n");
  }
}


static void disp_show_grid(DisplayState *disp) {
  if (disp->grid) {
    glColor3f(0.3, 0.3, 0.3);
    screen2d_show_grid(screen->dbl_scrn);
  }
}

static void rb_mpfrgl_show_rb_state(MPFRScreen2D *screen) {
  VALUE sleep_time;
  char **lines;
  int num = 1;
  screen2d_malloc_lines(&lines, num, SCREEN_MAX_STRING_SIZE_ONE_LINE);

  sleep_time = rb_iv_get(rb_mpfrgl_instance, RB_SLEEP_TIME_VAL);
  if (RTEST(sleep_time)) {
    sprintf(*lines, "sleep time: % .5lf", NUM2DBL(sleep_time));
  } else {
    sprintf(*lines, "sleep time: nil");
  }

  screen2d_show_lines_from_top(screen->dbl_scrn, lines, num);
  screen2d_free_lines(&lines, num);
}

static void disp_show_state(DisplayState *disp) {
  glColor4f(0.85, 0.85, 0.85, 0.9);

  switch(disp->state) {
  case 1:
    mpfr_screen2d_show_current_state(screen);
    break;
  case 2:
    mpfr_screen2d_show_basic_info(screen);
    break;
  case 3:
    rb_mpfrgl_show_rb_state(screen);
    break;
  }
  if (disp->scale_bar) {
    mpfr_screen2d_show_scale_bar(screen);
  }
}

static void display_func(void) {
  int i, j, len_objs, len_ary;
  VALUE *objs, *ary;

  glMatrixMode(GL_MODELVIEW);
  
  glClear(GL_COLOR_BUFFER_BIT);

  disp_show_grid(disp);

  color_count_reset(col);

  len_objs = RARRAY_LEN(rb_mpfrgl_instance_objects);
  objs = RARRAY_PTR(rb_mpfrgl_instance_objects);
  
  for (i = 0; i < len_objs; i++) {
    color_change(col);
    ary = RARRAY_PTR(*(objs + i));
    len_ary = RARRAY_LEN(*(objs + i));
    if (RTEST(*(ary + 3))) {
      for (j = 4; j < len_ary; j++) {
	rb_mpfrgl_draw(*(ary + j));
      }
    }
  }
  
  disp_show_state(disp);
  
  glFlush();
}

static void reshape_func(int w, int h) {
  screen2d_change_win_size(screen->dbl_scrn, w, h);
  glViewport(0, 0, w, h);
}

static void mouse_func(int button, int state, int x, int y) {
  if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
    screen2d_move_to_pixel(screen->dbl_scrn, x, y);
    glutPostRedisplay();
  }
}

static void keyboard_func(unsigned char key, int x, int y) {
  switch(key) {
  case 'Q':
    glutLeaveMainLoop();
    break;
  case 'w':
    rb_funcall(rb_mpfrgl_instance, rb_intern("cycle_sleep_time"), 0);
    break;
  case 'r':
    break;
  case 'h':
    mpfr_screen2d_left(screen);
    break;
  case 'j':
    mpfr_screen2d_down(screen);
    break;
  case 'k':
    mpfr_screen2d_up(screen);
    break;
  case 'l':
    mpfr_screen2d_right(screen);
    break;
  case 'i':
    mpfr_screen2d_zoom_in(screen);
    break;
  case 'o':
    mpfr_screen2d_zoom_out(screen);
    break;
  case 'I':
    if (!mpfr_screen2d_zoom_in(screen)) {
      mpfr_screen2d_change_base_scale(screen, screen->dbl_scrn->default_zoom);
      change_scale_of_objects();
    }
    break;
  case 'O':
    if (!mpfr_screen2d_zoom_out(screen)) {
      mpfr_screen2d_change_base_scale(screen, 1.0 / screen->dbl_scrn->default_zoom);
      change_scale_of_objects();
    }
    break;
  case 'a':
    mpfr_screen2d_zoom_to(screen, SCREEN_INITIAL_ZOOM);
    break;
  case 's':
    mpfr_screen2d_move_to(screen, SCREEN_INITIAL_CRD_X, SCREEN_INITIAL_CRD_Y);
    break;
  case 'd':
    disp_toggle_scale_bar(disp);
    break;
  case 'f':
    disp_cycle_state(disp);
    break;
  case 'g':
    disp_toggle_grid(disp);
    break;
  case 'p':
    rb_mpfrgl_gnuplot_objects(screen);
    break;
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    rb_funcall(rb_mpfrgl_instance, rb_intern("toggle_display_by_number"), 1, INT2NUM(key - 48));
    break;
  default:
    return;
  }
  glutPostRedisplay();
}

static void idle_func() {
  VALUE sleep_time;
  sleep_time = rb_iv_get(rb_mpfrgl_instance, RB_SLEEP_TIME_VAL);
  if (RTEST(sleep_time)) {
    rb_funcall(rb_mKernel, __sleep__, 1, sleep_time);
    glutPostRedisplay();
  }
}

static VALUE rb_mpfrgl_start(VALUE self) {
  int glut_argc = 0;
  char **glut_argv = NULL;
  glutInit(&glut_argc, glut_argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(screen->dbl_scrn->win_size[0], screen->dbl_scrn->win_size[1]);
  glutInitWindowPosition(INITIAL_WINDOW_POSITION_X, INITIAL_WINDOW_POSITION_Y);
  glutCreateWindow(INITIAL_WINDOW_TITLE);

  screen2d_initialize_font(screen->dbl_scrn);
  
  glutReshapeFunc(reshape_func);
  glutMouseFunc(mouse_func);
  glutKeyboardFunc(keyboard_func);
  glutDisplayFunc(display_func);
  glutIdleFunc(idle_func);

  glEnableClientState(GL_VERTEX_ARRAY);

#ifndef NO_ANTIALIAS
  glEnable (GL_POLYGON_SMOOTH);
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
#endif /* NO_ANTIALIAS */

  glutMainLoop();
  
  return Qnil;
}

static VALUE rb_mpfrgl_init(VALUE self) {
  mpfr_screen2d_init(screen, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
  return Qnil;
}

static VALUE rb_mpfrgl_set_prec(VALUE self, VALUE prec) {
  mpfr_screen2d_set_prec(screen, NUM2INT(prec));
  return Qnil;
}

/* This method must be called after MPFRGL.start_thread. */
static VALUE rb_mpfrgl_set_screen(VALUE self, VALUE center, VALUE scale) {
  MPFRMatrix *cen;
  MPFR *scl;
  r_mpfr_get_matrix_struct(cen, center);
  r_mpfr_get_struct(scl, scale);

  mpfr_screen2d_change_base(screen, cen->data, scl);

  screen2d_move_to(screen->dbl_scrn, 0, 0);
  screen2d_zoom_to(screen->dbl_scrn, 1.0);

  change_scale_of_objects();
  return Qnil;
}

void Init_ruby_opengl_mpfr() {
  rb_mpfrgl = rb_define_class("MPFRGL", rb_cObject);

  rb_define_singleton_method(rb_mpfrgl, "init", rb_mpfrgl_init, 0);
  rb_define_singleton_method(rb_mpfrgl, "start", rb_mpfrgl_start, 0);
  rb_define_singleton_method(rb_mpfrgl, "set_prec", rb_mpfrgl_set_prec, 1);
  rb_define_singleton_method(rb_mpfrgl, "set_screen", rb_mpfrgl_set_screen, 2);

  rb_define_private_method(rb_mpfrgl, "set_fr_ptdata", rb_mpfrgl_set_fr_ptdata, 5);
  rb_define_private_method(rb_mpfrgl, "set_fi_dbl_ptdata", rb_mpfrgl_set_fi_dbl_ptdata, 5);
  rb_define_private_method(rb_mpfrgl, "set_fi_quads_ptdata", rb_mpfrgl_set_fi_quads_ptdata, 4);
  
  rb_mpfrgl_instance = rb_funcall(rb_mpfrgl, rb_intern("instance"), 0);
  rb_mpfrgl_instance_objects = rb_iv_get(rb_mpfrgl_instance, "@objects");

  __data_ary__ = rb_intern("data_ary");
  __plot_style__ = rb_intern("plot_style");
  __sleep__ = rb_intern("sleep");

}
