#ifndef _RUBY_OPENGL_MPFR_H_
#define _RUBY_OPENGL_MPFR_H_

#include <ruby.h>
#include <stdio.h>
#include <ruby.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>
#include "ruby_mpfr.h"
#include "ruby_mpc.h"
#include "ruby_mpfr_matrix.h"
#include "ruby_mpfi.h"
#include "ruby_mpfi_complex.h"
#include "ruby_mpfi_matrix.h"

#include "../../ruby-mpfr-plot/ext/ruby_mpfr_plot.h"
#include "display_func_fr.h"

VALUE rb_mpfrgl;

#endif /* _RUBY_OPENGL_MPFR_H_ */
