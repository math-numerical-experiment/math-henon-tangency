require "singleton"

require "ruby_mpfr_plot"

class MPFRGL
  include Singleton

  SLEEP_CANDIDATES = [nil, 0.05, 0.1, 0.2, 0.5, 1.0, 1.5, 2.0]
  
  # \@objects has elements [obj_id, MPFRPlot::TYPE_FR or MPFRPlot::TYPE_FI, obj, true or nil, MPFRPlot::DataObject, ...].
  def initialize
    @objects = []
    @sleep_num = 0
    @sleep = SLEEP_CANDIDATES[@sleep_num]
  end

  def cycle_sleep_time
    @sleep_num += 1
    @sleep_num = 0 if @sleep_num >= SLEEP_CANDIDATES.size
    @sleep = SLEEP_CANDIDATES[@sleep_num]
  end

  def set_sleep_time(num)
    @sleep_num = num % (SLEEP_CANDIDATES.size)
    @sleep = SLEEP_CANDIDATES[@sleep_num]
  end
  
  def count_disp
    count = 0
    @objects.each do |ary|
      if ary[3]
        (4...(ary.size)).each do |data|
          count += 1 if data.size > 0
        end
      end
    end
    count
  end
  
  def toggle_display(obj_id)
    if ary = @objects.assoc(obj_id)
      ary[3] = (ary[3] ? nil : true)
    end
  end

  def toggle_display_by_number(num)
    if ary = @objects[num]
      ary[3] = (ary[3] ? nil : true)
    end
  end

  # The objects added for displaying are saved in @objects.
  # The key of an object is its object id.
  def add_object(*obj)
    obj.each do |o|
      if MPFR::DataFile === o || MPFR::Obj2D === o
        obj2d = o
      else
        obj2d = o.obj2d
      end

      obj_id = obj2d.__id__
      unless @objects.assoc(obj_id)
        case obj2d.data_type
        when MPFRPlot::TYPE_FR
          @objects << [obj_id, MPFRPlot::TYPE_FR, obj2d, true, MPFRPlot::DataObject.new]
        when MPFRPlot::TYPE_FI
          @objects << [obj_id, MPFRPlot::TYPE_FI, obj2d, true, MPFRPlot::DataObject.new, MPFRPlot::DataObject.new]
        else
          raise ArgumentError, "Invalid arguments."
        end
        obj2d.add_observer(self)
        obj2d.changed
        obj2d.notify_display_observer
      end
    end
  end

  def delete_object(obj_id)
    @objects.delete_if { |ary| ary[0] == obj_id }
  end

  def update(obj_id, change_start)
    if ary = @objects.assoc(obj_id)
      case ary[1]
      when MPFRPlot::TYPE_FR
        set_fr_ptdata(ary[4], ary[2].data_ary, change_start, ary[2].plot_style, ary[2].split)
      when MPFRPlot::TYPE_FI
        style = ary[2].plot_style
        case style
        when MPFRPlot::STY_DOTS, MPFRPlot::STY_LINES, MPFRPlot::STY_POINTS, MPFRPlot::STY_LINES_POINTS
          set_fi_dbl_ptdata(ary[4], ary[2].data_ary, change_start, style, ary[2].split)
        when MPFRPlot::STY_QUADS, MPFRPlot::STY_QUADS_EDGE
          set_fi_quads_ptdata(ary[5], ary[2].data_ary, change_start, style)
        end
      end
    end
  end

  def self.start_thread
    Thread.start do
      start
    end
  end

  ["add_object", "delete_object", "set_sleep_time"].each do |mth|
    module_eval %Q{
      def self.#{mth}(*args)
          self.instance.#{mth}(*args)
      end
    }
  end

end

# In ruby_opengl_mpfr.so we call MPFRGL.instance.
# So we must require ruby_opengl_mpfr.so after including Singleton in MPFRGL.
require "ruby_opengl_mpfr.so"

