require_relative 'spec_helper'

describe MPFR::Henon::Mapping do
  context "when mapping points" do
    before(:all) do
      MPFR.set_default_prec(128)
      MPFR.set_error_of_test(MPFR('1e-12'))
    end

    it "should return parameters" do
      prms = ['1.4', '-0.3']
      henon = MPFR::Henon::Mapping.new(prms)
      consts =  henon.constants
      consts.should be_an_instance_of Array
      consts.size.should == 2
      consts[0].check_error(1.4).should be_true
      consts[1].check_error(-0.3).should be_true
    end

    it "should return default period" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      henon.period.should == 1
    end

    it "should return period that we specify at initialization" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'], 10)
      henon.period.should == 10
    end

    it "should return duplicate" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'], 5)
      henon2 = henon.dup
      henon2.object_id.should_not == henon.object_id
      henon2.constants.should == henon.constants
      henon2.period.should == henon.period
    end

    it "should return coordinates that we map points to" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      fhenon = FloatHenon::Mapping.new([1.4, -0.3])
      periods = [1, -1, 5, -3]
      periods.each do |i|
        start_pt = [1, 0]
        fpt = fhenon.map(start_pt, i)
        pt = henon.map(MPFR::ColumnVector(start_pt), i)
        pt.check_error(fpt).should be_true
      end
    end

    it "should return same coordinates as mapping by double precision" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      dhenon = MPFR::Henon::DoubleMapping.new(['1.4', '-0.3'])
      periods = [1, 5]
      periods.each do |i|
        start_pt = MPFR::ColumnVector([1, 0])
        100.times do |j|
          fpt = dhenon.map(start_pt, i)
          pt = henon.map(start_pt, i)
          pt.check_error(fpt).should be_true
          start_pt = pt
        end
      end
    end

    it "should return same coordinates as inverse mapping by double precision" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'], 1)
      dhenon = MPFR::Henon::DoubleMapping.new(['1.4', '-0.3'], 1)
      periods = [-1, -2]
      periods.each do |i|
        start_pt = MPFR::ColumnVector([1, 0])
        fpt = dhenon.map(start_pt, i)
        pt = henon.map(start_pt, i)
        pt.check_error(fpt).should be_true

        fpt = dhenon.iterate_map(start_pt, i)
        pt = henon.iterate_map(start_pt, i)
        pt.check_error(fpt).should be_true
        start_pt = pt
      end
    end

    it "should return coordinatese that we map iteratively poins to" do
      period = 3
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'], period)
      iteration = [1, -1, 5, -3]
      iteration.each do |i|
        start_pt = [1, 0]
        pt1 = henon.map(MPFR::ColumnVector(start_pt), i * 3)
        pt2 = henon.iterate_map(MPFR::ColumnVector(start_pt), i)
        pt1.check_error(pt2).should be_true
      end
    end

    it "should return array of points on an orbit" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      start_pt = MPFR::ColumnVector(['0', '0'])
      point_num = 100
      orbit = henon.make_orbit(start_pt, point_num)
      orbit.should be_an_instance_of Array
      orbit.size.should == point_num
      pt = start_pt
      point_num.times do |i|
        pt.should == orbit[i]
        pt = henon.map(pt)
      end
    end

    it "should yield each points on an orbit" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      start_pt = MPFR::ColumnVector(['0', '0'])
      point_num = 100
      pt2 = start_pt
      henon.each_pt_on_orbit(start_pt, point_num) do |pt|
        pt.should == pt2
        pt2 = henon.map(pt2)
      end
    end

    it "should return cumulative distance" do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      start_pt = MPFR::ColumnVector(['0', '0'])
      point_num = 10
      cdistance = henon.cumulative_distance(start_pt, point_num)
      distance = 0
      henon.each_pt_on_orbit(start_pt, point_num) do |pt|
        distance += pt.distance_from(henon.map(pt, point_num))
      end
      cdistance.should == distance
    end

    it "should marshalize." do
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'], 4)
      d = Marshal.dump(henon)
      henon2 = Marshal.load(d)
      henon.constants.should == henon2.constants
      henon.period.should == henon2.period
    end
  end

  context "when getting Jacobian matrix" do
    it "should return a matrix" do
      pt = MPFR::ColumnVector([1.2, 0.3])
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      mat1 = henon.jacobian_matrix(pt)
      mat2 = henon.jacobian_matrix_iterate_map(pt, 1)
      mat1.should be_an_instance_of MPFR::SquareMatrix
      mat1.should == mat2
    end

    it "should return Jacobian matrix of iterated map" do
      pt = MPFR::ColumnVector([1.2, 0.3])
      henon = MPFR::Henon::Mapping.new(['1.4', '-0.3'])
      henon_period3 = MPFR::Henon::Mapping.new(['1.4', '-0.3'], 3)
      mat1 = henon.jacobian_matrix_iterate_map(pt, 3)
      mat2 = henon_period3.jacobian_matrix_iterate_map(pt)
      mat1.should == mat2
    end
  end
end
