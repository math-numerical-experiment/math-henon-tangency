require_relative 'spec_helper'

MPFR.set_default_prec(MFD_CALC_PREC)

[['1.4', '-0.3'], ['2.2', '-0.3'], ['2.0', '0.1'], ['2.0', '0.5'], ['2.2', '0.1']].each do |cst|
  [:stable, :unstable].each do |type|
    henon = MPFR::Henon::Mapping.new(cst)
    henon.fixed_point.each_with_index do |fixed_point, fp_num|
      vals, vecs = henon.jacobian_matrix(fixed_point).eigenvector
      if vals[0].abs < 1 && vals[1].abs > 1
        sval, uval = vals
        svec, uvec = vecs
      elsif vals[0].abs > 1 && vals[1].abs < 1
        uval, sval = vals
        uvec, svec = vecs
      else
        raise "Not saddle fixed point: #{cst.inspect}, #{type}"
      end

      if type == :stable
        flip = (sval < 0)
        basis = [svec, uvec]
      else
        flip = (uval < 0)
        basis = [uvec, svec]
      end
      error = MPFR.new(1) / MPFR::Math.exp2(MFD_CALC_PREC) * MPFR.new(100)

      describe MPFR::Henon::MfdCalculator, "when calculating points on invariant manifold" do
        before(:all) do
          MPFR.set_default_prec(MFD_CALC_PREC)
          puts "[test 1] constants: #{cst.inspect}, fixed point: #{fp_num}, type: #{type}"
          @calc = MPFR::Henon::MfdCalculator.new(cst, 1, fixed_point, basis, type, flip, error)
        end

        it "should not get error" do 
          num = 50
          res = []
          ratio = []
          lambda do
            (0..num).each { |i| res << @calc.line_crossing_mfd(MPFR.new(i.to_f / num)) }
          end.should_not raise_error
          res.map! { |a| a[0].midpoint(a[1]) }
          (1...(res.size)).each do |i|
            pt = res[i-1].midpoint(res[i])
            res1 = @calc.line_crossing_mfd2(pt)
            res2, new_ratio = @calc.get_mfd_point(pt)
            res3, new_ratio2 = @calc.get_pt_between_two_pts(res[i-1], res[i])
            res1[0].midpoint(res1[1]).should == res2
            res2.should == res3
            new_ratio.should == new_ratio2
            ratio << new_ratio
          end
          last_ratio = MPFR.new(0)
          ratio[0].should > 0
          ratio[0].should < 0.05
          ratio[-1].should > 0.95
          ratio[-1].should < 1.0
          (1...(ratio.size)).each { |i| ratio[i].should > ratio[i-1] }
        end
      end

      describe MPFR::Henon::MfdCalculator, "when calculating points on invariant manifold" do
        before(:all) do
          MPFR.set_default_prec(MFD_CALC_PREC)
          puts "[test 2] constants: #{cst.inspect}, fixed point: #{fp_num}, type: #{type}"
          @calc = MPFR::Henon::MfdCalculator.new(cst, 1, fixed_point, basis, type, flip, error)
        end

        it "should not get error" do 
          num = 100
          (0..num).each do |i|
            ratio = MPFR.new(i.to_f / num)
            res = @calc.line_crossing_mfd(ratio)
            pt = res[0].midpoint(res[1])
            res2 = @calc.get_mfd_point_from_ratio(ratio)
            pt.should == res2[0]
            @calc.get_ratio(pt).should == res2[1]
          end
        end
      end
    end
  end
end
