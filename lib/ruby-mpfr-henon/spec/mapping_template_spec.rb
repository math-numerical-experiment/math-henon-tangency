require_relative 'spec_helper.rb'

class MappingTemplate
  def valid_inverse_mapping?(pt, err = nil)
    pt2 = map(map(pt, 1), -1)
    pt3 = map(map(pt, -1), 1)
    pt2.check_error(pt, err) && pt3.check_error(pt, err)
  end
end

class TestMapping < MappingTemplate
  def map_one_time(pt)
    MPFR::ColumnVector([pt[0] + 1, pt[1] + 2])
  end

  def inverse_map_one_time(pt)
    MPFR::ColumnVector([pt[0] - 1, pt[1] - 2])
  end
end

class InvalidTestMapping < MappingTemplate
  def map_one_time(pt)
    MPFR::ColumnVector([pt[0] + 1, pt[1] + 1])
  end

  def inverse_map_one_time(pt)
    MPFR::ColumnVector([pt[0] - 1, pt[1] - 2])
  end
end

describe MappingTemplate do
  it "should return period of mapping" do
    num = 3
    test_mapping = TestMapping.new(nil, num)
    test_mapping.period.should == num
  end

  it "should return point by mapping" do
    pt = MPFR::ColumnVector([3, 3])
    test_mapping = TestMapping.new(nil, 2)
    pt2 = test_mapping.map(pt)
    pt2[0].should == (pt[0] + 1)
    pt2[1].should == (pt[0] + 2)
  end

  it "should return point by inverse mapping" do
    pt = MPFR::ColumnVector([3, 3])
    test_mapping = TestMapping.new(nil, 2)
    pt2 = test_mapping.map(pt, -1)
    pt2[0].should == (pt[0] - 1)
    pt2[1].should == (pt[0] - 2)
  end

  it "should return point by iteration (map)" do
    pt = MPFR::ColumnVector([3, 3])
    test_mapping = TestMapping.new(nil, 2)
    pt2 = test_mapping.map(pt, 10)
    pt2[0].should == (pt[0] + 10)
    pt2[1].should == (pt[0] + 20)
  end

  it "should return point by inverse iteration (map)" do
    pt = MPFR::ColumnVector([3, 3])
    test_mapping = TestMapping.new(nil, 2)
    pt2 = test_mapping.map(pt, -10)
    pt2[0].should == (pt[0] - 10)
    pt2[1].should == (pt[0] - 20)
  end

  it "should return point by iteration (iterate_map)" do
    pt = MPFR::ColumnVector([3, 3])
    test_mapping = TestMapping.new(nil, 2)
    pt2 = test_mapping.iterate_map(pt, 10)
    pt2[0].should == (pt[0] + 20)
    pt2[1].should == (pt[0] + 40)
  end

  it "should return point by inverse iteration (iterate_map)" do
    pt = MPFR::ColumnVector([3, 3])
    test_mapping = TestMapping.new(nil, 2)
    pt2 = test_mapping.iterate_map(pt, -10)
    pt2[0].should == (pt[0] - 20)
    pt2[1].should == (pt[0] - 40)
  end

  it "should return true for valid inverse map" do
    pt = MPFR::ColumnVector([3, 3])
    TestMapping.new(nil, 2).valid_inverse_mapping?(pt).should be_true
  end

  it "should return false for invalid inverse map" do
    pt = MPFR::ColumnVector([3, 3])
    InvalidTestMapping.new(nil, 2).valid_inverse_mapping?(pt).should be_false
  end
end
