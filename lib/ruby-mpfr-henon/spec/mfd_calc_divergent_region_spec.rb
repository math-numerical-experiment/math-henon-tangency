require_relative 'spec_helper'

describe "when calculating points on invariant manifold" do
  before(:all) do
    MPFR.set_default_prec(256)
    cst = ['1.4', '-0.3']
    @cst = cst.map { |a| MPFR.new(a) }
    @henon = MPFR::Henon::Mapping.new(cst)
    @pt = @henon.fixed_point[0]
    vals, vecs = @henon.jacobian_matrix(@pt).eigenvector
    svec, uvec = vecs
    @basis = [svec, uvec]
    error = MPFR.new(1) / MPFR::Math.exp2(MPFR.get_default_prec) * MPFR.new(100)

    @type = :stable
    @flip = nil

    @calc = MPFR::Henon::MfdCalculator.new(cst, 1, @pt, @basis, @type, @flip, error)
  end

  it "should return more than 10" do
    num = 50
    (0..num).each do |i|
      ratio = MPFR.new(i.to_f / num)
      res = @calc.line_crossing_mfd(ratio)
      pt = res[0].midpoint(res[1])
      @calc.in_divergent_region?(pt).should be_nil
      iteration, mapped = @calc.iteration_in_non_divergent_region(pt)
      iteration.should > 10
      @calc.in_divergent_region?(mapped).should be_nil
      @calc.in_divergent_region?(@henon.map(mapped, -1)).should be_true
    end
  end

end
