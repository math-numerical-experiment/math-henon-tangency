require_relative 'spec_helper'

MPFR.set_default_prec(MFD_CALC_PREC)

class MPFR::MfdCalculatorTest
  INITIAL_VECTOR_SIZE = MPFR.new('1e-10')
  RATIO_NORMAL_VECTOR_SIZE = MPFR.new('100')
  MAX_ITERATION = 500
  MAX_INITIAL_INTERVAL_TRIAL = 300
  MAX_INITIAL_VECTOR_SIZE_TRIAL = 10
  CRITERION_LINEAR_ITERATION = 5

  attr_reader :period, :pt, :basis, :criterion, :iteration_for_calc, :base_two_points, :base_vector, :normal_vec, :init_iteration

  # cst is array of string meaning constant.
  # period is positive integer.
  # pt is fixed point or periodic point.
  # basis is array of two vectors and first element corresponds to direction for calculation of manifold.
  # type is :stable or :unstable.
  # flip is true if the corresponding eigenvalue is negative, nil otherwise.
  def initialize(cst, period, pt, basis, type, flip, error)
    @henon = MPFR::Henon::Mapping.new(cst)
    @period = period
    @type = type
    @iteration_for_calc = 2 * @period * (@type == :stable ? 1 : -1)
    @pt = pt
    @basis = basis
    @flip = flip
    @bisection_error = error
    @max_bisection = MFD_CALC_PREC * 2

    initialize_some_variables
  end

  def initialize_some_variables
    init_vector_size, @init_iteration = initial_vector_size_and_iteration
    @criterion = [init_vector_size, -init_vector_size, init_vector_size * 2, -init_vector_size * 2]
    @normal_vec = MPFR::Vector2D.normal_vector(@basis[0]).mul_scalar(init_vector_size / RATIO_NORMAL_VECTOR_SIZE)
    @base_two_points = base_two_intervals(init_vector_size).map { |a| a[0].midpoint(a[1]) }
    @base_vector = @base_two_points[1] - @base_two_points[0]
  end
  private :initialize_some_variables
  
  # Return direction to which point is mapped (:pos, :neg or nil), and next_iteration.
  def direction_by_mapping(pt_r, initial_iteration)
    ret = nil
    pt = @henon.map(pt_r, initial_iteration * @iteration_for_calc)
    i = 0
    (0...MAX_ITERATION).each do
      coef = MPFR::Vector2D.project(pt - @pt, @basis[0], @basis[1])
      if coef[1] > @criterion[0]
        ret = :pos
        break
      elsif coef[1] < @criterion[1]
        ret = :neg
        break
      end
      pt = @henon.map(pt, @iteration_for_calc)
      i += 1
    end
    [ret, initial_iteration + i]
  end

  def rigorous_direction_by_mapping(pt_r)
    last_coef = MPFR::Vector2D.project(pt_r - @pt, @basis[0], @basis[1])
    coef = nil
    last_pt = pt_r
    pt = @henon.map(pt_r, @iteration_for_calc)
    iteration = 1
    close_in = nil
    (0..MAX_ITERATION).each do
      coef = MPFR::Vector2D.project(pt - @pt, @basis[0], @basis[1])
      if coef[0] >= last_coef[0] || coef[0] < 0
        close_in = true
        break
      end
      last_pt = pt
      pt = @henon.map(last_pt, @iteration_for_calc)
      last_coef = coef
      iteration += 1
    end
    ret = nil
    if close_in
      if coef[1] > 0 && (coef[1] - last_coef[1]) > 0
        ret = :pos
      elsif coef[1] < 0 && (coef[1] - last_coef[1]) < 0
        ret = :neg
      end
    end
    [ret, iteration]
  end

  def rigorous_direction_by_mapping_old(pt_r)
    ret = nil
    last_coef = MPFR::Vector2D.project(pt_r - @pt, @basis[0], @basis[1])
    pt = @henon.map(pt_r, @iteration_for_calc)
    iteration = 1
    while true
      coef = MPFR::Vector2D.project(pt - @pt, @basis[0], @basis[1])
      if coef[0] >= last_coef[0] || coef[0] < 0
        break
      end
      pt = @henon.map(pt, @iteration_for_calc)
      iteration += 1
    end
    coef = MPFR::Vector2D.project(pt - @pt, @basis[0], @basis[1])
    if coef[1] > 0
      ret = :pos
    elsif coef[1] < 0
      ret = :neg
    end
    [ret, iteration]
  end

  def bisect_points(pos_pt, neg_pt)
    pts = [pos_pt, neg_pt]
    initial_iterations = [@init_iteration, @init_iteration]
    count_bisect = 0
    while (pts[0] - pts[1]).abs >= @bisection_error
      new_pt = pts[0].midpoint(pts[1])
      direction, tmp_iteration = direction_by_mapping(new_pt, initial_iterations.min)
      if direction == :pos
        initial_iterations[0] = tmp_iteration
        pts[0] = new_pt
      elsif direction == :neg
        initial_iterations[1] = tmp_iteration
        pts[1] = new_pt
      elsif count_bisect > @max_bisection
        raise "Bisection has been run too many times."
      else
        raise "Can not determine direction to which point is mapped."
      end
      rig_dir = rigorous_direction_by_mapping(new_pt)
      count_bisect += 1
    end
    pts
  end

  # Return array [pt1, pt2] which direction_by_mapping(pt1, 0) returns :pos and
  # direction_by_mapping(pt2, 0) returns :neg.
  def initial_line_crossing_mfd(start_pt)
    pts = [start_pt - @normal_vec, start_pt + @normal_vec]
    last_pts = []
    direction = pts.map { |a| rigorous_direction_by_mapping(a) }
    if direction[0][0] != direction[1][0]
      direction[0][0] == :pos ? pts : [pts[1], pts[0]]
    else
      last_direction = direction.dup
      i = 0
      while direction[0][0] == last_direction[0][0] && direction[1][0] == last_direction[1][0]
        if i == MAX_INITIAL_INTERVAL_TRIAL
          return nil
        end
        last_pts = pts
        pts = [pts[0] - @normal_vec, pts[1] + @normal_vec]
        last_direction = direction
        direction = pts.map { |a| rigorous_direction_by_mapping(a) }
        i += 1
      end

      if direction[0] != last_direction[0]
        direction[0][0] == :pos ? [pts[0], last_pts[0]] : [last_pts[0], pts[0]]
      elsif  direction[1] != last_direction[1]
        direction[1][0] == :pos ? [pts[1], last_pts[1]] : [last_pts[1], pts[1]]
      end
    end
  end

  def initial_vector_size_and_iteration
    size = INITIAL_VECTOR_SIZE
    sufficient_linear = nil
    iteration = 1
    (0...MAX_INITIAL_VECTOR_SIZE_TRIAL).each do |i|
      pt = @pt + @basis[0].mul_scalar(size)
      iteration = 1
      last_coef = size
      while true
        pt = @henon.map(pt, @iteration_for_calc)
        coef = MPFR::Vector2D.project(pt - @pt, @basis[0], @basis[1])
        if coef[1] > last_coef
          break
        else
          last_coef = coef[1]
        end
        if iteration > CRITERION_LINEAR_ITERATION
          sufficient_linear = true
          break
        end
        iteration += 1
      end
      break if sufficient_linear
      size = size * INITIAL_VECTOR_SIZE
    end
    [(sufficient_linear ? size : nil), iteration]
  end
  private :initial_vector_size_and_iteration

  # Return two points [pt2, pt1] and pt2 is nearer to fixed point than pt1.
  def base_two_intervals(init_vector_size)
    pt_init = @pt + @basis[0].mul_scalar(init_vector_size)
    tmp = initial_line_crossing_mfd(pt_init)
    pts1 = bisect_points(*tmp)
    new = @henon.map(pts1[0].midpoint(pts1[1]), (@flip ? 2 : 1) * @period * (@type == :stable ? 1 : -1))
    tmp = initial_line_crossing_mfd(new)
    pts2 = bisect_points(*tmp)
    [pts2, pts1]
  end

  def line_crossing_mfd(ratio)
    tmp = initial_line_crossing_mfd(@base_two_points[0] + @base_vector.mul_scalar(ratio))
    bisect_points(*tmp)
  end

end

describe "when calculating points on invariant manifold" do
  before(:all) do
    MPFR.set_default_prec(MFD_CALC_PREC)
    cst = ['1.4', '-0.3']
    @cst = cst.map { |a| MPFR.new(a) }
    henon = MPFR::Henon::Mapping.new(cst)
    @pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(@pt).eigenvector
    svec, uvec = vecs
    @basis = [svec, uvec]
    error = MPFR.new(1) / MPFR::Math.exp2(MFD_CALC_PREC) * MPFR.new(100)

    @type = :stable
    @flip = nil

    @r_calc = MPFR::MfdCalculatorTest.new(cst, 1, @pt, @basis, @type, @flip, error)
    @c_calc = MPFR::Henon::MfdCalculator.new(cst, 1, @pt, @basis, @type, @flip, error)
  end

  it "should have same variables" do
    @c_calc.constants.should == @cst
    @c_calc.pt.should == @pt
    @c_calc.basis.should == @basis
    @c_calc.type.should == @type
    @c_calc.flip.should == @flip
  end

  it "should have results of direction_by_mapping" do
    @c_calc.normal_vec == @r_calc.normal_vec
    pt_start = @r_calc.pt + @r_calc.basis[0].mul_scalar(MPFR.new('1e-10'))
    pt_start += @r_calc.normal_vec.mul_scalar(MPFR.new('1e-10'))
    num = 100
    vec_step = @r_calc.normal_vec.mul_scalar(MPFR.new('1e-10') / num).neg
    (0..(2*num)).each do |i|
      r_dir = @r_calc.direction_by_mapping(pt_start, @r_calc.init_iteration)
      c_dir = @c_calc.direction_by_mapping(pt_start, @r_calc.init_iteration)
      r_dir.should == c_dir
      pt_start += vec_step
    end
  end

  it "should have same base points" do
    c_pts = @c_calc.base_two_points
    r_pts = @r_calc.base_two_points
    ((c_pts[0] - r_pts[0]).abs + (c_pts[1] - r_pts[1]).abs).should < MPFR('1e-70')
  end

  it "should get same result by line_crossing_mfd" do 
    (1...100).each do |i|
      rres = @r_calc.line_crossing_mfd(MPFR.new(i.to_f / 100.0))
      cres = @c_calc.line_crossing_mfd(MPFR.new(i.to_f / 100.0))
      ((rres[0] - cres[0]).abs + (rres[1] - cres[1]).abs).should < MPFR('1e-70')
    end
  end

end

describe "when initializing MPFR::Henon::MfdCalculator" do
  before(:all) do
    MPFR.set_default_prec(MFD_CALC_PREC)
    cst = ['1.4', '-0.3']
    @cst = cst.map { |a| MPFR.new(a) }
    henon = MPFR::Henon::Mapping.new(@cst)
    @pt = henon.fixed_point[0]
    vals, vecs = henon.jacobian_matrix(@pt).eigenvector
    svec, uvec = vecs
    @basis = [svec, uvec]
    error = MPFR.new(1) / MPFR::Math.exp2(MFD_CALC_PREC) * MPFR.new(100)

    @type = :stable
    @flip = nil

    @calc = MPFR::Henon::MfdCalculator.new(cst, 1, @pt, @basis, @type, @flip, error)
  end

  it "should have same variables" do
    @calc.constants.should == @cst
    @calc.pt.should == @pt
    @calc.basis.should == @basis
    @calc.type.should == @type
    @calc.flip.should == @flip
  end

end
