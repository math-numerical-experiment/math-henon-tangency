require 'mpfr'
require 'mpfr/matrix'
require 'ruby_mpfr_henon.so'
require "ruby_mpfr_henon/option_parse"

class MPFR
  module Henon
    class Mapping
      def _dump(limit)
        Marshal.dump([constants, period])
      end

      def self._load(s)
        self.new(*Marshal.load(s))
      end

      def lyapunov_number(pt, iterate = 1)
        jacobian_matrix_iterate_map(pt, iterate).eigenvalue
      end

      def maximum_lyapunov_exponent(pt, iterate = 1)
        MPFR::Math.log(lyapunov_number(pt, iterate).map { |a| a.abs }.sort[-1])
      end
    end

    class Mapping2
      def _dump(limit)
        Marshal.dump([constants, period])
      end

      def self._load(s)
        self.new(*Marshal.load(s))
      end
    end
  end
end
