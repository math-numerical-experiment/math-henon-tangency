class MPFR
  module Henon
    def self.command_option_define(opt, params, opts = {})
      option_names = (opts[:only] || [:prec, :param_a, :param_b, :period, :log_level])
      if opts[:exclude]
        option_names = option_names - opts[:exclude]
      end
      if option_names.include?(:prec)
        opt.on("--prec PREC", Integer, "Set the precision of MPFR. Default is #{params[:prec]}") do |v|
          params[:prec] = v
        end
      end
      if option_names.include?(:param_a)
        opt.on("-A PRM", "--param-a PRM", String, "Set the parameter a of Henon map. Default is #{params[:param_a]}") do |v|
          params[:param_a] = v
        end
      end
      if option_names.include?(:param_b)
        opt.on("-B PRM", "--param-b PRM", String, "Set the parameter b of Henon map. Default is #{params[:param_b]}") do |v|
          params[:param_b] = v
        end
      end
      if option_names.include?(:period)
        opt.on("-p NUM", "--period NUM", Integer, "Set the period of map. Default is #{params[:period]}") do |v|
          params[:period] = v
        end
      end
      if option_names.include?(:log_level)
        opt.on("-L LEVEL", "--log-level LEVEL", String, "Set the level of logging: #{ManageLog.log_level_args.join(' ')}") do |v|
          params[:log_level] = ManageLog.log_level_from_arg(v)
        end
      end
    end
  end
end
