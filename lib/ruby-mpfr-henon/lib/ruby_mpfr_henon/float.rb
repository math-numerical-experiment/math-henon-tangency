require 'matrix'
require 'mapping_template'

module FloatHenon
  class Mapping < MappingTemplate
    def fixed_point
      ret = [Vector[-(Math.sqrt(@cst[1]**2+2.0*@cst[1]+4.0*@cst[0]+1.0)+@cst[1]+1.0)/@cst[0]/2.0,
                    (@cst[1]*Math.sqrt(@cst[1]**2+2.0*@cst[1]+ 4.0 * @cst[0]+1.0)+@cst[1]**2+@cst[1])/@cst[0]/2.0],
             Vector[(Math.sqrt(@cst[1]**2+2.0*@cst[1]+4.0*@cst[0]+1.0)-@cst[1]-1.0)/@cst[0]/2.0,
                    -(@cst[1]*Math.sqrt(@cst[1]**2+2.0*@cst[1]+4.0*@cst[0]+1.0)-@cst[1]**2-@cst[1])/@cst[0]/2.0]]
      if ret[0][0] > ret[1][0]
        ret
      else
        [ret[1], ret[0]]
      end
    end

    def map(pt, iterate = 1)
      Vector.elements(FloatHenon.iterate_map_base(pt.to_a, iterate, @cst))
    end
    
    def iterate_map(pt, iterate = nil)
      Vector.elements(FloatHenon.iterate_map_base(pt.to_a, (iterate ? iterate * @period : @period), @cst))
    end
    
  end
end
