#include "func_float_henon.h"

/* Henon map */
/* x_{n+1} = y_n + 1 - a*x_{n}^2 */
/* y_{n+1} = -b*x_{n} */
/* Set the value getted by mapping x to y. */
/* cst = {CONST1, CONST2} */
void rfloat_henon_map (double *retx, double *rety, double x, double y, double a, double b)
{
  *retx = y + 1.0 - a * x * x;
  *rety = -b * x;
}

/* Inverse map */
/* x_n = - y_{n+1}/b */
/* y_n = (a*y_{n+1}*y_{n+1})/(b*b) + x_{n+1} - 1.0 */
/* Set the value getted by mapping x to y. */
/* cst = {CONST1, CONST2} */
void rfloat_henon_inverse_map (double *retx, double *rety, double x, double y, double a, double b)
{
  double tmp = y / b;
  *retx = -y / b;
  *rety = a * tmp * tmp + x - 1.0;
}

void rfloat_henon_iteration (double *retx, double *rety, double x, double y, double a, double b, int iteration)
{
  int i;
  if (iteration == 0) {
    *retx = x;
    *rety = y;
  } else {
    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	rfloat_henon_map(retx, rety, x, y, a, b);
	x = *retx;
	y = *rety;
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	rfloat_henon_inverse_map(retx, rety, x, y, a, b);
	x = *retx;
	y = *rety;
      }
    }
  }
}

void rfloat_henon_map2 (double *ret, double *x, double *cst)
{
  *ret = *(x + 1) + 1.0 - *cst * *x * *x;
  *(ret + 1) = - *(cst + 1) * *x;
}

void rfloat_henon_inverse_map2 (double *ret, double *x, double *cst)
{
  double tmp = *(x + 1) / *(cst + 1);
  *ret = - *(x + 1) / *(cst + 1);
  *(ret + 1) = *cst * tmp * tmp + *x - 1.0;
}

void rfloat_henon_iteration2 (double *ret, double *x, double *cst, int iteration)
{
  if (iteration == 0) {
    *ret = *x;
    *(ret + 1) = *(x + 1);
  } else {
    int i;
    double tmp[2];
    tmp[0] = *x;
    tmp[1] = *(x + 1);
    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	rfloat_henon_map2(ret, tmp, cst);
	tmp[0] = *ret;
	tmp[1] = *(ret + 1);
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	rfloat_henon_inverse_map2(ret, tmp, cst);
	tmp[0] = *ret;
	tmp[1] = *(ret + 1);
      }
    }
  }
}
