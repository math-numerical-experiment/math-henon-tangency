#ifndef _RUBY_MPFR_HENON_H_
#define _RUBY_MPFR_HENON_H_

#include "func_mpfr_henon.h"
#include "func_mpfr_calc_mfd_henon.h"

VALUE r_mpfr_vector_2d_module, mpfr_mfd_calculator, mpfr_henon_mapping, mpfr_henon_module, float_henon,
  mpfr_henon_double_mapping, mpfr_henon_mapping2;

typedef struct __HenonMPFRMap{
  MPFRMatrix *cst;
  int period;
  double d_cst[2];
} HenonMPFRMap;

#endif /* _RUBY_MPFR_HENON_H_ */

