#include "func_mpfr_henon.h"

/*
  Henon map
  x_{n+1} = y_n + 1 - a*x_{n}^2
  y_{n+1} = -b*x_{n}
  Set the value getted by mapping x to y.
  cst = {CONST1, CONST2}

  Note that *y and *x do NOT same pointer.
*/
void r_mpfr_henon_map (MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst)
{
  mpfr_mul(y->data + 1, cst->data + 1, x->data, GMP_RNDN);
  mpfr_neg(y->data + 1, y->data + 1, GMP_RNDN);

  mpfr_sqr(y->data, x->data, GMP_RNDN);
  mpfr_mul(y->data, y->data, cst->data, GMP_RNDN);
  mpfr_neg(y->data, y->data, GMP_RNDN);
  mpfr_add(y->data, x->data + 1, y->data, GMP_RNDN);
  mpfr_add_ui(y->data, y->data, 1, GMP_RNDN);
}

/*
  Inverse map
  x_n = - y_{n+1}/b
  y_n = (a*y_{n+1}*y_{n+1})/(b*b) + x_{n+1} - 1.0
  Set the value getted by mapping x to y.
  cst = {CONST1, CONST2}

  Note that *y and *x do NOT same pointer.
*/
void r_mpfr_henon_inverse_map (MPFRMatrix * y, MPFRMatrix * x, MPFRMatrix * cst)
{
  mpfr_div(y->data, x->data + 1, cst->data + 1, GMP_RNDN);
  mpfr_neg(y->data, y->data, GMP_RNDN);

  mpfr_sqr(y->data + 1, x->data + 1, GMP_RNDN);
  mpfr_mul(y->data + 1, cst->data, y->data + 1, GMP_RNDN);
  mpfr_div(y->data + 1, y->data + 1, cst->data + 1, GMP_RNDN);
  mpfr_div(y->data + 1, y->data + 1, cst->data + 1, GMP_RNDN);
  mpfr_add(y->data + 1, y->data + 1, x->data, GMP_RNDN);
  mpfr_sub_ui(y->data + 1, y->data + 1, 1, GMP_RNDN);
}

void r_mpfr_henon_iteration (MPFRMatrix * b, MPFRMatrix * a, MPFRMatrix * c, int iteration)
{
  if (iteration == 0) {
    mpfr_matrix_set(b, a);
  } else {
    int i;
    MPFRMatrix *tmp, *t_ret;
    r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);
    r_mpfr_matrix_temp_alloc_init(t_ret, 2, 1);

    mpfr_matrix_set(tmp, a);

    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	r_mpfr_henon_map(t_ret, tmp, c);
	mpfr_matrix_set(tmp, t_ret);
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	r_mpfr_henon_inverse_map(t_ret, tmp, c);
	mpfr_matrix_set(tmp, t_ret);
      }
    }
    mpfr_matrix_set(b, t_ret);

    r_mpfr_matrix_temp_free(tmp);
    r_mpfr_matrix_temp_free(t_ret);
  }
}

void r_mpfr_henon_iteration_double (MPFRMatrix * b, MPFRMatrix * a, double * c, int iteration)
{
  double d_ret[2], d_src[2];
  d_src[0] = mpfr_get_d(mpfr_matrix_get_ptr(a, 0), GMP_RNDN);
  d_src[1] = mpfr_get_d(mpfr_matrix_get_ptr(a, 1), GMP_RNDN);
  rfloat_henon_iteration2(d_ret, d_src, c, iteration);
  mpfr_set_d(mpfr_matrix_get_ptr(b, 0), d_ret[0], GMP_RNDN);
  mpfr_set_d(mpfr_matrix_get_ptr(b, 1), d_ret[1], GMP_RNDN);
}

void r_mpfr_henon_jacobian_matrix (MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants)
{
  mpfr_set_si(jac->data, -2, GMP_RNDN);
  mpfr_mul(jac->data, jac->data, constants->data, GMP_RNDN);
  mpfr_mul(jac->data, jac->data, pt->data, GMP_RNDN);
  mpfr_set_si(jac->data + 2, 1, GMP_RNDN);
  mpfr_neg(jac->data + 1, constants->data + 1, GMP_RNDN);
  mpfr_set_si(jac->data + 3, 0, GMP_RNDN);
}

void r_mpfr_henon_jacobian_matrix_iterate_map (MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants, int iteration)
{
  int i;
  MPFRMatrix *pt_new, *pt_old, *jac_tmp, *jac_new;
  if (iteration < 0) {
    fprintf(stderr, "Not implemented: Jacobian matrix of inverse map");
    abort();
  }
  r_mpfr_matrix_temp_alloc_init(pt_new, 2, 1);
  r_mpfr_matrix_temp_alloc_init(pt_old, 2, 1);
  r_mpfr_matrix_temp_alloc_init(jac_tmp, 2, 2);
  r_mpfr_matrix_temp_alloc_init(jac_new, 2, 2);
  mpfr_matrix_set(pt_old, pt);
  r_mpfr_henon_jacobian_matrix(jac, pt, constants);
  for (i = 1; i < iteration; i++) {
    r_mpfr_henon_map(pt_new, pt_old, constants);
    mpfr_matrix_set(pt_old, pt_new);
    r_mpfr_henon_jacobian_matrix(jac_new, pt_old, constants);
    mpfr_matrix_mul(jac_tmp, jac_new, jac);
    mpfr_matrix_set(jac, jac_tmp);
  }
  r_mpfr_matrix_temp_free(pt_new);
  r_mpfr_matrix_temp_free(pt_old);
  r_mpfr_matrix_temp_free(jac_tmp);
  r_mpfr_matrix_temp_free(jac_new);
}

void r_mpfr_henon_fixed_point (MPFRMatrix *fp1, MPFRMatrix *fp2, MPFRMatrix *constants)
{
  MPFRMatrix x, y;
  mpfr_matrix_init(&x, 2, 1);
  mpfr_matrix_init(&y, 2, 1);

  /* start: calculate fixed points */
  mpfr_add_ui(x.data, constants->data + 1, 1, GMP_RNDN);
  mpfr_mul(x.data + 1, x.data, x.data, GMP_RNDN);
  mpfr_mul_ui(y.data, constants->data, 4, GMP_RNDN);
  mpfr_add(x.data + 1, x.data + 1, y.data, GMP_RNDN);
  mpfr_sqrt(x.data + 1, x.data + 1, GMP_RNDN);
  mpfr_neg(x.data, x.data, GMP_RNDN);

  mpfr_add(fp1->data, x.data, x.data + 1, GMP_RNDN);
  mpfr_sub(fp2->data, x.data, x.data + 1, GMP_RNDN);

  mpfr_div_ui(fp1->data, fp1->data, 2, GMP_RNDN);
  mpfr_div(fp1->data, fp1->data, constants->data, GMP_RNDN);
  mpfr_mul(fp1->data + 1, fp1->data, constants->data + 1, GMP_RNDN);
  mpfr_neg(fp1->data + 1, fp1->data + 1, GMP_RNDN);

  mpfr_div_ui(fp2->data, fp2->data, 2, GMP_RNDN);
  mpfr_div(fp2->data, fp2->data, constants->data, GMP_RNDN);
  mpfr_mul(fp2->data + 1, fp2->data, constants->data + 1, GMP_RNDN);
  mpfr_neg(fp2->data + 1, fp2->data + 1, GMP_RNDN);

  mpfr_matrix_clear(&x);
  mpfr_matrix_clear(&y);
}

void r_mpfr_henon_cumulative_distance (MPFR *distance, MPFRMatrix *pt, MPFRMatrix *cst, int period)
{
  int i;
  MPFR tmp;
  MPFRMatrix *orbits;
  mpfr_init(&tmp);
  orbits = (MPFRMatrix *) malloc(sizeof(MPFRMatrix) * (period + 1));
  for (i = 0; i <= period; i++) {
    mpfr_matrix_init(&orbits[i], 2, 1);
  }
  r_mpfr_henon_map(&orbits[0], pt, cst);
  for (i = 1; i < period; i++) {
    r_mpfr_henon_map(&orbits[i], &orbits[i-1], cst);
  }

  mpfr_matrix_vector_distance(distance, &orbits[period - 1], pt);
  for (i = 0; i < period - 1; i++) {
    r_mpfr_henon_map(&orbits[period], &orbits[period - 1], cst);
    mpfr_matrix_set(&orbits[period - 1], &orbits[period]);
    mpfr_matrix_vector_distance(&tmp, &orbits[period], &orbits[i]);
    mpfr_add(distance, distance, &tmp, GMP_RNDN);
  }

  mpfr_clear(&tmp);
  for (i = 0; i <= period; i++) {
    mpfr_matrix_clear(&orbits[i]);
  }
  free(orbits);
}


/*
  Henon map
  x_{n+1} = a - x_{n}^2 + b * x_{n + 1}
  y_{n+1} = x_{n}
  Set the value getted by mapping x to y.
  cst = {CONST1, CONST2}

  Note that *y and *x do NOT same pointer.
*/
void r_mpfr_henon_map2 (MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst)
{
  mpfr_mul(y->data + 1, cst->data + 1, x->data + 1, GMP_RNDN);
  mpfr_mul(y->data, x->data, x->data, GMP_RNDN);
  mpfr_sub(y->data, cst->data, y->data, GMP_RNDN);
  mpfr_add(y->data, y->data, y->data + 1, GMP_RNDN);

  mpfr_set(y->data + 1, x->data, GMP_RNDN);
}

/*
  Inverse map
  x_n = y_{n+1}
  y_n = (x_{n+1} - a + y_{n}^2) / b
  Set the value getted by mapping x to y.
  cst = {CONST1, CONST2}

  Note that *y and *x do NOT same pointer.
*/
void r_mpfr_henon_inverse_map2 (MPFRMatrix * y, MPFRMatrix * x, MPFRMatrix * cst)
{
  mpfr_set(y->data, x->data + 1, GMP_RNDN);

  mpfr_mul(y->data + 1, x->data + 1, x->data + 1, GMP_RNDN);
  mpfr_sub(y->data + 1, y->data + 1, cst->data, GMP_RNDN);
  mpfr_add(y->data + 1, x->data, y->data + 1, GMP_RNDN);
  mpfr_div(y->data + 1, y->data + 1, cst->data + 1, GMP_RNDN);
}

void r_mpfr_henon_iteration2 (MPFRMatrix * b, MPFRMatrix * a, MPFRMatrix * c, int iteration)
{
  if (iteration == 0) {
    mpfr_matrix_set(b, a);
  } else {
    int i;
    MPFRMatrix *tmp, *t_ret;
    r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);
    r_mpfr_matrix_temp_alloc_init(t_ret, 2, 1);

    mpfr_matrix_set(tmp, a);

    if (iteration > 0) {
      for (i = 0; i < iteration; i++) {
	r_mpfr_henon_map2(t_ret, tmp, c);
	mpfr_matrix_set(tmp, t_ret);
      }
    } else if (iteration < 0) {
      for (i = 0; i < -iteration; i++) {
	r_mpfr_henon_inverse_map2(t_ret, tmp, c);
	mpfr_matrix_set(tmp, t_ret);
      }
    }
    mpfr_matrix_set(b, t_ret);

    r_mpfr_matrix_temp_free(tmp);
    r_mpfr_matrix_temp_free(t_ret);
  }
}

/* void r_mpfr_henon_iteration_double (MPFRMatrix * b, MPFRMatrix * a, double * c, int iteration) */
/* { */
/*   double d_ret[2], d_src[2]; */
/*   d_src[0] = mpfr_get_d(mpfr_matrix_get_ptr(a, 0), GMP_RNDN); */
/*   d_src[1] = mpfr_get_d(mpfr_matrix_get_ptr(a, 1), GMP_RNDN); */
/*   rfloat_henon_iteration2(d_ret, d_src, c, iteration); */
/*   mpfr_set_d(mpfr_matrix_get_ptr(b, 0), d_ret[0], GMP_RNDN); */
/*   mpfr_set_d(mpfr_matrix_get_ptr(b, 1), d_ret[1], GMP_RNDN); */
/* } */

void r_mpfr_henon_jacobian_matrix2 (MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants)
{
  mpfr_mul_si(jac->data, pt->data, -2, GMP_RNDN);
  mpfr_set(jac->data + 1, constants->data + 1, GMP_RNDN);
  mpfr_set_si(jac->data + 2, 1, GMP_RNDN);
  mpfr_set_si(jac->data + 3, 0, GMP_RNDN);
}
