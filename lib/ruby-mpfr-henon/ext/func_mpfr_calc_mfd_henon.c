#include "func_mpfr_calc_mfd_henon.h"
#include "func_mpfr_mfd_const.h"

#define ABORT() rb_raise(rb_eException, "Abort!\n")
/* #define ABORT() abort() */

/* #define DEBUG_OUTPUT */

static void mpfr_mfd_initial_line_with_modification_of_normal_vector (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFRMatrix *start_pt, MPFRCalcMfdData *data);

void mpfr_init_calc_mfd_data (MPFRCalcMfdData *data)
{
  r_mpfr_matrix_temp_alloc_init(data->cst, 2, 1);
  r_mpfr_matrix_temp_alloc_init(data->pt, 2, 1);
  r_mpfr_matrix_temp_alloc_init(data->basis[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(data->basis[1], 2, 1);
  r_mpfr_temp_alloc_init(data->bisection_error);
  r_mpfr_temp_alloc_init(data->criterion[0]);
  r_mpfr_temp_alloc_init(data->criterion[1]);
  r_mpfr_temp_alloc_init(data->criterion[2]);
  r_mpfr_temp_alloc_init(data->criterion[3]);
  r_mpfr_matrix_temp_alloc_init(data->normal_vec, 2, 1);
  r_mpfr_matrix_temp_alloc_init(data->base_two_points[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(data->base_two_points[1], 2, 1);
  r_mpfr_matrix_temp_alloc_init(data->base_vector, 2, 1);
  r_mpfr_temp_alloc_init(data->base_vector_size);
  r_mpfr_temp_alloc_init(data->divergent_condition[0]);
  r_mpfr_temp_alloc_init(data->divergent_condition[1]);
}

void mpfr_clear_calc_mfd_data (MPFRCalcMfdData *data)
{
  r_mpfr_matrix_temp_free(data->cst);
  r_mpfr_matrix_temp_free(data->pt);
  r_mpfr_matrix_temp_free(data->basis[0]);
  r_mpfr_matrix_temp_free(data->basis[1]);
  r_mpfr_temp_free(data->bisection_error);
  r_mpfr_temp_free(data->criterion[0]);
  r_mpfr_temp_free(data->criterion[1]);
  r_mpfr_temp_free(data->criterion[2]);
  r_mpfr_temp_free(data->criterion[3]);
  r_mpfr_matrix_temp_free(data->normal_vec);
  r_mpfr_matrix_temp_free(data->base_two_points[0]);
  r_mpfr_matrix_temp_free(data->base_two_points[1]);
  r_mpfr_matrix_temp_free(data->base_vector);
  r_mpfr_temp_free(data->base_vector_size);
  r_mpfr_temp_free(data->divergent_condition[0]);
  r_mpfr_temp_free(data->divergent_condition[1]);
}

static bool mpfr_mfd_set_divergent_condition (MPFRCalcMfdData *data)
{
  bool parameter_suitable_p = false;

  mpfr_mul(data->divergent_condition[1], mpfr_matrix_get_ptr(data->cst, 1), mpfr_matrix_get_ptr(data->cst, 1), GMP_RNDN);
  mpfr_div(data->divergent_condition[0], mpfr_matrix_get_ptr(data->cst, 0), data->divergent_condition[1], GMP_RNDN);
  mpfr_mul_ui(data->divergent_condition[0], data->divergent_condition[0], 25, GMP_RNDN);
  mpfr_ui_sub(data->divergent_condition[0], 6, data->divergent_condition[0], GMP_RNDN);
  mpfr_div(data->divergent_condition[1], data->divergent_condition[1], mpfr_matrix_get_ptr(data->cst, 0), GMP_RNDN);

  data->check_divergent2 = DEFAULT_CHECK_DIVERGENT2;

  if (mpfr_cmp_si(mpfr_matrix_get_ptr(data->cst, 1), 0) < 0) {
    MPFR *tmp;
    r_mpfr_temp_alloc_init(tmp);
    mpfr_set_si(tmp, -5, MPFR_RNDN);
    mpfr_div_ui(tmp, tmp, 6, MPFR_RNDN);
    if ((mpfr_cmp_si(mpfr_matrix_get_ptr(data->cst, 0), 0) > 0) && (mpfr_cmp(mpfr_matrix_get_ptr(data->cst, 1), tmp) > 0)) {
      parameter_suitable_p = true;
    }
    r_mpfr_temp_free(tmp);
  } else if (mpfr_cmp_si(mpfr_matrix_get_ptr(data->cst, 1), 0) > 0) {
    MPFR *tmp1, *tmp2;
    r_mpfr_temp_alloc_init(tmp1);
    r_mpfr_temp_alloc_init(tmp2);
    mpfr_div_ui(tmp1, mpfr_matrix_get_ptr(data->cst, 1), 5, MPFR_RNDN);
    mpfr_set_si(tmp2, 6, MPFR_RNDN);
    mpfr_div_ui(tmp2, tmp2, 25, MPFR_RNDN);
    mpfr_mul(tmp2, tmp2, mpfr_matrix_get_ptr(data->cst, 1), MPFR_RNDN);
    mpfr_mul(tmp2, tmp2, mpfr_matrix_get_ptr(data->cst, 1), MPFR_RNDN);
    mpfr_add(tmp1, tmp1, tmp2, MPFR_RNDN);
    if (mpfr_cmp(mpfr_matrix_get_ptr(data->cst, 0), tmp1) > 0) {
      parameter_suitable_p = true;
    }
    r_mpfr_temp_free(tmp1);
    r_mpfr_temp_free(tmp2);
  }
  return parameter_suitable_p;
}

static void mpfr_mfd_initial_vector_size_and_iteration (MPFR *size, int *iteration, MPFRCalcMfdData *data)
{
  MPFRMatrix *pt, *coef, *tmp;
  MPFR *last_coef;
  bool sufficient_linear;
  int i;

  sufficient_linear = false;
  r_mpfr_matrix_temp_alloc_init(pt, 2, 1);
  r_mpfr_matrix_temp_alloc_init(coef, 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);
  mpfr_set_str(size, INITIAL_VECTOR_SIZE, 10, GMP_RNDN);
  r_mpfr_temp_alloc_init(last_coef);

  for (i = 0; i < MAX_INITIAL_VECTOR_SIZE_TRIAL; i++) {
#ifdef DEBUG_OUTPUT
    mpfr_printf("Initial vector size: %d %.Re\n", i, size);
#endif
    mpfr_matrix_mul_scalar(pt, data->basis[0], size);
    mpfr_matrix_add(pt, pt, data->pt);
    *iteration = 1;
    mpfr_set(last_coef, size, GMP_RNDN);

    while (true) {
      r_mpfr_henon_iteration(pt, pt, data->cst, data->iteration_for_calc);
      mpfr_matrix_sub(tmp, pt, data->pt);
      mpfr_vec2_project(coef, tmp, data->basis[0], data->basis[1]);
#ifdef DEBUG_OUTPUT
      mpfr_printf("%.30Re %.30Re\n", mpfr_matrix_get_ptr(coef, 0), mpfr_matrix_get_ptr(coef, 1));
#endif
      if (mpfr_cmp(mpfr_matrix_get_ptr(coef, 1), last_coef) > 0) {
#ifdef DEBUG_OUTPUT
	mpfr_printf("Orbit leave: %.30Re %.30Re\n", mpfr_matrix_get_ptr(coef, 1), last_coef);
#endif
	break;
      }
      /* If the following code is comment out, this function does not work for some parameters. */
      /* else { */
      /* 	mpfr_set(last_coef, mpfr_matrix_get_ptr(coef, 1), GMP_RNDN); */
      /* } */
      if (*iteration > CRITERION_LINEAR_ITERATION) {
	sufficient_linear = true;
	break;
      }
      *iteration += 1;
    }
#ifdef DEBUG_OUTPUT
    printf("*iteration=%d\n", *iteration);
#endif
    if (sufficient_linear) {
      break;
    }
    mpfr_set_str(tmp->data, INITIAL_VECTOR_SIZE, 10, GMP_RNDN);
    mpfr_mul(size, size, tmp->data, GMP_RNDN);
  }

  r_mpfr_matrix_temp_free(pt);
  r_mpfr_matrix_temp_free(coef);
  r_mpfr_matrix_temp_free(tmp);
  r_mpfr_temp_free(last_coef);

  if (!sufficient_linear) {
    printf("Can not get initial vector size\n");
    ABORT();
  }
}

static void mpfr_mfd_base_two_intervals (MPFRMatrix *pt2_p, MPFRMatrix *pt2_n, MPFRMatrix *pt1_p, MPFRMatrix *pt1_n,
                                         MPFR *init_vector_size, MPFRCalcMfdData *data)
{
  MPFRMatrix *vec_ratio, *pt_init, *pt_pos, *pt_neg;
  r_mpfr_matrix_temp_alloc_init(vec_ratio, 2, 1);
  r_mpfr_matrix_temp_alloc_init(pt_init, 2, 1);
  r_mpfr_matrix_temp_alloc_init(pt_pos, 2, 1);
  r_mpfr_matrix_temp_alloc_init(pt_neg, 2, 1);

  mpfr_matrix_mul_scalar(vec_ratio, data->basis[0], init_vector_size);
  mpfr_matrix_add(pt_init, vec_ratio, data->pt);
  mpfr_mfd_initial_line_with_modification_of_normal_vector(pt_pos, pt_neg, pt_init, data);
  mpfr_mfd_bisect_points(pt1_p, pt1_n, pt_pos, pt_neg, data);

  mpfr_vector_midpoint(vec_ratio, pt1_p, pt1_n);
  r_mpfr_henon_iteration(pt_init, vec_ratio, data->cst, (data->flip ? 2 : 1) * data->period * (data->type == STABLE ? 1 : -1));
  mpfr_mfd_initial_line_crossing_mfd(pt_pos, pt_neg, pt_init, data);
  mpfr_mfd_bisect_points(pt2_p, pt2_n, pt_pos, pt_neg, data);

  r_mpfr_matrix_temp_free(vec_ratio);
  r_mpfr_matrix_temp_free(pt_init);
  r_mpfr_matrix_temp_free(pt_pos);
  r_mpfr_matrix_temp_free(pt_neg);
}

bool mpfr_set_calc_mfd_data (MPFRCalcMfdData *data, MPFRMatrix *cst, int period, MPFRMatrix *pt,
                             MPFRMatrix *basis1, MPFRMatrix *basis2,
                             MfdType type, bool flip, MPFR *bisection_error)
{
  bool parameter_suitable_p;
  MPFR *init_vector_size, *tmp;
  MPFRMatrix *tmp_pt1[2], *tmp_pt2[2];

  mpfr_matrix_set(data->cst, cst);
  data->period = period;
  mpfr_matrix_set(data->pt, pt);
  mpfr_matrix_set(data->basis[0], basis1);
  mpfr_matrix_set(data->basis[1], basis2);
  data->type = type;
  data->flip = flip;
  mpfr_set(data->bisection_error, bisection_error, GMP_RNDN);
  data->max_bisection = mpfr_get_default_prec() * 2;
  data->iteration_for_calc = 2 * data->period * (data->type == STABLE ? 1 : -1);

  r_mpfr_temp_alloc_init(init_vector_size);
  r_mpfr_temp_alloc_init(tmp);
  r_mpfr_matrix_temp_alloc_init(tmp_pt1[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp_pt1[1], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp_pt2[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp_pt2[1], 2, 1);

  mpfr_mfd_initial_vector_size_and_iteration(init_vector_size, &data->init_iteration, data);
  mpfr_set(data->criterion[0], init_vector_size, GMP_RNDN);
  mpfr_neg(data->criterion[1], init_vector_size, GMP_RNDN);
  mpfr_mul_si(data->criterion[2], init_vector_size, 2, GMP_RNDN);
  mpfr_neg(data->criterion[3], data->criterion[2], GMP_RNDN);

  mpfr_2d_normal_vector(data->normal_vec, data->basis[0]);
  mpfr_div_ui(tmp, init_vector_size, RATIO_NORMAL_VECTOR_SIZE, GMP_RNDN);
  mpfr_matrix_mul_scalar(data->normal_vec, data->normal_vec, tmp);
  
  mpfr_mfd_base_two_intervals(tmp_pt1[0], tmp_pt1[1], tmp_pt2[0], tmp_pt2[1], init_vector_size, data);
  mpfr_vector_midpoint(data->base_two_points[0], tmp_pt1[0], tmp_pt1[1]);
  mpfr_vector_midpoint(data->base_two_points[1], tmp_pt2[0], tmp_pt2[1]);

  mpfr_matrix_sub(data->base_vector, data->base_two_points[1], data->base_two_points[0]);
  mpfr_matrix_vector_norm(data->base_vector_size, data->base_vector);

  parameter_suitable_p = mpfr_mfd_set_divergent_condition(data);

  r_mpfr_temp_free(init_vector_size);
  r_mpfr_temp_free(tmp);
  r_mpfr_matrix_temp_free(tmp_pt1[0]);
  r_mpfr_matrix_temp_free(tmp_pt1[1]);
  r_mpfr_matrix_temp_free(tmp_pt2[0]);
  r_mpfr_matrix_temp_free(tmp_pt2[1]);
  return parameter_suitable_p;
}

void mpfr_mfd_direction_by_mapping(MapDirection *ret_dir, int *ret_iteration, MPFRMatrix *pt_arg,
                                   int initial_iteration, MPFRCalcMfdData *data)
{
  MPFRMatrix *pt, *coef, *tmp;
  int i, max_iterate;

  *ret_dir = DIR_ERROR;
  r_mpfr_matrix_temp_alloc_init(pt, 2, 1);
  r_mpfr_matrix_temp_alloc_init(coef, 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);

  r_mpfr_henon_iteration(pt, pt_arg, data->cst, initial_iteration * data->iteration_for_calc);
  max_iterate = mpfr_get_default_prec() * 10;
  for (i = 0; i < max_iterate; i++) {
    mpfr_matrix_sub(tmp, pt, data->pt);
    mpfr_vec2_project(coef, tmp, data->basis[0], data->basis[1]);
    if (mpfr_cmp(mpfr_matrix_get_ptr(coef, 1), data->criterion[0]) > 0) {
      *ret_dir = DIR_POS;
      break;
    } else if (mpfr_cmp(mpfr_matrix_get_ptr(coef, 1), data->criterion[1]) < 0) {
      *ret_dir = DIR_NEG;
      break;
    }
    r_mpfr_henon_iteration(pt, pt, data->cst, data->iteration_for_calc);
  }
  *ret_iteration = i + initial_iteration;
  r_mpfr_matrix_temp_free(pt);
  r_mpfr_matrix_temp_free(coef);
  r_mpfr_matrix_temp_free(tmp);

  if (*ret_dir == DIR_ERROR) {
    printf("Can not determine direction by mpfr_mfd_direction_by_mapping\n");
    ABORT();
  }
}

void mpfr_mfd_rigorous_direction_by_mapping(MapDirection *ret_dir, int *ret_iteration, MPFRMatrix *pt_arg,
                                            MPFRCalcMfdData *data)
{
  MPFRMatrix *pt, *last_pt, *coef, *last_coef, *tmp;
  MPFR *sub_coef;
  int i, max_iterate;

  *ret_dir = DIR_ERROR;
  r_mpfr_matrix_temp_alloc_init(pt, 2, 1);
  r_mpfr_matrix_temp_alloc_init(last_pt, 2, 1);
  r_mpfr_matrix_temp_alloc_init(coef, 2, 1);
  r_mpfr_matrix_temp_alloc_init(last_coef, 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);
  r_mpfr_temp_alloc_init(sub_coef);

  mpfr_matrix_sub(tmp, pt_arg, data->pt);
  mpfr_vec2_project(last_coef, tmp, data->basis[0], data->basis[1]);

  r_mpfr_henon_iteration(pt, pt_arg, data->cst, data->iteration_for_calc);

  *ret_iteration = 1;
  
  max_iterate = mpfr_get_default_prec() * 10;
  *ret_dir = DIR_ERROR;
  for (i = 0; i < max_iterate; i++) {
    mpfr_matrix_sub(tmp, pt, data->pt);
    mpfr_vec2_project(coef, tmp, data->basis[0], data->basis[1]);
    /* The point leaves from the stable direction of fixed point. */
    if (mpfr_cmp(mpfr_matrix_get_ptr(coef, 0), mpfr_matrix_get_ptr(last_coef, 0)) > 0 ||
       mpfr_cmp_si(mpfr_matrix_get_ptr(coef, 0), 0) <= 0) {
      /* mpfr_matrix_vector_distance(sub_coef, data->pt, pt); */
      /* mpfr_printf("%.10Re\n", sub_coef); */
      break;
    }
    mpfr_matrix_set(last_pt, pt);
    r_mpfr_henon_iteration(pt, last_pt, data->cst, data->iteration_for_calc);
    mpfr_matrix_set(last_coef, coef);
    *ret_iteration += 1;
  }

  mpfr_sub(sub_coef, mpfr_matrix_get_ptr(coef, 1), mpfr_matrix_get_ptr(last_coef, 1), GMP_RNDN);
  if (mpfr_cmp_ui(mpfr_matrix_get_ptr(coef, 1), 0) >= 0 && mpfr_cmp_ui(sub_coef, 0) >= 0) {
    *ret_dir = DIR_POS;
  } else if (mpfr_cmp_ui(mpfr_matrix_get_ptr(coef, 1), 0) < 0 && mpfr_cmp_ui(sub_coef, 0) < 0) {
    *ret_dir = DIR_NEG;
  }

  if (*ret_dir == DIR_ERROR) {
    fprintf(stderr, "Can not determine direction by mpfr_mfd_rigorous_direction_by_mapping:\n");
    mpfr_fprintf(stderr, "  coef = (%.10Re, %.10Re)\n", mpfr_matrix_get_ptr(coef, 0), mpfr_matrix_get_ptr(coef, 1));
    mpfr_fprintf(stderr, "  last_coef = (%.10Re, %.10Re)\n",
		 mpfr_matrix_get_ptr(last_coef, 0), mpfr_matrix_get_ptr(last_coef, 1));
    mpfr_fprintf(stderr, "  sub_coef = %.10Re\n", sub_coef);
    mpfr_fprintf(stderr, "  tmp = (%.10Re, %.10Re)\n", mpfr_matrix_get_ptr(tmp, 0), mpfr_matrix_get_ptr(tmp, 1));
    mpfr_fprintf(stderr, "  *ret_iteration = %d\n", *ret_iteration);
    ABORT();
  }

  r_mpfr_matrix_temp_free(pt);
  r_mpfr_matrix_temp_free(last_pt);
  r_mpfr_matrix_temp_free(coef);
  r_mpfr_matrix_temp_free(last_coef);
  r_mpfr_matrix_temp_free(tmp);
  r_mpfr_temp_free(sub_coef);
}

void mpfr_mfd_bisect_points (MPFRMatrix *r_pt_pos, MPFRMatrix *r_pt_neg, MPFRMatrix *pt_pos, MPFRMatrix *pt_neg,
                             MPFRCalcMfdData *data)
{
  MPFRMatrix *new_pt;
  MapDirection direction;
  int init_iteration_pos, init_iteration_neg, count_bisect, new_iteration;
  MPFR *distance;

  r_mpfr_matrix_temp_alloc_init(new_pt, 2, 1);
  mpfr_matrix_set(r_pt_pos, pt_pos);
  mpfr_matrix_set(r_pt_neg, pt_neg);
  
  init_iteration_pos = data->init_iteration;
  init_iteration_neg = data->init_iteration;
  count_bisect = 0;
  r_mpfr_temp_alloc_init(distance);

  mpfr_matrix_vector_distance(distance, r_pt_pos, r_pt_neg);
  while (mpfr_cmp(distance, data->bisection_error) >= 0) {
    mpfr_vector_midpoint(new_pt, r_pt_pos, r_pt_neg);
    mpfr_mfd_direction_by_mapping(&direction, &new_iteration, new_pt,
				  (init_iteration_pos < init_iteration_neg ? init_iteration_pos : init_iteration_neg), data);
    if (count_bisect > data->max_bisection) {
      printf("Too many bisections for calculating direction\n");
      ABORT();
    } else if (direction == DIR_POS) {
      init_iteration_pos = new_iteration;
      mpfr_matrix_set(r_pt_pos, new_pt);
    } else if (direction == DIR_NEG) {
      init_iteration_neg = new_iteration;
      mpfr_matrix_set(r_pt_neg, new_pt);
    } else {
      printf("Can not determine direction fo mapping.\n");
      ABORT();
    }
    mpfr_matrix_vector_distance(distance, r_pt_pos, r_pt_neg);
    count_bisect += 1;
  }
  r_mpfr_temp_free(distance);
  r_mpfr_matrix_temp_free(new_pt);
}

static bool mpfr_mfd_check_line_direction_by_mapping (MPFRMatrix *pt_start, MPFRMatrix *pt_end,
                                                      MPFRCalcMfdData *data)
{
  MPFRMatrix *vec, *pt;
  MPFR *vec_size;
  int step_num, i, iteration;
  MapDirection returned_dir, check_dir = DIR_ERROR;
  bool all_same_direction = true;

  step_num = 2 * MAX_INITIAL_INTERVAL_TRIAL;

  r_mpfr_matrix_temp_alloc_init(vec, 2, 1);
  r_mpfr_matrix_temp_alloc_init(pt, 2, 1);
  r_mpfr_temp_alloc_init(vec_size);

  mpfr_set_si(vec_size, 1, MPFR_RNDN);
  mpfr_div_ui(vec_size, vec_size, step_num, MPFR_RNDN);

  mpfr_matrix_sub(vec, pt_end, pt_start);
  mpfr_matrix_mul_scalar(vec, vec, vec_size);
  mpfr_matrix_set(pt, pt_start);

  for (i = 1; i < step_num; i++) {
    mpfr_matrix_add(pt, pt, vec);
    mpfr_mfd_rigorous_direction_by_mapping(&returned_dir, &iteration, pt, data);
    if (check_dir == DIR_ERROR) {
      check_dir = returned_dir;
    } else if (check_dir != returned_dir) {
#ifdef DEBUG_OUTPUT
      printf("%d / %d: ", i, step_num);
      if (returned_dir == DIR_POS) {
	printf("Positive to ");
	if (check_dir == DIR_NEG) {
	  printf("Negative\n");
	} else {
	  printf("Error\n");
	}
      } else if (returned_dir == DIR_NEG) {
	printf("Negative to ");
	if (check_dir == DIR_POS) {
	  printf("Positive\n");
	} else {
	  printf("Error\n");
	}
      } else {
	printf("Error to ");
	if (check_dir == DIR_POS) {
	  printf("Positive\n");
	} else {
	  printf("Negative\n");
	}
      }
#endif
      check_dir = returned_dir;
      all_same_direction = false;
    }
  }

  r_mpfr_matrix_temp_free(vec);
  r_mpfr_matrix_temp_free(pt);
  r_mpfr_temp_free(vec_size);
  return all_same_direction;
}

static void mpfr_mfd_retry_initial_line_crossing_mfd (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg,
                                                      MPFRMatrix *start_pt, MPFRCalcMfdData *data)
{
  MPFR *normal_vec_mul;
  r_mpfr_temp_alloc_init(normal_vec_mul);
  mpfr_set_d(normal_vec_mul, 0.1, MPFR_RNDN);
  mpfr_matrix_mul_scalar(data->normal_vec, data->normal_vec, normal_vec_mul);
#ifdef DEBUG_OUTPUT
  printf("Retry with finer normal vector: ");
  mpfr_matrix_vector_norm(normal_vec_mul, data->normal_vec);
  mpfr_printf("New size of normal vector is %.10Re\n", normal_vec_mul);
#endif
  r_mpfr_temp_free(normal_vec_mul);
  return mpfr_mfd_initial_line_with_modification_of_normal_vector(pt_pos, pt_neg, start_pt, data);
}

/* #define CHECK_AROUND_LINE 100 */

/* static bool mpfr_mfd_check_around_initial_line (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, */
/* 					       MapDirection dir_pos, MapDirection dir_neg, */
/* 					       MPFRCalcMfdData *data) */
/* { */
/*   int i, tmp_iteration, dir_change_num = 0; */
/*   MPFRMatrix *vec, *nvec, *pt; */
/*   MPFR *ratio; */
/*   MapDirection dir, current_dir; */
/*   bool ret = true; */

/*   r_mpfr_matrix_temp_alloc_init(vec, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(nvec, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(pt, 2, 1); */
/*   r_mpfr_temp_alloc_init(ratio);   */

/*   mpfr_matrix_sub(nvec, pt_neg, pt_pos); */
/*   mpfr_set_ui(ratio, 1, GMP_RNDN); */
/*   mpfr_div_ui(ratio, ratio, CHECK_AROUND_LINE, GMP_RNDN); */
/*   mpfr_matrix_mul_scalar(nvec, nvec, ratio); */
/*   mpfr_matrix_set(pt, pt_pos); */
/*   current_dir = dir_pos; */

/*   for (i = 0; i <= CHECK_AROUND_LINE; i++) { */
/*     mpfr_matrix_add(pt, pt, nvec); */
/*     mpfr_mfd_rigorous_direction_by_mapping(&dir, &tmp_iteration, pt, data); */
/*     if (dir != current_dir) { */
/* #ifdef DEBUG_OUTPUT */
/*       printf("direction changes at %d: dir=%d\n", i, dir); */
/* #endif */
/*       printf("direction changes at %d: dir=%d\n", i, dir); */
/*       current_dir = dir; */
/*       dir_change_num += 1; */
/*       if (dir_change_num > 1) { */
/* 	ret = false; */
/* 	break; */
/*       } */
/*     } */
/*   } */

/* /\* #ifdef DEBUG_OUTPUT *\/ */
/*   printf("number of direction changes: %d\n", dir_change_num); */
/* /\* #endif *\/ */

/*   r_mpfr_matrix_temp_free(vec); */
/*   r_mpfr_matrix_temp_free(pt); */
/*   r_mpfr_temp_free(ratio); */
/*   return ret; */
/* } */

static void mpfr_mfd_initial_line_with_modification_of_normal_vector (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg,
                                                                      MPFRMatrix *start_pt, MPFRCalcMfdData *data)
{
  int tmp_iteration;
  MapDirection direction_pos, direction_neg;

  mpfr_matrix_sub(pt_pos, start_pt, data->normal_vec);
  mpfr_matrix_add(pt_neg, start_pt, data->normal_vec);

  mpfr_mfd_rigorous_direction_by_mapping(&direction_pos, &tmp_iteration, pt_pos, data);
  mpfr_mfd_rigorous_direction_by_mapping(&direction_neg, &tmp_iteration, pt_neg, data);

  if (direction_pos != direction_neg) {
    return mpfr_mfd_retry_initial_line_crossing_mfd(pt_pos, pt_neg, start_pt, data);
    /* if (DIR_POS == direction_pos) { */
    /*   if (!mpfr_mfd_check_around_initial_line(pt_pos, pt_neg, direction_pos, direction_neg, data)) { */
    /* 	return mpfr_mfd_retry_initial_line_crossing_mfd(pt_pos, pt_neg, start_pt, data); */
    /*   } */
    /* } else { */
    /*   if (!mpfr_mfd_check_around_initial_line(pt_neg, pt_pos, direction_neg, direction_pos, data)) { */
    /* 	return mpfr_mfd_retry_initial_line_crossing_mfd(pt_pos, pt_neg, start_pt, data); */
    /*   } */
    /* } */
  } else {
    MPFRMatrix *last_pt_pos, *last_pt_neg;
    MapDirection last_dir_pos, last_dir_neg;
    int i;
    r_mpfr_matrix_temp_alloc_init(last_pt_pos, 2, 1);
    r_mpfr_matrix_temp_alloc_init(last_pt_neg, 2, 1);
    last_dir_pos = direction_pos;
    last_dir_neg = direction_neg;
    i = 0;
    while (direction_pos == last_dir_pos && direction_neg == last_dir_neg) {
      if (i == MAX_INITIAL_INTERVAL_TRIAL) {
	mpfr_printf("start_pt=\n[%.Re\n %.Re]\n", mpfr_matrix_get_ptr(start_pt, 0), mpfr_matrix_get_ptr(start_pt, 1));
	printf("Can not get two points going to different direction. __LINE__=%d\n", __LINE__);
	ABORT();
      }
      mpfr_matrix_set(last_pt_pos, pt_pos);
      mpfr_matrix_set(last_pt_neg, pt_neg);
      mpfr_matrix_sub(pt_pos, last_pt_pos, data->normal_vec);
      mpfr_matrix_add(pt_neg, last_pt_neg, data->normal_vec);
      last_dir_pos = direction_pos;
      last_dir_neg = direction_neg;
      mpfr_mfd_rigorous_direction_by_mapping(&direction_pos, &tmp_iteration, pt_pos, data);
      mpfr_mfd_rigorous_direction_by_mapping(&direction_neg, &tmp_iteration, pt_neg, data);
      i += 1;
    }
#ifdef DEBUG_OUTPUT
    printf("Get initial interval aftor trying %d times.\n", i);
#endif
    if (mpfr_mfd_check_line_direction_by_mapping(last_pt_pos, last_pt_neg, data)) {
      if (direction_pos != last_dir_pos && direction_pos == DIR_POS) {
	mpfr_matrix_set(pt_neg, last_pt_pos);
      } else if (direction_pos != last_dir_pos && direction_pos == DIR_NEG) {
	mpfr_matrix_set(pt_neg, pt_pos);
	mpfr_matrix_set(pt_pos, last_pt_pos);
      } else if (direction_neg != last_dir_neg && direction_neg == DIR_POS) {
	mpfr_matrix_set(pt_pos, pt_neg);
	mpfr_matrix_set(pt_neg, last_pt_neg);
      } else if (direction_neg != last_dir_neg && direction_neg == DIR_NEG) {
	mpfr_matrix_set(pt_pos, last_pt_neg);
      } else {
	printf("Invalid result for mpfr_mfd_initial_line_crossing_mfd.\n");
	ABORT();
      }
      r_mpfr_matrix_temp_free(last_pt_pos);
      r_mpfr_matrix_temp_free(last_pt_neg);
    } else {
      r_mpfr_matrix_temp_free(last_pt_pos);
      r_mpfr_matrix_temp_free(last_pt_neg);
      return mpfr_mfd_retry_initial_line_crossing_mfd(pt_pos, pt_neg, start_pt, data);
    }
  }
}

void mpfr_mfd_initial_line_crossing_mfd (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFRMatrix *start_pt,
                                         MPFRCalcMfdData *data)
{
  int tmp_iteration;
  MapDirection direction_pos, direction_neg;

  mpfr_matrix_sub(pt_pos, start_pt, data->normal_vec);
  mpfr_matrix_add(pt_neg, start_pt, data->normal_vec);

  mpfr_mfd_rigorous_direction_by_mapping(&direction_pos, &tmp_iteration, pt_pos, data);
  mpfr_mfd_rigorous_direction_by_mapping(&direction_neg, &tmp_iteration, pt_neg, data);

  if (direction_pos != direction_neg) {
#ifdef DEBUG_OUTPUT
    printf("Get initial interval at first step.\n");
#endif
    if (direction_pos == DIR_NEG) {
      mpfr_matrix_swap(pt_pos, pt_neg);
    }
  } else {
    MPFRMatrix *last_pt_pos, *last_pt_neg;
    MapDirection last_dir_pos, last_dir_neg;
    int i;
    r_mpfr_matrix_temp_alloc_init(last_pt_pos, 2, 1);
    r_mpfr_matrix_temp_alloc_init(last_pt_neg, 2, 1);
    last_dir_pos = direction_pos;
    last_dir_neg = direction_neg;
    i = 0;
    while (direction_pos == last_dir_pos && direction_neg == last_dir_neg) {
      if (i == MAX_INITIAL_INTERVAL_TRIAL) {
	mpfr_printf("start_pt=\n[%.Re\n %.Re]\n", mpfr_matrix_get_ptr(start_pt, 0), mpfr_matrix_get_ptr(start_pt, 1));
	printf("Can not get two points going to different direction. __LINE__=%d\n", __LINE__);
	ABORT();
      }
      mpfr_matrix_set(last_pt_pos, pt_pos);
      mpfr_matrix_set(last_pt_neg, pt_neg);
      mpfr_matrix_sub(pt_pos, last_pt_pos, data->normal_vec);
      mpfr_matrix_add(pt_neg, last_pt_neg, data->normal_vec);
      last_dir_pos = direction_pos;
      last_dir_neg = direction_neg;
      mpfr_mfd_rigorous_direction_by_mapping(&direction_pos, &tmp_iteration, pt_pos, data);
      mpfr_mfd_rigorous_direction_by_mapping(&direction_neg, &tmp_iteration, pt_neg, data);
      i += 1;
    }
#ifdef DEBUG_OUTPUT
    printf("Get initial interval aftor trying %d times.\n", i);
    if (mpfr_mfd_check_line_direction_by_mapping(last_pt_pos, last_pt_neg, data)) {
      printf("All points on line go to same side.\n");
    } else {
      printf("[Error] All points on line does not go to same side.\n");
    }
#endif
    if (direction_pos != last_dir_pos && direction_pos == DIR_POS) {
      mpfr_matrix_set(pt_neg, last_pt_pos);
    } else if (direction_pos != last_dir_pos && direction_pos == DIR_NEG) {
      mpfr_matrix_set(pt_neg, pt_pos);
      mpfr_matrix_set(pt_pos, last_pt_pos);
    } else if (direction_neg != last_dir_neg && direction_neg == DIR_POS) {
      mpfr_matrix_set(pt_pos, pt_neg);
      mpfr_matrix_set(pt_neg, last_pt_neg);
    } else if (direction_neg != last_dir_neg && direction_neg == DIR_NEG) {
      mpfr_matrix_set(pt_pos, last_pt_neg);
    } else {
      printf("Invalid result for mpfr_mfd_initial_line_crossing_mfd.\n");
      ABORT();
    }
    r_mpfr_matrix_temp_free(last_pt_pos);
    r_mpfr_matrix_temp_free(last_pt_neg);
  }
}

void mpfr_mfd_line_crossing_mfd (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFR *ratio, MPFRCalcMfdData *data)
{
  MPFRMatrix *tmp[2];
  r_mpfr_matrix_temp_alloc_init(tmp[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp[1], 2, 1);

  mpfr_matrix_mul_scalar(pt_pos, data->base_vector, ratio);
  mpfr_matrix_add(pt_pos, pt_pos, data->base_two_points[0]);
  mpfr_mfd_initial_line_crossing_mfd(tmp[0], tmp[1], pt_pos, data);
  mpfr_mfd_bisect_points(pt_pos, pt_neg, tmp[0], tmp[1], data);
  
  r_mpfr_matrix_temp_free(tmp[0]);
  r_mpfr_matrix_temp_free(tmp[1]);
  
}

void mpfr_mfd_line_crossing_mfd2 (MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFRMatrix *pt_start,
                                  MPFRCalcMfdData *data)
{
  MPFRMatrix *tmp[2];
  r_mpfr_matrix_temp_alloc_init(tmp[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp[1], 2, 1);

  mpfr_mfd_initial_line_crossing_mfd(tmp[0], tmp[1], pt_start, data);
  mpfr_mfd_bisect_points(pt_pos, pt_neg, tmp[0], tmp[1], data);
  
  r_mpfr_matrix_temp_free(tmp[0]);
  r_mpfr_matrix_temp_free(tmp[1]);
}

void mpfr_mfd_get_mfd_point (MPFRMatrix *pt_res, MPFRMatrix *pt_start, MPFRCalcMfdData *data)
{
  MPFRMatrix *tmp[4];
  r_mpfr_matrix_temp_alloc_init(tmp[0], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp[1], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp[2], 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp[3], 2, 1);

  mpfr_mfd_initial_line_crossing_mfd(tmp[0], tmp[1], pt_start, data);
  mpfr_mfd_bisect_points(tmp[3], tmp[2], tmp[0], tmp[1], data);
  mpfr_vector_midpoint(pt_res, tmp[3], tmp[2]);

  r_mpfr_matrix_temp_free(tmp[0]);
  r_mpfr_matrix_temp_free(tmp[1]);
  r_mpfr_matrix_temp_free(tmp[2]);
  r_mpfr_matrix_temp_free(tmp[3]);

}

void mpfr_mfd_ratio_of_point (MPFR *ratio, MPFRMatrix *pt, MPFRCalcMfdData *data)
{
  MPFRMatrix *vec;
  r_mpfr_matrix_temp_alloc_init(vec, 2, 1);
  mpfr_matrix_sub(vec, pt, data->base_two_points[0]);
  mpfr_matrix_vector_norm(ratio, vec);
  mpfr_div(ratio, ratio, data->base_vector_size, GMP_RNDN);
  r_mpfr_matrix_temp_free(vec);
}

/*
  Inverse:
  Note that if a > 0 and -5/6 < b < 0 don't hold, this function returns nonsensical result.
  A = { (x,y) | x > 6 - 25 * a / (b * b), y > 5 }
  If a > 0 and -5/6 < b < 0 then T^{-1}(A) is included in A.

  A' = { (x,y) | x > - a / (b * b) * y * y + 6, y > 5}
  If a > b / 5 + 6 / 25 * b * b and b > 0 then T^{-1}(A') is included in A'.
 */
bool mpfr_mfd_in_divergent_region (MPFRMatrix *x, MPFRCalcMfdData *data)
{
  bool ret = false;
  if (mpfr_cmp_si(x->data + 1, 5) >= 0) {
    if (mpfr_cmp_si(mpfr_matrix_get_ptr(data->cst, 1), 0) < 0) {
      if (mpfr_cmp(x->data, data->divergent_condition[0]) > 0) {
	ret = true;
      } else {
	MPFR *tmp;
	r_mpfr_temp_alloc_init(tmp);
	mpfr_ui_sub(tmp, 6, x->data, GMP_RNDN);
	mpfr_mul(tmp, tmp, data->divergent_condition[1], GMP_RNDN);
	mpfr_sqrt(tmp, tmp, GMP_RNDN);
	if (mpfr_cmp(mpfr_matrix_get_ptr(data->cst, 1), tmp) >= 0 ) {
	  ret = true;
	}
	r_mpfr_temp_free(tmp);
      }
    } else {
      MPFR *tmp;
      r_mpfr_temp_alloc_init(tmp);
      mpfr_neg(tmp, mpfr_matrix_get_ptr(data->cst, 0), MPFR_RNDN);
      mpfr_div(tmp, tmp, mpfr_matrix_get_ptr(data->cst, 1), MPFR_RNDN);
      mpfr_div(tmp, tmp, mpfr_matrix_get_ptr(data->cst, 1), MPFR_RNDN);
      mpfr_mul(tmp, tmp, mpfr_matrix_get_ptr(x, 1), MPFR_RNDN);
      mpfr_mul(tmp, tmp, mpfr_matrix_get_ptr(x, 1), MPFR_RNDN);
      mpfr_add_si(tmp, tmp, 6, MPFR_RNDN);
      if (mpfr_cmp(mpfr_matrix_get_ptr(x, 0), tmp) > 0) {
	ret = true;
      }
      r_mpfr_temp_free(tmp);
    }
  }
  return ret;
}

int mpfr_mfd_iteration_in_non_divergent_region (MPFRMatrix *mapped, MPFRMatrix *x, MPFRCalcMfdData *data)
{
  int iteration;
  MPFRMatrix *tmp;
  r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);

  mpfr_matrix_set(mapped, x);
  for (iteration = 0; iteration <= MAX_REP_FOR_CHECK_DIVERGENCE; iteration++) {
    r_mpfr_henon_iteration(tmp, mapped, data->cst, -data->period);
    if (mpfr_mfd_in_divergent_region(tmp, data)) {
      break;
    }
    mpfr_matrix_set(mapped, tmp);
  }
  r_mpfr_matrix_temp_free(tmp);
  return iteration;
}

/*
  Henon:
  B = { (x, y) | x < -5, y < 1 }
  If b < 0, a > 7 / 25 then T(B) is included in B.

  If b > 0 then we do not check divergence of orbit rigorously.
  If the point on orbit is apart from origin by 10,
  we ragard the orbit as divergent.
*/
bool mpfr_mfd_in_divergent_region2 (MPFRMatrix *x, MPFRCalcMfdData *data)
{
  if (mpfr_cmp_si(mpfr_matrix_get_ptr(data->cst, 1), 0) < 0) {
    return (mpfr_cmp_si(x->data, -5) < 0 && mpfr_cmp_si(x->data + 1, 1) < 0);
  } else {
    return (mpfr_cmp_si(x->data, -10) < 0 || mpfr_cmp_si(x->data, 10) > 0
	    || mpfr_cmp_si(x->data + 1, -10) < 0 || mpfr_cmp_si(x->data + 1, 10) > 0);
  }
}

int mpfr_mfd_iteration_in_non_divergent_region2 (MPFRMatrix *mapped, MPFRMatrix *x, MPFRCalcMfdData *data)
{
  int iteration;
  MPFRMatrix *tmp;
  r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);

  mpfr_matrix_set(mapped, x);
  for (iteration = 0; iteration <= data->check_divergent2; iteration++) {
    r_mpfr_henon_iteration(tmp, mapped, data->cst, data->period);
    if (mpfr_mfd_in_divergent_region2(tmp, data)) {
      break;
    }
    mpfr_matrix_set(mapped, tmp);
  }
  r_mpfr_matrix_temp_free(tmp);
  return iteration;
}
