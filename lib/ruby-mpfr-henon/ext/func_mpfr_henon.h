#ifndef _FUNC_MPFR_HENON_H_
#define _FUNC_MPFR_HENON_H_

#include <stdio.h>
#include <math.h>
#include <mpfr.h>
#include "ruby_mpfr_matrix.h"
#include "../../ruby-mpfr-matrix-extra/ext/func_mpfr_matrix_extra.h"
#include "func_float_henon.h"

void r_mpfr_henon_map(MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst);
void r_mpfr_henon_inverse_map(MPFRMatrix * y, MPFRMatrix * x, MPFRMatrix * cst);
void r_mpfr_henon_iteration(MPFRMatrix * b, MPFRMatrix * a, MPFRMatrix * c, int iteration);
void r_mpfr_henon_iteration_double(MPFRMatrix * b, MPFRMatrix * a, double * c, int iteration);
void r_mpfr_henon_jacobian_matrix(MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants);
void r_mpfr_henon_jacobian_matrix_iterate_map (MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants, int iteration);
void r_mpfr_henon_fixed_point(MPFRMatrix *fp1, MPFRMatrix *fp2, MPFRMatrix *constants);
void r_mpfr_henon_cumulative_distance(MPFR *distance, MPFRMatrix *pt, MPFRMatrix *cst, int period);

void r_mpfr_henon_map2(MPFRMatrix *y, MPFRMatrix *x, MPFRMatrix *cst);
void r_mpfr_henon_inverse_map2(MPFRMatrix * y, MPFRMatrix * x, MPFRMatrix * cst);
void r_mpfr_henon_iteration2(MPFRMatrix * b, MPFRMatrix * a, MPFRMatrix * c, int iteration);
void r_mpfr_henon_iteration_double2(MPFRMatrix * b, MPFRMatrix * a, double * c, int iteration);
void r_mpfr_henon_jacobian_matrix2(MPFRMatrix *jac, MPFRMatrix *pt, MPFRMatrix *constants);
/* void r_mpfr_henon_fixed_point2(MPFRMatrix *fp1, MPFRMatrix *fp2, MPFRMatrix *constants); */
/* void r_mpfr_henon_cumulative_distance2(MPFR *distance, MPFRMatrix *pt, MPFRMatrix *cst, int period); */

#endif /* _FUNC_MPFR_HENON_H_ */
