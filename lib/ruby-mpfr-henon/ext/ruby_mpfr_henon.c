#include "ruby_mpfr_henon.h"
#include "func_float_henon.h"

#define MAX_TRY_DETECT_LIMIT_CYCLE 100

static VALUE __mpfr_class__;
static ID mpfr_henon_mapping_const_id;

static VALUE mpfr_henon_module_periodic_orbits_same_p (VALUE self, VALUE orbit1, VALUE orbit2, VALUE threshold)
{
  VALUE ret_val;
  int period, i, ind1;
  MPFRMatrix **ptr_orbit1, **ptr_orbit2;
  MPFR *thres, *distance, *tmp;

  Check_Type(orbit1, T_ARRAY);
  Check_Type(orbit2, T_ARRAY);
  if (!RTEST(rb_obj_is_instance_of(threshold, __mpfr_class__))) {
    rb_raise(rb_eArgError, "Threshold value must be MPFR.");
  }
  period = RARRAY_LEN(orbit1);
  if (period != RARRAY_LEN(orbit2)) {
    return Qfalse;
  }

  r_mpfr_get_struct(thres, threshold);
  ptr_orbit1 = (MPFRMatrix **) malloc(sizeof(MPFRMatrix *) * period);
  ptr_orbit2 = (MPFRMatrix **) malloc(sizeof(MPFRMatrix *) * period);
  for (i = 0; i < period; i++) {
    r_mpfr_get_matrix_struct(ptr_orbit1[i], RARRAY_PTR(orbit1)[i]);
    r_mpfr_get_matrix_struct(ptr_orbit2[i], RARRAY_PTR(orbit2)[i]);
  }

  r_mpfr_temp_alloc_init(distance);
  r_mpfr_temp_alloc_init(tmp);

  ind1 = 0;
  mpfr_matrix_vector_distance(distance, ptr_orbit1[0], ptr_orbit2[0]);
  for (i = 1; i < period; i++) {
    mpfr_matrix_vector_distance(tmp, ptr_orbit1[i], ptr_orbit2[0]);
    if (mpfr_cmp(tmp, distance) < 0) {
      mpfr_set(distance, tmp, GMP_RNDN);
      ind1 = i;
    }
  }

  if (mpfr_cmp(distance, thres) >= 0) {
    ret_val = Qfalse;
  } else {
    for (i = 1; i < period; i++) {
      ind1 += 1;
      if (ind1 >= period) {
	ind1 -= period;
      }
      mpfr_matrix_vector_distance(tmp, ptr_orbit1[ind1], ptr_orbit2[i]);
      mpfr_add(distance, distance, tmp, GMP_RNDN);
    }
    if (mpfr_cmp(distance, thres) < 0) {
      ret_val = Qtrue;
    } else {
      ret_val = Qfalse;
    }
  }

  r_mpfr_temp_free(distance);
  r_mpfr_temp_free(tmp);
  free(ptr_orbit1);
  free(ptr_orbit2);
  return ret_val;
}

static void mpfr_henon_mapping_free(HenonMPFRMap *ptr)
{
  mpfr_matrix_clear(ptr->cst);
  free(ptr->cst);
  free(ptr);
}

static VALUE mpfr_henon_mapping_alloc (VALUE self)
{
  HenonMPFRMap *a;
  self = Data_Make_Struct(self, HenonMPFRMap, 0, mpfr_henon_mapping_free, a);
  a->cst = ALLOC_N(MPFRMatrix, 1);
  mpfr_matrix_init(a->cst, 2, 1);
  return self;
}

static VALUE mpfr_henon_mapping_initialize (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr;
  VALUE tmp;
  MPFRMatrix *ptr_tmp;
  Data_Get_Struct(self, HenonMPFRMap, ptr);
  switch(argc) {
  case 1:
    ptr->period = 1;
    break;
  case 2:
    ptr->period = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.new is one or two.");
  }
  switch(TYPE(argv[0])) {
  case T_ARRAY:
    tmp = rb_funcall(r_mpfr_col_vector, rb_intern("new"), 1, argv[0]);
    break;
  case T_STRUCT:
    tmp = rb_funcall(r_mpfr_col_vector, rb_intern("new"), 1, rb_funcall(argv[0], rb_intern("to_a"), 0));
    break;
  default:
    tmp = argv[0];
    break;
  }
  r_mpfr_get_matrix_struct(ptr_tmp, tmp);
  if (ptr_tmp->size != 2) {
    rb_raise(rb_eArgError, "Invalid number of parameters.");
  }
  mpfr_matrix_set(ptr->cst, ptr_tmp);
  ptr->d_cst[0] = mpfr_get_d(mpfr_matrix_get_ptr(ptr->cst, 0), GMP_RNDN);
  ptr->d_cst[1] = mpfr_get_d(mpfr_matrix_get_ptr(ptr->cst, 1), GMP_RNDN);

  rb_ivar_set(self, mpfr_henon_mapping_const_id, tmp);
  return Qtrue;
}

static VALUE mpfr_henon_mapping_initialize_copy (VALUE self, VALUE other)
{
  HenonMPFRMap *ptr_self, *ptr_other;
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  Data_Get_Struct(other, HenonMPFRMap, ptr_other);

  mpfr_matrix_set(ptr_other->cst, ptr_self->cst);
  ptr_other->period = ptr_self->period;
  
  rb_ivar_set(self, mpfr_henon_mapping_const_id, rb_ivar_get(other, mpfr_henon_mapping_const_id));
  return Qtrue;
}

static VALUE mpfr_henon_mapping_period (VALUE self)
{
  HenonMPFRMap *ptr_self;
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  return INT2NUM(ptr_self->period);
}

static VALUE mpfr_henon_mapping_constants (VALUE self)
{
  HenonMPFRMap *ptr_self;
  VALUE v_cst1, v_cst2;
  MPFR *r_cst1, *r_cst2;
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_make_struct_init(v_cst1, r_cst1);
  r_mpfr_make_struct_init(v_cst2, r_cst2);
  mpfr_set(r_cst1, mpfr_matrix_get_ptr(ptr_self->cst, 0), GMP_RNDN);
  mpfr_set(r_cst2, mpfr_matrix_get_ptr(ptr_self->cst, 1), GMP_RNDN);
  return rb_ary_new3(2, v_cst1, v_cst2);
}

/*
 * @overload map(pt, num = 1)
 *  @param [MPFR::ColumnVector] pt A point to be iterated
 *  @param [Fixnum] num Number of iteration
 *  @return [MPFR::ColumnVector] A point iterated by just the argument "num" times
 * @note Number of iterations is just the argument 'num' and ignore the period of the mapping.
 */
static VALUE mpfr_henon_mapping_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfr_henon_iteration(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/*
 * @overload iterate_map(pt, num = 1)
 *  @param [MPFR::ColumnVector] pt A point to be iterated
 *  @param [Fixnum] num Number of iteration
 *  @return [MPFR::ColumnVector] A point iterated by period * the argument "num" times
 * @note Number of iterations is calculated by period * the argument 'num'.
 */
static VALUE mpfr_henon_mapping_iterate_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);

  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.iterate_map is one or two.");
  }
  r_mpfr_henon_iteration(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/*
 * @return [Integer] Maximum number of iterations, that is, (INT_MAX / period).
 */
static VALUE mpfr_henon_mapping_max_iteration (VALUE self)
{
  HenonMPFRMap *ptr_self;
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  return INT2NUM(INT_MAX / ptr_self->period);
}

static VALUE mpfr_henon_mapping_make_orbit (VALUE self, VALUE start, VALUE num)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_start, *ptr_tmp;
  int n, i;
  VALUE ret, tmp;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_start, start);
  n = NUM2INT(num);
  ret = rb_ary_new3(1, start);
  r_mpfr_matrix_suitable_matrix_init (&tmp, &ptr_tmp, 2, 1);
  mpfr_matrix_set(ptr_tmp, ptr_start);
  for (i = 1; i < n; i++) {
    r_mpfr_henon_iteration(ptr_tmp, ptr_tmp, ptr_self->cst, ptr_self->period);
    rb_ary_push(ret, r_mpfr_matrix_robj(ptr_tmp));
  }
  return ret;
}

static VALUE mpfr_henon_mapping_each_pt_on_orbit (VALUE self, VALUE start, VALUE num)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_start, *ptr_tmp;
  int n, i;
  VALUE ret, tmp;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_start, start);
  n = NUM2INT(num);
  r_mpfr_matrix_suitable_matrix_init (&tmp, &ptr_tmp, 2, 1);
  mpfr_matrix_set(ptr_tmp, ptr_start);
  ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp));
  for (i = 1; i < n; i++) {
    r_mpfr_henon_iteration(ptr_tmp, ptr_tmp, ptr_self->cst, ptr_self->period);
    ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp));
  }
  return ret;
}

/*
 * @return [MPFR::SquareMatrix] Return the jacobian matrix of mapping.
 * @note This method does not return the jacobian matrix just one iteration, i.e.,
 *     this method ignores period of the mapping.
 */
static VALUE mpfr_henon_mapping_jacobian_matrix (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 2);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfr_henon_jacobian_matrix_iterate_map(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/*
 * @return [MPFR::SquareMatrix] Return the jacobian matrix of mapping iterated specified times.
 * @note This method returns jacobian matrix iterated just specified times, i.e.,
 *     this method ignores period of the mapping.
 */
static VALUE mpfr_henon_mapping_jacobian_matrix_iterate_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 2);
  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfr_henon_jacobian_matrix_iterate_map(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

static VALUE mpfr_henon_mapping_fixed_point (VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_ret1, *ptr_ret2;
  VALUE ret1, ret2;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_matrix_suitable_matrix_init (&ret1, &ptr_ret1, 2, 1);
  r_mpfr_matrix_suitable_matrix_init (&ret2, &ptr_ret2, 2, 1);

  r_mpfr_henon_fixed_point(ptr_ret1, ptr_ret2, ptr_self->cst);
  if (mpfr_cmp(ptr_ret1->data, ptr_ret2->data) > 0) {
    return rb_ary_new3(2, ret1, ret2);
  } else {
    return rb_ary_new3(2, ret2, ret1);
  }
}

static VALUE mpfr_henon_mapping_search_sink (VALUE self, VALUE prm_args, VALUE iterate_args, VALUE threshold_args)
{
  HenonMPFRMap *ptr_self;
  VALUE ret_hash;
  MPFRMatrix *ptr_pt_start, *ptr_old, *ptr_new;
  MPFR *threshold_in_basin, *threshold_apart_basin, *sub_x, *sub_y, *x_start, *y_start, *prm_end, *step;
  int ind, sgn, init_iteration, max_iteration, i;
  bool find_pt = false;

  ret_hash = rb_hash_new();
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_start, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_old, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_new, 2, 1);

  r_mpfr_get_struct(x_start, RARRAY_PTR(prm_args)[0]);
  r_mpfr_get_struct(y_start, RARRAY_PTR(prm_args)[1]);
  mpfr_set(mpfr_matrix_get_ptr(ptr_pt_start, 0), x_start, GMP_RNDN);
  mpfr_set(mpfr_matrix_get_ptr(ptr_pt_start, 1), y_start, GMP_RNDN);
  r_mpfr_get_struct(prm_end, RARRAY_PTR(prm_args)[2]);
  r_mpfr_get_struct(step, RARRAY_PTR(prm_args)[3]);
  ind = NUM2INT(RARRAY_PTR(prm_args)[4]);
  if (ind < 0 || ind > 1) {
    rb_raise(rb_eArgError, "Invalid index argument.");
  }
  sgn = mpfr_sgn(step);
  if (sgn == 0) {
    rb_raise(rb_eArgError, "Invalid step argument.");
  } else if (sgn < 0) {
    mpfr_neg(step, step, GMP_RNDN);
  }

  init_iteration = NUM2INT(RARRAY_PTR(iterate_args)[0]) * ptr_self->period;
  max_iteration = NUM2INT(RARRAY_PTR(iterate_args)[1]);

  r_mpfr_get_struct(threshold_in_basin, RARRAY_PTR(threshold_args)[0]);
  r_mpfr_get_struct(threshold_apart_basin, RARRAY_PTR(threshold_args)[1]);

  if (mpfr_cmp(mpfr_matrix_get_ptr(ptr_pt_start, ind), prm_end) > 0) {
    mpfr_swap(mpfr_matrix_get_ptr(ptr_pt_start, ind), prm_end);
  }

  r_mpfr_temp_alloc_init(sub_x);
  r_mpfr_temp_alloc_init(sub_y);
  while (mpfr_cmp(mpfr_matrix_get_ptr(ptr_pt_start, ind), prm_end) <= 0) {
    r_mpfr_henon_iteration(ptr_old, ptr_pt_start, ptr_self->cst, init_iteration);
    for (i = 0; i < max_iteration; i++) {
      r_mpfr_henon_iteration(ptr_new, ptr_old, ptr_self->cst, ptr_self->period);
      mpfr_sub(sub_x, mpfr_matrix_get_ptr(ptr_old, 0), mpfr_matrix_get_ptr(ptr_new, 0), GMP_RNDN);
      mpfr_sub(sub_y, mpfr_matrix_get_ptr(ptr_old, 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
      if (mpfr_sgn(sub_x) < 0) {
	mpfr_neg(sub_x, sub_x, GMP_RNDN);
      }
      if (mpfr_sgn(sub_y) < 0) {
	mpfr_neg(sub_y, sub_y, GMP_RNDN);
      }
      if (mpfr_cmp(sub_x, threshold_apart_basin) > 0 && mpfr_cmp(sub_y, threshold_apart_basin) > 0) {
	break;
      } else if (mpfr_cmp(sub_x, threshold_in_basin) < 0 && mpfr_cmp(sub_x, threshold_apart_basin) < 0) {
        VALUE pt;
	MPFRMatrix *ptr_pt;
	r_mpfr_matrix_suitable_matrix_init(&pt, &ptr_pt, 2, 1);
	mpfr_matrix_set(ptr_pt, ptr_new);
        rb_hash_aset(ret_hash, ID2SYM(rb_intern("found")), pt);
	find_pt = true;
	break;
      }
      mpfr_matrix_set(ptr_old, ptr_new);
    }
    if (find_pt) {
      break;
    } else if (i == max_iteration) {
      VALUE key, pt_ary, pt;
      MPFRMatrix *ptr_pt;
      key = ID2SYM(rb_intern("not_divergent"));
      pt_ary = rb_hash_aref(ret_hash, key);
      if (!RTEST(pt_ary)) {
        pt_ary = rb_ary_new();
        rb_hash_aset(ret_hash, key, pt_ary);
      }
      r_mpfr_matrix_suitable_matrix_init(&pt, &ptr_pt, 2, 1);
      mpfr_matrix_set(ptr_pt, ptr_old);
      rb_ary_push(pt_ary, pt);
    }
    mpfr_add(mpfr_matrix_get_ptr(ptr_pt_start, ind), mpfr_matrix_get_ptr(ptr_pt_start, ind), step, GMP_RNDN);
  }

  r_mpfr_matrix_temp_free(ptr_pt_start);
  r_mpfr_matrix_temp_free(ptr_old);
  r_mpfr_matrix_temp_free(ptr_new);
  r_mpfr_temp_free(sub_x);
  r_mpfr_temp_free(sub_y);
  return ret_hash;
}

static VALUE mpfr_henon_mapping_sink_period (VALUE self, VALUE sink, VALUE threshold, VALUE max_iterate)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_sink, *ptr_pt_old, *ptr_pt_new;
  MPFR *threshold_distance, *tmp_distance;
  int max_iterate_num, i;
  VALUE ret_val;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_sink, sink);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_old, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_pt_new, 2, 1);
  r_mpfr_get_struct(threshold_distance, threshold);
  r_mpfr_temp_alloc_init(tmp_distance);
  max_iterate_num = NUM2INT(max_iterate);
  ret_val = Qnil;
  mpfr_matrix_set(ptr_pt_old, ptr_sink);
  for (i = 1; i <= max_iterate_num; i++) {
    r_mpfr_henon_map(ptr_pt_new, ptr_pt_old, ptr_self->cst);
    mpfr_sub(tmp_distance, mpfr_matrix_get_ptr(ptr_sink, 0), mpfr_matrix_get_ptr(ptr_pt_new, 0), GMP_RNDN);
    mpfr_abs(tmp_distance, tmp_distance, GMP_RNDN);
    if (mpfr_cmp(tmp_distance, threshold_distance) < 0) {
      mpfr_sub(tmp_distance, mpfr_matrix_get_ptr(ptr_sink, 1), mpfr_matrix_get_ptr(ptr_pt_new, 1), GMP_RNDN);
      mpfr_abs(tmp_distance, tmp_distance, GMP_RNDN);
      if (mpfr_cmp(tmp_distance, threshold_distance) < 0) {
	ret_val = INT2NUM(i);
	break;
      }
    }
    mpfr_matrix_set(ptr_pt_old, ptr_pt_new);
  }
  r_mpfr_matrix_temp_free(ptr_pt_old);
  r_mpfr_matrix_temp_free(ptr_pt_new);
  r_mpfr_temp_free(tmp_distance);
  return ret_val;
}

static VALUE mpfr_henon_mapping_cumulative_distance (VALUE self, VALUE pt, VALUE iterate)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_pt;
  MPFR *distance;
  VALUE v_distance;
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_make_struct_init(v_distance, distance);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_henon_cumulative_distance(distance, ptr_pt, ptr_self->cst, NUM2INT(iterate));
  return v_distance;
}

static int mpfr_henon_mapping_nearest_point(MPFRMatrix **ptr_sorted_orbit, int num_end, MPFRMatrix *pt)
{
  int num_start, i;
  num_start = 0;

  if (mpfr_cmp(mpfr_matrix_get_ptr(pt, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[0], 0)) < 0) {
    return -1;
  } else if (mpfr_cmp(mpfr_matrix_get_ptr(pt, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[num_end], 0)) > 0) {
    return num_end;
  }

  while (num_end - num_start > 1) {
    i = (num_end + num_start) / 2;
    if (mpfr_cmp(mpfr_matrix_get_ptr(pt, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[i], 0)) >= 0) {
      num_start = i;
    } else {
      num_end = i;
    }
  }

  return num_start;
}

static VALUE mpfr_henon_mapping_transient_time (VALUE self, VALUE pt, VALUE sorted_orbit, VALUE threshold, VALUE max_iterate)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix **ptr_sorted_orbit, *ptr_new, *ptr_old, *ptr_pt;
  MPFR *ptr_sub, *ptr_threshold;
  VALUE ret_val;
  int sink_period, iterate, ind, max_ind, i, max_iterate_num;
  sink_period = RARRAY_LEN(sorted_orbit);
  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  ptr_sorted_orbit = (MPFRMatrix **) malloc(sizeof(MPFRMatrix *) * sink_period);
  for (i = 0; i < sink_period; i++) {
    r_mpfr_get_matrix_struct(ptr_sorted_orbit[i], RARRAY_PTR(sorted_orbit)[i]);
  }
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_matrix_temp_alloc_init(ptr_new, 2, 1);
  r_mpfr_matrix_temp_alloc_init(ptr_old, 2, 1);
  mpfr_matrix_set(ptr_old, ptr_pt);
  r_mpfr_temp_alloc_init(ptr_sub);
  r_mpfr_get_struct(ptr_threshold, threshold);
  max_ind = sink_period - 1;
  max_iterate_num = NUM2INT(max_iterate);

  iterate = 0;
  while (true) {
    r_mpfr_henon_iteration(ptr_new, ptr_old, ptr_self->cst, ptr_self->period);
    iterate += ptr_self->period;
    if (!mpfr_number_p(mpfr_matrix_get_ptr(ptr_new, 0))) {
      ret_val = Qnil;
      break;
    }
    ind = mpfr_henon_mapping_nearest_point(ptr_sorted_orbit, max_ind, ptr_new);
    if (ind == -1) {
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[0], 0), mpfr_matrix_get_ptr(ptr_new, 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[0], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
    } else if (ind == max_ind) {
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_new, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[max_ind], 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[max_ind], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
    } else {
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[ind], 0), mpfr_matrix_get_ptr(ptr_new, 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[ind], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
      mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_new, 0), mpfr_matrix_get_ptr(ptr_sorted_orbit[ind + 1], 0), GMP_RNDN);
      if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	mpfr_sub(ptr_sub, mpfr_matrix_get_ptr(ptr_sorted_orbit[ind + 1], 1), mpfr_matrix_get_ptr(ptr_new, 1), GMP_RNDN);
	mpfr_abs(ptr_sub, ptr_sub, GMP_RNDN);
	if (mpfr_cmp(ptr_sub, ptr_threshold) < 0) {
	  ret_val = INT2NUM(iterate);
	  break;
	}
      }
    }

    if (iterate > max_iterate_num) {
      ret_val = INT2NUM(-1);
      break;
    }
    mpfr_matrix_set(ptr_old, ptr_new);
  }

  r_mpfr_matrix_temp_free(ptr_new);
  r_mpfr_matrix_temp_free(ptr_old);
  r_mpfr_temp_free(ptr_sub);
  free(ptr_sorted_orbit);
  return ret_val;
}

static VALUE mpfr_henon_double_mapping_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfr_henon_iteration_double(ptr_ret, ptr_arg, ptr_self->d_cst, iterate);
  return ret;
}

static VALUE mpfr_henon_double_mapping_iterate_map (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);

  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.iterate_map is one or two.");
  }
  r_mpfr_henon_iteration_double(ptr_ret, ptr_arg, ptr_self->d_cst, iterate);
  return ret;
}

static VALUE mpfr_henon_double_mapping_sink_period (VALUE self, VALUE sink, VALUE threshold, VALUE max_iterate)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_sink;
  MPFR *threshold_distance;
  int max_iterate_num, i;
  VALUE ret_val;
  double threshold_double, pt_sink_d[2], pt_old[2], pt_new[2];

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_sink, sink);
  r_mpfr_get_struct(threshold_distance, threshold);
  max_iterate_num = NUM2INT(max_iterate);
  threshold_double = mpfr_get_d(threshold_distance, GMP_RNDN);

  pt_sink_d[0] = mpfr_get_d(mpfr_matrix_get_ptr(ptr_sink, 0), GMP_RNDN);
  pt_sink_d[1] = mpfr_get_d(mpfr_matrix_get_ptr(ptr_sink, 1), GMP_RNDN);
  pt_old[0] = pt_sink_d[0];
  pt_old[1] = pt_sink_d[1];

  ret_val = Qnil;
  for (i = 1; i <= max_iterate_num; i++) {
    rfloat_henon_map2(pt_new, pt_old, ptr_self->d_cst);
    if ((fabs(pt_new[0] - pt_sink_d[0]) < threshold_double) &&
	(fabs(pt_new[1] - pt_sink_d[1]) < threshold_double)) {
      ret_val = INT2NUM(i);
      break;
    }
    pt_old[0] = pt_new[0];
    pt_old[1] = pt_new[1];
  }
  return ret_val;
}


/* START: Calculator of stable and unstable manifolds */

#define ITERATION_FOR_UNSTABLE_MFD -1

static VALUE sym_pos, sym_neg;
static ID eqq;
static int sym_stable_id, sym_unstable_id;

static VALUE r_mpfr_mfd_calculator_alloc (VALUE self)
{
  MPFRCalcMfdData *data;
  self = Data_Make_Struct(mpfr_mfd_calculator, MPFRCalcMfdData, 0, mpfr_clear_calc_mfd_data, data);
  mpfr_init_calc_mfd_data(data);
  return self;
}

/* arguments
  cst: array of String or MPFR
  period: Integer
  pt: MPFR::ColumnVector
  basis: array of MPFR::ColumnVector
  type: :stable or :unstable
  flip: true or nil
  error: MPFR
*/
static VALUE r_mpfr_mfd_calculator_initialize (VALUE self, VALUE cst_ary, VALUE period, VALUE pt, VALUE basis_ary, VALUE type, VALUE flip, VALUE error)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_cst, *ptr_pt, *ptr_basis1, *ptr_basis2;
  int sym;
  MfdType mtype;
  bool cflip, suitable_parameter_p;
  MPFR *ptr_error;

  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_temp_alloc_init(ptr_cst, 2, 1);

  if (RTEST(rb_funcall(__mpfr_class__, eqq, 1, rb_ary_entry(cst_ary, 0)))) {
    MPFR *ptr_cst_ary0;
    r_mpfr_get_struct(ptr_cst_ary0, rb_ary_entry(cst_ary, 0));
    mpfr_set(mpfr_matrix_get_ptr(ptr_cst, 0), ptr_cst_ary0, GMP_RNDN);
  } else {
    r_mpfr_set_robj(mpfr_matrix_get_ptr(ptr_cst, 0), rb_ary_entry(cst_ary, 0), GMP_RNDN);
  }
  if (RTEST(rb_funcall(__mpfr_class__, eqq, 1, rb_ary_entry(cst_ary, 1)))) {
    MPFR *ptr_cst_ary1;
    r_mpfr_get_struct(ptr_cst_ary1, rb_ary_entry(cst_ary, 1));
    mpfr_set(mpfr_matrix_get_ptr(ptr_cst, 1), ptr_cst_ary1, GMP_RNDN);
  } else {
    r_mpfr_set_robj(mpfr_matrix_get_ptr(ptr_cst, 1), rb_ary_entry(cst_ary, 1), GMP_RNDN);
  }

  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_get_matrix_struct(ptr_basis1, rb_ary_entry(basis_ary, 0));
  r_mpfr_get_matrix_struct(ptr_basis2, rb_ary_entry(basis_ary, 1));

  sym = rb_to_id(type);
  if (sym_stable_id == sym) {
    mtype = STABLE;
  } else if (sym_unstable_id == sym) {
    mtype = UNSTABLE;
  } else {
    rb_raise(rb_eArgError, "Invalid symbol of type.");
  }

  cflip = (RTEST(flip) ? true : false);
  r_mpfr_get_struct(ptr_error, error);
  suitable_parameter_p = mpfr_set_calc_mfd_data(data, ptr_cst, NUM2INT(period), ptr_pt, ptr_basis1, ptr_basis2, mtype, cflip, ptr_error);

  r_mpfr_matrix_temp_free(ptr_cst);
  if (!suitable_parameter_p) {
    rb_raise(rb_eArgError, "Not support to calculate invariant manifolds for the parameter.");
  }
  return Qtrue;
}

static VALUE r_mpfr_mfd_calculator_initialize_copy (VALUE self, VALUE other)
{
  MPFRCalcMfdData *ptr_self, *ptr_other;
  Data_Get_Struct(self, MPFRCalcMfdData, ptr_self);
  Data_Get_Struct(other, MPFRCalcMfdData, ptr_other);

  mpfr_set_calc_mfd_data(ptr_other, ptr_self->cst, ptr_self->period, ptr_self->pt, ptr_self->basis[0], ptr_self->basis[1],
			 ptr_self->type, ptr_self->flip, ptr_self->bisection_error);
  return Qtrue;
}

static VALUE r_mpfr_mfd_calculator_constants (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE v_cst1, v_cst2;
  MPFR *r_cst1, *r_cst2;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_make_struct_init(v_cst1, r_cst1);
  r_mpfr_make_struct_init(v_cst2, r_cst2);
  mpfr_set(r_cst1, mpfr_matrix_get_ptr(data->cst, 0), GMP_RNDN);
  mpfr_set(r_cst2, mpfr_matrix_get_ptr(data->cst, 1), GMP_RNDN);
  return rb_ary_new3(2, v_cst1, v_cst2);
}

static VALUE r_mpfr_mfd_calculator_period (VALUE self)
{
  MPFRCalcMfdData *data;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  return INT2NUM(data->period);
}

static VALUE r_mpfr_mfd_calculator_type (VALUE self)
{
  MPFRCalcMfdData *data;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  return (data->type == STABLE ? ID2SYM(sym_stable_id) : ID2SYM(sym_unstable_id));
}

static VALUE r_mpfr_mfd_calculator_pt (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE r_pt;
  MPFRMatrix *ptr_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_pt, &ptr_pt, data->pt->row, data->pt->column);
  mpfr_matrix_set(ptr_pt, data->pt);
  return r_pt;
}

static VALUE r_mpfr_mfd_calculator_basis (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE r_basis0, r_basis1;
  MPFRMatrix *c_basis0, *c_basis1;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_basis0, &c_basis0, data->basis[0]->row, data->basis[0]->column);
  r_mpfr_matrix_suitable_matrix_init(&r_basis1, &c_basis1, data->basis[1]->row, data->basis[1]->column);
  mpfr_matrix_set(c_basis0, data->basis[0]);
  mpfr_matrix_set(c_basis1, data->basis[1]);
  return rb_ary_new3(2, r_basis0, r_basis1);
}

static VALUE r_mpfr_mfd_calculator_flip (VALUE self)
{
  MPFRCalcMfdData *data;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  return (data->flip ? Qtrue : Qnil);
}

static VALUE r_mpfr_mfd_calculator_init_iteration (VALUE self)
{
  MPFRCalcMfdData *data;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  return INT2NUM(data->init_iteration);
}

static VALUE r_mpfr_mfd_calculator_criterion (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE ret[4];
  MPFR *c_val[4];
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_make_struct_init(ret[0], c_val[0]);
  r_mpfr_make_struct_init(ret[1], c_val[1]);
  r_mpfr_make_struct_init(ret[2], c_val[2]);
  r_mpfr_make_struct_init(ret[3], c_val[3]);

  mpfr_set(c_val[0], data->criterion[0], GMP_RNDN);
  mpfr_set(c_val[1], data->criterion[1], GMP_RNDN);
  mpfr_set(c_val[2], data->criterion[2], GMP_RNDN);
  mpfr_set(c_val[3], data->criterion[3], GMP_RNDN);
  return rb_ary_new4(4, ret);
}

static VALUE r_mpfr_mfd_calculator_normal_vec (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE r_vec;
  MPFRMatrix *c_vec;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_vec, &c_vec, data->normal_vec->row, data->normal_vec->column);
  mpfr_matrix_set(c_vec, data->normal_vec);
  return r_vec;
}

static VALUE r_mpfr_mfd_calculator_base_two_points (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE r_base_two_points0, r_base_two_points1;
  MPFRMatrix *c_base_two_points0, *c_base_two_points1;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_base_two_points0, &c_base_two_points0, data->base_two_points[0]->row, data->base_two_points[0]->column);
  r_mpfr_matrix_suitable_matrix_init(&r_base_two_points1, &c_base_two_points1, data->base_two_points[1]->row, data->base_two_points[1]->column);
  mpfr_matrix_set(c_base_two_points0, data->base_two_points[0]);
  mpfr_matrix_set(c_base_two_points1, data->base_two_points[1]);
  return rb_ary_new3(2, r_base_two_points0, r_base_two_points1);
}

static VALUE r_mpfr_mfd_calculator_base_vector (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE r_base_vector;
  MPFRMatrix *ptr_base_vector;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&r_base_vector, &ptr_base_vector, data->base_vector->row, data->base_vector->column);
  mpfr_matrix_set(ptr_base_vector, data->base_vector);
  return r_base_vector;
}

static VALUE r_mpfr_mfd_calculator_base_vector_size (VALUE self)
{
  MPFRCalcMfdData *data;
  VALUE r_base_vector_size;
  MPFR *c_base_vector_size;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_make_struct_init(r_base_vector_size, c_base_vector_size);
  mpfr_set(c_base_vector_size, data->base_vector_size, GMP_RNDN);
  return r_base_vector_size;
}

static VALUE r_mpfr_mfd_calculator_direction_by_mapping (VALUE self, VALUE pt, VALUE initial_iteration)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_pt;
  MapDirection ret_dir;
  int ret_iteration;
  VALUE ret_dir_val;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  mpfr_mfd_direction_by_mapping(&ret_dir, &ret_iteration, ptr_pt, NUM2INT(initial_iteration), data);
  ret_dir_val = Qnil;
  if (ret_dir == DIR_POS) {
    ret_dir_val = sym_pos;
  } else if (ret_dir == DIR_NEG) {
    ret_dir_val = sym_neg;
  }
  return rb_ary_new3(2, ID2SYM(ret_dir_val), INT2NUM(ret_iteration));
}

static VALUE r_mpfr_mfd_calculator_rigorous_direction_by_mapping (VALUE self, VALUE pt)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_pt;
  MapDirection ret_dir;
  int ret_iteration;
  VALUE ret_dir_val;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  mpfr_mfd_rigorous_direction_by_mapping(&ret_dir, &ret_iteration, ptr_pt, data);
  ret_dir_val = Qnil;
  if (ret_dir == DIR_POS) {
    ret_dir_val = sym_pos;
  } else if (ret_dir == DIR_NEG) {
    ret_dir_val = sym_neg;
  }
  return rb_ary_new3(2, ret_dir_val, INT2NUM(ret_iteration));
}

static VALUE r_mpfr_mfd_calculator_bisect_points (VALUE self, VALUE pos_pt, VALUE neg_pt)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_pos_pt, *ptr_neg_pt, *r_ptr_pos_pt, *r_ptr_neg_pt;
  VALUE v_ptr_pos_pt, v_ptr_neg_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pos_pt, pos_pt);
  r_mpfr_get_matrix_struct(ptr_neg_pt, neg_pt);
  r_mpfr_make_matrix_struct(v_ptr_pos_pt, r_ptr_pos_pt);
  r_mpfr_make_matrix_struct(v_ptr_neg_pt, r_ptr_neg_pt);
  mpfr_mfd_bisect_points(r_ptr_pos_pt, r_ptr_neg_pt, ptr_pos_pt, ptr_neg_pt, data);
  return rb_ary_new3(2, v_ptr_pos_pt, v_ptr_neg_pt);
}

static VALUE r_mpfr_mfd_calculator_initial_line_crossing_mfd (VALUE self, VALUE start_pt)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_start_pt, *r_ptr_pos_pt, *r_ptr_neg_pt;
  VALUE v_ptr_pos_pt, v_ptr_neg_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_start_pt, start_pt);
  r_mpfr_make_matrix_struct(v_ptr_pos_pt, r_ptr_pos_pt);
  r_mpfr_make_matrix_struct(v_ptr_neg_pt, r_ptr_neg_pt);
  mpfr_mfd_initial_line_crossing_mfd(r_ptr_pos_pt, r_ptr_neg_pt, ptr_start_pt, data);
  return rb_ary_new3(2, v_ptr_pos_pt, v_ptr_neg_pt);
}

static VALUE r_mpfr_mfd_calculator_line_crossing_mfd (VALUE self, VALUE ratio)
{
  MPFRCalcMfdData *data;
  MPFR *ptr_ratio;
  VALUE v_ptr_pos_pt, v_ptr_neg_pt;
  MPFRMatrix *r_ptr_pos_pt, *r_ptr_neg_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_struct(ptr_ratio, ratio);
  r_mpfr_matrix_suitable_matrix_init(&v_ptr_pos_pt, &r_ptr_pos_pt, 2, 1);
  r_mpfr_matrix_suitable_matrix_init(&v_ptr_neg_pt, &r_ptr_neg_pt, 2, 1);
  mpfr_mfd_line_crossing_mfd(r_ptr_pos_pt, r_ptr_neg_pt, ptr_ratio, data);
  return rb_ary_new3(2, v_ptr_pos_pt, v_ptr_neg_pt);
}

static VALUE r_mpfr_mfd_calculator_line_crossing_mfd2 (VALUE self, VALUE start_pt)
{
  MPFRCalcMfdData *data;
  VALUE res_pt, v_ptr_pos_pt, v_ptr_neg_pt;
  MPFRMatrix *ptr_res_pt, *ptr_start_pt, *r_ptr_pos_pt, *r_ptr_neg_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&res_pt, &ptr_res_pt, 2, 1);
  r_mpfr_get_matrix_struct(ptr_start_pt, start_pt);
  r_mpfr_matrix_suitable_matrix_init(&v_ptr_pos_pt, &r_ptr_pos_pt, 2, 1);
  r_mpfr_matrix_suitable_matrix_init(&v_ptr_neg_pt, &r_ptr_neg_pt, 2, 1);
  mpfr_mfd_line_crossing_mfd2(r_ptr_pos_pt, r_ptr_neg_pt, ptr_start_pt, data);
  return rb_ary_new3(2, v_ptr_pos_pt, v_ptr_neg_pt);
}

static VALUE r_mpfr_mfd_calculator_get_mfd_point (VALUE self, VALUE start_pt)
{
  MPFRCalcMfdData *data;
  VALUE res_pt, ratio;
  MPFRMatrix *ptr_res_pt, *ptr_start_pt, *r_ptr_pt;
  MPFR *ptr_ratio;
  VALUE v_ptr_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&res_pt, &ptr_res_pt, 2, 1);
  r_mpfr_get_matrix_struct(ptr_start_pt, start_pt);
  r_mpfr_make_struct_init(ratio, ptr_ratio);
  r_mpfr_matrix_suitable_matrix_init(&v_ptr_pt, &r_ptr_pt, 2, 1);
  mpfr_mfd_get_mfd_point(r_ptr_pt, ptr_start_pt, data);
  mpfr_mfd_ratio_of_point(ptr_ratio, r_ptr_pt, data);
  return rb_ary_new3(2, v_ptr_pt, ratio);
}

static VALUE r_mpfr_mfd_calculator_get_mfd_point_from_ratio (VALUE self, VALUE ratio)
{
  MPFRCalcMfdData *data;
  VALUE ret_pt, ret_ratio;
  MPFRMatrix *ptr_ret_pt, *tmp_pos, *tmp_neg;
  MPFR *ptr_ratio, *ptr_ret_ratio;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  r_mpfr_get_struct(ptr_ratio, ratio);
  r_mpfr_make_struct_init(ret_ratio, ptr_ret_ratio);
  r_mpfr_matrix_temp_alloc_init(tmp_pos, 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp_neg, 2, 1);
  mpfr_mfd_line_crossing_mfd(tmp_pos, tmp_neg, ptr_ratio, data);
  mpfr_vector_midpoint(ptr_ret_pt, tmp_pos, tmp_neg);
  mpfr_mfd_ratio_of_point(ptr_ret_ratio, ptr_ret_pt, data);
  r_mpfr_matrix_temp_free(tmp_pos);
  r_mpfr_matrix_temp_free(tmp_neg);
  return rb_ary_new3(2, ret_pt, ret_ratio);
}

static VALUE r_mpfr_mfd_calculator_get_pt_between_two_pts (VALUE self, VALUE pt1, VALUE pt2)
{
  MPFRCalcMfdData *data;
  VALUE ret_pt, ret_ratio;
  MPFRMatrix *ptr_ret_pt, *ptr_pt1, *ptr_pt2, *ptr_midpt;
  MPFR *ptr_ret_ratio;
  Data_Get_Struct(self, MPFRCalcMfdData, data);

  r_mpfr_matrix_suitable_matrix_init(&ret_pt, &ptr_ret_pt, 2, 1);
  r_mpfr_get_matrix_struct(ptr_pt1, pt1);
  r_mpfr_get_matrix_struct(ptr_pt2, pt2);
  r_mpfr_matrix_temp_alloc_init(ptr_midpt, 2, 1);
  r_mpfr_make_struct_init(ret_ratio, ptr_ret_ratio);

  mpfr_vector_midpoint(ptr_midpt, ptr_pt1, ptr_pt2);
  mpfr_mfd_get_mfd_point(ptr_ret_pt, ptr_midpt, data);
  mpfr_mfd_ratio_of_point(ptr_ret_ratio, ptr_ret_pt, data);

  r_mpfr_matrix_temp_free(ptr_midpt);
  return rb_ary_new3(2, ret_pt, ret_ratio);
}

static VALUE r_mpfr_mfd_calculator_get_ratio(VALUE self, VALUE pt)
{
  MPFRCalcMfdData *data;
  VALUE ret_ratio;
  MPFR *ratio;
  MPFRMatrix *ptr_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);

  r_mpfr_make_struct_init(ret_ratio, ratio);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  mpfr_mfd_ratio_of_point(ratio, ptr_pt, data);
  return ret_ratio;
}

static VALUE r_mpfr_mfd_calculator_in_divergent_region_p (VALUE self, VALUE pt)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_pt;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  if (data->type == STABLE) {
    if (mpfr_mfd_in_divergent_region(ptr_pt, data)) {
      return Qtrue;
    }
  } else {
    if (mpfr_mfd_in_divergent_region2(ptr_pt, data)) {
      return Qtrue;
    }
  }
  return Qnil;
}

static VALUE r_mpfr_mfd_calculator_iteration_in_non_divergent_region (VALUE self, VALUE pt)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_pt, *ptr_mapped;
  VALUE val;
  int iteration;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_matrix_suitable_matrix_init(&val, &ptr_mapped, 2, 1);
  if (data->type == UNSTABLE) {
    iteration = mpfr_mfd_iteration_in_non_divergent_region2(ptr_mapped, ptr_pt, data);
  } else {
    iteration = mpfr_mfd_iteration_in_non_divergent_region(ptr_mapped, ptr_pt, data);
  }
  return rb_ary_new3(2, INT2NUM(iteration), val);
}

static VALUE r_mpfr_mfd_calculator_interspace (VALUE self, VALUE pt1, VALUE pt2)
{
  MPFRMatrix *ptr_pt1, *ptr_pt2;
  VALUE val_interspace;
  MPFR *interspace;
  r_mpfr_get_matrix_struct(ptr_pt1, pt1);
  r_mpfr_get_matrix_struct(ptr_pt2, pt2);
  r_mpfr_make_struct_init(val_interspace, interspace);
  mpfr_matrix_vector_distance(interspace, ptr_pt1, ptr_pt2);
  return val_interspace;
}

static VALUE r_mpfr_mfd_calculator_make_fr_cache (VALUE self, VALUE pt, VALUE num)
{
  MPFRCalcMfdData *data;
  MPFRMatrix *ptr_pt, *ptr_ret;
  VALUE val_ret;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_matrix_suitable_matrix_init(&val_ret, &ptr_ret, 2, 1);
  r_mpfr_henon_iteration(ptr_ret, ptr_pt, data->cst, (data->type == STABLE ? -NUM2INT(num) : NUM2INT(num)));
  return val_ret;
}

static VALUE r_mpfr_mfd_calculator_set_max_iteration_for_unstable_manifold (VALUE self, VALUE num)
{
  MPFRCalcMfdData *data;
  Data_Get_Struct(self, MPFRCalcMfdData, data);
  data->check_divergent2 = NUM2INT(num);
  return Qnil;
}

/* START: Henon mapping with float calculation */


/* Arguments are [x, y], iteration and [a, b]. iteration is optional. */
static VALUE float_henon_map_base (int argc, VALUE *argv, VALUE self)
{
  double retx, rety;

  int iterate;
  switch(argc) {
  case 2:
    iterate = 1;
    break;
  case 3:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }

  rfloat_henon_iteration(&retx, &rety, NUM2DBL(rb_ary_entry(argv[0], 0)), NUM2DBL(rb_ary_entry(argv[0], 1)),
			 NUM2DBL(rb_ary_entry(argv[2], 0)), NUM2DBL(rb_ary_entry(argv[2], 1)), iterate);
  return rb_ary_new3(2, rb_float_new(retx), rb_float_new(rety));
}

/* END: Henon mapping with float calculation */

/* argv is array having one or two elements. First element is MPFR::ColumnVector instance. */
/* Second element is number of iteration. Default is 1. */
/* Return map MPFR::ColumnVector instance argv[1] times. */
static VALUE mpfr_henon_mapping_map2 (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);
  switch(argc) {
  case 1:
    iterate = 1;
    break;
  case 2:
    iterate = NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.map is one or two.");
  }
  r_mpfr_henon_iteration2(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

/* argv is array having one or two elements. First element is MPFR::ColumnVector instance. */
/* Second element is number of iteration. Default is ptr_self->period. */
/* Return map MPFR::ColumnVector instance argv[1] * ptr_self->period times. */
static VALUE mpfr_henon_mapping_iterate_map2 (int argc, VALUE *argv, VALUE self)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  int iterate;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_arg, argv[0]);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 1);

  switch(argc) {
  case 1:
    iterate = ptr_self->period;
    break;
  case 2:
    iterate = ptr_self->period * NUM2INT(argv[1]);
    break;
  default:
    rb_raise(rb_eArgError, "Number of arguments for Henon::Mapping.iterate_map is one or two.");
  }
  r_mpfr_henon_iteration2(ptr_ret, ptr_arg, ptr_self->cst, iterate);
  return ret;
}

static VALUE mpfr_henon_mapping_make_orbit2 (VALUE self, VALUE start, VALUE num)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_start, *ptr_tmp;
  int n, i;
  VALUE ret, tmp;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_start, start);
  n = NUM2INT(num);
  ret = rb_ary_new3(1, start);
  r_mpfr_matrix_suitable_matrix_init (&tmp, &ptr_tmp, 2, 1);
  mpfr_matrix_set(ptr_tmp, ptr_start);
  for (i = 1; i < n; i++) {
    r_mpfr_henon_iteration2(ptr_tmp, ptr_tmp, ptr_self->cst, ptr_self->period);
    rb_ary_push(ret, r_mpfr_matrix_robj(ptr_tmp));
  }
  return ret;
}

static VALUE mpfr_henon_mapping_each_pt_on_orbit2 (VALUE self, VALUE start, VALUE num)
{
  HenonMPFRMap *ptr_self;
  MPFRMatrix *ptr_start, *ptr_tmp;
  int n, i;
  VALUE ret, tmp;

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);
  r_mpfr_get_matrix_struct(ptr_start, start);
  n = NUM2INT(num);
  r_mpfr_matrix_suitable_matrix_init (&tmp, &ptr_tmp, 2, 1);
  mpfr_matrix_set(ptr_tmp, ptr_start);
  ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp));
  for (i = 1; i < n; i++) {
    r_mpfr_henon_iteration2(ptr_tmp, ptr_tmp, ptr_self->cst, ptr_self->period);
    ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp));
  }
  return ret;
}

/* static VALUE mpfr_henon_mapping_jacobian_matrix2 (VALUE self, VALUE arg) */
/* { */
/*   HenonMPFRMap *ptr_self; */
/*   MPFRMatrix *ptr_arg, *ptr_ret; */
/*   VALUE ret; */

/*   Data_Get_Struct(self, HenonMPFRMap, ptr_self); */
/*   r_mpfr_get_matrix_struct(ptr_arg, arg); */
/*   r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, 2, 2); */
/*   r_mpfr_henon_jacobian_matrix2(ptr_ret, ptr_arg, ptr_self->cst); */
/*   return ret; */
/* } */

static VALUE mpfr_henon_mapping2_bw_method (VALUE self, VALUE sn_ary, VALUE xn_ary, VALUE max_step_num, VALUE rk_step_size, VALUE max_force_err, VALUE max_crd_value)
{
  HenonMPFRMap *ptr_self;
  MPFR **xn_old, **xn_new, **rk[4], *rk_step, *rk_step_half, *rk_step6,
    *max_err, *max_crd, *tmp, *tmp_val;
  int period, *sn, step_num, i, j, k, index_minus, index_plus;
  VALUE ret = ID2SYM(rb_intern("unfinished"));

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);

  period = RARRAY_LEN(sn_ary);
  xn_old = (MPFR **) malloc(sizeof(MPFR *) * period);
  xn_new = (MPFR **) malloc(sizeof(MPFR *) * period);
  for (i = 0; i < 4; i++) {
    rk[i] = (MPFR **) malloc(sizeof(MPFR *) * period);
    for (j = 0; j < period; j++) {
      r_mpfr_temp_alloc_init(rk[i][j]);
    }
  }
  sn = (int *) malloc(sizeof(int) * period);
  r_mpfr_temp_alloc_init(tmp);
  for (i = 0; i < period; i++) {
    r_mpfr_temp_alloc_init(xn_old[i]);
    r_mpfr_temp_alloc_init(xn_new[i]);
    r_mpfr_get_struct(tmp_val, rb_ary_entry(xn_ary, i));
    mpfr_set(xn_old[i], tmp_val, GMP_RNDN);
    sn[i] = NUM2INT(rb_ary_entry(sn_ary, i));
  }

  step_num = NUM2INT(max_step_num);
  r_mpfr_get_struct(rk_step, rk_step_size);
  r_mpfr_get_struct(max_err, max_force_err);
  r_mpfr_get_struct(max_crd, max_crd_value);
  r_mpfr_temp_alloc_init(rk_step_half);
  r_mpfr_temp_alloc_init(rk_step6);
  mpfr_div_ui(rk_step_half, rk_step, 2, GMP_RNDN);
  mpfr_div_ui(rk_step6, rk_step, 6, GMP_RNDN);
  /* finish initialization */

  for (i = 0; i < step_num; i++) {
    for (j = 0; j < period; j++) {
      if (!mpfr_number_p(xn_old[j])) {
	break;
      } else {
	mpfr_abs(tmp, xn_old[j], GMP_RNDN);
	if (mpfr_cmp(tmp, max_crd) > 0) {
	  break;
	}
      }

      index_minus = (j == 0 ? period - 1 : j - 1);
      index_plus = (j == (period - 1) ? 0 : j + 1);
      mpfr_mul(tmp, xn_old[j], xn_old[j], GMP_RNDN);
      mpfr_mul(rk[0][j], mpfr_matrix_get_ptr(ptr_self->cst, 1), xn_old[index_minus], GMP_RNDN);
      mpfr_sub(rk[0][j], rk[0][j], tmp, GMP_RNDN);
      mpfr_sub(rk[0][j], rk[0][j], xn_old[index_plus], GMP_RNDN);
      mpfr_add(rk[0][j], rk[0][j], mpfr_matrix_get_ptr(ptr_self->cst, 0), GMP_RNDN);
      mpfr_mul_si(rk[0][j], rk[0][j], sn[j], GMP_RNDN);
    }

    if (j < period) {
      ret = ID2SYM(rb_intern("divergent"));
      break;
    }

    for (j = 0; j < period; j++) {
      mpfr_abs(tmp, rk[0][j], GMP_RNDN);
      if (mpfr_cmp(tmp, max_err) > 0) {
    	break;
      }
    }
    if (j == period) {
      ret = rb_ary_new3(0);
      /* I have not implemented of detection of a limit cycle. */
      rb_ary_push(ret, ID2SYM(rb_intern("convergent")));
      for (k = 0; k < period; k++) {
    	rb_ary_push(ret, r_mpfr_make_new_fr_obj(xn_old[k]));
      }
      break;
    }

    for (k = 0; k < 2; k++) {
      for (j = 0; j < period; j++) {
    	mpfr_mul(xn_new[j], rk[k][j], rk_step_half, GMP_RNDN);
    	mpfr_add(xn_new[j], xn_new[j], xn_old[j], GMP_RNDN);
      }

      for (j = 0; j < period; j++) {
	index_minus = (j == 0 ? period - 1 : j - 1);
	index_plus = (j == (period - 1) ? 0 : j + 1);
    	mpfr_mul(tmp, xn_new[j], xn_new[j], GMP_RNDN);
    	mpfr_mul(rk[k + 1][j], mpfr_matrix_get_ptr(ptr_self->cst, 1), xn_new[index_minus], GMP_RNDN);
    	mpfr_sub(rk[k + 1][j], rk[k + 1][j], tmp, GMP_RNDN);
    	mpfr_sub(rk[k + 1][j], rk[k + 1][j], xn_new[index_plus], GMP_RNDN);
    	mpfr_add(rk[k + 1][j], rk[k + 1][j], mpfr_matrix_get_ptr(ptr_self->cst, 0), GMP_RNDN);
    	mpfr_mul_si(rk[k + 1][j], rk[k + 1][j], sn[j], GMP_RNDN);
      }
    }

    for (j = 0; j < period; j++) {
      mpfr_mul(xn_new[j], rk[2][j], rk_step, GMP_RNDN);
      mpfr_add(xn_new[j], xn_new[j], xn_old[j], GMP_RNDN);
    }

    for (j = 0; j < period; j++) {
      index_minus = (j == 0 ? period - 1 : j - 1);
      index_plus = (j == (period - 1) ? 0 : j + 1);
      mpfr_mul(tmp, xn_new[j], xn_new[j], GMP_RNDN);
      mpfr_mul(rk[3][j], mpfr_matrix_get_ptr(ptr_self->cst, 1), xn_new[index_minus], GMP_RNDN);
      mpfr_sub(rk[3][j], rk[3][j], tmp, GMP_RNDN);
      mpfr_sub(rk[3][j], rk[3][j], xn_new[index_plus], GMP_RNDN);
      mpfr_add(rk[3][j], rk[3][j], mpfr_matrix_get_ptr(ptr_self->cst, 0), GMP_RNDN);
      mpfr_mul_si(rk[3][j], rk[3][j], sn[j], GMP_RNDN);
    }

    for (j = 0; j < period; j++) {
      mpfr_add(xn_new[j], rk[0][j], rk[3][j], GMP_RNDN);
      mpfr_add(tmp, rk[1][j], rk[2][j], GMP_RNDN);
      mpfr_mul_ui(tmp, tmp, 2, GMP_RNDN);
      mpfr_add(xn_new[j], xn_new[j], tmp, GMP_RNDN);
      mpfr_mul(xn_new[j], xn_new[j], rk_step6, GMP_RNDN);
      mpfr_add(xn_old[j], xn_new[j], xn_old[j], GMP_RNDN);
    }
  }

  /* free variables */
  r_mpfr_temp_free(rk_step_half);
  r_mpfr_temp_free(rk_step6);
  for (i = 0; i < 4; i++) {
    for (j = 0; j < period; j++) {
      r_mpfr_temp_free(rk[i][j]);
    }
  }
  for (i = 0; i < 4; i++) {
    free(rk[i]);
  }
  for (i = 0; i < period; i++) {
    r_mpfr_temp_free(xn_old[i]);
    r_mpfr_temp_free(xn_new[i]);
  }
  free(xn_old);
  free(xn_new);
  free(sn);
  r_mpfr_temp_free(tmp);
  return ret;
}

static VALUE mpfr_henon_mapping2_bw_method_double (VALUE self, VALUE sn_ary, VALUE xn_ary, VALUE max_step_num, VALUE rk_step_size, VALUE max_force_err, VALUE max_crd_value)
{
  HenonMPFRMap *ptr_self;
  double *xn_old, *xn_new, *xn_result, *rk[4], rk_step, rk_step_half, rk_step6, max_err, max_crd, prm[2];
  MPFR *tmp_val;
  int period, *sn, step_num, i, j, k, index_minus, index_plus, try_detect_limit_cycle = 0;
  VALUE ret = ID2SYM(rb_intern("unfinished"));

  Data_Get_Struct(self, HenonMPFRMap, ptr_self);

  prm[0] = mpfr_get_d(mpfr_matrix_get_ptr(ptr_self->cst, 0), GMP_RNDN);
  prm[1] = mpfr_get_d(mpfr_matrix_get_ptr(ptr_self->cst, 1), GMP_RNDN);
  period = RARRAY_LEN(sn_ary);
  xn_old = (double *) malloc(sizeof(double) * period);
  xn_new = (double *) malloc(sizeof(double) * period);
  xn_result = NULL;
  for (i = 0; i < 4; i++) {
    rk[i] = (double *) malloc(sizeof(double) * period);
  }
  sn = (int *) malloc(sizeof(int) * period);
  for (i = 0; i < period; i++) {
    r_mpfr_get_struct(tmp_val, rb_ary_entry(xn_ary, i));
    xn_old[i] = mpfr_get_d(tmp_val, GMP_RNDN);
    sn[i] = NUM2INT(rb_ary_entry(sn_ary, i));
  }

  step_num = NUM2INT(max_step_num);
  r_mpfr_get_struct(tmp_val, rk_step_size);
  rk_step = mpfr_get_d(tmp_val, GMP_RNDN);
  r_mpfr_get_struct(tmp_val, max_force_err);
  max_err = mpfr_get_d(tmp_val, GMP_RNDN);
  r_mpfr_get_struct(tmp_val, max_crd_value);
  max_crd = mpfr_get_d(tmp_val, GMP_RNDN);
  rk_step_half = rk_step / 2.0;
  rk_step6 = rk_step / 6.0;
  /* finish initialization */

  for (i = 0; i < step_num; i++) {
    for (j = 0; j < period; j++) {
      if (xn_old[j] != xn_old[j] || (fabs(xn_old[j]) > max_crd)) { /* check nan and inf*/
  	break;
      }

      index_minus = (j == 0 ? period - 1 : j - 1);
      index_plus = (j == (period - 1) ? 0 : j + 1);
      rk[0][j] = sn[j] * (- xn_old[index_plus] + prm[0] - xn_old[j] * xn_old[j] + prm[1] * xn_old[index_minus]);
    }

    if (j < period) {
      ret = ID2SYM(rb_intern("divergent"));
      break;
    }

    if (xn_result) {
      if (try_detect_limit_cycle < MAX_TRY_DETECT_LIMIT_CYCLE) {
	for (k = 0; k < period; k++) {
	  /* max_err is not a threshold value for detection of a limit cycle.
	     If this code does not work then I should add an appropriate threshold value.
	     (Mon Feb  6 11:06:25 2012) */
	  if (fabs(xn_old[k] - xn_result[k]) > max_err) {
	    break;
	  }
	}
	if (k < period) {
	  rb_ary_store(ret, 0, ID2SYM(rb_intern("limit_cycle")));
	  free(xn_result);
	  break;
	} else {
	  try_detect_limit_cycle += 1;
	}
      } else {
	free(xn_result);
	break;
      }
    } else {
      for (j = 0; j < period; j++) {
	if (rk[0][j] > max_err) {
	  break;
	}
      }
      if (j == period) {
	ret = rb_ary_new3(0);
	rb_ary_push(ret, ID2SYM(rb_intern("convergent")));
	for (k = 0; k < period; k++) {
	  rb_ary_push(ret, rb_float_new(xn_old[k]));
	}
	xn_result = (double *) malloc(sizeof(double) * period);
	for (k = 0; k < period; k++) {
	  xn_result[k] = xn_old[k];
	}
      }
    }

    for (k = 0; k < 2; k++) {
      for (j = 0; j < period; j++) {
    	xn_new[j] = rk[k][j] * rk_step_half + xn_old[j];
      }

      for (j = 0; j < period; j++) {
    	index_minus = (j == 0 ? period - 1 : j - 1);
    	index_plus = (j == (period - 1) ? 0 : j + 1);
	rk[k + 1][j] = sn[j] * (- xn_new[index_plus] + prm[0] - xn_new[j] * xn_new[j] + prm[1] * xn_new[index_minus]);
      }
    }

    for (j = 0; j < period; j++) {
      xn_new[j] = rk[2][j] * rk_step + xn_old[j];
    }

    for (j = 0; j < period; j++) {
      index_minus = (j == 0 ? period - 1 : j - 1);
      index_plus = (j == (period - 1) ? 0 : j + 1);
      rk[3][j] = sn[j] * (- xn_new[index_plus] + prm[0] - xn_new[j] * xn_new[j] + prm[1] * xn_new[index_minus]);
    }

    for (j = 0; j < period; j++) {
      xn_old[j] = xn_old[j] + (rk[0][j] + rk[3][j] + 2.0 * (rk[1][j] + rk[2][j])) * rk_step6;
    }
  }

  /* free variables */
  for (i = 0; i < 4; i++) {
    free(rk[i]);
  }
  free(xn_old);
  free(xn_new);
  free(sn);
  return ret;
}

void Init_ruby_mpfr_henon()
{
  __mpfr_class__ = rb_define_class("MPFR", rb_cNumeric);
  /* __mpfr_class__ = rb_eval_string("MPFR"); */

  /* START: Basic methods of Henon mapping */

  mpfr_henon_module = rb_define_module_under(__mpfr_class__, "Henon");
  mpfr_henon_mapping = rb_define_class_under(mpfr_henon_module, "Mapping", rb_cObject);
  rb_define_alloc_func(mpfr_henon_mapping, mpfr_henon_mapping_alloc);
  rb_define_private_method(mpfr_henon_mapping, "initialize", mpfr_henon_mapping_initialize, -1);
  rb_define_private_method(mpfr_henon_mapping, "initialize_copy", mpfr_henon_mapping_initialize_copy, 1);

  rb_define_method(mpfr_henon_mapping, "period", mpfr_henon_mapping_period, 0);
  rb_define_method(mpfr_henon_mapping, "constants", mpfr_henon_mapping_constants, 0);
  rb_define_method(mpfr_henon_mapping, "map", mpfr_henon_mapping_map, -1);
  rb_define_method(mpfr_henon_mapping, "iterate_map", mpfr_henon_mapping_iterate_map, -1);
  rb_define_method(mpfr_henon_mapping, "max_iteration", mpfr_henon_mapping_max_iteration, 0);
  rb_define_method(mpfr_henon_mapping, "make_orbit", mpfr_henon_mapping_make_orbit, 2);
  rb_define_method(mpfr_henon_mapping, "each_pt_on_orbit", mpfr_henon_mapping_each_pt_on_orbit, 2);
  rb_define_method(mpfr_henon_mapping, "jacobian_matrix", mpfr_henon_mapping_jacobian_matrix, -1);
  rb_define_method(mpfr_henon_mapping, "jacobian_matrix_iterate_map", mpfr_henon_mapping_jacobian_matrix_iterate_map, -1);
  rb_define_method(mpfr_henon_mapping, "fixed_point", mpfr_henon_mapping_fixed_point, 0);

  /* END: Basic methods of Henon mapping */

  rb_define_method(mpfr_henon_mapping, "search_sink", mpfr_henon_mapping_search_sink, 3);
  rb_define_method(mpfr_henon_mapping, "sink_period", mpfr_henon_mapping_sink_period, 3);
  rb_define_method(mpfr_henon_mapping, "cumulative_distance", mpfr_henon_mapping_cumulative_distance, 2);
  rb_define_method(mpfr_henon_mapping, "transient_time", mpfr_henon_mapping_transient_time, 4);

  mpfr_henon_mapping_const_id = rb_intern("@const");

  rb_define_singleton_method(mpfr_henon_module, "periodic_orbits_same?", mpfr_henon_module_periodic_orbits_same_p, 3);

  mpfr_henon_double_mapping = rb_define_class_under(mpfr_henon_module, "DoubleMapping", mpfr_henon_mapping);
  rb_define_method(mpfr_henon_double_mapping, "map", mpfr_henon_double_mapping_map, -1);
  rb_define_method(mpfr_henon_double_mapping, "iterate_map", mpfr_henon_double_mapping_iterate_map, -1);
  rb_define_method(mpfr_henon_double_mapping, "sink_period", mpfr_henon_double_mapping_sink_period, 3);

  /* START: Calculator of stable and unstable manifolds */

  mpfr_mfd_calculator = rb_define_class_under(mpfr_henon_module, "MfdCalculator", rb_cObject);
  rb_define_alloc_func(mpfr_mfd_calculator, r_mpfr_mfd_calculator_alloc);
  rb_define_private_method(mpfr_mfd_calculator, "initialize", r_mpfr_mfd_calculator_initialize, 7);
  rb_define_private_method(mpfr_mfd_calculator, "initialize_copy", r_mpfr_mfd_calculator_initialize_copy, 1);

  rb_define_method(mpfr_mfd_calculator, "constants", r_mpfr_mfd_calculator_constants, 0);
  rb_define_method(mpfr_mfd_calculator, "period", r_mpfr_mfd_calculator_period, 0);
  rb_define_method(mpfr_mfd_calculator, "type", r_mpfr_mfd_calculator_type, 0);
  rb_define_method(mpfr_mfd_calculator, "pt", r_mpfr_mfd_calculator_pt, 0);
  rb_define_method(mpfr_mfd_calculator, "basis", r_mpfr_mfd_calculator_basis, 0);
  rb_define_method(mpfr_mfd_calculator, "flip", r_mpfr_mfd_calculator_flip, 0);
  rb_define_method(mpfr_mfd_calculator, "init_iteration", r_mpfr_mfd_calculator_init_iteration, 0);
  rb_define_method(mpfr_mfd_calculator, "criterion", r_mpfr_mfd_calculator_criterion, 0);
  rb_define_method(mpfr_mfd_calculator, "normal_vec", r_mpfr_mfd_calculator_normal_vec, 0);
  rb_define_method(mpfr_mfd_calculator, "base_two_points", r_mpfr_mfd_calculator_base_two_points, 0);
  rb_define_method(mpfr_mfd_calculator, "base_vector", r_mpfr_mfd_calculator_base_vector, 0);
  rb_define_method(mpfr_mfd_calculator, "base_vector_size", r_mpfr_mfd_calculator_base_vector_size, 0);

  rb_define_method(mpfr_mfd_calculator, "direction_by_mapping", r_mpfr_mfd_calculator_direction_by_mapping, 2);
  rb_define_method(mpfr_mfd_calculator, "rigorous_direction_by_mapping", r_mpfr_mfd_calculator_rigorous_direction_by_mapping, 1);
  rb_define_method(mpfr_mfd_calculator, "bisect_points", r_mpfr_mfd_calculator_bisect_points, 2);
  rb_define_method(mpfr_mfd_calculator, "initial_line_crossing_mfd", r_mpfr_mfd_calculator_initial_line_crossing_mfd, 1);
  rb_define_method(mpfr_mfd_calculator, "line_crossing_mfd", r_mpfr_mfd_calculator_line_crossing_mfd, 1);
  rb_define_method(mpfr_mfd_calculator, "line_crossing_mfd2", r_mpfr_mfd_calculator_line_crossing_mfd2, 1);
  rb_define_method(mpfr_mfd_calculator, "get_mfd_point", r_mpfr_mfd_calculator_get_mfd_point, 1);
  rb_define_method(mpfr_mfd_calculator, "get_mfd_point_from_ratio", r_mpfr_mfd_calculator_get_mfd_point_from_ratio, 1);
  rb_define_method(mpfr_mfd_calculator, "get_pt_between_two_pts", r_mpfr_mfd_calculator_get_pt_between_two_pts, 2);
  rb_define_method(mpfr_mfd_calculator, "get_ratio", r_mpfr_mfd_calculator_get_ratio, 1);
  rb_define_method(mpfr_mfd_calculator, "in_divergent_region?", r_mpfr_mfd_calculator_in_divergent_region_p, 1);
  rb_define_method(mpfr_mfd_calculator, "iteration_in_non_divergent_region", r_mpfr_mfd_calculator_iteration_in_non_divergent_region, 1);
  rb_define_method(mpfr_mfd_calculator, "interspace", r_mpfr_mfd_calculator_interspace, 2);
  rb_define_method(mpfr_mfd_calculator, "make_fr_cache", r_mpfr_mfd_calculator_make_fr_cache, 2);
  rb_define_method(mpfr_mfd_calculator, "set_max_iteration_for_unstable_manifold",
		   r_mpfr_mfd_calculator_set_max_iteration_for_unstable_manifold, 1);
  
  sym_stable_id = rb_intern("stable");
  sym_unstable_id = rb_intern("unstable");
  eqq = rb_intern("===");
  sym_pos = rb_intern("pos");
  sym_neg = rb_intern("neg");

  /* END: Calculator of stable and unstable manifolds */

  /* START: Henon mapping with float calculation */

  float_henon = rb_define_module("FloatHenon");
  rb_define_singleton_method(float_henon, "iterate_map_base", float_henon_map_base, -1);

  /* END: Henon mapping with float calculation */  


  /* START: Another type of Henon map */

  mpfr_henon_mapping2 = rb_define_class_under(mpfr_henon_module, "Mapping2", rb_cObject);
  rb_define_alloc_func(mpfr_henon_mapping2, mpfr_henon_mapping_alloc);
  rb_define_private_method(mpfr_henon_mapping2, "initialize", mpfr_henon_mapping_initialize, -1);
  rb_define_private_method(mpfr_henon_mapping2, "initialize_copy", mpfr_henon_mapping_initialize_copy, 1);

  rb_define_method(mpfr_henon_mapping2, "period", mpfr_henon_mapping_period, 0);
  rb_define_method(mpfr_henon_mapping2, "constants", mpfr_henon_mapping_constants, 0);
  rb_define_method(mpfr_henon_mapping2, "map", mpfr_henon_mapping_map2, -1);
  rb_define_method(mpfr_henon_mapping2, "iterate_map", mpfr_henon_mapping_iterate_map2, -1);
  rb_define_method(mpfr_henon_mapping2, "make_orbit", mpfr_henon_mapping_make_orbit2, 2);
  rb_define_method(mpfr_henon_mapping2, "each_pt_on_orbit", mpfr_henon_mapping_each_pt_on_orbit2, 2);
  /* rb_define_method(mpfr_henon_mapping2, "jacobian_matrix", mpfr_henon_mapping_jacobian_matrix2, 1); */
  /* rb_define_method(mpfr_henon_mapping2, "jacobian_matrix_iterate_map", mpfr_henon_mapping_jacobian_matrix_iterate_map, 2); */
  /* rb_define_method(mpfr_henon_mapping2, "fixed_point", mpfr_henon_mapping_fixed_point, 0); */
  rb_define_method(mpfr_henon_mapping2, "bw_method", mpfr_henon_mapping2_bw_method, 6);
  rb_define_method(mpfr_henon_mapping2, "bw_method_double", mpfr_henon_mapping2_bw_method_double, 6);

  /* END: Basic methods of Henon mapping */

  /* rb_define_method(mpfr_henon_mapping2, "search_sink", mpfr_henon_mapping_search_sink, 3); */
  /* rb_define_method(mpfr_henon_mapping2, "sink_period", mpfr_henon_mapping_sink_period, 3); */
  /* rb_define_method(mpfr_henon_mapping2, "cumulative_distance", mpfr_henon_mapping_cumulative_distance, 2); */
  /* rb_define_method(mpfr_henon_mapping2, "transient_time", mpfr_henon_mapping_transient_time, 4); */
}
