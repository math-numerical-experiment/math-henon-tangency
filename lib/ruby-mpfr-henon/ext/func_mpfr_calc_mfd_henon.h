#ifndef _FUNC_MPFR_CALC_MFD_HENON_H_
#define _FUNC_MPFR_CALC_MFD_HENON_H_

#include <stdio.h>
#include <stdbool.h>
#include <mpfr.h>
#include "ruby_mpfr.h"
#include "ruby_mpfr_matrix.h"
#include "func_mpfr_henon.h"

typedef enum _MfdType { STABLE, UNSTABLE } MfdType;
typedef enum _MapDirection { DIR_ERROR = -1, DIR_POS = 0, DIR_NEG = 1, DIR_UNDECIDED = 2, DIR_ARY_SIZE = 3 } MapDirection;


typedef struct _MPFRCalcMfdData {
  MPFRMatrix *cst;
  int period;
  MfdType type;
  int iteration_for_calc;
  MPFRMatrix *pt;
  MPFRMatrix *basis[2];
  bool flip;
  MPFR *bisection_error;
  int max_bisection;

  int init_iteration;
  MPFR *criterion[4];
  MPFRMatrix *normal_vec;
  MPFRMatrix *base_two_points[2];
  MPFRMatrix *base_vector;

  MPFR *base_vector_size;
  MPFR *divergent_condition[2];
  int check_divergent2;
} MPFRCalcMfdData;

void mpfr_init_calc_mfd_data(MPFRCalcMfdData *data);
void mpfr_clear_calc_mfd_data(MPFRCalcMfdData *data);
bool mpfr_set_calc_mfd_data(MPFRCalcMfdData *data, MPFRMatrix *cst, int period, MPFRMatrix *pt, MPFRMatrix *basis1, MPFRMatrix *basis2,
			    MfdType type, bool flip, MPFR *bisection_error);
void mpfr_mfd_direction_by_mapping(MapDirection *ret_dir, int *ret_iteration, MPFRMatrix *pt_arg, int initial_iteration, MPFRCalcMfdData *data);
void mpfr_mfd_rigorous_direction_by_mapping(MapDirection *ret_dir, int *ret_iteration, MPFRMatrix *pt_arg, MPFRCalcMfdData *data);
void mpfr_mfd_bisect_points(MPFRMatrix *r_pos_pt, MPFRMatrix *r_neg_pt, MPFRMatrix *pos_pt, MPFRMatrix *neg_pt, MPFRCalcMfdData *data);
void mpfr_mfd_initial_line_crossing_mfd(MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFRMatrix *start_pt, MPFRCalcMfdData *data);
void mpfr_mfd_line_crossing_mfd(MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFR *ratio, MPFRCalcMfdData *data);
void mpfr_mfd_line_crossing_mfd2(MPFRMatrix *pt_pos, MPFRMatrix *pt_neg, MPFRMatrix *pt_start, MPFRCalcMfdData *data);
void mpfr_mfd_get_mfd_point(MPFRMatrix *pt_res, MPFRMatrix *pt_start, MPFRCalcMfdData *data);
void mpfr_mfd_ratio_of_point(MPFR *ratio, MPFRMatrix *pt, MPFRCalcMfdData *data);
bool mpfr_mfd_in_divergent_region(MPFRMatrix *x, MPFRCalcMfdData *data);
int mpfr_mfd_iteration_in_non_divergent_region(MPFRMatrix *mapped, MPFRMatrix *x, MPFRCalcMfdData *data);
bool mpfr_mfd_in_divergent_region2(MPFRMatrix *x, MPFRCalcMfdData *data);
int mpfr_mfd_iteration_in_non_divergent_region2(MPFRMatrix *mapped, MPFRMatrix *x, MPFRCalcMfdData *data);

#endif /* _FUNC_MPFR_CALC_MFD_HENON_H_ */

