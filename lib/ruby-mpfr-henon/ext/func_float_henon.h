#ifndef _FUNC_FLOAT_HENON_H_
#define _FUNC_FLOAT_HENON_H_

void rfloat_henon_map(double *retx, double *rety, double x, double y, double a, double b);
void rfloat_henon_inverse_map(double *retx, double *rety, double x, double y, double a, double b);
void rfloat_henon_iteration(double *retx, double *rety, double x, double y, double a, double b, int iteration);

void rfloat_henon_map2(double *ret, double *x, double *cst);
void rfloat_henon_inverse_map2(double *ret, double *x, double *cst);
void rfloat_henon_iteration2(double *ret, double *x, double *cst, int iteration);

#endif /* _FUNC_FLOAT_HENON_H_ */
