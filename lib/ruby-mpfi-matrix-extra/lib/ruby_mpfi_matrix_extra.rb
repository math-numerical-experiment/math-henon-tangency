require 'mpfi'
require 'mpfi/complex'
require 'mpfi/matrix'
require 'mpfi_matrix_extra'

require 'yaml'

class MPFI
  class Matrix
    def to_yaml(opts={})
      YAML::quick_emit(object_id, opts) do |out|
        out.scalar("tag:yt,2009-03-12:#{self.class}", str_ary_for_inspect2.to_yaml)
      end
    end

  end

  module Vector

    def to_yaml(opts={})
      YAML::quick_emit(object_id, opts) do |out|
        out.scalar("tag:yt,2009-03-12:#{self.class}", str_ary_for_inspect.to_yaml)
      end
    end

  end

end

YAML.add_domain_type("yt,2009-03-12", "MPFI::ColumnVector") { |type, val|
  MPFI::ColumnVector.new(YAML.load(val))
}

YAML.add_domain_type("yt,2009-03-12", "MPFI::RowVector") { |type, val|
  MPFI::RowVector.new(YAML.load(val))
}

YAML.add_domain_type("yt,2009-03-12", "MPFI::Matrix") { |type, val|
  MPFI::Matrix.new(YAML.load(val))
}

YAML.add_domain_type("yt,2009-03-12", "MPFI::SquareMatrix") { |type, val|
  MPFI::SquareMatrix.new(YAML.load(val))
}
