#ifndef _FUNC_MPFI_MATRIX_EXTRA_H_
#define _FUNC_MPFI_MATRIX_EXTRA_H_

#include <stdio.h>
#include <ruby.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>
#include "ruby_mpfr_matrix.h"
#include "ruby_mpfi.h"
#include "ruby_mpfi_complex.h"
#include "ruby_mpfi_matrix.h"

int mpfi_vec2_normal_vector(MPFIMatrix *new, MPFIMatrix *x);
void mpfi_vec2_project(MPFIMatrix *a, MPFIMatrix *w, MPFIMatrix *u, MPFIMatrix *v);

int mpfi_2d_square_matrix_inverse_matrix(MPFIMatrix *inv, MPFIMatrix *x);
int mpfi_2d_square_matrix_eigenvalue(MPFI *val1, MPFI *val2, MPFIMatrix *x);
void mpfi_2d_square_matrix_real_eigenvector(MPFIMatrix *vec, MPFIMatrix *x, MPFI *eigenval);

#endif /* _FUNC_MPFI_MATRIX_EXTRA_H_ */

