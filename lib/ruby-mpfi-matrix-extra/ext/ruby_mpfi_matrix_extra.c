#include "ruby_mpfi_matrix_extra.h"

static VALUE r_mpfi_vector_2d_normal_vector (VALUE self, VALUE arg)
{
  MPFIMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  r_mpfi_get_matrix_struct(ptr_arg, arg);
  r_mpfi_matrix_suitable_matrix_init (&ret, &ptr_ret, ptr_arg->row, ptr_arg->column);
  if (ptr_arg->size == 2) {
    if (mpfi_vec2_normal_vector(ptr_ret, ptr_arg) == 0) {
      return ret;
    } else {
      return Qnil;
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
}

static VALUE r_mpfi_vector_2d_project (VALUE self, VALUE arg1, VALUE arg2, VALUE arg3)
{
  VALUE ret_val;
  MPFIMatrix *ptr_return, *ptr_arg1, *ptr_arg2, *ptr_arg3;
  r_mpfi_matrix_suitable_matrix_init (&ret_val, &ptr_return, 2, 1);
  r_mpfi_get_matrix_struct(ptr_arg1, arg1);
  r_mpfi_get_matrix_struct(ptr_arg2, arg2);
  r_mpfi_get_matrix_struct(ptr_arg3, arg3);
  mpfi_vec2_project(ptr_return, ptr_arg1, ptr_arg2, ptr_arg3);
  return ret_val;
}

static VALUE r_mpfi_square_matrix_inverse (VALUE self)
{
  MPFIMatrix *ptr_self, *ptr_ret;
  VALUE ret;
  r_mpfi_get_matrix_struct(ptr_self, self);
  if (ptr_self->column == 2 && ptr_self->row == 2) {
    r_mpfi_matrix_suitable_matrix_init (&ret, &ptr_ret, ptr_self->row, ptr_self->column);
    if (mpfi_2d_square_matrix_inverse_matrix(ptr_ret, ptr_self) == 1) {
      return Qnil;
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
  return ret;
}

static VALUE r_mpfi_square_matrix_eigenvalue (VALUE self)
{
  MPFIMatrix *ptr_self;
  MPFI *ptr_ret1, *ptr_ret2;
  VALUE ret1, ret2;
  int res;
  r_mpfi_get_matrix_struct(ptr_self, self);
  if (ptr_self->row == 2) {
    r_mpfi_make_struct_init(ret1, ptr_ret1);
    r_mpfi_make_struct_init(ret2, ptr_ret2);
    res = mpfi_2d_square_matrix_eigenvalue(ptr_ret1, ptr_ret2, ptr_self);
    if (res > 0) {
      return rb_ary_new3(2, ret1, ret2);
    } else if (res == 0) {
      return rb_ary_new3(1, ret1);
    } else {
      MPFIComplex *c1, *c2;
      VALUE c_ret1, c_ret2;
      r_mpfi_make_complex_struct_init(c_ret1, c1);
      r_mpfi_make_complex_struct_init(c_ret2, c2);
      mpfi_set(c1->re, ptr_ret1);
      mpfi_set(c2->re, ptr_ret1);
      mpfi_set(c1->im, ptr_ret2);
      mpfi_neg(c2->im, ptr_ret2);
      return rb_ary_new3(2, c_ret1, c_ret2);
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
}

static VALUE r_mpfi_square_matrix_eigenvector (VALUE self)
{
  MPFIMatrix *ptr_self, *ptr_ret3, *ptr_ret4;
  MPFI *ptr_ret1, *ptr_ret2;
  VALUE ret1, ret2, ret3, ret4;
  int res;
  r_mpfi_get_matrix_struct(ptr_self, self);
  if (ptr_self->row == 2) {
    r_mpfi_make_struct_init(ret1, ptr_ret1);
    r_mpfi_make_struct_init(ret2, ptr_ret2);
    r_mpfi_matrix_suitable_matrix_init (&ret3, &ptr_ret3, 2, 1);
    r_mpfi_matrix_suitable_matrix_init (&ret4, &ptr_ret4, 2, 1);
    res = mpfi_2d_square_matrix_eigenvalue(ptr_ret1, ptr_ret2, ptr_self);
    if (res > 0) {
      mpfi_2d_square_matrix_real_eigenvector(ptr_ret3, ptr_self, ptr_ret1);
      mpfi_2d_square_matrix_real_eigenvector(ptr_ret4, ptr_self, ptr_ret2);
      return rb_ary_new3(2, rb_ary_new3(2, ret1, ret2), rb_ary_new3(2, ret3, ret4));
    } else if (res == 0) {
      mpfi_2d_square_matrix_real_eigenvector(ptr_ret3, ptr_self, ptr_ret1);
      return rb_ary_new3(2, rb_ary_new3(1, ret1), rb_ary_new3(1, ret3));
    } else {
      MPFIComplex *c1, *c2;
      VALUE c_ret1, c_ret2;
      r_mpfi_make_complex_struct_init(c_ret1, c1);
      r_mpfi_make_complex_struct_init(c_ret2, c2);
      mpfi_set(c1->re, ptr_ret1);
      mpfi_set(c2->re, ptr_ret1);
      mpfi_set(c1->im, ptr_ret2);
      mpfi_neg(c2->im, ptr_ret2);
      return rb_ary_new3(2, rb_ary_new3(2, c_ret1, c_ret2), rb_ary_new3(2, Qnil, Qnil));
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
}

void Init_mpfi_matrix_extra ()
{
  rb_define_method(r_mpfi_square_matrix, "inverse", r_mpfi_square_matrix_inverse, 0);
  rb_define_method(r_mpfi_square_matrix, "eigenvalue", r_mpfi_square_matrix_eigenvalue, 0);
  rb_define_method(r_mpfi_square_matrix, "eigenvector", r_mpfi_square_matrix_eigenvector, 0);

  r_mpfi_vector_2d_module = rb_define_module_under(r_mpfi_class, "Vector2D");

  rb_define_singleton_method(r_mpfi_vector_2d_module, "normal_vector", r_mpfi_vector_2d_normal_vector, 1);
  rb_define_singleton_method(r_mpfi_vector_2d_module, "project", r_mpfi_vector_2d_project, 3);

}
