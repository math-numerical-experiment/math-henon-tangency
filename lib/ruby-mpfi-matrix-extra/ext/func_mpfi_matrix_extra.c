#include "ruby_mpfi_matrix.h"

/* If length of MPFIMatrix *x, return 1. Set normalized normal vector to MPFIMatrix *new. */
int mpfi_vec2_normal_vector (MPFIMatrix *new, MPFIMatrix *x)
{
  int ret = 0;
  if (mpfi_vector_normalize(new, x) == 0) {
    mpfi_swap(new->data, new->data + 1);
    mpfi_neg(new->data + 1, new->data + 1);
  } else {
    ret = 1;
  }
  return ret;
}

/* /\* Return cosine of angle between vector a and b. *\/ */
/* void mpfi_vec2_cos(MPFI *cos, MPFIMatrix *a, MPFIMatrix *b) { */
/*   MPFIMatrix *l; */
/*   r_mpfi_matrix_temp_alloc_init(l, 2, 1); */
  
/*   mpfi_mul(l->data, a->data, b->data); */
/*   mpfi_mul(l->data + 1, a->data + 1, b->data + 1); */
/*   mpfi_add(cos, l->data, l->data + 1); */
/*   mpfi_matrix_vector_norm(l->data, a); */
/*   mpfi_matrix_vector_norm(l->data + 1, b); */
/*   mpfi_div(cos, cos, l->data); */
/*   mpfi_div(cos, cos, l->data + 1); */

/*   r_mpfi_matrix_temp_free(l); */
/* } */

/* /\* Return 0 if line st has intersection with line uv. Otherwise return positive value. *\/ */
/* int mpfi_vec2_transverse_p(MPFIMatrix *s, MPFIMatrix *t, MPFIMatrix *u, MPFIMatrix *v) { */
/*   int ret = 0; */
/*   MPFIMatrix *a, *b; */
/*   r_mpfi_matrix_temp_alloc_init(a, 2, 1); */
/*   r_mpfi_matrix_temp_alloc_init(b, 2, 1); */
/*   MPFI *tmp[6]; */
/*   r_mpfi_temp_alloc_init(tmp[0]); */
/*   r_mpfi_temp_alloc_init(tmp[1]); */
    
/*   mpfi_matrix_sub(a, s, t); */
/*   mpfi_matrix_sub(b, u, v); */
/*   mpfi_mul(tmp[0], a->data, b->data + 1); */
/*   mpfi_mul(tmp[1], a->data + 1, b->data); */
/*   mpfi_sub(tmp[0], tmp[0], tmp[1]); */

/*   if (mpfi_cmp_ui(tmp[0], 0) == 0) { */
/*     if (mpfi_matrix_equal_p(s, u) != 0 && mpfi_matrix_equal_p(s, v) != 0 && */
/* 	mpfi_matrix_equal_p(t, u) != 0 && mpfi_matrix_equal_p(t, v) != 0) { */
/*       ret = 1; */
/*     } */
/*   } else { */
/*     r_mpfi_temp_alloc_init(tmp[2]); */
/*     r_mpfi_temp_alloc_init(tmp[3]); */
/*     r_mpfi_temp_alloc_init(tmp[4]); */
/*     r_mpfi_temp_alloc_init(tmp[5]); */

/*     mpfi_sub(tmp[3], u->data + 1, s->data + 1); */
/*     mpfi_sub(tmp[4], u->data, s->data); */
/*     mpfi_mul(tmp[1], tmp[3], a->data); */
/*     mpfi_mul(tmp[5], tmp[4], a->data + 1); */
/*     mpfi_sub(tmp[1], tmp[1], tmp[5]); */

/*     mpfi_sub(tmp[2], v->data + 1, s->data + 1); */
/*     mpfi_mul(tmp[2], tmp[2], a->data); */
/*     mpfi_sub(tmp[5], v->data, s->data); */
/*     mpfi_mul(tmp[5], tmp[5], a->data + 1); */
/*     mpfi_sub(tmp[2], tmp[2], tmp[5]); */
/*     if (mpfi_sgn(tmp[1]) * mpfi_sgn(tmp[2]) <= 0) { */
/*       mpfi_mul(tmp[1], tmp[4], b->data + 1); */
/*       mpfi_mul(tmp[5], tmp[3], b->data); */
/*       mpfi_sub(tmp[1], tmp[1], tmp[5]); */

/*       mpfi_sub(tmp[2], t->data + 1, u->data + 1); */
/*       mpfi_mul(tmp[2], tmp[2], b->data); */
/*       mpfi_sub(tmp[5], t->data, u->data); */
/*       mpfi_mul(tmp[5], tmp[5], b->data + 1); */
/*       mpfi_sub(tmp[2], tmp[2], tmp[5]); */
/*       if (mpfi_sgn(tmp[1]) * mpfi_sgn(tmp[2]) > 0) { */
/* 	ret = 3; */
/*       } */
/*     } else { */
/*       ret = 2; */
/*     } */
    
/*     r_mpfi_temp_free(tmp[2]); */
/*     r_mpfi_temp_free(tmp[3]); */
/*     r_mpfi_temp_free(tmp[4]); */
/*     r_mpfi_temp_free(tmp[5]); */
/*   } */
  
/*   r_mpfi_matrix_temp_free(a); */
/*   r_mpfi_matrix_temp_free(b); */
/*   r_mpfi_temp_free(tmp[0]); */
/*   r_mpfi_temp_free(tmp[1]); */
/*   return ret; */
/* } */

/* int mpfi_vec2_intersection_point(MPFIMatrix *new, MPFIMatrix *s, MPFIMatrix *t, MPFIMatrix *u, MPFIMatrix *v) { */
/*   int ret = 0; */
/*   MPFIMatrix *a, *b; */
/*   r_mpfi_matrix_temp_alloc_init(a, 2, 1); */
/*   r_mpfi_matrix_temp_alloc_init(b, 2, 1); */
/*   MPFI *tmp[6]; */
/*   r_mpfi_temp_alloc_init(tmp[0]); */
/*   r_mpfi_temp_alloc_init(tmp[1]); */
    
/*   mpfi_matrix_sub(a, s, t); */
/*   mpfi_matrix_sub(b, u, v); */
/*   mpfi_mul(tmp[0], a->data, b->data + 1); */
/*   mpfi_mul(tmp[1], a->data + 1, b->data); */
/*   mpfi_sub(tmp[0], tmp[0], tmp[1]); /\* tmp[0] will be used last in this function *\/ */

/*   if (mpfi_cmp_ui(tmp[0], 0) == 0) { */
/*     if (mpfi_matrix_equal_p(s, u) == 0 || mpfi_matrix_equal_p(s, v) == 0) { */
/*       mpfi_matrix_set(new, s); */
/*     } else if (mpfi_matrix_equal_p(t, u) == 0|| mpfi_matrix_equal_p(t, v) == 0) { */
/*       mpfi_matrix_set(new, t); */
/*     } else { */
/*       ret = 1; */
/*     } */
/*   } else { */
/*     r_mpfi_temp_alloc_init(tmp[2]); */
/*     r_mpfi_temp_alloc_init(tmp[3]); */
/*     r_mpfi_temp_alloc_init(tmp[4]); */
/*     r_mpfi_temp_alloc_init(tmp[5]); */

/*     mpfi_sub(tmp[3], u->data + 1, s->data + 1); /\* u1 - s1 *\/ */
/*     mpfi_sub(tmp[4], u->data, s->data);	       /\* u0 - s0 *\/ */
/*     mpfi_mul(tmp[1], tmp[3], a->data); */
/*     mpfi_mul(tmp[5], tmp[4], a->data + 1); */
/*     mpfi_sub(tmp[1], tmp[1], tmp[5]); */

/*     mpfi_sub(tmp[2], v->data + 1, s->data + 1); */
/*     mpfi_mul(tmp[2], tmp[2], a->data); */
/*     mpfi_sub(tmp[5], v->data, s->data); */
/*     mpfi_mul(tmp[5], tmp[5], a->data + 1); */
/*     mpfi_sub(tmp[2], tmp[2], tmp[5]); */
/*     if (mpfi_sgn(tmp[1]) * mpfi_sgn(tmp[2]) <= 0) { */
/*       mpfi_mul(tmp[1], tmp[4], b->data + 1); */
/*       mpfi_mul(tmp[5], tmp[3], b->data); */
/*       mpfi_sub(tmp[1], tmp[1], tmp[5]); */

/*       mpfi_sub(tmp[2], t->data + 1, u->data + 1); */
/*       mpfi_mul(tmp[2], tmp[2], b->data); */
/*       mpfi_sub(tmp[5], t->data, u->data); */
/*       mpfi_mul(tmp[5], tmp[5], b->data + 1); */
/*       mpfi_sub(tmp[2], tmp[2], tmp[5]); */
/*       if (mpfi_sgn(tmp[1]) * mpfi_sgn(tmp[2]) <= 0) { */
/* 	mpfi_mul(new->data, b->data + 1, v->data); */
/* 	mpfi_sub(tmp[1], t->data + 1, v->data + 1); */
/* 	mpfi_mul(tmp[1], tmp[1], b->data); */
/* 	mpfi_add(new->data, new->data, tmp[1]); */
/* 	mpfi_mul(new->data, new->data, a->data); */
/* 	mpfi_mul(tmp[1], b->data, a->data + 1); */
/* 	mpfi_mul(tmp[1], tmp[1], t->data); */
/* 	mpfi_sub(new->data, new->data, tmp[1]); */
/* 	mpfi_div(new->data, new->data, tmp[0]); */
/* 	mpfi_abs(tmp[3], a->data); */
/* 	mpfi_abs(tmp[4], b->data); */
/* 	if (mpfi_cmp(tmp[3], tmp[4]) >= 0) { */
/* 	  mpfi_sub(new->data + 1, new->data, s->data); */
/* 	  mpfi_mul(new->data + 1, new->data + 1, a->data + 1); */
/* 	  mpfi_div(new->data + 1, new->data + 1, a->data); */
/* 	  mpfi_add(new->data + 1, new->data + 1, s->data + 1); */
/* 	} else { */
/* 	  mpfi_sub(new->data + 1, new->data, u->data); */
/* 	  mpfi_mul(new->data + 1, new->data + 1, b->data + 1); */
/* 	  mpfi_div(new->data + 1, new->data + 1, b->data); */
/* 	  mpfi_add(new->data + 1, new->data + 1, u->data + 1); */
/* 	} */
/*       } else { */
/* 	ret = 3; */
/*       } */
/*     } else { */
/*       ret = 2; */
/*     } */
    
/*     r_mpfi_temp_free(tmp[2]); */
/*     r_mpfi_temp_free(tmp[3]); */
/*     r_mpfi_temp_free(tmp[4]); */
/*     r_mpfi_temp_free(tmp[5]); */
/*   } */
  
/*   r_mpfi_matrix_temp_free(a); */
/*   r_mpfi_matrix_temp_free(b); */
/*   r_mpfi_temp_free(tmp[0]); */
/*   r_mpfi_temp_free(tmp[1]); */
/*   return ret; */
/* } */

/* This function solves  */
/* (1 - a) * s[0] + a * t[0] = (1 - b) * u[0] + b * v[0] */
/* (1 - a) * s[1] + a * t[1] = (1 - b) * u[1] + b * v[1] */
/* and returns */
/* 0 and substitutes *pt[] for coordinate of intersection. */
/* If lines do not have intersection, this function returns negative value. */
/* The solutions a and b are */
/* a = (s[0]*(v[1]-u[1])+u[0]*(s[1]-v[1])+v[0]*(u[1]-s[1]))/ */
/*   (s[0]*(v[1]-u[1])+t[0]*(u[1]-v[1])+v[0]*(t[1]-s[1])+u[0]*(s[1]-t[1])) */
/* b = -(s[0]*(u[1]-t[1])+t[0]*(s[1]-u[1])+u[0]*(t[1]-s[1]))/ */
/*   (s[0]*(v[1]-u[1])+t[0]*(u[1]-v[1])+v[0]*(t[1]-s[1])+u[0]*(s[1]-t[1])) */
/* int mpfi_vec2_transverse(MPFIMatrix *pt, MPFIMatrix *s, MPFIMatrix *t, MPFIMatrix *u, MPFIMatrix *v) { */
/*   MPFI *a, *b, *c, *d; */
/*   MPFIMatrix *tmp; */

/*   r_mpfi_temp_alloc_init(a); */
/*   r_mpfi_temp_alloc_init(b); */
/*   r_mpfi_temp_alloc_init(c); */
/*   r_mpfi_temp_alloc_init(d); */
/*   r_mpfi_matrix_temp_alloc_init(tmp, 3, 1); */

/*   int ret = -60, condition; */

/*   if (mpfi_cmp(s->data + 1, t->data + 1) > 0) { */
/*     mpfi_set(a, s->data + 1); */
/*     mpfi_set(b, t->data + 1); */
/*   } else { */
/*     mpfi_set(a, t->data + 1); */
/*     mpfi_set(b, s->data + 1); */
/*   } */
/*   if (mpfi_cmp(u->data + 1, v->data + 1) > 0) { */
/*     mpfi_set(c, u->data + 1); */
/*     mpfi_set(d, v->data + 1); */
/*   } else { */
/*     mpfi_set(c, v->data + 1); */
/*     mpfi_set(d, u->data + 1); */
/*   } */

/*   if (mpfi_cmp(b, c) > 0 || mpfi_cmp(d, a) > 0) { */
/*     return -50; */
/*   } */

/*   mpfi_sub(tmp->data, a, b); */
/*   mpfi_sub(tmp->data + 1, c, d); */

/*   if (mpfi_cmp(s->data, t->data) > 0) { */
/*     mpfi_set(a, s->data); */
/*     mpfi_set(b, t->data); */
/*   } else { */
/*     mpfi_set(a, t->data); */
/*     mpfi_set(b, s->data); */
/*   } */
/*   if (mpfi_cmp(u->data, v->data) > 0) { */
/*     mpfi_set(c, u->data); */
/*     mpfi_set(d, v->data); */
/*   } else { */
/*     mpfi_set(c, v->data); */
/*     mpfi_set(d, u->data); */
/*   } */

/*   if (mpfi_cmp(b, c) > 0 || mpfi_cmp(d, a) > 0) { */
/*     return -51; */
/*   } */

/*   mpfi_sub(tmp->data + 2, a, b); */
/*   mpfi_add(tmp->data, tmp->data, tmp->data + 2); */
/*   mpfi_sub(tmp->data + 2, c, d); */
/*   mpfi_add(tmp->data + 1, tmp->data + 1, tmp->data + 2); */
/*   if (mpfi_cmp(tmp->data, tmp->data + 1) < 0) { */
/*     condition = 0; */
/*   } else { */
/*     condition = 1; */
/*   } */

/*   mpfi_sub(a, u->data, v->data); */
/*   mpfi_sub(b, u->data + 1, v->data + 1); */
/*   mpfi_sub(c, s->data + 1, t->data + 1); */
/*   mpfi_sub(d, s->data, t->data); */

/*   if (mpfi_cmp_si(a, 0) == 0 || mpfi_cmp_si(d, 0) == 0) { */
/*     if (mpfi_cmp_si(a, 0) == 0 && mpfi_cmp_si(d, 0) == 0) { */
/*       if (mpfi_cmp(s->data, u->data) == 0) { */
/* 	if ((mpfi_cmp(s->data + 1, u->data + 1) * mpfi_cmp(s->data + 1, v->data + 1) > 0) && */
/* 	   (mpfi_cmp(t->data + 1, u->data + 1) * mpfi_cmp(t->data + 1, v->data + 1) > 0) && */
/* 	   (mpfi_cmp(s->data + 1, u->data + 1) * mpfi_cmp(t->data + 1, u->data + 1) > 0)) { */
/* 	  ret = -30; */
/* 	} else { */
/* 	  ret = -10; */
/* 	} */
/*       } */
/*     } else if (mpfi_cmp_si(a, 0) == 0 && mpfi_cmp_si(b, 0) == 0) { */
/*       ret = -40; */
/*     } else if (mpfi_cmp_si(d, 0) == 0 && mpfi_cmp_si(c, 0) == 0) { */
/*       ret = -41; */
/*     } else if (mpfi_cmp_si(a, 0) == 0) { */
/*       mpfi_sub(pt->data, u->data, s->data); */
/*       mpfi_mul(pt->data, pt->data, c); */
/*       mpfi_div(pt->data, pt->data, d); */
/*       mpfi_add(pt->data, pt->data, s->data + 1); */
/*       if (mpfi_cmp(pt->data, u->data + 1) * mpfi_cmp(pt->data, v->data + 1) < 0) { */
/* 	ret = 0; */
/* 	mpfi_set(pt->data + 1, u->data); */
/*       } else { */
/* 	ret = -20; */
/*       } */
/*     } else if (mpfi_cmp_si(d, 0) == 0) { */
/*       mpfi_sub(pt->data, s->data, u->data); */
/*       mpfi_mul(pt->data, pt->data, b); */
/*       mpfi_div(pt->data, pt->data, a); */
/*       mpfi_add(pt->data, pt->data, u->data + 1); */
/*       if (mpfi_cmp(pt->data, s->data + 1) * mpfi_cmp(pt->data, t->data + 1) < 0) { */
/* 	ret = 0; */
/* 	mpfi_set(pt->data + 1, s->data); */
/*       } else { */
/* 	ret = -21; */
/*       } */
/*     } */
/*   } else { */
/*     mpfi_div(pt->data, b, a); */
/*     mpfi_div(pt->data + 1, c, d); */
/*     if (mpfi_cmp(pt->data, pt->data + 1) == 0) { */
/*       mpfi_sub(a, s->data, u->data); */
/*       if (mpfi_cmp_si(a, 0) == 0) { */
/* 	ret = -11; */
/*       } else { */
/* 	mpfi_sub(b, s->data + 1, u->data + 1); */
/* 	mpfi_div(a, b, a); */
/* 	if (mpfi_cmp(a, pt->data) == 0) { */
/* 	  if ((mpfi_cmp(s->data + 1, u->data + 1) * mpfi_cmp(s->data + 1, v->data + 1) > 0) && */
/* 	     (mpfi_cmp(t->data + 1, u->data + 1) * mpfi_cmp(t->data + 1, v->data + 1) > 0) && */
/* 	     (mpfi_cmp(s->data + 1, u->data + 1) * mpfi_cmp(t->data + 1, u->data + 1) > 0)) { */
/* 	    ret = -31; */
/* 	  } else { */
/* 	    ret = -12; */
/* 	  } */
/* 	} else { */
/* 	  ret = -32; */
/* 	} */
/*       } */
/*     } else { */
/*       mpfi_mul(pt->data + 1, c, a); */
/*       mpfi_mul(pt->data, d, b); */
/*       mpfi_sub(pt->data + 1, pt->data + 1, pt->data); */

/*       mpfi_mul(a, a, s->data + 1); */
/*       mpfi_mul(b, b, s->data); */
/*       mpfi_sub(a, a, b); */
/*       mpfi_mul(b, u->data, v->data + 1); */
/*       mpfi_sub(a, a, b); */
/*       mpfi_mul(b, u->data + 1, v->data); */
/*       mpfi_add(a, a, b); */
/*       mpfi_div(a, a, pt->data + 1); */
        
/*       if ((mpfi_cmp_si(a, 0) < 0) || (mpfi_cmp_si(a, 1) > 0)) { */
/* 	ret = -21; */
/*       } else { */
/* 	mpfi_mul(c, c, u->data); */
/* 	mpfi_mul(d, d, u->data + 1); */
/* 	mpfi_sub(b, c, d); */
/* 	mpfi_mul(c, s->data, t->data + 1); */
/* 	mpfi_add(b, b, c); */
/* 	mpfi_mul(c, s->data + 1, t->data); */
/* 	mpfi_sub(b, b, c); */
/* 	mpfi_div(b, b, pt->data + 1); */
      
/* 	if ((mpfi_cmp_si(b, 0) < 0) || (mpfi_cmp_si(b, 1) > 0)) { */
/* 	  ret = -22; */
/* 	} else { */
/* 	  ret = 0; */
/* 	  if (condition == 0) { */
/* 	    mpfi_ui_sub(b, 1, a); */
/* 	    mpfi_mul(c, b, s->data); */
/* 	    mpfi_mul(d, a, t->data); */
/* 	    mpfi_add(pt->data, c, d); */
/* 	    mpfi_mul(c, b, s->data + 1); */
/* 	    mpfi_mul(d, a, t->data + 1); */
/* 	    mpfi_add(pt->data + 1, c, d); */
/* 	  } else { */
/* 	    mpfi_ui_sub(a, 1, b); */
/* 	    mpfi_mul(c, a, u->data); */
/* 	    mpfi_mul(d, b, v->data); */
/* 	    mpfi_add(pt->data, c, d); */
/* 	    mpfi_mul(c, a, u->data + 1); */
/* 	    mpfi_mul(d, b, v->data + 1); */
/* 	    mpfi_add(pt->data + 1, c, d); */
/* 	  } */
/* 	} */
/*       } */
/*     } */
/*   } */

/*   r_mpfi_temp_free(a); */
/*   r_mpfi_temp_free(b); */
/*   r_mpfi_temp_free(c); */
/*   r_mpfi_temp_free(d); */
/*   r_mpfi_matrix_temp_free(tmp); */
/*   return ret; */
/* } */

/* This function solves w1=a*u1+b*v1,w2=a*u2+b*v2 for a, b */
/* (w = a * u + b * v, */
/*  a, b: scalar, */
/*  u, v, w: vector) */
/* and set the solution to a[0] and a[1]. */
/* a = -(v1*w2-v2*w1)/(u1*v2-u2*v1) */
/* b = (u1*w2-u2*w1)/(u1*v2-u2*v1) */
/* c = u1*v2 - u2*v1 */
void mpfi_vec2_project (MPFIMatrix *a, MPFIMatrix *w, MPFIMatrix *u, MPFIMatrix *v)
{
  MPFI *x, *y;

  r_mpfi_temp_alloc_init(x);
  r_mpfi_temp_alloc_init(y);

  mpfi_mul(x, v->data, w->data + 1);
  mpfi_mul(y, v->data + 1, w->data);
  mpfi_sub(a->data, y, x);

  mpfi_mul(x, u->data, w->data + 1);
  mpfi_mul(y, u->data + 1, w->data);
  mpfi_sub(a->data + 1, x, y);

  mpfi_mul(x, u->data, v->data + 1);
  mpfi_mul(y, u->data + 1, v->data);
  mpfi_sub(x, x, y);
  
  mpfi_div(a->data, a->data, x);
  mpfi_div(a->data + 1, a->data + 1, x);

  r_mpfi_temp_free(x);
  r_mpfi_temp_free(y);
}


/* If inverse matrix does not exist, return 1. Otherwise return 0. */
int mpfi_2d_square_matrix_inverse_matrix (MPFIMatrix *inv, MPFIMatrix *x)
{
  MPFIMatrix *t_mat;
  MPFI *t_fi;
  r_mpfi_matrix_temp_alloc_init(t_mat, inv->row, inv->column);
  r_mpfi_temp_alloc_init(t_fi);
  mpfi_square_matrix_determinant(t_fi, x);
  if (mpfi_has_zero(t_fi) > 0) {
    return 1;
  } else {
    mpfi_ui_div(t_fi, 1, t_fi);
    mpfi_mul(t_mat->data, x->data + 3, t_fi);
    mpfi_mul(t_mat->data + 3, x->data, t_fi);
    mpfi_neg(t_fi, t_fi);
    mpfi_mul(t_mat->data + 1, x->data + 1, t_fi);
    mpfi_mul(t_mat->data + 2, x->data + 2, t_fi);
  }
  mpfi_matrix_set(inv, t_mat);
  r_mpfi_matrix_temp_free(t_mat);
  r_mpfi_temp_free(t_fi);
  return 0;
}

/* x = -(sqrt(a11**2-2*a00*a11+4*a01*a10+a00**2)-a11-a00)/2.0E+0 */
/* x = (sqrt(a11**2-2*a00*a11+4*a01*a10+a00**2)+a11+a00)/2.0E+0 */
/* If there are two real eigenvalues, return positive number. */
/* If only one eigenvalue exists, return 0. */
/* If there are two complex eigenvalues, this functionreturn negative number and */
/* first returned value is real part and second one is imaginary part. */
int mpfi_2d_square_matrix_eigenvalue (MPFI *val1, MPFI *val2, MPFIMatrix *x)
{
  int ret;
  MPFI *d;
  r_mpfi_temp_alloc_init(d);

  mpfi_sub(val1, x->data, x->data + 3);
  mpfi_mul(val1, val1, val1);
  mpfi_mul(d, x->data + 1, x->data + 2);
  mpfi_mul_ui(d, d, 4);
  mpfi_add(d, d, val1);

  mpfi_add(val1, x->data, x->data + 3);
  mpfi_div_ui(val1, val1, 2);
  if (mpfr_cmp_si(r_mpfi_right_ptr(d), 0) > 0) {
    ret = 1;
    mpfi_sqrt(d, d);
    mpfi_div_ui(d, d, 2);
    mpfi_sub(val2, val1, d);
    mpfi_add(val1, val1, d);
  } else if (mpfr_cmp_si(r_mpfi_right_ptr(d), 0) < 0) {
    ret = -1;
    mpfi_neg(d, d);
    mpfi_sqrt(d, d);
    mpfi_div_ui(val2, d, 2);
  } else {
    ret = 0;
    mpfi_set(val2, val1);
  }

  r_mpfi_temp_free(d);
  return ret;
}

void mpfi_2d_square_matrix_real_eigenvector (MPFIMatrix *vec, MPFIMatrix *x, MPFI *eigenval)
{
  MPFIMatrix *tmp;
  MPFI *tmp_fi;
  r_mpfi_matrix_temp_alloc_init(tmp, 2, 1);
  r_mpfi_temp_alloc_init(tmp_fi);
  mpfi_sub(tmp_fi, x->data + 3, eigenval);
  if (mpfi_has_zero(x->data + 1) > 0 && mpfi_has_zero(tmp_fi) > 0) {
    mpfi_set(tmp->data, x->data + 2);
    mpfi_sub(tmp->data + 1, eigenval, x->data);
  } else {
    mpfi_sub(tmp->data, eigenval, x->data + 3);
    mpfi_set(tmp->data + 1, x->data + 1);
  }
  if (mpfi_vector_normalize(vec, tmp) == 1) {
    gmp_printf("Argument matrix\n%.Ff\n%.Ff\n%.Ff\n%.Ff\n", x->data, x->data + 1, x->data + 2, x->data + 3);
    gmp_printf("Argument eigenvalue\n%.Ff\n", eigenval);
    rb_raise(rb_eArgError, "Invalid eigenvalue or eigenvector.");
  }
  r_mpfi_matrix_temp_free(tmp);
  r_mpfi_temp_free(tmp_fi);
}
