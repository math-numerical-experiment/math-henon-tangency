require 'mkmf'
require "extconf_task/mkmf_utils"

$CFLAGS += " -Wall"

find_header_in_gem("ruby_mpfr.h", "ruby-mpfr")
find_header_in_gem("ruby_mpfr_matrix.h", "ruby-mpfr")
find_header_in_gem("ruby_mpfi.h", "ruby-mpfi")
find_header_in_gem("ruby_mpfi_matrix.h", "ruby-mpfi")
find_header_in_gem("ruby_mpfi_complex.h", "ruby-mpfi")

dir_config("mpfr")
dir_config("mpfi")
dir_config("gmp")

if have_header('mpfr.h') && have_library('mpfr') && have_header('gmp.h') && have_library('gmp')
  create_makefile("mpfi_matrix_extra")
end
