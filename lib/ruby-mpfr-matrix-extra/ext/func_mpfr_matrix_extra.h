#ifndef _FUNC_MPFR_MATRIX_EXTRA_H_
#define _FUNC_MPFR_MATRIX_EXTRA_H_

#include <stdio.h>
#include <mpfr.h>
#include "ruby_mpfr_matrix.h"
#include "ruby_mpc.h"

int mpfr_2d_normal_vector(MPFRMatrix *new, MPFRMatrix *x);

int mpfr_2d_square_matrix_inverse_matrix(MPFRMatrix *inv, MPFRMatrix *x);
int mpfr_2d_square_matrix_eigenvalue(MPFR *val1, MPFR *val2, MPFRMatrix *x);
void mpfr_2d_square_matrix_real_eigenvector(MPFRMatrix *vec, MPFRMatrix *x, MPFR *eigenval);


void mpfr_distance_between_point_and_line_segment(MPFR *dist, MPFRMatrix *pt, MPFRMatrix *spt, MPFRMatrix *ept);
void mpfr_vec2_cos(MPFR *cos, MPFRMatrix *a, MPFRMatrix *b);
int mpfr_vec2_transverse_p(MPFRMatrix *s, MPFRMatrix *t, MPFRMatrix *u, MPFRMatrix *v);
int mpfr_vec2_intersection_point(MPFRMatrix *new, MPFRMatrix *s, MPFRMatrix *t, MPFRMatrix *u, MPFRMatrix *v);
void mpfr_vec2_project(MPFRMatrix *a, MPFRMatrix *w, MPFRMatrix *u, MPFRMatrix *v);

#endif /* _FUNC_MPFR_MATRIX_EXTRA_H_ */

