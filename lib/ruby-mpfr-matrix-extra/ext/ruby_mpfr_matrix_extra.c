#include "ruby_mpfr_matrix_extra.h"

static ID at, size;


static VALUE r_mpfr_vector_2d_normal_vector (VALUE self, VALUE arg)
{
  MPFRMatrix *ptr_arg, *ptr_ret;
  VALUE ret;
  r_mpfr_get_matrix_struct(ptr_arg, arg);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, ptr_arg->row, ptr_arg->column);
  if (ptr_arg->size == 2) {
    if (mpfr_2d_normal_vector(ptr_ret, ptr_arg) == 0) {
      return ret;
    } else {
      return Qnil;
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
}

static VALUE r_mpfr_vector_2d_translating_and_scaling (VALUE self, VALUE pt, VALUE center, VALUE scale)
{
  VALUE ret_val;
  MPFRMatrix *ptr_return, *ptr_pt, *ptr_center;
  MPFR *ptr_scale1, *ptr_scale2;
  r_mpfr_matrix_suitable_matrix_init (&ret_val, &ptr_return, 2, 1);
  r_mpfr_get_matrix_struct(ptr_pt, pt);
  r_mpfr_get_matrix_struct(ptr_center, center);
  if (RARRAY_LEN(scale) == 2) {
    r_mpfr_get_struct(ptr_scale1, RARRAY_PTR(scale)[0]);
    r_mpfr_get_struct(ptr_scale2, RARRAY_PTR(scale)[1]);
  } else if (RARRAY_LEN(scale) == 1) {
    r_mpfr_get_struct(ptr_scale1, RARRAY_PTR(scale)[0]);
    r_mpfr_get_struct(ptr_scale2, RARRAY_PTR(scale)[0]);
  } else {
    rb_raise(rb_eArgError, "Size of array must be one or two.");
  }
  mpfr_matrix_sub(ptr_return, ptr_pt, ptr_center);
  mpfr_mul(ptr_return->data, ptr_return->data, ptr_scale1, mpfr_get_default_rounding_mode());
  mpfr_mul(ptr_return->data + 1, ptr_return->data + 1, ptr_scale2, mpfr_get_default_rounding_mode());
  return ret_val;
}

static VALUE r_mpfr_vector_2d_cos (VALUE self, VALUE arg1, VALUE arg2)
{
  VALUE ret_val;
  MPFR *ptr_return;
  MPFRMatrix *ptr_arg1, *ptr_arg2;
  r_mpfr_make_struct_init(ret_val, ptr_return);
  r_mpfr_get_matrix_struct(ptr_arg1, arg1);
  r_mpfr_get_matrix_struct(ptr_arg2, arg2);
  mpfr_vec2_cos(ptr_return, ptr_arg1, ptr_arg2);
  return ret_val;
}

/* If line arg1 to arg2 and line arg3 to arg4 intersect each other, */
/* this function returns true. */
/* Otherwise this returns nils. */
static VALUE r_mpfr_vector_2d_transverse_p (VALUE self, VALUE arg1, VALUE arg2, VALUE arg3, VALUE arg4)
{
  MPFRMatrix *ptr_arg1, *ptr_arg2, *ptr_arg3, *ptr_arg4;
  int result;
  r_mpfr_get_matrix_struct(ptr_arg1, arg1);
  r_mpfr_get_matrix_struct(ptr_arg2, arg2);
  r_mpfr_get_matrix_struct(ptr_arg3, arg3);
  r_mpfr_get_matrix_struct(ptr_arg4, arg4);
  result = mpfr_vec2_transverse_p(ptr_arg1, ptr_arg2, ptr_arg3, ptr_arg4);
  return (result > 0 ? Qnil : Qtrue);
}

/* If line arg1 to arg2 and line arg3 to arg4 intersect each other, */
/* this function returns intersection point MPF::ColumnVector object. */
/* Otherwise this returns nil. */
static VALUE r_mpfr_vector_2d_intersection_point (VALUE self, VALUE arg1, VALUE arg2, VALUE arg3, VALUE arg4)
{
  VALUE ret_val;
  MPFRMatrix *ptr_return, *ptr_arg1, *ptr_arg2, *ptr_arg3, *ptr_arg4;
  int result;
  r_mpfr_matrix_suitable_matrix_init (&ret_val, &ptr_return, 2, 1);
  r_mpfr_get_matrix_struct(ptr_arg1, arg1);
  r_mpfr_get_matrix_struct(ptr_arg2, arg2);
  r_mpfr_get_matrix_struct(ptr_arg3, arg3);
  r_mpfr_get_matrix_struct(ptr_arg4, arg4);
  result = mpfr_vec2_intersection_point(ptr_return, ptr_arg1, ptr_arg2, ptr_arg3, ptr_arg4);
  return (result > 0 ? Qnil : ret_val);
}

static VALUE r_mpfr_vector_2d_project (VALUE self, VALUE arg1, VALUE arg2, VALUE arg3)
{
  VALUE ret_val;
  MPFRMatrix *ptr_return, *ptr_arg1, *ptr_arg2, *ptr_arg3;
  r_mpfr_matrix_suitable_matrix_init (&ret_val, &ptr_return, 2, 1);
  r_mpfr_get_matrix_struct(ptr_arg1, arg1);
  r_mpfr_get_matrix_struct(ptr_arg2, arg2);
  r_mpfr_get_matrix_struct(ptr_arg3, arg3);
  mpfr_vec2_project(ptr_return, ptr_arg1, ptr_arg2, ptr_arg3);
  return ret_val;
}

static VALUE r_mpfr_vector_2d_interesction_ary (VALUE line1, int start1, int end1, VALUE line2, int start2, int end2)
{
  int i, j, ret_trans;
  VALUE s, t, u, v, trans, ret_val;
  MPFRMatrix *ptr_s, *ptr_t, *ptr_u, *ptr_v, *ptr_trans;
  r_mpfr_matrix_suitable_matrix_init (&trans, &ptr_trans, 2, 1);

  ret_val = rb_ary_new();

  r_mpfr_matrix_temp_alloc_init(ptr_trans, 2, 1);
  
  for (i = start1; i < end1; i += 1) {
    s = rb_funcall(line1, at, 1, INT2FIX(i));
    t = rb_funcall(line1, at, 1, INT2FIX(i+1));
    r_mpfr_get_matrix_struct(ptr_s, s);
    r_mpfr_get_matrix_struct(ptr_t, t);
    for (j = start2; j < end2; j += 1) {
      u = rb_funcall(line2, at, 1, INT2FIX(j));
      v = rb_funcall(line2, at, 1, INT2FIX(j+1));
      r_mpfr_get_matrix_struct(ptr_u, u);
      r_mpfr_get_matrix_struct(ptr_v, v);
      ret_trans = mpfr_vec2_intersection_point(ptr_trans, ptr_s, ptr_t, ptr_u, ptr_v);
      if (ret_trans == 0) {
	rb_ary_push(ret_val, rb_ary_new3(3, r_mpfr_matrix_robj(ptr_trans), INT2FIX(i), INT2FIX(j)));
      }
    }
  }
  return ret_val;
}

/*
  This function returns array of which elements are
  array [coordinate_of_x, coordinate_of_y, index1, index2] and
  these coordinates express coordinates of intersections and
  these indexes are the indexes corresponding to intersections.
*/
static VALUE r_mpfr_vector_2d_intersection_of_lines (VALUE self, VALUE line1, VALUE line2)
{
  return r_mpfr_vector_2d_interesction_ary(line1, 0, FIX2INT(rb_funcall(line1, size, 0)) - 1,
					   line2, 0, FIX2INT(rb_funcall(line2, size, 0)) - 1);
}

static VALUE r_mpfr_vector_2d_intersection_of_lines2 (VALUE self, VALUE ary_line1, VALUE ary_line2)
{
  VALUE *ary1, *ary2;
  ary1 = RARRAY_PTR(ary_line1);
  ary2 = RARRAY_PTR(ary_line2);

  return r_mpfr_vector_2d_interesction_ary(ary1[0], FIX2INT(ary1[1]), FIX2INT(ary1[2]),
					   ary2[0], FIX2INT(ary2[1]), FIX2INT(ary2[2]));
}

static VALUE r_mpfr_vector_2d_distance_between_point_and_line (VALUE self, VALUE point, VALUE line)
{
  int i, index = 0;
  int line_size = FIX2INT(rb_funcall(line, size, 0));
  VALUE ret_dist, s, t;
  MPFR *ptr_dist, *ptr_ret_dist;
  MPFRMatrix *ptr_s, *ptr_t, *ptr_pt;
  r_mpfr_make_struct_init(ret_dist, ptr_ret_dist);
  r_mpfr_temp_alloc_init(ptr_dist);
  s = rb_funcall(line, at, 1, INT2FIX(0));
  t = rb_funcall(line, at, 1, INT2FIX(1));
  r_mpfr_get_matrix_struct(ptr_s, s);
  r_mpfr_get_matrix_struct(ptr_t, t);
  r_mpfr_get_matrix_struct(ptr_pt, point);
  mpfr_distance_between_point_and_line_segment(ptr_ret_dist, ptr_pt, ptr_s, ptr_t);

  for (i = 1; i < line_size-1; i += 1) {
    s = rb_funcall(line, at, 1, INT2FIX(i));
    t = rb_funcall(line, at, 1, INT2FIX(i+1));
    r_mpfr_get_matrix_struct(ptr_s, s);
    r_mpfr_get_matrix_struct(ptr_t, t);
    mpfr_distance_between_point_and_line_segment(ptr_dist, ptr_pt, ptr_s, ptr_t);
    if (mpfr_cmp(ptr_dist, ptr_ret_dist) < 0) {
      mpfr_set(ptr_ret_dist, ptr_dist, mpfr_get_default_rounding_mode());
      index = i;
    }
  }
  r_mpfr_temp_free(ptr_dist);
  return rb_ary_new3(2, ret_dist, INT2FIX(index));
}

static VALUE r_mpfr_vector_2d_maximum_distance_between_point_and_line (VALUE self, VALUE point, VALUE line)
{
  int i, index = 0;
  int line_size = FIX2INT(rb_funcall(line, size, 0));
  VALUE ret_dist, s, t;
  MPFR *ptr_dist, *ptr_ret_dist;
  MPFRMatrix *ptr_s, *ptr_t, *ptr_pt;
  r_mpfr_make_struct_init(ret_dist, ptr_ret_dist);
  r_mpfr_temp_alloc_init(ptr_dist);
  s = rb_funcall(line, at, 1, INT2FIX(0));
  t = rb_funcall(line, at, 1, INT2FIX(1));
  r_mpfr_get_matrix_struct(ptr_s, s);
  r_mpfr_get_matrix_struct(ptr_t, t);
  r_mpfr_get_matrix_struct(ptr_pt, point);
  mpfr_distance_between_point_and_line_segment(ptr_ret_dist, ptr_pt, ptr_s, ptr_t);

  for (i = 1; i < line_size-1; i += 1) {
    s = rb_funcall(line, at, 1, INT2FIX(i));
    t = rb_funcall(line, at, 1, INT2FIX(i+1));
    r_mpfr_get_matrix_struct(ptr_s, s);
    r_mpfr_get_matrix_struct(ptr_t, t);
    mpfr_distance_between_point_and_line_segment(ptr_dist, ptr_pt, ptr_s, ptr_t);
    if (mpfr_cmp(ptr_dist, ptr_ret_dist) > 0) {
      mpfr_set(ptr_ret_dist, ptr_dist, mpfr_get_default_rounding_mode());
      index = i;
    }
  }
  r_mpfr_temp_free(ptr_dist);
  return rb_ary_new3(2, ret_dist, INT2FIX(index));
}

/* This method calculates the distance between line1 and line2 which have method 'at' */
/* and returns array [distance, index1, index2] */
static VALUE r_mpfr_vector_2d_distance_between_lines (VALUE self, VALUE line1, VALUE line2)
{
  int i, j, index1 = 0, index2 = 0;
  int line_size1 = FIX2INT(rb_funcall(line1, size, 0));
  int line_size2 = FIX2INT(rb_funcall(line2, size, 0));
  VALUE ret_dist, s, t, u, v;
  MPFRMatrix *ptr_s, *ptr_t, *ptr_u, *ptr_v, *ptr_dist;
  MPFR *ptr_ret_dist;
  r_mpfr_make_struct_init(ret_dist, ptr_ret_dist);
  r_mpfr_matrix_temp_alloc_init(ptr_dist, 2, 1);

  s = rb_funcall(line1, at, 1, INT2FIX(0));
  u = rb_funcall(line2, at, 1, INT2FIX(0));
  v = rb_funcall(line2, at, 1, INT2FIX(1));
  r_mpfr_get_matrix_struct(ptr_s, s);
  r_mpfr_get_matrix_struct(ptr_u, u);
  r_mpfr_get_matrix_struct(ptr_v, v);
  mpfr_distance_between_point_and_line_segment(ptr_ret_dist, ptr_s, ptr_u, ptr_v);

  for (i = 0; i < line_size1 - 1; i += 1) {
    s = rb_funcall(line1, at, 1, INT2FIX(i));
    t = rb_funcall(line1, at, 1, INT2FIX(i+1));
    r_mpfr_get_matrix_struct(ptr_s, s);
    r_mpfr_get_matrix_struct(ptr_t, t);
    for (j = 0; j < line_size2 - 1; j += 1) {
      u = rb_funcall(line2, at, 1, INT2FIX(j));
      v = rb_funcall(line2, at, 1, INT2FIX(j+1));
      r_mpfr_get_matrix_struct(ptr_u, u);
      r_mpfr_get_matrix_struct(ptr_v, v);
      mpfr_distance_between_point_and_line_segment(ptr_dist->data, ptr_s, ptr_u, ptr_v);
      mpfr_distance_between_point_and_line_segment(ptr_dist->data + 1, ptr_t, ptr_u, ptr_v);
      if (mpfr_cmp(ptr_dist->data, ptr_ret_dist) < 0) {
	mpfr_set(ptr_ret_dist, ptr_dist->data, mpfr_get_default_rounding_mode());
	index1 = i;
	index2 = j;
      }
      if (mpfr_cmp(ptr_dist->data + 1, ptr_ret_dist) < 0) {
	mpfr_set(ptr_ret_dist, ptr_dist->data + 1, mpfr_get_default_rounding_mode());
	index1 = i;
	index2 = j;
      }
    }
  }
  r_mpfr_matrix_temp_free(ptr_dist);
  return rb_ary_new3(3, ret_dist, INT2FIX(index1), INT2FIX(index2));
}


/* This method calculates the maximum distance between line1 and line2 which have method 'at' */
/* and returns array [distance, index1, index2] */
/* To get usual distance (that is, minimum distance), we use the method distance_between_lines. */
static VALUE r_mpfr_vector_2d_maximum_distance_between_lines (VALUE self, VALUE line1, VALUE line2)
{
  int i, j, index1 = 0, index2 = 0;
  int line_size1 = FIX2INT(rb_funcall(line1, size, 0));
  int line_size2 = FIX2INT(rb_funcall(line2, size, 0));
  VALUE ret_dist, s, t, u, v;
  MPFRMatrix *ptr_s, *ptr_t, *ptr_u, *ptr_v, *ptr_dist;
  MPFR *ptr_ret_dist;
  r_mpfr_make_struct_init(ret_dist, ptr_ret_dist);
  r_mpfr_matrix_temp_alloc_init(ptr_dist, 2, 1);

  s = rb_funcall(line1, at, 1, INT2FIX(0));
  u = rb_funcall(line2, at, 1, INT2FIX(0));
  v = rb_funcall(line2, at, 1, INT2FIX(1));
  r_mpfr_get_matrix_struct(ptr_s, s);
  r_mpfr_get_matrix_struct(ptr_u, u);
  r_mpfr_get_matrix_struct(ptr_v, v);
  mpfr_distance_between_point_and_line_segment(ptr_ret_dist, ptr_s, ptr_u, ptr_v);

  for (i = 0; i < line_size1 - 1; i += 1) {
    s = rb_funcall(line1, at, 1, INT2FIX(i));
    t = rb_funcall(line1, at, 1, INT2FIX(i+1));
    r_mpfr_get_matrix_struct(ptr_s, s);
    r_mpfr_get_matrix_struct(ptr_t, t);
    for (j = 0; j < line_size2 - 1; j += 1) {
      u = rb_funcall(line2, at, 1, INT2FIX(j));
      v = rb_funcall(line2, at, 1, INT2FIX(j+1));
      r_mpfr_get_matrix_struct(ptr_u, u);
      r_mpfr_get_matrix_struct(ptr_v, v);
      mpfr_distance_between_point_and_line_segment(ptr_dist->data, ptr_s, ptr_u, ptr_v);
      mpfr_distance_between_point_and_line_segment(ptr_dist->data + 1, ptr_t, ptr_u, ptr_v);
      if (mpfr_cmp(ptr_dist->data, ptr_ret_dist) > 0) {
	mpfr_set(ptr_ret_dist, ptr_dist->data, mpfr_get_default_rounding_mode());
	index1 = i;
	index2 = j;
      }
      if (mpfr_cmp(ptr_dist->data + 1, ptr_ret_dist) > 0) {
	mpfr_set(ptr_ret_dist, ptr_dist->data + 1, mpfr_get_default_rounding_mode());
	index1 = i;
	index2 = j;
      }
    }
  }
  r_mpfr_matrix_temp_free(ptr_dist);
  return rb_ary_new3(3, ret_dist, INT2FIX(index1), INT2FIX(index2));
}

/* Return array [ary, distance, index1, index2]. */
/* 'ary' has array of index pairs of which points are closer than 'base_dist' each other.*/
/* 'distance' is the distance between 'line1' and 'line2'. */
/* 'index1' and 'index2' are indexes of which points are closest each other.*/
static VALUE r_mpfr_vector_2d_close_points_on_lines_base (VALUE self, VALUE line1, VALUE line2, VALUE base_dist)
{
  int i, j, index1 = 0, index2 = 0;
  int line_size1 = FIX2INT(rb_funcall(line1, size, 0));
  int line_size2 = FIX2INT(rb_funcall(line2, size, 0));
  VALUE ret_dist, ret_index, s, t, u, v;
  MPFRMatrix *ptr_s, *ptr_t, *ptr_u, *ptr_v, *ptr_dist;
  MPFR *ptr_ret_dist, *ptr_base_dist;
  r_mpfr_get_struct(ptr_base_dist, base_dist);
  r_mpfr_make_struct_init(ret_dist, ptr_ret_dist);
  ret_index = rb_ary_new();
  r_mpfr_matrix_temp_alloc_init(ptr_dist, 2, 1);

  s = rb_funcall(line1, at, 1, INT2FIX(0));
  u = rb_funcall(line2, at, 1, INT2FIX(0));
  v = rb_funcall(line2, at, 1, INT2FIX(1));
  r_mpfr_get_matrix_struct(ptr_s, s);
  r_mpfr_get_matrix_struct(ptr_u, u);
  r_mpfr_get_matrix_struct(ptr_v, v);

  mpfr_distance_between_point_and_line_segment(ptr_ret_dist, ptr_s, ptr_u, ptr_v);

  for (i = 0; i < line_size1 - 1; i += 1) {
    s = rb_funcall(line1, at, 1, INT2FIX(i));
    t = rb_funcall(line1, at, 1, INT2FIX(i+1));
    r_mpfr_get_matrix_struct(ptr_s, s);
    r_mpfr_get_matrix_struct(ptr_t, t);
    for (j = 0; j < line_size2 - 1; j += 1) {
      u = rb_funcall(line2, at, 1, INT2FIX(j));
      v = rb_funcall(line2, at, 1, INT2FIX(j+1));
      r_mpfr_get_matrix_struct(ptr_u, u);
      r_mpfr_get_matrix_struct(ptr_v, v);
      mpfr_distance_between_point_and_line_segment(ptr_dist->data, ptr_s, ptr_u, ptr_v);
      mpfr_distance_between_point_and_line_segment(ptr_dist->data + 1, ptr_t, ptr_u, ptr_v);
      if (mpfr_cmp(ptr_dist->data, ptr_base_dist) < 0 || mpfr_cmp(ptr_dist->data + 1, ptr_base_dist) < 0) {
	rb_ary_push(ret_index, rb_ary_new3(2, INT2FIX(i), INT2FIX(j)));
      }
      if (mpfr_cmp(ptr_dist->data, ptr_ret_dist) < 0) {
	mpfr_set(ptr_ret_dist, ptr_dist->data, mpfr_get_default_rounding_mode());
	index1 = i;
	index2 = j;
      }
      if (mpfr_cmp(ptr_dist->data + 1, ptr_ret_dist) < 0) {
	mpfr_set(ptr_ret_dist, ptr_dist->data + 1, mpfr_get_default_rounding_mode());
	index1 = i;
	index2 = j;
      }
    }
  }
  r_mpfr_matrix_temp_free(ptr_dist);
  return rb_ary_new3(4, ret_index, ret_dist, INT2FIX(index1), INT2FIX(index2));
}

static VALUE r_mpfr_vector_2d_rectangle_each_pt (VALUE self, VALUE pt1, VALUE pt2, VALUE pitch1, VALUE pitch2)
{
  VALUE ret = Qnil, tmp_pt;
  MPFRMatrix *ptr_pt1, *ptr_pt2, *ptr_tmp_pt;
  MPFR *ptr_pitch1, *ptr_pitch2, *ptr_tmp_pitch_x, *ptr_tmp_pitch_y;
  int cond_x, cond_y;
  r_mpfr_get_matrix_struct(ptr_pt1, pt1);
  r_mpfr_get_matrix_struct(ptr_pt2, pt2);
  r_mpfr_get_struct(ptr_pitch1, pitch1);
  r_mpfr_get_struct(ptr_pitch2, pitch2);
  r_mpfr_matrix_suitable_matrix_init(&tmp_pt, &ptr_tmp_pt, 2, 1);

  r_mpfr_temp_alloc_init(ptr_tmp_pitch_x);
  r_mpfr_temp_alloc_init(ptr_tmp_pitch_y);
  
  mpfr_matrix_set(ptr_tmp_pt, ptr_pt1);
  cond_x = mpfr_cmp(ptr_pt2->data, ptr_pt1->data);
  cond_y = mpfr_cmp(ptr_pt2->data + 1, ptr_pt1->data + 1);

  if (cond_x >= 0) {
    mpfr_set(ptr_tmp_pitch_x, ptr_pitch1, mpfr_get_default_rounding_mode());
  } else {
    mpfr_neg(ptr_tmp_pitch_x, ptr_pitch1, mpfr_get_default_rounding_mode());
  }
  if (cond_y >= 0) {
    mpfr_set(ptr_tmp_pitch_y, ptr_pitch2, mpfr_get_default_rounding_mode());
  } else {
    mpfr_neg(ptr_tmp_pitch_y, ptr_pitch2, mpfr_get_default_rounding_mode());
  }

  if (cond_x == 0 && cond_y == 0) {
    ret = rb_yield(pt1);
  } else if (cond_x == 0) {
    while (mpfr_cmp(ptr_pt2->data + 1, ptr_tmp_pt->data + 1) * cond_y >= 0) {
      ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp_pt));
      mpfr_add(ptr_tmp_pt->data + 1, ptr_tmp_pt->data + 1, ptr_tmp_pitch_y, mpfr_get_default_rounding_mode());
    }
  } else if (cond_y == 0) {
    while (mpfr_cmp(ptr_pt2->data, ptr_tmp_pt->data) * cond_x >= 0) {
      ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp_pt));
      mpfr_add(ptr_tmp_pt->data, ptr_tmp_pt->data, ptr_tmp_pitch_x, mpfr_get_default_rounding_mode());
    }
  } else {
    while (mpfr_cmp(ptr_pt2->data + 1, ptr_tmp_pt->data + 1) * cond_y >= 0) {
      while (mpfr_cmp(ptr_pt2->data, ptr_tmp_pt->data) * cond_x >= 0) {
	ret = rb_yield(r_mpfr_matrix_robj(ptr_tmp_pt));
	mpfr_add(ptr_tmp_pt->data, ptr_tmp_pt->data, ptr_tmp_pitch_x, mpfr_get_default_rounding_mode());
      }
      mpfr_set(ptr_tmp_pt->data, ptr_pt1->data, mpfr_get_default_rounding_mode());
      mpfr_add(ptr_tmp_pt->data + 1, ptr_tmp_pt->data + 1, ptr_tmp_pitch_y, mpfr_get_default_rounding_mode());
    }
  }
  r_mpfr_temp_free(ptr_tmp_pitch_x);
  r_mpfr_temp_free(ptr_tmp_pitch_y);
  return ret;
}


static VALUE r_mpfr_square_matrix_inverse (VALUE self)
{
  MPFRMatrix *ptr_self, *ptr_ret;
  VALUE ret;
  r_mpfr_get_matrix_struct(ptr_self, self);
  if (ptr_self->column == 2 && ptr_self->row == 2) {
    r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_ret, ptr_self->row, ptr_self->column);
    if (mpfr_2d_square_matrix_inverse_matrix(ptr_ret, ptr_self) == 1) {
      return Qnil;
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
  return ret;
}

static VALUE r_mpfr_square_matrix_eigenvalue (VALUE self)
{
  MPFRMatrix *ptr_self;
  MPFR *ptr_ret1, *ptr_ret2;
  VALUE ret1, ret2;
  int res;
  r_mpfr_get_matrix_struct(ptr_self, self);
  if (ptr_self->row == 2) {
    r_mpfr_make_struct_init(ret1, ptr_ret1);
    r_mpfr_make_struct_init(ret2, ptr_ret2);
    res = mpfr_2d_square_matrix_eigenvalue(ptr_ret1, ptr_ret2, ptr_self);
    if (res > 0) {
      return rb_ary_new3(2, ret1, ret2);
    } else if (res == 0) {
      return rb_ary_new3(1, ret1);
    } else {
      MPC *c1, *c2;
      VALUE c_ret1, c_ret2;
      r_mpc_make_struct_init(c_ret1, c1);
      r_mpc_make_struct_init(c_ret2, c2);
      mpfr_set(c1->re, ptr_ret1, GMP_RNDN);
      mpfr_set(c2->re, ptr_ret1, GMP_RNDN);
      mpfr_set(c1->im, ptr_ret2, GMP_RNDN);
      mpfr_neg(c2->im, ptr_ret2, GMP_RNDN);
      return rb_ary_new3(2, c_ret1, c_ret2);
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
}

static VALUE r_mpfr_square_matrix_eigenvector (VALUE self)
{
  MPFRMatrix *ptr_self, *ptr_ret3, *ptr_ret4;
  MPFR *ptr_ret1, *ptr_ret2;
  VALUE ret1, ret2, ret3, ret4;
  int res;
  r_mpfr_get_matrix_struct(ptr_self, self);
  if (ptr_self->row == 2) {
    r_mpfr_make_struct_init(ret1, ptr_ret1);
    r_mpfr_make_struct_init(ret2, ptr_ret2);
    r_mpfr_matrix_suitable_matrix_init (&ret3, &ptr_ret3, 2, 1);
    r_mpfr_matrix_suitable_matrix_init (&ret4, &ptr_ret4, 2, 1);
    res = mpfr_2d_square_matrix_eigenvalue(ptr_ret1, ptr_ret2, ptr_self);
    if (res > 0) {
      mpfr_2d_square_matrix_real_eigenvector(ptr_ret3, ptr_self, ptr_ret1);
      mpfr_2d_square_matrix_real_eigenvector(ptr_ret4, ptr_self, ptr_ret2);
      return rb_ary_new3(2, rb_ary_new3(2, ret1, ret2), rb_ary_new3(2, ret3, ret4));
    } else if (res == 0) {
      mpfr_2d_square_matrix_real_eigenvector(ptr_ret3, ptr_self, ptr_ret1);
      return rb_ary_new3(2, rb_ary_new3(1, ret1), rb_ary_new3(1, ret3));
    } else {
      MPC *c1, *c2;
      VALUE c_ret1, c_ret2;
      r_mpc_make_struct_init(c_ret1, c1);
      r_mpc_make_struct_init(c_ret2, c2);
      mpfr_set(c1->re, ptr_ret1, GMP_RNDN);
      mpfr_set(c2->re, ptr_ret1, GMP_RNDN);
      mpfr_set(c1->im, ptr_ret2, GMP_RNDN);
      mpfr_neg(c2->im, ptr_ret2, GMP_RNDN);
      return rb_ary_new3(2, rb_ary_new3(2, c_ret1, c_ret2), rb_ary_new3(2, Qnil, Qnil));
    }
  } else {
    rb_raise(rb_eArgError, "This method have not been implemented yet for matrix having more than two dimensions.");
  }
}

void Init_mpfr_matrix_extra()
{
  rb_define_method(r_mpfr_square_matrix, "inverse", r_mpfr_square_matrix_inverse, 0);
  rb_define_method(r_mpfr_square_matrix, "eigenvalue", r_mpfr_square_matrix_eigenvalue, 0);
  rb_define_method(r_mpfr_square_matrix, "eigenvector", r_mpfr_square_matrix_eigenvector, 0);

  r_mpfr_vector_2d_module = rb_define_module_under(r_mpfr_class, "Vector2D");

  rb_define_singleton_method(r_mpfr_vector_2d_module, "normal_vector", r_mpfr_vector_2d_normal_vector, 1);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "translating_and_scaling", r_mpfr_vector_2d_translating_and_scaling, 3);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "cos", r_mpfr_vector_2d_cos, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "transverse?", r_mpfr_vector_2d_transverse_p, 4);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "intersection_point", r_mpfr_vector_2d_intersection_point, 4);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "project", r_mpfr_vector_2d_project, 3);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "intersection_of_lines", r_mpfr_vector_2d_intersection_of_lines, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "intersection_of_lines2", r_mpfr_vector_2d_intersection_of_lines2, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "distance_between_point_and_line", r_mpfr_vector_2d_distance_between_point_and_line, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "maximum_distance_between_point_and_line", r_mpfr_vector_2d_maximum_distance_between_point_and_line, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "distance_between_lines", r_mpfr_vector_2d_distance_between_lines, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "maximum_distance_between_lines", r_mpfr_vector_2d_maximum_distance_between_lines, 2);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "close_points_on_lines_base", r_mpfr_vector_2d_close_points_on_lines_base, 3);
  rb_define_singleton_method(r_mpfr_vector_2d_module, "rectangle_each_pt", r_mpfr_vector_2d_rectangle_each_pt, 4);

  at = rb_intern("at");
  size = rb_intern("size");
}
