#include "func_mpfr_matrix_extra.h"

/* Square Matrix */

/* If inverse matrix does not exist, return 1. Otherwise return 0. */
int mpfr_2d_square_matrix_inverse_matrix (MPFRMatrix *inv, MPFRMatrix *x)
{
  MPFRMatrix *tmp;
  MPFR *det;
  r_mpfr_matrix_temp_alloc_init(tmp, inv->row, inv->column);
  r_mpfr_temp_alloc_init(det);
  mpfr_square_matrix_determinant(det, x);
  if (mpfr_cmp_si(det, 0) == 0) {
    return 1;
  } else {
    mpfr_ui_div(det, 1, det, GMP_RNDN);
    mpfr_mul(mpfr_matrix_get_element(tmp, 0, 0), mpfr_matrix_get_element(x, 1, 1), det, GMP_RNDN);
    mpfr_mul(mpfr_matrix_get_element(tmp, 1, 1), mpfr_matrix_get_element(x, 0, 0), det, GMP_RNDN);
    mpfr_neg(det, det, GMP_RNDN);
    mpfr_mul(mpfr_matrix_get_element(tmp, 0, 1), mpfr_matrix_get_element(x, 0, 1), det, GMP_RNDN);
    mpfr_mul(mpfr_matrix_get_element(tmp, 1, 0), mpfr_matrix_get_element(x, 1, 0), det, GMP_RNDN);
  }
  mpfr_matrix_set(inv, tmp);
  r_mpfr_matrix_temp_free(tmp);
  r_mpfr_temp_free(det);
  return 0;
}

/* x = -(sqrt(a11**2-2*a00*a11+4*a01*a10+a00**2)-a11-a00)/2.0E+0 */
/* x = (sqrt(a11**2-2*a00*a11+4*a01*a10+a00**2)+a11+a00)/2.0E+0 */
/* If there are two real eigenvalues, return positive number. */
/* If only one eigenvalue exists, return 0. */
/* If there are two complex eigenvalues, this functionreturn negative number and */
/* first returned value is real part and second one is imaginary part. */
int mpfr_2d_square_matrix_eigenvalue (MPFR *val1, MPFR *val2, MPFRMatrix *x)
{
  int ret;
  MPFR *d;
  r_mpfr_temp_alloc_init(d);

  mpfr_sub(val1, x->data, x->data + 3, GMP_RNDN);
  mpfr_mul(val1, val1, val1, GMP_RNDN);
  mpfr_mul(d, x->data + 1, x->data + 2, GMP_RNDN);
  mpfr_mul_ui(d, d, 4, GMP_RNDN);
  mpfr_add(d, d, val1, GMP_RNDN);

  ret = mpfr_cmp_si(d, 0);
  mpfr_add(val1, x->data, x->data + 3, GMP_RNDN);
  mpfr_div_ui(val1, val1, 2, GMP_RNDN);
  if (ret > 0) {
    mpfr_sqrt(d, d, GMP_RNDN);
    mpfr_div_ui(d, d, 2, GMP_RNDN);
    mpfr_sub(val2, val1, d, GMP_RNDN);
    mpfr_add(val1, val1, d, GMP_RNDN);
  } else if (ret < 0) {
    mpfr_neg(d, d, GMP_RNDN);
    mpfr_sqrt(d, d, GMP_RNDN);
    mpfr_div_ui(val2, d, 2, GMP_RNDN);
  } else {
    mpfr_set(val2, val1, GMP_RNDN);
  }

  r_mpfr_temp_free(d);
  return ret;
}

void mpfr_2d_square_matrix_real_eigenvector (MPFRMatrix *vec, MPFRMatrix *x, MPFR *eigenval)
{
  MPFRMatrix *tmp;
  r_mpfr_matrix_temp_alloc_init(tmp, 2, 1);

  if (mpfr_cmp_ui(x->data + 1, 0) == 0 && mpfr_cmp(x->data + 3, eigenval) == 0) {
    mpfr_set(tmp->data, x->data + 2, GMP_RNDN);
    mpfr_sub(tmp->data + 1, eigenval, x->data, GMP_RNDN);
  } else {
    mpfr_sub(tmp->data, eigenval, x->data + 3, GMP_RNDN);
    mpfr_set(tmp->data + 1, x->data + 1, GMP_RNDN);
  }
  if (mpfr_vector_normalize(vec, tmp) == 1) {
    gmp_printf("Argument matrix\n%.Ff\n%.Ff\n%.Ff\n%.Ff\n", x->data, x->data + 1, x->data + 2, x->data + 3);
    gmp_printf("Argument eigenvalue\n%.Ff\n", eigenval);
    rb_raise(rb_eArgError, "Invalid eigenvalue or eigenvector.");
  }
  r_mpfr_matrix_temp_free(tmp);
}



/* Vector */
void mpfr_distance_between_point_and_line_segment (MPFR *dist, MPFRMatrix *pt, MPFRMatrix *spt, MPFRMatrix *ept)
{
  MPFR *a, *b, *c, *d, *tmp_dist1, *tmp_dist2;
  r_mpfr_temp_alloc_init(a);
  r_mpfr_temp_alloc_init(b);
  r_mpfr_temp_alloc_init(c);
  r_mpfr_temp_alloc_init(d);
  r_mpfr_temp_alloc_init(tmp_dist1);
  r_mpfr_temp_alloc_init(tmp_dist2);

  mpfr_matrix_vector_distance(tmp_dist1, pt, spt);
  mpfr_matrix_vector_distance(tmp_dist2, pt, ept);
  mpfr_matrix_vector_distance(c, spt, ept);

  mpfr_mul(a, tmp_dist1, tmp_dist1, GMP_RNDN);
  mpfr_mul(b, tmp_dist2, tmp_dist2, GMP_RNDN);
  mpfr_mul(d, c, c, GMP_RNDN);

  mpfr_sub(dist, b, a, GMP_RNDN);
  mpfr_add(dist, dist, d, GMP_RNDN);
  mpfr_div(dist, dist, c, GMP_RNDN);
  mpfr_div_ui(dist, dist, 2, GMP_RNDN);
  if (mpfr_sgn(dist) < 0 || mpfr_cmp(dist, c) > 0) {
    if (mpfr_cmp(tmp_dist1, tmp_dist2) < 0) {
      mpfr_set(dist, tmp_dist1, GMP_RNDN);
    } else {
      mpfr_set(dist, tmp_dist2, GMP_RNDN);
    }
  } else {
    mpfr_mul(dist, dist, dist, GMP_RNDN);
    mpfr_sub(dist, b, dist, GMP_RNDN);
    mpfr_sqrt(dist, dist, GMP_RNDN);
  }
  r_mpfr_temp_free(a);
  r_mpfr_temp_free(b);
  r_mpfr_temp_free(c);
  r_mpfr_temp_free(d);
  r_mpfr_temp_free(tmp_dist1);
  r_mpfr_temp_free(tmp_dist2);
}

/* Return cosine of angle between vector a and b. */
void mpfr_vec2_cos (MPFR *cos, MPFRMatrix *a, MPFRMatrix *b)
{
  MPFRMatrix *l;
  r_mpfr_matrix_temp_alloc_init(l, 2, 1);
  
  mpfr_mul(l->data, a->data, b->data, GMP_RNDN);
  mpfr_mul(l->data + 1, a->data + 1, b->data + 1, GMP_RNDN);
  mpfr_add(cos, l->data, l->data + 1, GMP_RNDN);
  mpfr_matrix_vector_norm(l->data, a);
  mpfr_matrix_vector_norm(l->data + 1, b);
  mpfr_div(cos, cos, l->data, GMP_RNDN);
  mpfr_div(cos, cos, l->data + 1, GMP_RNDN);

  r_mpfr_matrix_temp_free(l);
}

/* Return 0 if line st has intersection with line uv. Otherwise return positive value. */
int mpfr_vec2_transverse_p (MPFRMatrix *s, MPFRMatrix *t, MPFRMatrix *u, MPFRMatrix *v)
{
  int ret = 0;
  MPFRMatrix *a, *b;
  MPFR *tmp[6];

  r_mpfr_matrix_temp_alloc_init(a, 2, 1);
  r_mpfr_matrix_temp_alloc_init(b, 2, 1);
  r_mpfr_temp_alloc_init(tmp[0]);
  r_mpfr_temp_alloc_init(tmp[1]);
    
  mpfr_matrix_sub(a, s, t);
  mpfr_matrix_sub(b, u, v);
  mpfr_mul(tmp[0], a->data, b->data + 1, GMP_RNDN);
  mpfr_mul(tmp[1], a->data + 1, b->data, GMP_RNDN);
  mpfr_sub(tmp[0], tmp[0], tmp[1], GMP_RNDN);

  if (mpfr_cmp_ui(tmp[0], 0) == 0) {
    if (mpfr_matrix_equal_p(s, u) != 0 && mpfr_matrix_equal_p(s, v) != 0 &&
	mpfr_matrix_equal_p(t, u) != 0 && mpfr_matrix_equal_p(t, v) != 0) {
      ret = 1;
    }
  } else {
    r_mpfr_temp_alloc_init(tmp[2]);
    r_mpfr_temp_alloc_init(tmp[3]);
    r_mpfr_temp_alloc_init(tmp[4]);
    r_mpfr_temp_alloc_init(tmp[5]);

    mpfr_sub(tmp[3], u->data + 1, s->data + 1, GMP_RNDN);
    mpfr_sub(tmp[4], u->data, s->data, GMP_RNDN);
    mpfr_mul(tmp[1], tmp[3], a->data, GMP_RNDN);
    mpfr_mul(tmp[5], tmp[4], a->data + 1, GMP_RNDN);
    mpfr_sub(tmp[1], tmp[1], tmp[5], GMP_RNDN);

    mpfr_sub(tmp[2], v->data + 1, s->data + 1, GMP_RNDN);
    mpfr_mul(tmp[2], tmp[2], a->data, GMP_RNDN);
    mpfr_sub(tmp[5], v->data, s->data, GMP_RNDN);
    mpfr_mul(tmp[5], tmp[5], a->data + 1, GMP_RNDN);
    mpfr_sub(tmp[2], tmp[2], tmp[5], GMP_RNDN);
    if (mpfr_sgn(tmp[1]) * mpfr_sgn(tmp[2]) <= 0) {
      mpfr_mul(tmp[1], tmp[4], b->data + 1, GMP_RNDN);
      mpfr_mul(tmp[5], tmp[3], b->data, GMP_RNDN);
      mpfr_sub(tmp[1], tmp[1], tmp[5], GMP_RNDN);

      mpfr_sub(tmp[2], t->data + 1, u->data + 1, GMP_RNDN);
      mpfr_mul(tmp[2], tmp[2], b->data, GMP_RNDN);
      mpfr_sub(tmp[5], t->data, u->data, GMP_RNDN);
      mpfr_mul(tmp[5], tmp[5], b->data + 1, GMP_RNDN);
      mpfr_sub(tmp[2], tmp[2], tmp[5], GMP_RNDN);
      if (mpfr_sgn(tmp[1]) * mpfr_sgn(tmp[2]) > 0) {
	ret = 3;
      }
    } else {
      ret = 2;
    }
    
    r_mpfr_temp_free(tmp[2]);
    r_mpfr_temp_free(tmp[3]);
    r_mpfr_temp_free(tmp[4]);
    r_mpfr_temp_free(tmp[5]);
  }
  
  r_mpfr_matrix_temp_free(a);
  r_mpfr_matrix_temp_free(b);
  r_mpfr_temp_free(tmp[0]);
  r_mpfr_temp_free(tmp[1]);
  return ret;
}

int mpfr_vec2_intersection_point (MPFRMatrix *new, MPFRMatrix *s, MPFRMatrix *t, MPFRMatrix *u, MPFRMatrix *v)
{
  int ret = 0;
  MPFRMatrix *a, *b;
  MPFR *tmp[6];

  r_mpfr_matrix_temp_alloc_init(a, 2, 1);
  r_mpfr_matrix_temp_alloc_init(b, 2, 1);
  r_mpfr_temp_alloc_init(tmp[0]);
  r_mpfr_temp_alloc_init(tmp[1]);
    
  mpfr_matrix_sub(a, s, t);
  mpfr_matrix_sub(b, u, v);
  mpfr_mul(tmp[0], a->data, b->data + 1, GMP_RNDN);
  mpfr_mul(tmp[1], a->data + 1, b->data, GMP_RNDN);
  mpfr_sub(tmp[0], tmp[0], tmp[1], GMP_RNDN); /* tmp[0] will be used last in this function */

  if (mpfr_cmp_ui(tmp[0], 0) == 0) {
    if (mpfr_matrix_equal_p(s, u) == 0 || mpfr_matrix_equal_p(s, v) == 0) {
      mpfr_matrix_set(new, s);
    } else if (mpfr_matrix_equal_p(t, u) == 0|| mpfr_matrix_equal_p(t, v) == 0) {
      mpfr_matrix_set(new, t);
    } else {
      ret = 1;
    }
  } else {
    r_mpfr_temp_alloc_init(tmp[2]);
    r_mpfr_temp_alloc_init(tmp[3]);
    r_mpfr_temp_alloc_init(tmp[4]);
    r_mpfr_temp_alloc_init(tmp[5]);

    mpfr_sub(tmp[3], u->data + 1, s->data + 1, GMP_RNDN); /* u1 - s1 */
    mpfr_sub(tmp[4], u->data, s->data, GMP_RNDN);	       /* u0 - s0 */
    mpfr_mul(tmp[1], tmp[3], a->data, GMP_RNDN);
    mpfr_mul(tmp[5], tmp[4], a->data + 1, GMP_RNDN);
    mpfr_sub(tmp[1], tmp[1], tmp[5], GMP_RNDN);

    mpfr_sub(tmp[2], v->data + 1, s->data + 1, GMP_RNDN);
    mpfr_mul(tmp[2], tmp[2], a->data, GMP_RNDN);
    mpfr_sub(tmp[5], v->data, s->data, GMP_RNDN);
    mpfr_mul(tmp[5], tmp[5], a->data + 1, GMP_RNDN);
    mpfr_sub(tmp[2], tmp[2], tmp[5], GMP_RNDN);
    if (mpfr_sgn(tmp[1]) * mpfr_sgn(tmp[2]) <= 0) {
      mpfr_mul(tmp[1], tmp[4], b->data + 1, GMP_RNDN);
      mpfr_mul(tmp[5], tmp[3], b->data, GMP_RNDN);
      mpfr_sub(tmp[1], tmp[1], tmp[5], GMP_RNDN);

      mpfr_sub(tmp[2], t->data + 1, u->data + 1, GMP_RNDN);
      mpfr_mul(tmp[2], tmp[2], b->data, GMP_RNDN);
      mpfr_sub(tmp[5], t->data, u->data, GMP_RNDN);
      mpfr_mul(tmp[5], tmp[5], b->data + 1, GMP_RNDN);
      mpfr_sub(tmp[2], tmp[2], tmp[5], GMP_RNDN);
      if (mpfr_sgn(tmp[1]) * mpfr_sgn(tmp[2]) <= 0) {
	mpfr_mul(new->data, b->data + 1, v->data, GMP_RNDN);
	mpfr_sub(tmp[1], t->data + 1, v->data + 1, GMP_RNDN);
	mpfr_mul(tmp[1], tmp[1], b->data, GMP_RNDN);
	mpfr_add(new->data, new->data, tmp[1], GMP_RNDN);
	mpfr_mul(new->data, new->data, a->data, GMP_RNDN);
	mpfr_mul(tmp[1], b->data, a->data + 1, GMP_RNDN);
	mpfr_mul(tmp[1], tmp[1], t->data, GMP_RNDN);
	mpfr_sub(new->data, new->data, tmp[1], GMP_RNDN);
	mpfr_div(new->data, new->data, tmp[0], GMP_RNDN);
	mpfr_abs(tmp[3], a->data, GMP_RNDN);
	mpfr_abs(tmp[4], b->data, GMP_RNDN);
	if (mpfr_cmp(tmp[3], tmp[4]) >= 0) {
	  mpfr_sub(new->data + 1, new->data, s->data, GMP_RNDN);
	  mpfr_mul(new->data + 1, new->data + 1, a->data + 1, GMP_RNDN);
	  mpfr_div(new->data + 1, new->data + 1, a->data, GMP_RNDN);
	  mpfr_add(new->data + 1, new->data + 1, s->data + 1, GMP_RNDN);
	} else {
	  mpfr_sub(new->data + 1, new->data, u->data, GMP_RNDN);
	  mpfr_mul(new->data + 1, new->data + 1, b->data + 1, GMP_RNDN);
	  mpfr_div(new->data + 1, new->data + 1, b->data, GMP_RNDN);
	  mpfr_add(new->data + 1, new->data + 1, u->data + 1, GMP_RNDN);
	}
      } else {
	ret = 3;
      }
    } else {
      ret = 2;
    }
    
    r_mpfr_temp_free(tmp[2]);
    r_mpfr_temp_free(tmp[3]);
    r_mpfr_temp_free(tmp[4]);
    r_mpfr_temp_free(tmp[5]);
  }
  
  r_mpfr_matrix_temp_free(a);
  r_mpfr_matrix_temp_free(b);
  r_mpfr_temp_free(tmp[0]);
  r_mpfr_temp_free(tmp[1]);
  return ret;
}

/* This function solves  */
/* (1 - a) * s[0] + a * t[0] = (1 - b) * u[0] + b * v[0] */
/* (1 - a) * s[1] + a * t[1] = (1 - b) * u[1] + b * v[1] */
/* and returns */
/* 0 and substitutes *pt[] for coordinate of intersection. */
/* If lines do not have intersection, this function returns negative value. */
/* The solutions a and b are */
/* a = (s[0]*(v[1]-u[1])+u[0]*(s[1]-v[1])+v[0]*(u[1]-s[1]))/ */
/*   (s[0]*(v[1]-u[1])+t[0]*(u[1]-v[1])+v[0]*(t[1]-s[1])+u[0]*(s[1]-t[1])) */
/* b = -(s[0]*(u[1]-t[1])+t[0]*(s[1]-u[1])+u[0]*(t[1]-s[1]))/ */
/*   (s[0]*(v[1]-u[1])+t[0]*(u[1]-v[1])+v[0]*(t[1]-s[1])+u[0]*(s[1]-t[1])) */
/* int mpfr_vec2_transverse (MPFRMatrix *pt, MPFRMatrix *s, MPFRMatrix *t, MPFRMatrix *u, MPFRMatrix *v) { */
/*   MPFR *a, *b, *c, *d; */
/*   MPFRMatrix *tmp; */

/*   r_mpfr_temp_alloc_init(a); */
/*   r_mpfr_temp_alloc_init(b); */
/*   r_mpfr_temp_alloc_init(c); */
/*   r_mpfr_temp_alloc_init(d); */
/*   r_mpfr_matrix_temp_alloc_init(tmp, 3, 1); */

/*   int ret = -60, condition; */

/*   if (mpfr_cmp(s->data + 1, t->data + 1) > 0) { */
/*     mpfr_set(a, s->data + 1); */
/*     mpfr_set(b, t->data + 1); */
/*   } else { */
/*     mpfr_set(a, t->data + 1); */
/*     mpfr_set(b, s->data + 1); */
/*   } */
/*   if (mpfr_cmp(u->data + 1, v->data + 1) > 0) { */
/*     mpfr_set(c, u->data + 1); */
/*     mpfr_set(d, v->data + 1); */
/*   } else { */
/*     mpfr_set(c, v->data + 1); */
/*     mpfr_set(d, u->data + 1); */
/*   } */

/*   if (mpfr_cmp(b, c) > 0 || mpfr_cmp(d, a) > 0) { */
/*     return -50; */
/*   } */

/*   mpfr_sub(tmp->data, a, b); */
/*   mpfr_sub(tmp->data + 1, c, d); */

/*   if (mpfr_cmp(s->data, t->data) > 0) { */
/*     mpfr_set(a, s->data); */
/*     mpfr_set(b, t->data); */
/*   } else { */
/*     mpfr_set(a, t->data); */
/*     mpfr_set(b, s->data); */
/*   } */
/*   if (mpfr_cmp(u->data, v->data) > 0) { */
/*     mpfr_set(c, u->data); */
/*     mpfr_set(d, v->data); */
/*   } else { */
/*     mpfr_set(c, v->data); */
/*     mpfr_set(d, u->data); */
/*   } */

/*   if (mpfr_cmp(b, c) > 0 || mpfr_cmp(d, a) > 0) { */
/*     return -51; */
/*   } */

/*   mpfr_sub(tmp->data + 2, a, b); */
/*   mpfr_add(tmp->data, tmp->data, tmp->data + 2); */
/*   mpfr_sub(tmp->data + 2, c, d); */
/*   mpfr_add(tmp->data + 1, tmp->data + 1, tmp->data + 2); */
/*   if (mpfr_cmp(tmp->data, tmp->data + 1) < 0) { */
/*     condition = 0; */
/*   } else { */
/*     condition = 1; */
/*   } */

/*   mpfr_sub(a, u->data, v->data); */
/*   mpfr_sub(b, u->data + 1, v->data + 1); */
/*   mpfr_sub(c, s->data + 1, t->data + 1); */
/*   mpfr_sub(d, s->data, t->data); */

/*   if (mpfr_cmp_si(a, 0) == 0 || mpfr_cmp_si(d, 0) == 0) { */
/*     if (mpfr_cmp_si(a, 0) == 0 && mpfr_cmp_si(d, 0) == 0) { */
/*       if (mpfr_cmp(s->data, u->data) == 0) { */
/* 	if ((mpfr_cmp(s->data + 1, u->data + 1) * mpfr_cmp(s->data + 1, v->data + 1) > 0) && */
/* 	   (mpfr_cmp(t->data + 1, u->data + 1) * mpfr_cmp(t->data + 1, v->data + 1) > 0) && */
/* 	   (mpfr_cmp(s->data + 1, u->data + 1) * mpfr_cmp(t->data + 1, u->data + 1) > 0)) { */
/* 	  ret = -30; */
/* 	} else { */
/* 	  ret = -10; */
/* 	} */
/*       } */
/*     } else if (mpfr_cmp_si(a, 0) == 0 && mpfr_cmp_si(b, 0) == 0) { */
/*       ret = -40; */
/*     } else if (mpfr_cmp_si(d, 0) == 0 && mpfr_cmp_si(c, 0) == 0) { */
/*       ret = -41; */
/*     } else if (mpfr_cmp_si(a, 0) == 0) { */
/*       mpfr_sub(pt->data, u->data, s->data); */
/*       mpfr_mul(pt->data, pt->data, c); */
/*       mpfr_div(pt->data, pt->data, d); */
/*       mpfr_add(pt->data, pt->data, s->data + 1); */
/*       if (mpfr_cmp(pt->data, u->data + 1) * mpfr_cmp(pt->data, v->data + 1) < 0) { */
/* 	ret = 0; */
/* 	mpfr_set(pt->data + 1, u->data); */
/*       } else { */
/* 	ret = -20; */
/*       } */
/*     } else if (mpfr_cmp_si(d, 0) == 0) { */
/*       mpfr_sub(pt->data, s->data, u->data); */
/*       mpfr_mul(pt->data, pt->data, b); */
/*       mpfr_div(pt->data, pt->data, a); */
/*       mpfr_add(pt->data, pt->data, u->data + 1); */
/*       if (mpfr_cmp(pt->data, s->data + 1) * mpfr_cmp(pt->data, t->data + 1) < 0) { */
/* 	ret = 0; */
/* 	mpfr_set(pt->data + 1, s->data); */
/*       } else { */
/* 	ret = -21; */
/*       } */
/*     } */
/*   } else { */
/*     mpfr_div(pt->data, b, a); */
/*     mpfr_div(pt->data + 1, c, d); */
/*     if (mpfr_cmp(pt->data, pt->data + 1) == 0) { */
/*       mpfr_sub(a, s->data, u->data); */
/*       if (mpfr_cmp_si(a, 0) == 0) { */
/* 	ret = -11; */
/*       } else { */
/* 	mpfr_sub(b, s->data + 1, u->data + 1); */
/* 	mpfr_div(a, b, a); */
/* 	if (mpfr_cmp(a, pt->data) == 0) { */
/* 	  if ((mpfr_cmp(s->data + 1, u->data + 1) * mpfr_cmp(s->data + 1, v->data + 1) > 0) && */
/* 	     (mpfr_cmp(t->data + 1, u->data + 1) * mpfr_cmp(t->data + 1, v->data + 1) > 0) && */
/* 	     (mpfr_cmp(s->data + 1, u->data + 1) * mpfr_cmp(t->data + 1, u->data + 1) > 0)) { */
/* 	    ret = -31; */
/* 	  } else { */
/* 	    ret = -12; */
/* 	  } */
/* 	} else { */
/* 	  ret = -32; */
/* 	} */
/*       } */
/*     } else { */
/*       mpfr_mul(pt->data + 1, c, a); */
/*       mpfr_mul(pt->data, d, b); */
/*       mpfr_sub(pt->data + 1, pt->data + 1, pt->data); */

/*       mpfr_mul(a, a, s->data + 1); */
/*       mpfr_mul(b, b, s->data); */
/*       mpfr_sub(a, a, b); */
/*       mpfr_mul(b, u->data, v->data + 1); */
/*       mpfr_sub(a, a, b); */
/*       mpfr_mul(b, u->data + 1, v->data); */
/*       mpfr_add(a, a, b); */
/*       mpfr_div(a, a, pt->data + 1); */
        
/*       if ((mpfr_cmp_si(a, 0) < 0) || (mpfr_cmp_si(a, 1) > 0)) { */
/* 	ret = -21; */
/*       } else { */
/* 	mpfr_mul(c, c, u->data); */
/* 	mpfr_mul(d, d, u->data + 1); */
/* 	mpfr_sub(b, c, d); */
/* 	mpfr_mul(c, s->data, t->data + 1); */
/* 	mpfr_add(b, b, c); */
/* 	mpfr_mul(c, s->data + 1, t->data); */
/* 	mpfr_sub(b, b, c); */
/* 	mpfr_div(b, b, pt->data + 1); */
      
/* 	if ((mpfr_cmp_si(b, 0) < 0) || (mpfr_cmp_si(b, 1) > 0)) { */
/* 	  ret = -22; */
/* 	} else { */
/* 	  ret = 0; */
/* 	  if (condition == 0) { */
/* 	    mpfr_ui_sub(b, 1, a); */
/* 	    mpfr_mul(c, b, s->data); */
/* 	    mpfr_mul(d, a, t->data); */
/* 	    mpfr_add(pt->data, c, d); */
/* 	    mpfr_mul(c, b, s->data + 1); */
/* 	    mpfr_mul(d, a, t->data + 1); */
/* 	    mpfr_add(pt->data + 1, c, d); */
/* 	  } else { */
/* 	    mpfr_ui_sub(a, 1, b); */
/* 	    mpfr_mul(c, a, u->data); */
/* 	    mpfr_mul(d, b, v->data); */
/* 	    mpfr_add(pt->data, c, d); */
/* 	    mpfr_mul(c, a, u->data + 1); */
/* 	    mpfr_mul(d, b, v->data + 1); */
/* 	    mpfr_add(pt->data + 1, c, d); */
/* 	  } */
/* 	} */
/*       } */
/*     } */
/*   } */

/*   r_mpfr_temp_free(a); */
/*   r_mpfr_temp_free(b); */
/*   r_mpfr_temp_free(c); */
/*   r_mpfr_temp_free(d); */
/*   r_mpfr_matrix_temp_free(tmp); */
/*   return ret; */
/* } */

/* This function solves w1=a*u1+b*v1,w2=a*u2+b*v2 for a, b */
/* (w = a * u + b * v, */
/*  a, b: scalar, */
/*  u, v, w: vector) */
/* and set the solution to a[0] and a[1]. */
/* a = -(v1*w2-v2*w1)/(u1*v2-u2*v1) */
/* b = (u1*w2-u2*w1)/(u1*v2-u2*v1) */
/* c = u1*v2 - u2*v1 */
void mpfr_vec2_project (MPFRMatrix *a, MPFRMatrix *w, MPFRMatrix *u, MPFRMatrix *v)
{
  MPFR *x, *y;

  r_mpfr_temp_alloc_init(x);
  r_mpfr_temp_alloc_init(y);

  mpfr_mul(x, v->data, w->data + 1, GMP_RNDN);
  mpfr_mul(y, v->data + 1, w->data, GMP_RNDN);
  mpfr_sub(a->data, y, x, GMP_RNDN);

  mpfr_mul(x, u->data, w->data + 1, GMP_RNDN);
  mpfr_mul(y, u->data + 1, w->data, GMP_RNDN);
  mpfr_sub(a->data + 1, x, y, GMP_RNDN);

  mpfr_mul(x, u->data, v->data + 1, GMP_RNDN);
  mpfr_mul(y, u->data + 1, v->data, GMP_RNDN);
  mpfr_sub(x, x, y, GMP_RNDN);
  
  mpfr_div(a->data, a->data, x, GMP_RNDN);
  mpfr_div(a->data + 1, a->data + 1, x, GMP_RNDN);

  r_mpfr_temp_free(x);
  r_mpfr_temp_free(y);
}
