require 'mpfr'
require 'mpc'
require 'mpfr/matrix'
require 'mpfr_matrix_extra.so'

require 'yaml'

class MPFR
  class Matrix

    def to_yaml(opts={})
      YAML::quick_emit(object_id, opts) do |out|
        out.scalar("tag:yt,2008-12-29:#{self.class}", str_ary2("%.Re").to_yaml)
      end
    end

    def bounded?
      each_element do |el|
        unless el.number_p
          return false
        end
      end
      true
    end

  end

  module Vector

    def to_yaml(opts={})
      YAML::quick_emit(object_id, opts) do |out|
        out.scalar("tag:yt,2008-12-29:#{self.class}", str_ary("%.Re").to_yaml)
      end
    end

  end

end

YAML.add_domain_type("yt,2008-12-29", "MPFR::ColumnVector") { |type, val|
  MPFR::ColumnVector.new(YAML.load(val))
}

YAML.add_domain_type("yt,2008-12-29", "MPFR::RowVector") { |type, val|
  MPFR::RowVector.new(YAML.load(val))
}

YAML.add_domain_type("yt,2008-12-29", "MPFR::Matrix") { |type, val|
  MPFR::Matrix.new(YAML.load(val))
}

YAML.add_domain_type("yt,2008-12-29", "MPFR::SquareMatrix") { |type, val|
  MPFR::SquareMatrix.new(YAML.load(val))
}
