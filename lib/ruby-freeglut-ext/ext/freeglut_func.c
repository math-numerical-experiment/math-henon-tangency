#include "ruby_freeglut_ext.h"

static VALUE r_glutMainLoopEvent (VALUE self)
{
  glutMainLoopEvent();
  return Qnil;
}

static VALUE r_glutLeaveMainLoop (VALUE self)
{
  glutLeaveMainLoop();
  return Qnil;
}

/* FGAPI void    FGAPIENTRY glutMouseWheelFunc( void (* callback)( int, int, int, int ) ); */
/* FGAPI void    FGAPIENTRY glutCloseFunc( void (* callback)( void ) ); */
/* FGAPI void    FGAPIENTRY glutWMCloseFunc( void (* callback)( void ) ); */
/* FGAPI void    FGAPIENTRY glutMenuDestroyFunc( void (* callback)( void ) ); */

static VALUE r_glutSetOption (VALUE self, VALUE arg1, VALUE arg2)
{
  glutSetOption(NUM2INT(arg1), NUM2INT(arg2));
  return Qnil;
}

static VALUE r_glutGetWindowData (VALUE self)
{
  glutGetWindowData();
  return Qnil;
}

/* FGAPI void    FGAPIENTRY glutSetWindowData(void* data); */

static VALUE r_glutGetMenuData (VALUE self)
{
  glutGetMenuData();
  return Qnil;
}

/* FGAPI void    FGAPIENTRY glutSetMenuData(void* data); */

/* FGAPI int     FGAPIENTRY glutBitmapHeight( void* font ); */
/* FGAPI GLfloat FGAPIENTRY glutStrokeHeight( void* font ); */
/* FGAPI void    FGAPIENTRY glutBitmapString( void* font, const unsigned char *string ); */
/* FGAPI void    FGAPIENTRY glutStrokeString( void* font, const unsigned char *string ); */

static VALUE r_glutWireRhombicDodecahedron (VALUE self)
{
  glutWireRhombicDodecahedron();
  return Qnil;
}

static VALUE r_glutSolidRhombicDodecahedron (VALUE self)
{
  glutSolidRhombicDodecahedron();
  return Qnil;
}

/* FGAPI void    FGAPIENTRY glutWireSierpinskiSponge ( int num_levels, GLdouble offset[3], GLdouble scale ) ; */
/* FGAPI void    FGAPIENTRY glutSolidSierpinskiSponge ( int num_levels, GLdouble offset[3], GLdouble scale ) ; */
/* FGAPI void    FGAPIENTRY glutWireCylinder( GLdouble radius, GLdouble height, GLint slices, GLint stacks); */
/* FGAPI void    FGAPIENTRY glutSolidCylinder( GLdouble radius, GLdouble height, GLint slices, GLint stacks); */

/* FGAPI GLUTproc FGAPIENTRY glutGetProcAddress( const char *procName ); */

void initialize_freeglut_func (VALUE module_glut)
{
  rb_define_module_function(module_glut, "glutMainLoopEvent", r_glutMainLoopEvent, 0);
  rb_define_module_function(module_glut, "glutLeaveMainLoop", r_glutLeaveMainLoop, 0);
  rb_define_module_function(module_glut, "glutSetOption", r_glutSetOption, 2);


  rb_define_module_function(module_glut, "glutGetWindowData", r_glutGetWindowData, 0);
  rb_define_module_function(module_glut, "glutGetMenuData", r_glutGetMenuData, 0);
  rb_define_module_function(module_glut, "glutWireRhombicDodecahedron", r_glutWireRhombicDodecahedron, 0);
  rb_define_module_function(module_glut, "glutSolidRhombicDodecahedron", r_glutSolidRhombicDodecahedron, 0);
  
}
