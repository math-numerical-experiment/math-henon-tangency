#include "ruby_freeglut_ext.h"

static VALUE module_glut;

void Init_ruby_freeglut_ext ()
{
  module_glut = rb_define_module("Glut");

  initialize_freeglut_const(module_glut);
  initialize_freeglut_func(module_glut);
}
