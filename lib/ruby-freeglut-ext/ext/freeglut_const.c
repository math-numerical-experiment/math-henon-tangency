#include "ruby_freeglut_ext.h"

void initialize_freeglut_const (VALUE module_glut)
{
  rb_define_const(module_glut, "GLUT_ACTION_EXIT", INT2NUM(GLUT_ACTION_EXIT));
  rb_define_const(module_glut, "GLUT_ACTION_GLUTMAINLOOP_RETURNS", INT2NUM(GLUT_ACTION_GLUTMAINLOOP_RETURNS));
  rb_define_const(module_glut, "GLUT_ACTION_CONTINUE_EXECUTION", INT2NUM(GLUT_ACTION_CONTINUE_EXECUTION));
  rb_define_const(module_glut, "GLUT_CREATE_NEW_CONTEXT", INT2NUM(GLUT_CREATE_NEW_CONTEXT));
  rb_define_const(module_glut, "GLUT_USE_CURRENT_CONTEXT", INT2NUM(GLUT_USE_CURRENT_CONTEXT));
  rb_define_const(module_glut, "GLUT_FORCE_INDIRECT_CONTEXT", INT2NUM(GLUT_FORCE_INDIRECT_CONTEXT));
  rb_define_const(module_glut, "GLUT_ALLOW_DIRECT_CONTEXT", INT2NUM(GLUT_ALLOW_DIRECT_CONTEXT));
  rb_define_const(module_glut, "GLUT_TRY_DIRECT_CONTEXT", INT2NUM(GLUT_TRY_DIRECT_CONTEXT));
  rb_define_const(module_glut, "GLUT_FORCE_DIRECT_CONTEXT", INT2NUM(GLUT_FORCE_DIRECT_CONTEXT));
  rb_define_const(module_glut, "GLUT_ACTION_ON_WINDOW_CLOSE", INT2NUM(GLUT_ACTION_ON_WINDOW_CLOSE));
  rb_define_const(module_glut, "GLUT_WINDOW_BORDER_WIDTH", INT2NUM(GLUT_WINDOW_BORDER_WIDTH));
  rb_define_const(module_glut, "GLUT_WINDOW_HEADER_HEIGHT", INT2NUM(GLUT_WINDOW_HEADER_HEIGHT));
  rb_define_const(module_glut, "GLUT_VERSION", INT2NUM(GLUT_VERSION));
  rb_define_const(module_glut, "GLUT_RENDERING_CONTEXT", INT2NUM(GLUT_RENDERING_CONTEXT));
  rb_define_const(module_glut, "GLUT_DIRECT_RENDERING", INT2NUM(GLUT_DIRECT_RENDERING));
  rb_define_const(module_glut, "GLUT_AUX1", INT2NUM(GLUT_AUX1));
  rb_define_const(module_glut, "GLUT_AUX2", INT2NUM(GLUT_AUX2));
  rb_define_const(module_glut, "GLUT_AUX3", INT2NUM(GLUT_AUX3));
  rb_define_const(module_glut, "GLUT_AUX4", INT2NUM(GLUT_AUX4));

}

