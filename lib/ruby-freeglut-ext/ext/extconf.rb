require 'mkmf'
dir_config('glut')

$CFLAGS += ' -Wall'

if have_header('GL/freeglut.h') && have_library('glut')
  create_makefile("ruby_freeglut_ext")
end
