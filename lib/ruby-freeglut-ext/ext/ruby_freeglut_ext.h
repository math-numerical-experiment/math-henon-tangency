#ifndef _RUBY_FREEGLUT_EXT_H_
#define _RUBY_FREEGLUT_EXT_H_

#include <ruby.h>
#include <GL/freeglut.h>

void initialize_freeglut_const(VALUE module_glut);
void initialize_freeglut_func(VALUE module_glut);

#endif /* _RUBY_FREEGLUT_EXT_H_ */
