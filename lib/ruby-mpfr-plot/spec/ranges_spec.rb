require_relative 'spec_helper'

MPFR.set_default_prec(256)

describe MPFR::Obj2D, "when calculating ranges of MPFR objects" do
  it "should return maximum and minimum." do
    data = [[1, 2], [2, 2], [-1, 0], [0.2, -0.9]].map { |a| MPFR::ColumnVector.new(a) }
    obj = MPFR::Obj2D.new(:style => MPFRPlot::STY_LINES, :data => data)
    res = obj.ranges
    res[0][0].should == MPFR.new(-1)
    res[0][1].should == MPFR.new(2)
    res[1][0].should == MPFR.new(-0.9)
    res[1][1].should == MPFR.new(2)
  end
end

describe MPFR::Obj2D, "when calculating ranges of MPFI objects" do
  it "should return maximum and minimum." do
    data = [[[1, 2], [-3, 2]], [[2, 2], [-1, 0]], [[-0.2, 0.9], [3, 8]]].map { |a| MPFI::ColumnVector.interval(a) }
    obj = MPFR::Obj2D.new(:style => MPFRPlot::STY_LINES, :data => data)
    res = obj.ranges
    res[0][0].should == MPFR.new(-0.2)
    res[0][1].should == MPFR.new(2)
    res[1][0].should == MPFR.new(-3)
    res[1][1].should == MPFR.new(8)
  end
end

