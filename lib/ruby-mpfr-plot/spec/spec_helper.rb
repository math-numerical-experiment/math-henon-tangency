begin
  require 'rspec'
rescue LoadError
  require 'rubygems' unless ENV['NO_RUBYGEMS']
  gem 'rspec'
  require 'rspec'
end

require_relative "../../henon_tangency.rb"
require 'pp'
require 'mpfr'
require 'mpc'
require 'mpfr/matrix'
require 'mpfi'
require 'mpfi/complex'
require 'mpfi/matrix'
require 'ruby_mpfr_plot.rb'
