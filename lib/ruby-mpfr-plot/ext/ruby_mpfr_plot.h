#ifndef _RUBY_MPFR_PLOT_H_
#define _RUBY_MPFR_PLOT_H_

#include <ruby.h>
#include <stdio.h>
#include <ruby.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>
#include "ruby_mpfr.h"
#include "ruby_mpc.h"
#include "ruby_mpfr_matrix.h"
#include "ruby_mpfi.h"
#include "ruby_mpfi_complex.h"
#include "ruby_mpfi_matrix.h"

#include "display_func_fr.h"

typedef enum __mpfr_plot_type { MPFR_PLOT_TYPE_FR, MPFR_PLOT_TYPE_FI } MPFRPlotType;

void rb_mpfr_screen_set_dbl_from_fr(DataObject *dtobj, VALUE ary, int start, MPFRScreen2D *screen);
void rb_mpfr_screen_set_dbl_from_fi(DataObject *dtobj, VALUE ary, int start, MPFRScreen2D *screen);
void rb_mpfr_screen_set_quads_from_fi(DataObject *dtobj, VALUE ary, int start, MPFRScreen2D *screen);

VALUE rb_dataobj, rb_mpfr_plot, rb_mpfr_screen;

#endif /* _RUBY_MPFR_PLOT_H_ */
