require 'mkmf'
require "extconf_task/mkmf_utils"

unless File.exist?(File.join(__dir__, "dataobj.c"))
  system("ruby #{File.join(__dir__, "import_files.rb")} import")
end

i = 0
while i < ARGV.size
  case ARGV[i]
  when '--ldflags'
    if args = ARGV[i+1]
      i += 1
      $LDFLAGS += " #{args}"
    end
  else
    raise "Invalid option: #{ARGV[i]}"
  end
  i += 1
end

find_header_in_gem("ruby_mpfr.h", "ruby-mpfr")
find_header_in_gem("ruby_mpfr_matrix.h", "ruby-mpfr")
find_header_in_gem("ruby_mpc.h", "ruby-mpc")
find_header_in_gem("ruby_mpfi.h", "ruby-mpfi")
find_header_in_gem("ruby_mpfi_matrix.h", "ruby-mpfi")
find_header_in_gem("ruby_mpfi_complex.h", "ruby-mpfi")

dir_config('mpfr')
dir_config('gmp')
dir_config('glut')
dir_config('GLC')
dir_config("glib-2.0", pkg_config("glib-2.0"))

$CFLAGS += " -Wall"

if have_header('mpfr.h') && have_library('mpfr') && have_header('gmp.h') && have_library('gmp') &&
    have_library('GLC')  && have_header('glib.h') && have_library('glib-2.0')
  create_makefile("ruby_mpfr_plot")
end
