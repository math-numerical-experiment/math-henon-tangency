#include "ruby_mpfr_plot.h"

#define DATAFILE_PATH "@path"
#define DATAFILE_PLOT_STYLE "@plot_style"
#define DATAFILE_TYPE "@data_type"
#define DATAFILE_MAX_DATA_STRING_SIZE_ONE_LINE 256

ID __parse_file_line__;

static void rb_dataobj_free(void *ptr)
{
  dataobj_free(ptr);
  free(ptr);
}

static VALUE rb_dataobj_alloc (VALUE self)
{
  DataObject *dtobj;
  self = Data_Make_Struct(rb_dataobj, DataObject, 0, rb_dataobj_free, dtobj);
  return self;
}

static VALUE rb_dataobj_initialize (VALUE self)
{
  DataObject *dtobj;
  Data_Get_Struct(self, DataObject, dtobj);
  dataobj_init(dtobj);
  return self;
}

static VALUE rb_dataobj_each (VALUE self)
{
  DataObject *dtobj;
  VALUE ret = Qnil;
  int i;
  Data_Get_Struct(self, DataObject, dtobj);
  for (i = 0; i < dtobj->data_size; i++) {
    volatile VALUE num = rb_float_new(*ptdata_get_dbl_ptr(dtobj->data, i));
    ret = rb_yield(num);
  }
  
  return ret;
}

static VALUE rb_dataobj_size (VALUE self)
{
  DataObject *dtobj;
  Data_Get_Struct(self, DataObject, dtobj);
  return INT2NUM(dtobj->data_size);
}

static VALUE rb_dataobj_add(int argc, VALUE *argv, VALUE self)
{
  DataObject *dtobj;
  int i;
  Data_Get_Struct(self, DataObject, dtobj);
  for (i = 0; i < argc; i++) {
    dataobj_add_dbl(dtobj, NUM2DBL(argv[i]));
  }
  return Qnil;
}

static VALUE rb_dataobj_clear (VALUE self)
{
  DataObject *dtobj;
  Data_Get_Struct(self, DataObject, dtobj);
  dataobj_free(dtobj);
  return Qnil;
}

static void rb_mpfr_screen_free(MPFRScreen2D *screen)
{
  mpfr_clear(screen->base_pt);
  mpfr_clear(screen->base_pt + 1);
  mpfr_clear(screen->base_scale);
  free(screen->base_pt);
  free(screen);
}

static VALUE rb_mpfr_screen_alloc (VALUE self)
{
  MPFRScreen2D *screen;
  self = Data_Make_Struct(rb_mpfr_screen, MPFRScreen2D, 0, rb_mpfr_screen_free, screen);
  mpfr_screen2d_init(screen, 800, 800);
  return self;
}

static VALUE rb_mpfr_screen_initialize (VALUE self, VALUE center, VALUE scale)
{
  MPFRScreen2D *screen;
  MPFR *ptr_scale;
  MPFRMatrix *ptr_center;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_get_struct(ptr_scale, scale);
  r_mpfr_get_matrix_struct(ptr_center, center);

  mpfr_screen2d_change_base(screen, ptr_center->data, ptr_scale);
  return Qnil;
}

static VALUE rb_mpfr_screen_crd_dbl_to_fr (VALUE self, VALUE dbl_pt_ary)
{
  MPFRScreen2D *screen;
  VALUE ret;
  MPFRMatrix *ptr_pt;
  double dbl_crd[2];
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_pt, 2, 1);
  Data_Get_Struct(self, MPFRScreen2D, screen);
  dbl_crd[0] = NUM2DBL(*RARRAY_PTR(dbl_pt_ary));
  dbl_crd[1] = NUM2DBL(*(RARRAY_PTR(dbl_pt_ary) + 1));
  mpfr_screen2d_change_crd_dbl_to_fr(screen, ptr_pt->data, dbl_crd);
  return ret;
}

static VALUE rb_mpfr_screen_crd_fr_to_dbl (VALUE self, VALUE fr_pt)
{
  MPFRScreen2D *screen;
  MPFRMatrix *ptr_pt;
  double dbl_pt[2];
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_get_matrix_struct(ptr_pt, fr_pt);
  mpfr_screen2d_change_crd_fr_to_dbl(screen, dbl_pt, ptr_pt->data);
  return rb_ary_new3(2, rb_float_new(dbl_pt[0]), rb_float_new(dbl_pt[1]));
}

static VALUE rb_mpfr_screen_set_base_pt (VALUE self, VALUE fr_pt)
{
  MPFRScreen2D *screen;
  MPFRMatrix *ptr_pt;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_get_matrix_struct(ptr_pt, fr_pt);
  mpfr_set(screen->base_pt, ptr_pt->data, GMP_RNDN);
  mpfr_set(screen->base_pt + 1, ptr_pt->data + 1, GMP_RNDN);
  screen->dbl_scrn->center[0] = 0.0;
  screen->dbl_scrn->center[1] = 0.0;
  return Qnil;
}

static VALUE rb_mpfr_screen_get_base_pt (VALUE self)
{
  MPFRScreen2D *screen;
  VALUE ret;
  MPFRMatrix *ptr_pt;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_matrix_suitable_matrix_init (&ret, &ptr_pt, 2, 1);
  mpfr_set(ptr_pt->data, screen->base_pt, GMP_RNDN);
  mpfr_set(ptr_pt->data + 1, screen->base_pt + 1, GMP_RNDN);
  return ret;
}

static VALUE rb_mpfr_screen_set_base_scale (VALUE self, VALUE scale)
{
  MPFRScreen2D *screen;
  MPFR *ptr_scale;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_get_struct(ptr_scale, scale);
  mpfr_set(screen->base_scale, ptr_scale, GMP_RNDN);
  screen->dbl_scrn->zoom = 1.0;
  return Qnil;;
}

static VALUE rb_mpfr_screen_get_base_scale (VALUE self)
{
  MPFRScreen2D *screen;
  VALUE ret;
  MPFR *ptr_scale;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_make_struct_init(ret, ptr_scale);
  mpfr_set(ptr_scale, screen->base_scale, GMP_RNDN);
  return ret;
}

static VALUE rb_mpfr_screen_set_dbl_screen_size (VALUE self, VALUE dbl_size_ary)
{
  MPFRScreen2D *screen;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  screen->dbl_scrn->size[0] = NUM2DBL(*RARRAY_PTR(dbl_size_ary));
  screen->dbl_scrn->size[1] = NUM2DBL(*(RARRAY_PTR(dbl_size_ary) + 1));
  return Qnil;
}

static VALUE rb_mpfr_screen_get_dbl_screen_size (VALUE self)
{
  MPFRScreen2D *screen;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  return rb_ary_new3(2, rb_float_new(screen->dbl_scrn->size[0]),  rb_float_new(screen->dbl_scrn->size[1]));
}

static VALUE rb_mpfr_screen_get_ranges (VALUE self)
{
  MPFRScreen2D *screen;
  MPFRMatrix *tmp1, *tmp2;
  VALUE r_xmin, r_xmax, r_ymin, r_ymax;
  MPFR *xmin, *xmax, *ymin, *ymax;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  r_mpfr_matrix_temp_alloc_init(tmp1, 2, 1);
  r_mpfr_matrix_temp_alloc_init(tmp2, 2, 1);
  mpfr_screen2d_ranges(screen, tmp1->data, tmp2->data);
  r_mpfr_make_struct_init(r_xmin, xmin);
  r_mpfr_make_struct_init(r_xmax, xmax);
  r_mpfr_make_struct_init(r_ymin, ymin);
  r_mpfr_make_struct_init(r_ymax, ymax);
  mpfr_set(xmin, tmp1->data, GMP_RNDN);
  mpfr_set(xmax, tmp1->data + 1, GMP_RNDN);
  mpfr_set(ymin, tmp2->data, GMP_RNDN);
  mpfr_set(ymax, tmp2->data + 1, GMP_RNDN);
  r_mpfr_matrix_temp_free(tmp1);
  r_mpfr_matrix_temp_free(tmp2);
  return rb_ary_new3(4, r_xmin, r_xmax, r_ymin, r_ymax);
}

static VALUE rb_mpfr_screen_set_dbl_screen_center (VALUE self, VALUE center_pt_ary)
{
  MPFRScreen2D *screen;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  screen->dbl_scrn->center[0] = NUM2DBL(*RARRAY_PTR(center_pt_ary));
  screen->dbl_scrn->center[1] = NUM2DBL(*(RARRAY_PTR(center_pt_ary) + 1));
  return Qnil;
}

static VALUE rb_mpfr_screen_get_dbl_screen_center (VALUE self)
{
  MPFRScreen2D *screen;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  return rb_ary_new3(2, rb_float_new(screen->dbl_scrn->center[0]),  rb_float_new(screen->dbl_scrn->center[1]));
}

static VALUE rb_mpfr_screen_set_dbl_screen_zoom (VALUE self, VALUE dbl_zoom)
{
  MPFRScreen2D *screen;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  screen->dbl_scrn->zoom = NUM2DBL(dbl_zoom);
  return Qnil;
}

static VALUE rb_mpfr_screen_get_dbl_screen_zoom (VALUE self)
{
  MPFRScreen2D *screen;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  return rb_float_new(screen->dbl_scrn->zoom);
}

static void rb_mpfr_datafile_puts_gnuplot_style (FILE *pipe_gp, VALUE datafile)
{
  draw_ptdata_puts_gnuplot_style(pipe_gp, NUM2INT(rb_iv_get(datafile, DATAFILE_PLOT_STYLE)));
}

/* static void rb_mpfr_datafile_scaling_output_fr_for_gnuplot1(FILE *pipe_gp, FILE *fp, VALUE datafile, VALUE r_screen) { */
/*   MPFRScreen2D *screen; */
/*   Data_Get_Struct(r_screen, MPFRScreen2D, screen); */

/*   MPFRMatrix *xrange, *yrange; */
/*   r_mpfr_matrix_temp_alloc_init(xrange, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(yrange, 2, 1); */
/*   mpfr_screen2d_ranges(screen, xrange->data, yrange->data); */

/*   VALUE *ary_ptr, str_ary; */
/*   double dbl_crd[2]; */
/*   MPFRMatrix *mat, *last_mat; */
/*   r_mpfr_matrix_temp_alloc_init(mat, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(last_mat, 2, 1); */

/*   int max_line_size, line_size; */
/*   char *line; */
/*   dataobj_read_line_init(&max_line_size, &line); */

/*   while (dataobj_gets_line(&line, &line_size, &max_line_size, fp) != NULL) { */
/*     if (line[0] != '#') { */
/*       if (line[0] != '\0') { */
/* 	str_ary = rb_funcall(datafile, __parse_file_line__, 1, rb_str_new(line, line_size)); */

/* 	ary_ptr = RARRAY_PTR(str_ary); */
/* 	mpfr_set_str(mat->data, StringValuePtr(*ary_ptr), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat->data + 1, StringValuePtr(*(ary_ptr + 1)), 10, GMP_RNDN); */
/* 	mpfr_dataobj_translation(dbl_crd, mat->data, screen->base_pt, screen->base_scale); */

/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd[0], dbl_crd[1]); */
/*       } else { */
/* 	fprintf(pipe_gp, "\n"); */
/*       } */
/*     } */
/*   } */
/*   fprintf(pipe_gp, "e\n"); */
/*   free(line); */

/*   r_mpfr_matrix_temp_free(mat); */
/*   r_mpfr_matrix_temp_free(last_mat); */

/*   r_mpfr_matrix_temp_free(xrange); */
/*   r_mpfr_matrix_temp_free(yrange); */
/* } */

static void rb_mpfr_datafile_scaling_output_fr_for_gnuplot(FILE *pipe_gp, FILE *fp, VALUE datafile, VALUE r_screen)
{
  MPFRScreen2D *screen;
  MPFRMatrix *xrange, *yrange;
  VALUE *ary_ptr, str_ary;
  double dbl_crd[2];
  MPFRMatrix *mat, *last_mat;
  bool output_pt, last_pt_in_region = false, last_pt_exist = false;
  int max_line_size, line_size, blank_line_num = 0;
  char *line;

  Data_Get_Struct(r_screen, MPFRScreen2D, screen);
  r_mpfr_matrix_temp_alloc_init(xrange, 2, 1);
  r_mpfr_matrix_temp_alloc_init(yrange, 2, 1);
  mpfr_screen2d_ranges(screen, xrange->data, yrange->data);

  r_mpfr_matrix_temp_alloc_init(mat, 2, 1);
  r_mpfr_matrix_temp_alloc_init(last_mat, 2, 1);

  dataobj_read_line_init(&max_line_size, &line);

  while (dataobj_gets_line(&line, &line_size, &max_line_size, fp) != NULL) {
    if (line[0] != '#') {
      if (line[0] != '\0') {
	str_ary = rb_funcall(datafile, __parse_file_line__, 1, rb_str_new(line, line_size));

	ary_ptr = RARRAY_PTR(str_ary);
	mpfr_set_str(mat->data, StringValuePtr(*ary_ptr), 10, GMP_RNDN);
	mpfr_set_str(mat->data + 1, StringValuePtr(*(ary_ptr + 1)), 10, GMP_RNDN);

	if (mpfr_cmp(xrange->data, mat->data) <= 0 && mpfr_cmp(xrange->data + 1, mat->data) >= 0 &&
	    mpfr_cmp(yrange->data, mat->data + 1) <= 0 && mpfr_cmp(yrange->data + 1, mat->data + 1) >= 0) {
	  if (last_pt_exist) {
	    last_pt_exist = false;
	    /* mpfr_printf("%.40Re\t%.40Re\n", last_mat->data, last_mat->data + 1); */
	    mpfr_dataobj_translation(dbl_crd, last_mat->data, screen->base_pt, screen->base_scale);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd[0], dbl_crd[1]);
	  }
	  last_pt_in_region = true;
	  output_pt = true;
	} else {
	  if (last_pt_in_region) {
	    output_pt = true;
	    last_pt_exist = false;
	  } else {
	    last_pt_exist = true;
	    mpfr_matrix_set(last_mat, mat);
	    output_pt = false;
	  }
	  last_pt_in_region = false;
	}
      } else {
	output_pt = false;
	last_pt_exist = false;
	last_pt_in_region = false;
      }
      if (output_pt) {
	blank_line_num = 0;
	/* mpfr_printf("%.40Re\t%.40Re\n", mat->data, mat->data + 1); */
	mpfr_dataobj_translation(dbl_crd, mat->data, screen->base_pt, screen->base_scale);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd[0], dbl_crd[1]);
      } else if (blank_line_num == 0) {
	blank_line_num += 1;
	fprintf(pipe_gp, "\n");
      } else {
	blank_line_num += 1;
      }
    }
  }
  fprintf(pipe_gp, "e\n");
  free(line);

  r_mpfr_matrix_temp_free(mat);
  r_mpfr_matrix_temp_free(last_mat);

  r_mpfr_matrix_temp_free(xrange);
  r_mpfr_matrix_temp_free(yrange);
}

/* static void rb_mpfr_datafile_scaling_output_fi_pt_for_gnuplot1(FILE *pipe_gp, FILE *fp, VALUE datafile, VALUE r_screen) { */
/*   MPFRScreen2D *screen; */
/*   Data_Get_Struct(r_screen, MPFRScreen2D, screen); */

/*   MPFRMatrix *xrange, *yrange; */
/*   r_mpfr_matrix_temp_alloc_init(xrange, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(yrange, 2, 1); */
/*   mpfr_screen2d_ranges(screen, xrange->data, yrange->data); */

/*   VALUE *ary_ptr, str_ary; */
/*   double dbl_crd[2]; */
/*   MPFRMatrix *mat1, *mat2, *last_mat; */
/*   r_mpfr_matrix_temp_alloc_init(mat1, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(mat2, 2, 1); */
/*   r_mpfr_matrix_temp_alloc_init(last_mat, 2, 1); */

/*   int max_line_size, line_size; */
/*   char *line; */
/*   dataobj_read_line_init(&max_line_size, &line); */

/*   while (dataobj_gets_line(&line, &line_size, &max_line_size, fp) != NULL) { */
/*     if (line[0] != '#') { */
/*       if (line[0] != '\0') { */
/* 	str_ary = rb_funcall(datafile, __parse_file_line__, 1, rb_str_new(line, line_size)); */
/* 	ary_ptr = RARRAY_PTR(str_ary); */
/* 	mpfr_set_str(mat1->data, StringValuePtr(*ary_ptr), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat1->data + 1, StringValuePtr(*(ary_ptr + 2)), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat2->data, StringValuePtr(*(ary_ptr + 1)), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat2->data + 1, StringValuePtr(*(ary_ptr + 3)), 10, GMP_RNDN); */
/* 	mpfr_matrix_add(mat1, mat1, mat2); */
/* 	mpfr_div_ui(mat1->data, mat1->data, 2, GMP_RNDN); */
/* 	mpfr_div_ui(mat1->data + 1, mat1->data + 1, 2, GMP_RNDN); */
/* 	mpfr_dataobj_translation(dbl_crd, mat1->data, screen->base_pt, screen->base_scale); */

/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd[0], dbl_crd[1]); */
/*       } else { */
/* 	fprintf(pipe_gp, "\n"); */
/*       } */
/*     } */
/*   } */
/*   fprintf(pipe_gp, "e\n"); */
/*   free(line); */

/*   r_mpfr_matrix_temp_free(mat1); */
/*   r_mpfr_matrix_temp_free(mat2); */
/*   r_mpfr_matrix_temp_free(last_mat); */

/*   r_mpfr_matrix_temp_free(xrange); */
/*   r_mpfr_matrix_temp_free(yrange); */
/* } */

static void rb_mpfr_datafile_scaling_output_fi_pt_for_gnuplot(FILE *pipe_gp, FILE *fp, VALUE datafile, VALUE r_screen)
{
  MPFRScreen2D *screen;
  MPFRMatrix *xrange, *yrange;
  VALUE *ary_ptr, str_ary;
  double dbl_crd[2];
  MPFRMatrix *mat1, *mat2, *last_mat;
  bool output_pt, last_pt_in_region = false, last_pt_exist = false;
  int max_line_size, line_size, blank_line_num = 0;
  char *line;

  Data_Get_Struct(r_screen, MPFRScreen2D, screen);

  r_mpfr_matrix_temp_alloc_init(xrange, 2, 1);
  r_mpfr_matrix_temp_alloc_init(yrange, 2, 1);
  mpfr_screen2d_ranges(screen, xrange->data, yrange->data);

  r_mpfr_matrix_temp_alloc_init(mat1, 2, 1);
  r_mpfr_matrix_temp_alloc_init(mat2, 2, 1);
  r_mpfr_matrix_temp_alloc_init(last_mat, 2, 1);

  dataobj_read_line_init(&max_line_size, &line);

  while (dataobj_gets_line(&line, &line_size, &max_line_size, fp) != NULL) {
    if (line[0] != '#') {
      if (line[0] != '\0') {
	str_ary = rb_funcall(datafile, __parse_file_line__, 1, rb_str_new(line, line_size));

	ary_ptr = RARRAY_PTR(str_ary);
	mpfr_set_str(mat1->data, StringValuePtr(*ary_ptr), 10, GMP_RNDN);
	mpfr_set_str(mat1->data + 1, StringValuePtr(*(ary_ptr + 2)), 10, GMP_RNDN);
	mpfr_set_str(mat2->data, StringValuePtr(*(ary_ptr + 1)), 10, GMP_RNDN);
	mpfr_set_str(mat2->data + 1, StringValuePtr(*(ary_ptr + 3)), 10, GMP_RNDN);
	mpfr_matrix_add(mat1, mat1, mat2);
	mpfr_div_ui(mat1->data, mat1->data, 2, GMP_RNDN);
	mpfr_div_ui(mat1->data + 1, mat1->data + 1, 2, GMP_RNDN);

	if (mpfr_cmp(xrange->data, mat1->data) <= 0 && mpfr_cmp(xrange->data + 1, mat1->data) >= 0 &&
	    mpfr_cmp(yrange->data, mat1->data + 1) <= 0 && mpfr_cmp(yrange->data + 1, mat1->data + 1) >= 0) {
	  if (last_pt_exist) {
	    last_pt_exist = false;
	    /* mpfr_printf("%.40Re\t%.40Re\n", last_mat->data, last_mat->data + 1); */
	    mpfr_dataobj_translation(dbl_crd, last_mat->data, screen->base_pt, screen->base_scale);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd[0], dbl_crd[1]);
	  }
	  last_pt_in_region = true;
	  output_pt = true;
	} else {
	  if (last_pt_in_region) {
	    output_pt = true;
	    last_pt_exist = false;
	  } else {
	    last_pt_exist = true;
	    mpfr_matrix_set(last_mat, mat1);
	    output_pt = false;
	  }
	  last_pt_in_region = false;
	}
      } else {
	output_pt = false;
	last_pt_exist = false;
	last_pt_in_region = false;
      }
      if (output_pt) {
	blank_line_num = 0;
	/* mpfr_printf("%.40Re\t%.40Re\n", mat->data, mat->data + 1); */
	mpfr_dataobj_translation(dbl_crd, mat1->data, screen->base_pt, screen->base_scale);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd[0], dbl_crd[1]);
      } else if (blank_line_num == 0) {
	blank_line_num += 1;
	fprintf(pipe_gp, "\n");
      } else {
	blank_line_num += 1;
      }
    }
  }

  fprintf(pipe_gp, "e\n");
  free(line);

  r_mpfr_matrix_temp_free(mat1);
  r_mpfr_matrix_temp_free(mat2);
  r_mpfr_matrix_temp_free(last_mat);

  r_mpfr_matrix_temp_free(xrange);
  r_mpfr_matrix_temp_free(yrange);
}

/* static void rb_mpfr_datafile_scaling_output_fi_quads_for_gnuplot1(FILE *pipe_gp, FILE *fp, VALUE datafile, VALUE r_screen) { */
/*   MPFRScreen2D *screen; */
/*   Data_Get_Struct(r_screen, MPFRScreen2D, screen); */

/*   VALUE *ary_ptr, str_ary; */
/*   double dbl_crd1[2], dbl_crd2[2]; */
/*   MPFRMatrix mat1[1], mat2[1]; */
/*   mpfr_matrix_init(mat1, 2, 1); */
/*   mpfr_matrix_init(mat2, 2, 1); */

/*   int max_line_size, line_size; */
/*   char *line; */
/*   dataobj_read_line_init(&max_line_size, &line); */

/*   while (dataobj_gets_line(&line, &line_size, &max_line_size, fp) != NULL) { */
/*     if (line[0] != '#') { */
/*       if (line[0] != '\0') { */
/* 	str_ary = rb_funcall(datafile, __parse_file_line__, 1, rb_str_new(line, line_size)); */
	
/* 	ary_ptr = RARRAY_PTR(str_ary); */
/* 	/\* mat1: left bottom corner, mat2: right top corner *\/ */
/* 	mpfr_set_str(mat1->data, StringValuePtr(*ary_ptr), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat1->data + 1, StringValuePtr(*(ary_ptr + 2)), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat2->data, StringValuePtr(*(ary_ptr + 1)), 10, GMP_RNDN); */
/* 	mpfr_set_str(mat2->data + 1, StringValuePtr(*(ary_ptr + 3)), 10, GMP_RNDN); */

/* 	mpfr_dataobj_translation(dbl_crd1, mat1->data, screen->base_pt, screen->base_scale); */
/* 	mpfr_dataobj_translation(dbl_crd2, mat2->data, screen->base_pt, screen->base_scale); */

/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd1[1]); */
/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd2[0], dbl_crd1[1]); */
/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd2[0], dbl_crd2[1]); */
/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd2[1]); */
/* 	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd1[1]); */
/* 	fprintf(pipe_gp, "\n"); */
/*       } */
/*     } */
/*   } */
/*   fprintf(pipe_gp, "e\n"); */
/*   free(line); */
/* } */

static void rb_mpfr_datafile_scaling_output_fi_quads_for_gnuplot(FILE *pipe_gp, FILE *fp, VALUE datafile, VALUE r_screen)
{
  MPFRScreen2D *screen;
  MPFRMatrix *xrange, *yrange;
  VALUE *ary_ptr, str_ary;
  double dbl_crd1[2], dbl_crd2[2];
  MPFRMatrix *mat1, *mat2, *last_mat1, *last_mat2;
  bool output_pt, last_pt_in_region = false, last_pt_exist = false;
  int max_line_size, line_size, blank_line_num = 0;
  char *line;

  Data_Get_Struct(r_screen, MPFRScreen2D, screen);

  r_mpfr_matrix_temp_alloc_init(xrange, 2, 1);
  r_mpfr_matrix_temp_alloc_init(yrange, 2, 1);
  mpfr_screen2d_ranges(screen, xrange->data, yrange->data);

  r_mpfr_matrix_temp_alloc_init(mat1, 2, 1);
  r_mpfr_matrix_temp_alloc_init(mat2, 2, 1);
  r_mpfr_matrix_temp_alloc_init(last_mat1, 2, 1);
  r_mpfr_matrix_temp_alloc_init(last_mat2, 2, 1);

  dataobj_read_line_init(&max_line_size, &line);

  while (dataobj_gets_line(&line, &line_size, &max_line_size, fp) != NULL) {
    if (line[0] != '#') {
      if (line[0] != '\0') {
	str_ary = rb_funcall(datafile, __parse_file_line__, 1, rb_str_new(line, line_size));
	
	ary_ptr = RARRAY_PTR(str_ary);
	/* mat1: left bottom corner, mat2: right top corner */
	mpfr_set_str(mat1->data, StringValuePtr(*ary_ptr), 10, GMP_RNDN);
	mpfr_set_str(mat1->data + 1, StringValuePtr(*(ary_ptr + 2)), 10, GMP_RNDN);
	mpfr_set_str(mat2->data, StringValuePtr(*(ary_ptr + 1)), 10, GMP_RNDN);
	mpfr_set_str(mat2->data + 1, StringValuePtr(*(ary_ptr + 3)), 10, GMP_RNDN);

	if (mpfr_cmp(xrange->data, mat2->data) <= 0 && mpfr_cmp(xrange->data + 1, mat1->data) >= 0 &&
	    mpfr_cmp(yrange->data, mat2->data + 1) <= 0 && mpfr_cmp(yrange->data + 1, mat1->data + 1) >= 0) {
	  if (last_pt_exist) {
	    last_pt_exist = false;
	    mpfr_dataobj_translation(dbl_crd1, last_mat1->data, screen->base_pt, screen->base_scale);
	    mpfr_dataobj_translation(dbl_crd2, last_mat2->data, screen->base_pt, screen->base_scale);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd1[1]);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd2[0], dbl_crd1[1]);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd2[0], dbl_crd2[1]);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd2[1]);
	    fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd1[1]);
	    fprintf(pipe_gp, "\n");
	  }
	  last_pt_in_region = true;
	  output_pt = true;
	} else {
	  if (last_pt_in_region) {
	    output_pt = true;
	    last_pt_exist = false;
	  } else {
	    last_pt_exist = true;
	    mpfr_matrix_set(last_mat1, mat1);
	    mpfr_matrix_set(last_mat2, mat2);
	    output_pt = false;
	  }
	  last_pt_in_region = false;
	}
      } else {
	output_pt = false;
	last_pt_exist = false;
	last_pt_in_region = false;
      }
      if (output_pt) {
	blank_line_num = 0;
	mpfr_dataobj_translation(dbl_crd1, mat1->data, screen->base_pt, screen->base_scale);
	mpfr_dataobj_translation(dbl_crd2, mat2->data, screen->base_pt, screen->base_scale);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd1[1]);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd2[0], dbl_crd1[1]);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd2[0], dbl_crd2[1]);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd2[1]);
	fprintf(pipe_gp, "%.18e %.18e\n", dbl_crd1[0], dbl_crd1[1]);
	fprintf(pipe_gp, "\n");
      } else if (blank_line_num == 0) {
	blank_line_num += 1;
	fprintf(pipe_gp, "\n");
      } else {
	blank_line_num += 1;
      }
    }
  }
  fprintf(pipe_gp, "e\n");
  free(line);

  r_mpfr_matrix_temp_free(mat1);
  r_mpfr_matrix_temp_free(mat2);
  r_mpfr_matrix_temp_free(last_mat1);
  r_mpfr_matrix_temp_free(last_mat2);

  r_mpfr_matrix_temp_free(xrange);
  r_mpfr_matrix_temp_free(yrange);
}

static void rb_mpfr_datafile_output_for_gnuplot(FILE *pipe_gp, VALUE datafile, VALUE r_screen)
{
  VALUE file_path;
  char *path;
  int type;
  FILE *fp;

  file_path = rb_iv_get(datafile, DATAFILE_PATH);
  path = StringValuePtr(file_path);
  type = NUM2INT(rb_iv_get(datafile, DATAFILE_TYPE));
  
  if ((fp = fopen(path, "r")) == NULL) {
    printf("ERROR: Can not open \"%s\".\n", path);
    abort();
  }
  if (type == MPFR_PLOT_TYPE_FR) {
    rb_mpfr_datafile_scaling_output_fr_for_gnuplot(pipe_gp, fp, datafile, r_screen);
  } else {
    int style = NUM2INT(rb_iv_get(datafile, DATAFILE_PLOT_STYLE));
    if (style == STY_QUADS || style == STY_QUADS_EDGE) {
      rb_mpfr_datafile_scaling_output_fi_quads_for_gnuplot(pipe_gp, fp, datafile, r_screen);      
    } else {
      rb_mpfr_datafile_scaling_output_fi_pt_for_gnuplot(pipe_gp, fp, datafile, r_screen);
    }
  }
  
  fclose(fp);
}

static void rb_mpfr_screen_gnuplot_output_plot_cmd (VALUE *objs, int len_objs, VALUE *fdata, int len_files, FILE *io)
{
  VALUE *ary;
  DataObject *dtobj;
  int i, j, len_ary;
  bool first = true;

  fprintf(io, "plot ");
  
  for (i = 0; i < len_objs; i++) {
    ary = RARRAY_PTR(*(objs + i));
    len_ary = RARRAY_LEN(*(objs + i));
    for (j = 1; j < len_ary; j++) {
      Data_Get_Struct(*(ary + j), DataObject, dtobj);
      if (dtobj->data_size > 0) {
	if (!first) {
	  fprintf(io, ", ");
	} else {
	  first = false;
	}
	draw_ptdata_puts_gnuplot_style(io, dtobj->default_style);
      }
    }
  }
  for (i = 0; i < len_files; i++) {
    if (!first) {
      fprintf(io, ", ");
    } else {
      first = false;
    }
    rb_mpfr_datafile_puts_gnuplot_style(io, *(fdata + i));
  }
  
  fprintf(io, "\n");
}

static void rb_mpfr_screen_gnuplot_output_point_crd (VALUE screen, VALUE *objs, int len_objs, VALUE *fdata, int len_files, FILE *io)
{
  VALUE *ary;
  DataObject *dtobj;
  int i, j, len_ary;

  for (i = 0; i < len_objs; i++) {
    ary = RARRAY_PTR(*(objs + i));
    len_ary = RARRAY_LEN(*(objs + i));
    for (j = 1; j < len_ary; j++) {
      Data_Get_Struct(*(ary + j), DataObject, dtobj);
      if (dtobj->data_size > 0) {
	dataobj_output_to_gnuplot_default_style(dtobj, io);
      }
    }
  }
  for (i = 0; i < len_files; i++) {
    rb_mpfr_datafile_output_for_gnuplot(io, *(fdata + i), screen);
  }
}

static void rb_mpfr_screen_gnuplot_output (VALUE screen, VALUE objs_ary, VALUE files_ary, FILE *io)
{
  int len_objs, len_files;
  VALUE *objs, *fdata;
  len_objs = RARRAY_LEN(objs_ary);
  len_files = RARRAY_LEN(files_ary);
  objs = RARRAY_PTR(objs_ary);
  fdata = RARRAY_PTR(files_ary);

  rb_mpfr_screen_gnuplot_output_plot_cmd(objs, len_objs, fdata, len_files, io);
  rb_mpfr_screen_gnuplot_output_point_crd(screen, objs, len_objs, fdata, len_files, io);
}

static void rb_mpfr_screen_gnuplot_output_preparation (VALUE r_screen, VALUE gnuplot_cmds, FILE *io)
{
  MPFRScreen2D *screen;
  int len_cmds, i;
  VALUE *cmds;

  Data_Get_Struct(r_screen, MPFRScreen2D, screen);

  /* mpfr_printf("base_pt: %.Re\n         %.Re\nbase_scale: %.Re\n", screen->base_pt, screen->base_pt + 1, screen->base_scale); */
  /* printf("center: %.18f\n        %.18f\nzoom: %.18f\n", */
  /* 	 screen->dbl_scrn->center[0], screen->dbl_scrn->center[1], screen->dbl_scrn->zoom); */
  len_cmds = RARRAY_LEN(gnuplot_cmds);
  cmds = RARRAY_PTR(gnuplot_cmds);
  for (i = 0; i < len_cmds; i++) {
    fprintf(io, "%s\n", StringValuePtr(*(cmds + i)));
  }
  screen2d_gnuplot_set_range(screen->dbl_scrn, io);
  mpfr_screen2d_gnuplot_set_divisions(screen, io);
}

/* gnuplot_cmds is an array which has commands of gnuplot. */
/* For example ["set term postscript eps", "set output 'filename.eps'"]. */
static VALUE rb_mpfr_screen_gnuplot_objects (VALUE self, VALUE objs_ary, VALUE files_ary, VALUE gnuplot_cmds)
{
  FILE *gp;
  gp = popen("gnuplot","w");

  rb_mpfr_screen_gnuplot_output_preparation(self, gnuplot_cmds, gp);
  rb_mpfr_screen_gnuplot_output(self, objs_ary, files_ary, gp);
  
  pclose(gp);
  return Qtrue;
}

static VALUE rb_mpfr_screen_save_gnuplot_cmds (VALUE self, VALUE gp_cmds_file, VALUE gp_pt_file, VALUE objs_ary, VALUE files_ary, VALUE gnuplot_cmds)
{
  int len_objs, len_files;
  VALUE *objs, *fdata;
  FILE *out;

  len_objs = RARRAY_LEN(objs_ary);
  len_files = RARRAY_LEN(files_ary);
  objs = RARRAY_PTR(objs_ary);
  fdata = RARRAY_PTR(files_ary);

  out = fopen(StringValuePtr(gp_cmds_file), "w");
  rb_mpfr_screen_gnuplot_output_preparation(self, gnuplot_cmds, out);
  rb_mpfr_screen_gnuplot_output_plot_cmd(objs, len_objs, fdata, len_files, out);
  fclose(out);

  out = fopen(StringValuePtr(gp_pt_file), "w");
  rb_mpfr_screen_gnuplot_output_point_crd(self, objs, len_objs, fdata, len_files, out);
  fclose(out);
  
  return Qtrue;
}

void rb_mpfr_screen_set_dbl_from_fr(DataObject *dtobj, VALUE ary, int start, MPFRScreen2D *screen) {
  MPFRMatrix *mat;
  int i, len;
  VALUE *vec;
  len = RARRAY_LEN(ary);
  vec = RARRAY_PTR(ary);

  if (start >= 0 && start < len) {
    dataobj_allocate_if_needed(dtobj, len * 2);
    for (i = start; i < len; i += 1) {
      r_mpfr_get_matrix_struct(mat, *(vec + i));
      mpfr_dataobj_translation(ptdata_get_dbl_ptr(dtobj->data, 2 * i), mat->data, screen->base_pt, screen->base_scale);
    }
    dtobj->data_size = len * 2;
  }
}

void rb_mpfr_screen_set_dbl_from_fi(DataObject *dtobj, VALUE ary, int start, MPFRScreen2D *screen) {
  MPFIMatrix *mat;
  int i, len;
  VALUE *vec;

  len = RARRAY_LEN(ary);
  vec = RARRAY_PTR(ary);

  if (start >= 0 && start < len) {
    dataobj_allocate_if_needed(dtobj, len * 2);
    for (i = start; i < len; i += 2) {
      r_mpfi_get_matrix_struct(mat, *(vec + i));
      mpfi_dataobj_mid_translation(ptdata_get_dbl_ptr(dtobj->data, i), mat->data, screen->base_pt, screen->base_scale);
    }
    dtobj->data_size = len * 2;
  }
}

void rb_mpfr_screen_set_quads_from_fi(DataObject *dtobj, VALUE ary, int start, MPFRScreen2D *screen) {
  MPFIMatrix *mat;
  int i, j, len;
  VALUE *vec;
  double x[2], y[2];

  len = RARRAY_LEN(ary);
  vec = RARRAY_PTR(ary);

  if (start >= 0 && start < len) {
    dataobj_allocate_if_needed(dtobj, len * 8);
    for (i = start; i < len; i += 1) {
      j = i * 8;
      r_mpfi_get_matrix_struct(mat, *(vec + i));
      mpfi_dataobj_quads_translation(x, y, mat->data, screen->base_pt, screen->base_scale);

      *ptdata_get_dbl_ptr(dtobj->data, j) = x[0];
      *ptdata_get_dbl_ptr(dtobj->data, j + 1) = y[0];
      *ptdata_get_dbl_ptr(dtobj->data, j + 2) = x[1];
      *ptdata_get_dbl_ptr(dtobj->data, j + 3) = y[0];
      *ptdata_get_dbl_ptr(dtobj->data, j + 4) = x[1];
      *ptdata_get_dbl_ptr(dtobj->data, j + 5) = y[1];
      *ptdata_get_dbl_ptr(dtobj->data, j + 6) = x[0];
      *ptdata_get_dbl_ptr(dtobj->data, j + 7) = y[1];
    }
    dtobj->data_size = (len * 8);
  }
}

static VALUE rb_mpfr_screen_set_fr_ptdata (VALUE self, VALUE obj, VALUE ary, VALUE change_start, VALUE style, VALUE split)
{
  MPFRScreen2D *screen;
  DataObject *dtobj;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  Data_Get_Struct(obj, DataObject, dtobj);
  rb_mpfr_screen_set_dbl_from_fr(dtobj, ary, NUM2INT(change_start), screen);
  dataobj_set_default_style(dtobj, NUM2INT(style));
  if (RARRAY_LEN(split) > 0) {
    int i, size = RARRAY_LEN(split);
    for (i = 0; i < size; i++) {
      dataobj_set_split(dtobj, NUM2INT(*(RARRAY_PTR(split) + i)) * 2);
    }
  }
  return Qnil;
}

static VALUE rb_mpfr_screen_set_fi_dbl_ptdata (VALUE self, VALUE obj, VALUE ary, VALUE change_start, VALUE style, VALUE split)
{
  MPFRScreen2D *screen;
  DataObject *dtobj;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  Data_Get_Struct(obj, DataObject, dtobj);
  rb_mpfr_screen_set_dbl_from_fi(dtobj, ary, NUM2INT(change_start), screen);
  dataobj_set_default_style(dtobj, NUM2INT(style));
  if (RARRAY_LEN(split) > 0) {
    int i, size = RARRAY_LEN(split);
    for (i = 0; i < size; i++) {
      dataobj_set_split(dtobj, NUM2INT(*(RARRAY_PTR(split) + i)) * 2);
    }
  }
  return Qnil;
}

static VALUE rb_mpfr_screen_set_fi_quads_ptdata (VALUE self, VALUE obj, VALUE ary, VALUE change_start, VALUE style)
{
  MPFRScreen2D *screen;
  DataObject *dtobj;
  Data_Get_Struct(self, MPFRScreen2D, screen);
  Data_Get_Struct(obj, DataObject, dtobj);
  rb_mpfr_screen_set_quads_from_fi(dtobj, ary, NUM2INT(change_start), screen);
  dataobj_set_default_style(dtobj, NUM2INT(style));
  return Qnil;
}

void Init_ruby_mpfr_plot()
{
  rb_mpfr_plot = rb_define_class("MPFRPlot", rb_cObject);

  rb_define_const(rb_mpfr_plot, "TYPE_FR", INT2NUM(MPFR_PLOT_TYPE_FR));
  rb_define_const(rb_mpfr_plot, "TYPE_FI", INT2NUM(MPFR_PLOT_TYPE_FI));

  rb_define_const(rb_mpfr_plot, "STY_DOTS", INT2NUM(STY_DOTS));
  rb_define_const(rb_mpfr_plot, "STY_LINES", INT2NUM(STY_LINES));
  rb_define_const(rb_mpfr_plot, "STY_POINTS", INT2NUM(STY_POINTS));
  rb_define_const(rb_mpfr_plot, "STY_LINES_POINTS", INT2NUM(STY_LINES_POINTS));
  rb_define_const(rb_mpfr_plot, "STY_QUADS", INT2NUM(STY_QUADS));
  rb_define_const(rb_mpfr_plot, "STY_QUADS_EDGE", INT2NUM(STY_QUADS_EDGE));

  rb_dataobj = rb_define_class_under(rb_mpfr_plot, "DataObject", rb_cObject);
  rb_define_alloc_func(rb_dataobj, rb_dataobj_alloc);
  rb_define_private_method(rb_dataobj, "initialize", rb_dataobj_initialize, 0);

  rb_define_method(rb_dataobj, "each", rb_dataobj_each, 0);
  rb_define_method(rb_dataobj, "size", rb_dataobj_size, 0);
  rb_define_method(rb_dataobj, "add", rb_dataobj_add, -1);
  rb_define_method(rb_dataobj, "clear", rb_dataobj_clear, 0);
  
  rb_include_module(rb_dataobj, rb_mEnumerable);

  rb_mpfr_screen = rb_define_class_under(rb_mpfr_plot, "Screen", rb_cObject);

  rb_define_const(rb_mpfr_screen, "MAX_SCALE", rb_float_new(SCREEN_MAX_SCALE));
  rb_define_const(rb_mpfr_screen, "MIN_SCALE", rb_float_new(SCREEN_MIN_SCALE));
  rb_define_const(rb_mpfr_screen, "MAX_SIZE", rb_float_new(SCREEN_MAX_SIZE));
  
  rb_define_alloc_func(rb_mpfr_screen, rb_mpfr_screen_alloc);
  rb_define_private_method(rb_mpfr_screen, "initialize", rb_mpfr_screen_initialize, 2);

  rb_define_method(rb_mpfr_screen, "set_base_pt", rb_mpfr_screen_set_base_pt, 1);
  rb_define_method(rb_mpfr_screen, "get_base_pt", rb_mpfr_screen_get_base_pt, 0);
  rb_define_method(rb_mpfr_screen, "set_base_scale", rb_mpfr_screen_set_base_scale, 1);
  rb_define_method(rb_mpfr_screen, "get_base_scale", rb_mpfr_screen_get_base_scale, 0);
  rb_define_method(rb_mpfr_screen, "get_ranges", rb_mpfr_screen_get_ranges, 0);
  rb_define_method(rb_mpfr_screen, "set_dbl_screen_size", rb_mpfr_screen_set_dbl_screen_size, 1);
  rb_define_method(rb_mpfr_screen, "get_dbl_screen_size", rb_mpfr_screen_get_dbl_screen_size, 0);
  rb_define_method(rb_mpfr_screen, "set_dbl_screen_center", rb_mpfr_screen_set_dbl_screen_center, 1);
  rb_define_method(rb_mpfr_screen, "get_dbl_screen_center", rb_mpfr_screen_get_dbl_screen_center, 0);
  rb_define_method(rb_mpfr_screen, "set_dbl_screen_zoom", rb_mpfr_screen_set_dbl_screen_zoom, 1);
  rb_define_method(rb_mpfr_screen, "get_dbl_screen_zoom", rb_mpfr_screen_get_dbl_screen_zoom, 0);
  rb_define_method(rb_mpfr_screen, "crd_dbl_to_fr", rb_mpfr_screen_crd_dbl_to_fr, 1);
  rb_define_method(rb_mpfr_screen, "crd_fr_to_dbl", rb_mpfr_screen_crd_fr_to_dbl, 1);
  rb_define_method(rb_mpfr_screen, "gnuplot_objects", rb_mpfr_screen_gnuplot_objects, 3);
  rb_define_method(rb_mpfr_screen, "save_gnuplot_cmds", rb_mpfr_screen_save_gnuplot_cmds, 5);
  rb_define_method(rb_mpfr_screen, "set_fr_ptdata", rb_mpfr_screen_set_fr_ptdata, 5);
  rb_define_method(rb_mpfr_screen, "set_fi_dbl_ptdata", rb_mpfr_screen_set_fi_dbl_ptdata, 5);
  rb_define_method(rb_mpfr_screen, "set_fi_quads_ptdata", rb_mpfr_screen_set_fi_quads_ptdata, 4);

  __parse_file_line__ = rb_intern("parse_file_line");
}

