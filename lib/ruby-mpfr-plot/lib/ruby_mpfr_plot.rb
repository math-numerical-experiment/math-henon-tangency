require 'ruby_mpfr_plot.so'
require "observer"

class MPFR

  # The instances of this class must have method data_ary and
  # instance variables plot_style and type.
  # The method data_ary returns array of MPFR::ColumnVector or MPFI::ColumnVector.
  # The intstance variable plot_style is MPFRPlot::STY_LINES, MPFRPlot::STY_POINTS,
  # MPFRPlot::STY_LINES_POINTS, MPFRPlot::STY_QUADS, MPFRPlot::STY_QUADS_EDGE.
  class Obj2D
    include Observable
    include Enumerable

    attr_reader :data_ary, :plot_style, :data_type, :split

    # The following acceptable keys of hash:
    #  - :style (obligatory) Style at displaying or plotting:
    #                        MPFRPlot::STY_LINES, MPFRPlot::STY_POINTS, MPFRPlot::STY_LINES_POINTS, MPFRPlot::STY_QUADS, MPFRPlot::STY_QUADS_EDGE
    #  - :data (optional)    An array of coordinates of points
    #  - :type (optional)    MPFRPlot::TYPE_FR or MPFRPlot::TYPE_FI
    def initialize(hash)
      unless hash[:style]
        raise ArgumentError, "Not having key :style."
      end
      @data_ary = (hash[:data] || [])
      @plot_style = hash[:style]
      @data_type = (hash[:type] || guess_type)
      @split = []
      @change_start = 0
    end

    def guess_type
      if @data_ary.size > 0
        case @data_ary[0][0]
        when MPFR
          return MPFRPlot::TYPE_FR
        when MPFI
          return MPFRPlot::TYPE_FI
        end
      end
      raise ArgumentError, "Invalid arguments."
    end
    private :guess_type

    def [](i)
      @data_ary[i]
    end

    alias_method :at, :[]

    def size
      @data_ary.size
    end

    def each(&block)
      @data_ary.each(&block)
    end

    def add(pt, num = self.size)
      @data_ary.insert(num, pt)
      @change_start = num if @change_start > num
    end

    def set_data_ary(new_ary)
      @data_ary = new_ary
      @change_start = 0
    end
    
    def set(pt, num)
      @data_ary[num] = pt
      @change_start = num if @change_start > num
    end

    def delete_at(num)
      @data_ary.delete_at(num)
      @change_start = num if @change_start > num
    end

    def ranges_all_pts
      case @data_type
      when MPFRPlot::TYPE_FR
        xrange = [@data_ary[0][0], @data_ary[0][0]]
        yrange = [@data_ary[0][1], @data_ary[0][1]]
        @data_ary.each do |pt|
          x = pt[0]
          xrange[0] = x if x < xrange[0]
          xrange[1] = x if x > xrange[1]
          y = pt[1]
          yrange[0] = y if y < yrange[0]
          yrange[1] = y if y > yrange[1]
        end
      when MPFRPlot::TYPE_FI
        first = @data_ary[0]
        xrange = first[0].endpoints
        yrange = first[1].endpoints
        @data_ary.each do |pt|
          xmin, xmax = pt[0].endpoints
          xrange[0] = xmin if xmin < xrange[0]
          xrange[1] = xmax if xmax > xrange[1]
          ymin, ymax = pt[1].endpoints
          yrange[0] = ymin if ymin < yrange[0]
          yrange[1] = ymax if ymax > yrange[1]
        end
      end
      [xrange, yrange]
    end
    private :ranges_all_pts

    def range_another_crd(rest_ind, restriction)
      ind = (rest_ind + 1) % 2
      range = nil
      case @data_type
      when MPFRPlot::TYPE_FR
        i = 0
        while i  < @data_ary.size
          crd = @data_ary[i][rest_ind]
          if restriction[0] <= crd && restriction[1] >= crd
            val = @data_ary[i][ind]
            range = [val, val]
            break
          end
          i += 1
        end
        i += 1
        while i < @data_ary.size
          crd = @data_ary[i][rest_ind]
          if restriction[0] <= crd && restriction[1] >= crd
            val = @data_ary[i][rest_ind]
            range[0] = val if val < range[0]
            range[1] = val if val > range[1]
          end
          i += 1
        end
        range
      when MPFRPlot::TYPE_FI
        i = 0
        while i  < @data_ary.size
          crd = @data_ary[i][rest_ind].endpoints
          if restriction[0] <= crd[1] && restriction[1] >= crd[0]
            range = @data_ary[i][ind].endpoints
            break
          end
          i += 1
        end
        i += 1
        while i < @data_ary.size
          crd = @data_ary[i][rest_ind].endpoints
          if restriction[0] <= crd[1] && restriction[1] >= crd[0]
            min, max = @data_ary[i][ind].endpoints
            range[0] = min if min < range[0]
            range[1] = max if max > range[1]
          end
          i += 1
        end
        range
      end
    end
    private :range_another_crd
    
    def ranges(hash = {})
      if @data_ary.size > 0
        if hash[:x]
          if yrange = range_another_crd(0, hash[:x])
            return [hash[:x], yrange]
          end
        elsif hash[:y]
          if xrange = range_another_crd(1, hash[:y])
            return [xrange, hash[:y]]
          end
        else
          return ranges_all_pts
        end
      end
      return nil
    end

    def transform(&block)
      ary = @data_ary.map(&block)
      self.class.new(:data => ary, :style => @plot_style, :type => @data_type)
    end

    def transform!(&block)
      @data_ary.map!(&block)
      changed_notify_display_observer
    end

    # Mainly notify MPFRGL instance.
    def notify_display_observer
      notify_observers(self.__id__, @change_start)
    end

    def changed_notify_display_observer
      changed
      notify_display_observer
    end

    # Change style for plotting and notify observers.
    def change_plot_style(style)
      @plot_style = style
      changed_notify_display_observer
    end

    def add_split(*args)
      args.each do |num|
        if Integer === num && num < @data_ary.size && (not @split.include?(num))
          @split << num
        end
      end
      changed_notify_display_observer
    end

    def delete_all_split
      @split.clear
      changed_notify_display_observer
    end

    def output_restriction_for_point(io, rest, func_output)
      @data_ary.each { |pt| func_output.call(pt) if rest.include?(pt) }
    end
    private :output_restriction_for_point
    
    def output_restriction_for_line(io, rest, func_output)
      output_pt = false
      last_pt_in_region = false
      last_pt = nil
      blank_line_num = 0
      @data_ary.each_with_index do |pt, i|
        if rest.include?(pt)
          if last_pt
            func_output.call(last_pt)
            last_pt = nil
          end
          last_pt_in_region = true
          output_pt = true
        else
          if last_pt_in_region
            output_pt = true
            last_pt = nil
          else
            output_pt = false
            last_pt = pt
          end
          last_pt_in_region = false
        end
        if output_pt
          blank_line_num = 0
          func_output.call(pt)
        elsif blank_line_num == 0
          blank_line_num += 1
          f.puts "\n"
        else
          blank_line_num += 1
        end
        if output_pt && @split.include?(i)
          last_pt = nil
          last_pt_in_region = false
          blank_line_num += 1
          io.puts "\n"
        end
      end
    end
    private :output_restriction_for_line

    def output_restriction(io, rest, format)
      case @data_type
      when MPFRPlot::TYPE_FR
        func_output = lambda { |p| io.puts "#{p[0].to_strf(format)} #{p[1].to_strf(format)}" }
      when MPFRPlot::TYPE_FI
        func_output = lambda { |p|
          tmp_ary = p.to_strf_ary(format)
          io.puts "#{tmp_ary[0][0].join(' ')} #{tmp_ary[1][0].join(' ')}"
        }
      end

      if @plot_style == MPFRPlot::STY_LINES || @plot_style == MPFRPlot::STY_LINES_POINTS
        output_restriction_for_line(io, rest, func_output)
      else
        # MPFRPlot::STY_DOTS, MPFRPlot::STY_POINTS, MPFRPlot::STY_QUADS, MPFRPlot::STY_QUADS_EDGE        
        output_restriction_for_point(io, rest, func_output)
      end

    end
    
  end

  class DataFile
    def initialize(path, plot_style, using = '1:2')
      unless File.exist?(path)
        raise ArgumentError, "#{path} does not exist."
      end
      @path = path
      @plot_style = plot_style
      @using = using.split(":").map { |s| s.to_i - 1 }
      @data_type = guess_type
    end

    def guess_type
      case @using.size
      when 2
        MPFRPlot::TYPE_FR
      when 4
        MPFRPlot::TYPE_FI
      else
        raise ArgumentError, "Invalid argument meaning using parameter"
      end
    end

    def parse_file_line(str)
      ary = str.split
      @using.map { |i| ary[i] }
    end

    def pt_data(str)
      ary = parse_file_line(str)
      if ary.size == 4
        MPFI::ColumnVector.interval([ary[0..1], ary[2..3]])
      else
        MPFR::ColumnVector.new(ary)
      end
    end

    # Skip comment line and return stripped string.
    def gets_data_line(f)
      com = "#"[0]
      while l = f.gets
        if l[0] != com
          l.strip!
          break
        end
      end
      l
    end
    private :gets_data_line

    def ranges_all_pts
      xrange = []
      yrange = []
      case @data_type
      when MPFRPlot::TYPE_FR
        open(@path, 'r') do |f|
          begin
            l = gets_data_line(f)
          end while l && l.size == 0
          if l
            ary = parse_file_line(l).map { |a| MPFR.new(a) }
            xrange = [ary[0], ary[0]]
            yrange = [ary[1], ary[1]]
          else
            raise ArgumentError, "Invalid data file."
          end
          while l = gets_data_line(f)
            if l.size > 0
              x, y = parse_file_line(l).map { |a| MPFR.new(a) }
              xrange[0] = x if x < xrange[0]
              xrange[1] = x if x > xrange[1]
              yrange[0] = y if y < yrange[0]
              yrange[1] = y if y > yrange[1]  
            end
          end
        end
      when MPFRPlot::TYPE_FI
        open(@path, 'r') do |f|
          begin
            l = gets_data_line(f)
          end while l && l.size == 0
          if l
            ary = parse_file_line(l).map { |a| MPFR.new(a) }
            xrange = [ary[0], ary[1]]
            yrange = [ary[2], ary[3]]
          else
            raise ArgumentError, "Invalid data file."
          end
          while l = gets_data_line(f)
            if l.size > 0
              xmin, xmax, ymin, ymax = parse_file_line(l).map { |a| MPFR.new(a) }
              xrange[0] = xmin if xmin < xrange[0]
              xrange[1] = xmax if xmax > xrange[1]
              yrange[0] = ymin if ymin < yrange[0]
              yrange[1] = ymax if ymax > yrange[1]
            end
          end
        end
      end
      (xrange.size == 0 || yrange.size == 0) ? nil : [xrange, yrange]
    end
    private :ranges_all_pts

    def range_another_crd(rest_ind, restriction)
      ind = (rest_ind + 1) % 2
      range = nil
      case @data_type
      when MPFRPlot::TYPE_FR
        open(@path, 'r') do |f|
          while l = gets_data_line(f)
            if l.size > 0
              ary = parse_file_line(l).map { |a| MPFR.new(a) }
              if restriction[0] <= ary[rest_ind] && restriction[1] >= ary[rest_ind]
                range = [ary[ind], ary[ind]]
                break
              end  
            end
          end
          while l = gets_data_line(f)
            if l.size > 0
              ary = parse_file_line(l).map { |a| MPFR.new(a) }
              if restriction[0] <= ary[rest_ind] && restriction[1] >= ary[rest_ind]
                range[0] = ary[ind] if ary[ind] < xrange[0]
                range[1] = ary[ind] if ary[ind] > xrange[1]
              end
            end
          end
        end
      when MPFRPlot::TYPE_FI
        open(@path, 'r') do |f|
          while l = gets_data_line(f)
            if l.size > 0
              ary_str = parse_file_line(l)
              left_bottom = [MPFR.new(ary_str[0]), MPFR.new(ary[2])]
              right_top = [MPFR.new(ary[1]), MPFR.new(ary[3])]
              if restriction[0] <= right_top[rest_ind] && restriction[1] >= left_bottom[rest_ind]
                range = [left_bottom[ind], right_top[ind]]
                break
              end  
            end
          end
          while l = gets_data_line(f)
            if l.size > 0
              ary_str = parse_file_line(l)
              left_bottom = [MPFR.new(ary_str[0]), MPFR.new(ary[2])]
              right_top = [MPFR.new(ary[1]), MPFR.new(ary[3])]
              if restriction[0] <= right_top[rest_ind] && restriction[1] >= left_bottom[rest_ind]
                range[0] = left_bottom[ind] if left_bottom[ind] < range[0]
                range[1] = right_top[ind] if right_top[ind] > range[1]
              end  
            end
          end
        end
      end
      range
    end
    private :range_another_crd

    def ranges(hash = {})
      if hash[:x]
        if yrange = range_another_crd(0, hash[:x])
          return [hash[:x], yrange]
        end
      elsif hash[:y]
        if xrange = range_another_crd(1, hash[:y])
          return [xrange, hash[:y]]
        end
      else
        return ranges_all_pts
      end
      return nil
    end

    def output_restriction_for_point(io, rest, func_output)
      number_sign = "#"[0]
      open(@path, 'r') do |f|
        if func_output
          while l = f.gets
            l.strip!
            if l[0] != number_sign && l.size > 0
              pt = pt_data(l)
              func_output.call(pt) if rest.include?(pt)
            end
          end
        else
          while l = f.gets
            l.strip!
            if l[0] != number_sign && l.size > 0
              pt = pt_data(l)
              io.puts(l) if rest.include?(pt)
            end
          end
        end
      end
    end
    private :output_restriction_for_point
    
    def output_restriction_for_line(io, rest, func_output)
      output_pt = false
      last_pt_in_region = false
      last_pt = nil
      blank_line_num = 0
      number_sign = "#"[0]
      open(@path, 'r') do |f|
        while l = f.gets
          l.strip!
          if l[0] != number_sign
            if l.size == 0
              output_pt = false
              last_pt = nil
              last_pt_in_region = false
            else
              pt = pt_data(l)
              if rest.include?(pt)
                if last_pt
                  func_output.call(last_pt)
                  last_pt = nil
                end
                last_pt_in_region = true
                output_pt = true
              else
                if last_pt_in_region
                  output_pt = true
                  last_pt = nil
                else
                  output_pt = false
                  last_pt = pt                  
                end
                last_pt_in_region = false
              end
            end
            if output_pt
              blank_line_num = 0
              func_output.call(pt)
            elsif blank_line_num == 0
              blank_line_num += 1
              io.puts "\n"
            else
              blank_line_num += 1
            end
          end
        end
      end
    end
    private :output_restriction_for_line

    def output_restriction_for_line_not_changed(io, rest)
      output_pt = false
      last_pt_in_region = false
      last_pt = nil
      blank_line_num = 0
      number_sign = "#"[0]
      open(@path, 'r') do |f|
        while l = f.gets
          l.strip!
          if l[0] != number_sign
            if l.size == 0
              output_pt = false
              last_pt = nil
              last_pt_in_region = false
            else
              pt = pt_data(l)
              if rest.include?(pt)
                if last_pt
                  io.puts(last_pt)
                  last_pt = nil
                end
                last_pt_in_region = true
                output_pt = true
              else
                if last_pt_in_region
                  output_pt = true
                  last_pt = nil
                else
                  output_pt = false
                  last_pt = l
                end
                last_pt_in_region = false
              end
            end
            if output_pt
              blank_line_num = 0
              io.puts(l)
            elsif blank_line_num == 0
              blank_line_num += 1
              io.puts "\n"
            else
              blank_line_num += 1
            end
          end
        end
      end
    end
    private :output_restriction_for_line_not_changed
    
    def output_restriction(io, rest, format)
      if format
        case @data_type
        when MPFRPlot::TYPE_FR
          func_output = lambda { |a| io.puts "#{a[0].to_strf(format)} #{a[1].to_strf(format)}" }
        when MPFRPlot::TYPE_FI
          func_output = lambda { |a|
            tmp_ary = a.to_strf_ary(format)
            io.puts "#{tmp_ary[0][0].join(' ')} #{tmp_ary[1][0].join(' ')}"
          }
        end
      else
        func_output = nil
      end

      if @plot_style == MPFRPlot::STY_LINES || @plot_style == MPFRPlot::STY_LINES_POINTS
        if func_output
          output_restriction_for_line(io, rest, func_output)
        else
          output_restriction_for_line_not_changed(io, rest)
        end
      else
        # MPFRPlot::STY_DOTS, MPFRPlot::STY_POINTS, MPFRPlot::STY_QUADS, MPFRPlot::STY_QUADS_EDGE        
        output_restriction_for_point(io, rest, func_output)
      end
    end
  end
end

class MPFRPlot
  def self.create_basename(basename, style_suffix)
    FileName.create(basename, :position => :middle, :extension => style_suffix.to_s)
  end
  
  AUTO_SCREEN_EXTENT_RATIO = 0.05
  
  def initialize(hash = {})
    @objects = []
    @files = []
    @screen = MPFRPlot::Screen.new(hash[:center] || MPFR::ColumnVector.new([0, 0]),
                                   hash[:scale] || MPFR.new(1))
    @label = []
    @gnuplot_options = []
  end

  def get_range(hash = {})
    ary = (@objects.map { |obj| obj[0].ranges(hash) }) + (@files.map { |obj| obj.ranges(hash) })
    ary.compact!
    if ary.size > 0
      xrange, yrange = ary[0]
      (1...(ary.size)).each do |i|
        r = ary[i]
        xrange[0] = r[0][0] if r[0][0] < xrange[0]
        xrange[1] = r[0][1] if r[0][1] > xrange[1]
        yrange[0] = r[1][0] if r[1][0] < yrange[0]
        yrange[1] = r[1][1] if r[1][1] > yrange[1]
      end
      return [xrange, yrange]
    end
    return nil
  end

  # Optional keys
  # :range            [[MPFR.new(1.0), MPFR.new(1.2)], [MPFR.new(-3.0), MPFR.new(-2.5)]]
  # :center and :size :center => MPFR::ColumnVector.new([1, 1]), :size => MPFR.new(3) or [MPFR.new(2), MPFR.new(3)]
  # :xrange           [MPFR.new(1.0), MPFR.new(1.2)]
  # :yrange           [MPFR.new(-3.0), MPFR.new(-2.5)]
  # :margin           MPFR, true or nil
  # :square_region    true or nil
  def set_screen(hash = {})
    arg = nil
    if hash[:range] || (hash[:center] && hash[:size])
      arg = hash
    elsif hash[:xrange] && hash[:yrange]
      arg = { :range => [hash[:xrange], hash[:yrange]] }
    elsif ary = get_range(hash)
      arg = { :range => ary }
      if hash[:xrange]
        arg[:range][0] = hash[:xrange]
      elsif hash[:yrange]
        arg[:range][1] = hash[:yrange]
      end
    end
    if arg
      if arg[:range] && hash[:margin]
        xrange, yrange = arg[:range]
        if hash[:margin]
          margin_ratio = (Numeric === hash[:margin] ? hash[:margin] : AUTO_SCREEN_EXTENT_RATIO)
          xext = (xrange[1] - xrange[0]) * margin_ratio
          yext = (yrange[1] - yrange[0]) * margin_ratio
          xrange[0] -= xext
          xrange[1] += xext
          yrange[0] -= yext
          yrange[1] += yext
        end
        arg[:range] = [xrange, yrange]
      end
      if hash[:square_region]
        if !hash[:size]
          center = MPFR::ColumnVector.new([(arg[:range][0][0] + arg[:range][0][1]) / 2, (arg[:range][1][0] + arg[:range][1][1]) / 2])
          size = MPFR::Math.max(arg[:range][0][1] - arg[:range][0][0], arg[:range][1][1] - arg[:range][1][0])
          arg = { :center => center, :size => size }
        elsif Array === arg[:size]
          arg[:size] = MPFR::Math.max(*arg[:size])
        end
      end
      if @screen.set(arg)
        delete_plotdata
      end
    else
      raise ArgumentError, "Can not set range."
    end
  end

  # :square - true or nil
  # :ratio  - number
  def set_screen_option(hash)
    if hash[:square]
      @gnuplot_options << "set size square"
    elsif hash[:ratio]
      @gnuplot_options << "set size ratio #{hash[:ratio].to_s}"
    end
  end

  def clear_objects
    @objects.clear
    @files.clear
  end

  def delete_plotdata
    @objects.map! { |ary| [ary[0]] }
  end

  # obj[i] must be MPFR::DataFile, MPFR::Obj2D,
  # or an instance of class having obj[i].obj2d (a method or an instance variable).
  def add_object(*obj)
    obj.each do |o|
      if MPFR::DataFile === o
        @files << o
      elsif MPFR::Obj2D === o
        @objects << [o]
      else
        @objects << [o.obj2d]
      end
    end
  end

  # 'crd' is MPFR::ColumnVector.
  def set_label(str, crd, font = nil)
    ary = @screen.crd_fr_to_dbl(crd).map { |a| a.to_s }
    str = "set label '#{str}' at #{ary.join(',')}"
    str += " font '#{font}'" if font
    @label << str
  end

  def clear_label
    @label.clear
  end

  def make_plotdata
    @objects.each do |ary|
      unless ary[1]
        ary << MPFRPlot::DataObject.new
        case ary[0].data_type
        when MPFRPlot::TYPE_FR
          @screen.set_fr_ptdata(ary[1], ary[0].data_ary, 0, ary[0].plot_style, ary[0].split)
        when MPFRPlot::TYPE_FI
          ary << MPFRPlot::DataObject.new
          style = ary[0].plot_style
          case style
          when MPFRPlot::STY_DOTS, MPFRPlot::STY_LINES, MPFRPlot::STY_POINTS, MPFRPlot::STY_LINES_POINTS
            @screen.set_fi_dbl_ptdata(ary[1], ary[0].data_ary, 0, style, ary[0].split)
          when MPFRPlot::STY_QUADS, MPFRPlot::STY_QUADS_EDGE
            @screen.set_fi_quads_ptdata(ary[2], ary[0].data_ary, 0, style)
          end
        end        
      end
    end
  end
  private :make_plotdata

  def make_gnuplot_cmds(basename, style)
    cmds = @label.dup + @gnuplot_options
    out = FileName.create(basename, :position => :middle, :extension => style.to_s)
    case style.intern
    when :png
      cmds << "set term png size 1024,768"
      cmds << "set output '#{out}'"
    when :eps
      cmds << "set term postscript eps"
      cmds << "set output '#{out}'"
    when :pdf
      cmds << "set term pdf"
      cmds << "set output '#{out}'"
    else
      raise ArgumentError, "Invalid style for an output image by gnuplot."
    end
    cmds
  end
  private :make_gnuplot_cmds
  
  def output(basename, style = :png)
    if (@objects.size + @files.size) > 0
      make_plotdata
      cmds = make_gnuplot_cmds(basename, style)
      @screen.gnuplot_objects(@objects, @files, cmds)
    else
      false
    end
  end

  def save_gnuplot_cmds(gp_cmds_file, gp_pt_file, basename, style = :png)
    if (@objects.size + @files.size) > 0
      make_plotdata
      cmds = make_gnuplot_cmds(basename, style)
      @screen.save_gnuplot_cmds(gp_cmds_file, gp_pt_file, @objects, @files, cmds)
    else
      false
    end
  end

  def save_restricted_data(filename, format, hash)
    rest = MPFRPlot::RestrictRange.new(hash)
    open(filename, 'w') do |f|
      @objects.each { |obj| obj[0].output_restriction(f, rest, format); f.puts "\n" }
      @files.each { |obj| obj.output_restriction(f, rest, format); f.puts "\n" }
    end
  end

  class Screen

    # If the base scale is changed, return true.
    def set_scale(new_scale)
      screen_scale = new_scale / get_base_scale
      if screen_scale < MAX_SCALE && screen_scale > MIN_SCALE
        set_dbl_screen_zoom(screen_scale.to_f)
        nil
      else
        set_base_scale(new_scale)
        true
      end
    end
    private :set_scale

    # If the base point is changed, return true.
    def set_center(new_center)
      dbl_pt = crd_fr_to_dbl(new_center)
      if dbl_pt[0].abs < MAX_SIZE && dbl_pt[1].abs < MAX_SIZE
        set_dbl_screen_center(dbl_pt)
        nil
      else
        set_base_pt(new_center)
        true
      end
    end
    private :set_center

    # The acceptable keys of 'hash' is the followings.
    #  - :center and :size
    #  - :range
    def set(hash)
      if hash[:center] and hash[:size]
        new_center = hash[:center]
        if Array === hash[:size]
          max_size = MPFR::Math.max(*hash[:size])
          xsize = hash[:size][0] / MPFR.new(2)
          ysize = hash[:size][1] / MPFR.new(2)
        else
          max_size = hash[:size]
          xsize = hash[:size] / MPFR.new(2)
          ysize = xsize
        end
        corner = new_center + MPFR::ColumnVector.new([xsize, ysize])
      elsif hash[:range] and Array === hash[:range] and hash[:range].size == 2
        new_center = MPFR::ColumnVector.new([(hash[:range][0][0] + hash[:range][0][1]) / MPFR.new(2),
                                             (hash[:range][1][0] + hash[:range][1][1]) / MPFR.new(2)])
        xsize = hash[:range][0][1] - hash[:range][0][0]
        ysize = hash[:range][1][1] - hash[:range][1][0]
        max_size = MPFR::Math.max(xsize, ysize)
        corner = new_center + MPFR::ColumnVector.new([xsize / MPFR.new(2), ysize / MPFR.new(2)])
      else
        raise ArgumentError, "Invalid Hash argument."
      end
      unless max_size > 0
        raise ArgumentError, "Invalid max size. Try to set larger precision."
      end

      new_scale = MPFR.new(1) / max_size
      if set_scale(new_scale)
        set_base_pt(new_center)
        changed = true
      else
        changed = set_center(new_center)
      end
      dbl_zoom = get_dbl_screen_zoom
      dbl_corner = crd_fr_to_dbl(corner)
      dbl_center = crd_fr_to_dbl(new_center)
      set_dbl_screen_size([(dbl_corner[0] - dbl_center[0]) * dbl_zoom,
                           (dbl_corner[1] - dbl_center[1]) * dbl_zoom])
      changed
    end
    
  end

  class RestrictRange
    def initialize(hash)
      @xrange = hash[:xrange]
      @yrange = hash[:yrange]
    end

    def include?(pt)
      case pt
      when MPFR::ColumnVector, MPFR::RowVector
        (!@xrange || (@xrange[0] <= pt[0] && @xrange[1] >= pt[0])) &&
          (!@yrange || (@yrange[0] <= pt[1] && @yrange[1] >= pt[1]))
      when MPFI::ColumnVector, MPFI::RowVector
        if !@xrange
          x = pt[0].endpoints
          if @xrange[0] > x[1] || @xrange[1] < x[0]
            return nil
          end
        end
        if !@yrange
          y = pt[1].endpoints
          if @yrange[0] > y[1] && @yrange[1] < y[0]
            return nil
          end
        end
        true
      else
        raise ArgumentError, "Invalid point type."
      end
    end
  end

end
